$(function(){
	  var modForm = new initCommonForm({
			formId:"modForm",
			url:BUS_URL+"test/saveTest",
			submitBtnId:'addBtn',
			successBackFunction:function(obj){
				$("#win").window('close');
				$("#wxConfigManage").datagrid("load");
			}
	 });
	  loadFormData();
});

function loadFormData(){
	  var refcode = op_refcode;
	  var req = {useToken:true};
		req['reqJSON'] = {
			url:  SSO_URL+'test/getTest',
			type:'get',
			data:{refcode:refcode}
		};
		req['successFn'] = function(respData){
				if(respData.code == SUCCESS_CODE){
					$('#modForm').form('load',respData.data);
				}else{
					$.messager.alert('提示',respData.message);
				}
		};
		ajaxRequest(req);
}