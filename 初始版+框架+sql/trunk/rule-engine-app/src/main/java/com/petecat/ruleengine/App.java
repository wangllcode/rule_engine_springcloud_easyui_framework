package com.petecat.ruleengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableAspectJAutoProxy(proxyTargetClass = true)
@PropertySources({
		@PropertySource("classpath:message/default_message.properties"),
		@PropertySource("classpath:message/system_message.properties"),
		@PropertySource("classpath:message/basedata_message.properties") })
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
