package com.petecat.ruleengine;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String VERSION = "1.0.0";

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Swagger API")
                .description("This is to show api description")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .termsOfServiceUrl("")
                .version(VERSION)
                .build();
    }

    @Bean
    public Docket customImplementation(Environment env){
    	String [] acts = env.getActiveProfiles();
    	if(ArrayUtils.isNotEmpty(acts) && ArrayUtils.contains(acts, "pro")) {
    		   return new Docket(DocumentationType.SWAGGER_2)
    	                .select()
    	                .apis(RequestHandlerSelectors.basePackage("com.petecat.ruleengine.remoteservice"))
    	                .build() 
    	                .apiInfo(apiInfo());
    	}else {
    		    ParameterBuilder tokenPar = new ParameterBuilder();  
    	        List<Parameter> pars = new ArrayList<Parameter>();  
    	        tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();  
    	        pars.add(tokenPar.build());  
		    return new Docket(DocumentationType.SPRING_WEB)
 	                .select()
 	                .apis(RequestHandlerSelectors.basePackage("com.petecat.ruleengine"))
 	                .build()
 	                .globalOperationParameters(pars)
 	                .apiInfo(apiInfo());
    	}
      
    }
}