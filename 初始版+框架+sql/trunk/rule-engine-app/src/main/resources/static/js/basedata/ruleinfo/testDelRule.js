$(function(){
	  loadFormData();
	  $("#delBtn").click(function(){
		  var refcode = op_refcode;
		  var req = {useToken:true};
			req['reqJSON'] = {
				url:  SSO_URL+'test/deleteTest?refcode='+op_refcode,
				type:'delete'
			};
			req['successFn'] = function(respData){
					if(respData.code == SUCCESS_CODE){
						$("#win").window('close');
						$("#wxConfigManage").datagrid("load");
					}else{
						$.messager.alert('提示',respData.message);
					}
			};
			ajaxRequest(req);
	  })
});
function loadFormData(){
	  var refcode = op_refcode;
	  var req = {useToken:true};
		req['reqJSON'] = {
			url:  SSO_URL+'test/getTest',
			type:'get',
			data:{refcode:refcode}
		};
		req['successFn'] = function(respData){
				if(respData.code == SUCCESS_CODE){
					$('#deleteForm').form('load',respData.data);
				}else{
					$.messager.alert('提示',respData.message);
				}
		};
		ajaxRequest(req);
}