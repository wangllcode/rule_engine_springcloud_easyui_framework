$(function(){
	  loadFormData();
});

function loadFormData(){
	  var refcode = op_refcode;
	  var req = {useToken:true};
		req['reqJSON'] = {
			url:  SSO_URL+'test/getTest',
			type:'get',
			data:{refcode:refcode}
		};
		req['successFn'] = function(respData){
				if(respData.code == SUCCESS_CODE){
					$('#qryForm').form('load',respData.data);
				}else{
					$.messager.alert('提示',respData.message);
				}
		};
		ajaxRequest(req);
}