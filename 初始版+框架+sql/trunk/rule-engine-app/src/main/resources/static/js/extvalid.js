$.extend($.fn.validatebox.defaults.rules, {   
    maxlength: {   
        validator: function(value,param){ 
            return value.toString().length<=param[0];   
        },   
        message: '长度不能大于{0}'
    },
    equals: {      
         validator: function(value,param){    
    	  return value == $(param[0]).val();        
       },       
      message: '两次的值不一致，请重新输入！'    
     },
     regex: {      
         validator: function(value,param){ 
           var regExp = new RegExp(value, "gm");
           return  regExp.test($(param[0]).val()); 
       },       
       message: '数据不符合规范！'    
     },
     equalsScsmMobile: {
    	 validator: function(value,param){
             if(value.indexOf("*") != -1 && value == "*"){
          		return true;
          	 }    
    		 return value != $(param[0]).val();        
    	 },       
    	 message: '配偶联系电话不能与客户移动电话相同！'    
     },
     equalsScsmOrMateMobile: {      
    	 validator: function(value,param){
             if(value.indexOf("*") != -1 && value == "*"){
          		return true;
          	 }
    		 var r = true;
    		 if($(param[0]).val() != null && $(param[0]).val() != '' && $(param[0]).val() == value){
    			 r = false;
    		 }else if(param[1] != null && param[1] != '' && param[1] == value){
    			 r = false;
    		 }
    		 return r;
    	 },       
    	 message: '联系方式不能与客户或配偶联系电话相同！'    
     },
     equalsOtherMobile: {      
    	 validator: function(value,param){
             if(value.indexOf("*") != -1 && value == "*"){
          		return true;
          	 }
    		 var len = 0;
    		 var r = true;
    		 if(param[0] != null && param[0] != ''){
    			 len=$(param[0]).length;
    		 }
    		 for(var i=1;i<=len;i++){
    			 if(param[2] != null && param[2] != '' && param[2] != i){
    				 if($(param[1]+i).val() != null && $(param[1]+i).val() != '' && $(param[1]+i).val() == value){
    					 r=false;
    				 }
    			 }
    		 }
    		 return r;
    	 },       
    	 message: '其他人的联系方式不能相同！'    
     },
     equalsOtherCards: {      
    	 validator: function(value,param){
             if(value.indexOf("*") != -1 && value == "*"){
          		return true;
          	 }
    		 var len = 0;
    		 var r = true;
    		 if(param[0] != null && param[0] != ''){
    			 len=$(param[0]).length;
    		 }
    		 for(var i=1;i<=len;i++){
    			 if(param[2] != null && param[2] != '' && param[2] != i){
    				 if($(param[1]+i).val() != null && $(param[1]+i).val() != '' && $(param[1]+i).val() == value){
    					 r=false;
    				 }
    			 }
    		 }
    		 return r;
    	 },       
    	 message: '共有人身份证号不能相同！'    
     },
     equalsOwnnerIdCard: {      
    	 validator: function(value,param){
             if(value.indexOf("*") != -1 && value == "*"){
         		return true;
         	 }
    		 var r = true;
    		 if($(param[0]).val() != null && $(param[0]).val() != '' && $(param[0]).val() == value){
    			 r = false;
    		 }
    		 return r;
    	 },       
    	 message: '共有人身份证号不能和所有权人身份号一致！'    
     },
     equalsScsmCredcode: {      
    	 validator: function(value,param){    
             if(value.indexOf("*") != -1 && value == "*"){
         		return true;
         	 }
    		 return value != $(param[0]).val();        
    	 },       
    	 message: '配偶证件号码不能与客户证件号码相同！'    
     },
     equalsOwnnerDbdNo: {      
    	 validator: function(value,param){
    		 var r = true;
    		 if($(param[0]).val() != null && $(param[0]).val() != '' && $(param[0]).val() == value){
    			 r = false;
    		 }
    		 return r;
    	 },       
    	 message: '共有人产权证号不能与抵押物产权证号相同！'    
     },
     equalsOtherDbdmNos: {      
    	 validator: function(value,param){
    		 var len = 0;
    		 var r = true;
    		 if(param[0] != null && param[0] != ''){
    			 len=$(param[0]).length;
    		 }
    		 for(var i=1;i<=len;i++){
    			 if(param[2] != null && param[2] != '' && param[2] != i){
    				 if($(param[1]+i).val() != null && $(param[1]+i).val() != '' && $(param[1]+i).val() == value){
    					 r=false;
    				 }
    			 }
    		 }
    		 return r;
    	 },       
    	 message: '共有人产权证号不能相同！'    
     },
    maxLength: {   
        validator: function(value,param){ 
            return value.toString().length<=param[0];   
        },   
        message: '长度不能大于{0}'
    },
    minlength: {   
        validator: function(value,param){ 
            return value.toString().length>=param[0];   
        },   
        message: '至少输入{0}个字符'
    },
    minLength: {   
        validator: function(value,param){ 
            return value.toString().length>=param[0];   
        },   
        message: '至少输入{0}个字符'
    },
    eqLength: {   
        validator: function(value,param){ 
        	return value.toString().length==param[0];   
        },   
        message: '长度必须为{0}'
    },
    maxValue: {   
        validator: function(value,param){
        	try{
        	value=value.replace(/,/g,"");
        	}catch(e){
        	}
        	return parseFloat(value)<=parseFloat(param[0]);   
        },   
        message: '不能大于{0}'
    },
    minValue: {   
        validator: function(value,param){ 
        	try{
            	value=value.replace(/,/g,"");
            	}catch(e){
            }
        	return parseFloat(value)>=parseFloat(param[0]);   
        },   
        message: '不能小于{0}'
    },
    eqlength: {   
        validator: function(value,param){
            return value.toString().length==param[0];   
        },   
        message: '长度必须为{0}'
    }, 
    wordCheck: {   
        validator: function(value, param){
          return /^\w+?$/.test(value)
        },   
        message: '请输入英文字符或者数字'  
    }, 
    simpleIdCardCheck: {     
        validator: function(value, param){ 
            if(value.indexOf("*") != -1 && value == "*"){
        		return true;
        	}  
        	return isValidIDCardNo(value) == "true";
        },   
        message: '身份证不正确！'  
    }, 
    lteCheck: {   
        validator: function(value, param){   
        	var value1=$("#"+param[0]).numberbox('getValue');
        	return !value1 ||!value || parseFloat(value1)>=parseFloat(value.replace(/,/g,""));
        },   
        message: '值必须低于或等于{1}的值！'  
    }, 
    mteCheck: {   
        validator: function(value, param){   
        	var value1=$("#"+param[0]).numberbox('getValue');
        	return !value1 ||!value || parseFloat(value1)<=parseFloat(value.replace(/,/g,""));
        },   
        message: '值必须大于或等于{1}的值！'  
    }, 
    phoneCheck: {   
        validator: function(value, param){ 
            if(value.indexOf("*") != -1 && value == "*"){
        		return true;
        	}  
        	var isPhone = /^(?:(?:0\d{2,3})-)?(?:\d{7,8})(-(?:\d{3,}))?$/;
        	if(isPhone.test(value)){
        		return true;
        	}else{
        		return false;
        	}
        },   
        message: '请输入正确的座机号！'  
    },
    telphoneCheck: {   
        validator: function(value, param){ 
            if(value.indexOf("*") != -1 && value == "*"){
        		return true;
        	}  
        	var isPhone = /^(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/;
        	if(isPhone.test(value)){
        		return true;
        	}else{
        		return false;
        	}
        },   
        message: '请输入正确的座机号！'  
    },
    mobileCheck: {     
        validator: function(value, param){
        	if(value.indexOf("*") != -1 && value == "*"){
        		return true;
        	}
        	//var isMob=/^((\+?86)|(\(\+86\)))?(13[012356789][0-9]{8}|15[012356789][0-9]{8}|17[012356789][0-9]{8}|18[02356789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
        	var isMob=/^((\+?86)|(\(\+86\)))?(1[0-9]{10})$/;
        	if(isMob.test(value)){
        		return true;
        	}else{
        		return false;
        	}
        },   
        message: '请输入正确的手机号！'  
    },
    phoneOrMobCheck: {   
        validator: function(value, param){
        	if(value.indexOf("*") != -1 && value == "*"){
        		return true;
        	}
        	var isPhone = /^(?:(?:0\d{2,3}))?(?:\d{7,8})(-(?:\d{3,}))?$/;
        	//var isMob=/^((\+?86)|(\(\+86\)))?(13[012356789][0-9]{8}|15[012356789][0-9]{8}|17[012356789][0-9]{8}|18[02356789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
        	var isMob=/^((\+?86)|(\(\+86\)))?(1[0-9]{10})$/;
        	if(isMob.test(value)||isPhone.test(value)){
        		return true;
        	}else{
        		return false;
        	}
        },   
        message: '请输入正确的座机号或手机号！'  
    },
    checkOnlyNum: {
   	 validator: function(value,param){
         if(value.indexOf("*") != -1 && value == "*"){
     		return true;
     	 }
   		var isNumber = /^[0-9]*$/;
   		
    	if(isNumber.test(value)){
    		return true;
    	}else{
    		return false;
    	}
   	 },
   	 message: '只能输入数字！'
    },
    dateCheck: {   
        validator: function(value, param){   
   		 var dt_val = value;
   		 var busDate = new Date();
   		 var creDt2 = new Date();
   		 if(param[0] != null && param[0] != ''){
   			 	var ds = param[0].split('-');//系统日期
   			 	var ds2 = dt_val.split('-');//日期
   				busDate.setFullYear(ds[0],Number(ds[1])-1,ds[2]);
   				creDt2.setFullYear(ds2[0],Number(ds2[1])-1,ds2[2]);
   				if(creDt2.dateDiff('d',busDate) >= 0){
   					return true;
   				}else{
   					return false;
   				}
   		 }
       },   
       message: '日期不能大于系统营业日期！'  
   }, 
   equaldDate: {   
       validator: function(value, param){   
    	   var start = $(param[0]).datetimebox('getValue');  //获取开始时间    
           return value >= start;                             //有效范围为当前时间大于开始时间    
      },   
      message: '还款日不能小于放款日！'  
  },
  equaldDateR: {   
      validator: function(value, param){   
   	   var start = $(param[0]).datetimebox('getValue');  //获取开始时间    
          return value >= start;                             //有效范围为当前时间大于开始时间    
     },   
     message: '不能小于起始日期！'  
 },
    msgCheck: {   
        validator: function(value, param){ 
        	var check=/^(([0-9]|[1-2]\d|30),)*([0-9]|[1-2]\d|30)$/ ;
        	if(check.test(value)){
        		return true;
        	}else{
        		return false;
        	}
        },   
        message: '输入错误!{格式为：如 1 或者1,3,5}'  
    },
    checkBankCard: {   
        validator: function(value, param){
        	value=value.replace(/\s/g,'');
        	var f = true;
        	var isNumber = /^[0-9]*$/;
        	if(isNumber.test(value)){
            	if(value.length != 16 && value.length !=19){
            		f = false;
            	}else{
            		f = true;
            	}
        	}else{
        		f = false;
        	}
        	cardAddSpace(value,param[0]);
        	return f;
        },   
        message: '卡号只能为16位或19位的数字！'  
    },
    
    checkBankCardLength: {   
        validator: function(value, param){
        	value=value.replace(/\s/g,'');
        	var f = true;
        	var isNumber = /^[0-9]*$/;
        	if(isNumber.test(value)){
            	if(value.length < 5 || value.length >25){
            		f = false;
            	}else{
            		f = true;
            	}
        	}else{
        		f = false;
        	}
        	cardAddSpace(value,param[0]);
        	return f;
        },   
        message: '只能输入5-25位数字！'  
    },
    
    //验证渠道昵称
    checkQuDAOName:{
    	validator: function(value, param){
        	value=value.replace(/[\u4E00-\u9FA5\uF900-\uFA2D]/g,"**");//将中文转换为两个字符
        	var f = true;
        	console.dir(value.length);
            if(value.length >20){
            		f = false;
            	}else{
            		f = true;
            	}
        	return f;
        },   
        message: '只能输入20个字符（20个英文,10个中文字符）'  
    },
  //不能输入中文
    checkCName:{
    	validator: function(value, param){
        	value=value.replace(/[\u4E00-\u9FA5\uF900-\uFA2D]/g,"**");//将中文转换为两个字符
        	var f = true;
        	console.dir(value.length);
            if(value.length >1){
            		f = false;
            	}else{
            		f = true;
            	}
        	return f;
        },   
        message: '只能输入1个字符'  
    }
});  



/**
 * 简单检查输入的身份证号是否无效，有效返回false，无效返回true
 */
function isValidIDCardNo(code){
    var Errors = new Array("true",
            "身份证号码位数不对,必须是15位或者18位!",
            "身份证号码出生年月日格式不对!",
            "身份证号码校验位错误!",
            "身份证地区非法!",
            "15位身份证号码由数字组成!",
            "18位身份证号码前17位由数字组成,第18位可以是数字或者大写\"X\"!");
    if (code.length != 15 && code.length != 18) {// 身份证长度不正确
        return Errors[1];
    }
    var area = {
        11:"北京",
        12:"天津",
        13:"河北",
        14:"山西",
        15:"内蒙古",
        21:"辽宁",
        22:"吉林",
        23:"黑龙江",
        31:"上海",
        32:"江苏",
        33:"浙江",
        34:"安徽",
        35:"福建",
        36:"江西",
        37:"山东",
        41:"河南",
        42:"湖北",
        43:"湖南",
        44:"广东",
        45:"广西",
        46:"海南",
        50:"重庆",
        51:"四川",
        52:"贵州",
        53:"云南",
        54:"西藏",
        61:"陕西",
        62:"甘肃",
        63:"青海",
        64:"宁夏",
        65:"新疆",
        71:"台湾",
        81:"香港",
        82:"澳门",
        91:"国外"
    }
    var Y, JYM;
    var S, M;
    var ereg;
    var idcard_array = new Array();
    idcard_array = code.split("");
    // 地区检验
    if (area[parseInt(code.substr(0, 2))] == null)
        return Errors[4];
    // 身份号码位数及格式检验
    switch (code.length) {
        case 15:
            if (!/^[0-9]{15}$/.test(code)) {
                return Errors[5];
            }
            var sBirthday = "19" + code.substr(6, 2) + "-"
                    + Number(code.substr(8, 2)) + "-"
                    + Number(code.substr(10, 2));
            var d = new Date(sBirthday.replace(/-/g, "/"));
            var flag = (sBirthday != (d.getFullYear() + "-"
                    + (d.getMonth() + 1) + "-" + d.getDate()));
            if (!flag)
                return Errors[0];
            else
                return Errors[2];
            break;
        case 18:
            if (!/^[0-9]{17}([0-9X])$/.test(code)) {
                return Errors[6];
            }

            var sBirthday = code.substr(6, 4) + "-"
                    + Number(code.substr(10, 2)) + "-"
                    + Number(code.substr(12, 2));
            var d = new Date(sBirthday.replace(/-/g, "/"));
            var flag = (sBirthday != (d.getFullYear() + "-"
                    + (d.getMonth() + 1) + "-" + d.getDate()));
            if (!flag) {// 测试出生日期的合法性
                // 计算校验位
                S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10]))
                        * 7
                        + (parseInt(idcard_array[1]) + parseInt(idcard_array[11]))
                        * 9
                        + (parseInt(idcard_array[2]) + parseInt(idcard_array[12]))
                        * 10
                        + (parseInt(idcard_array[3]) + parseInt(idcard_array[13]))
                        * 5
                        + (parseInt(idcard_array[4]) + parseInt(idcard_array[14]))
                        * 8
                        + (parseInt(idcard_array[5]) + parseInt(idcard_array[15]))
                        * 4
                        + (parseInt(idcard_array[6]) + parseInt(idcard_array[16]))
                        * 2 + parseInt(idcard_array[7]) * 1
                        + parseInt(idcard_array[8]) * 6
                        + parseInt(idcard_array[9]) * 3;
                Y = S % 11;
                M = "F";
                JYM = "10X98765432";
                M = JYM.substr(Y, 1);
                // 判断校验位
                if (M == idcard_array[17])
                    return Errors[0];// 检测ID的校验位
                else
                    return Errors[3];
            } else
                return Errors[2];
            break;
        default:
            return Errors[1];
            break;
    }
};