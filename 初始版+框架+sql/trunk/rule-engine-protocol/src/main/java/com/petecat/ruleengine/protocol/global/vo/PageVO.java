package com.petecat.ruleengine.protocol.global.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * @ClassName: PageVO
 * @Description: 分页Vo
 * @author zhxh
 * @date 2014年3月30日 下午2:52:26
 * 
 */
@Data
public class PageVO<T> {

	/**
	 * @Fields total : 总条数
	 */
	private int total = 0;

	private List<T> rows = new ArrayList<T>();

	private List<T> footer = new ArrayList<T>();


}
