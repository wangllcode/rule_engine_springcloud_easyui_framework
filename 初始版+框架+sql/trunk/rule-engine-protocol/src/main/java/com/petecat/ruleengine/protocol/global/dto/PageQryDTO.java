/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  PageQryDTO.java   
 * @Package com.petecat.formdesigner.protocol.global   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年11月27日 上午11:38:50   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.protocol.global.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**   
 * @ClassName:  PageQryDTO   
 * @Description:分页查询使用
 * @author: admin
 * @date:   2017年11月27日 上午11:38:50   
 */

public class PageQryDTO {

	@ApiModelProperty(hidden=true)
	private int start=0;
	
	@Getter
	@Setter
	private int rows=15;
	
	@Getter
	@Setter
	private int page=1;

	/**  
	 * @Title:  getStart
	 * @Description:获取start的值
	 * @return: int
	 */
	public int getStart() {
		int start = (page-1)*rows;
		return start;
	}

}
