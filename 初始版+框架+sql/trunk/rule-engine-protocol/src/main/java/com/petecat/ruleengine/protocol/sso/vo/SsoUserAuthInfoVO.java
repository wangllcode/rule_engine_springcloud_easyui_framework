/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  SsoUserInfo.java   
 * @Package com.petecat.ruleengine.protocol.sso.vo   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 下午8:38:04   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.protocol.sso.vo;

import lombok.Data;

/**   
 * @ClassName:  SsoUserAuth   
 * @Description:用户信息
 * @author: admin
 * @date:   2017年12月6日 下午8:38:04   
 */
@Data
public class SsoUserAuthInfoVO implements java.io.Serializable{
   
	private String  funid;
	private String  fundesc;
	private String  parentid;
	private String  seqno;
	private String  botflag;
	private String  displayfun;
	private String  layid;
	private String  funpath;
}
