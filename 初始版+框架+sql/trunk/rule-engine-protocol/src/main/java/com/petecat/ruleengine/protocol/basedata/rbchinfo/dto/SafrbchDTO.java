package com.petecat.ruleengine.protocol.basedata.rbchinfo.dto;

import java.io.Serializable;

import lombok.Data;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

/**
 * 分机构信息类
 * 
 * @author 王来利
 *
 */
@Data
public class SafrbchDTO extends PageQryDTO implements Serializable {

	/**
	 * 机构代号
	 */
	private String bchId;

	/**
	 * 机构名称
	 */
	private String BchDesc;

	/**
	 * 机构全称
	 */
	private String BchFullDesc;

	/**
	 * 机构所在区域代码
	 */
	private String areaCode;

	/**
	 * 状态【0:禁用/1:启用】
	 */
	private String isDisble;

	/**
	 * 最后操作用户
	 */
	private String lastModUser;
	/**
	 * 最后操作时间
	 */
	private String lastModDate;

	private static final long serialVersionUID = 1L;
}
