package com.petecat.ruleengine.basedata.rbchinfo.service;

import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.vo.SafrbchVO;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

public interface IRbchInfoService {

	/**
	 * 
	 * @Title: listRbchByPage
	 * @Description: 分页查询分机构信息列表
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return PageVO<SafrbchVO> 返回类型
	 * @throws
	 */
	PageVO<SafrbchVO> listRbchByPage(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: saveRbch
	 * @Description: 增加分机构信息
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveRbch(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: getRbchById
	 * @Description: 通过机构获取ID
	 * @param @param bchId
	 * @param @return 参数
	 * @return SafrbchVO 返回类型
	 * @throws
	 */
	SafrbchVO getRbchById(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: removeRbchById
	 * @Description: 修改机构状态为禁用
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeRbchById(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: updateRbch
	 * @Description: 修改分机构信息
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateRbch(SafrbchDTO safrbchDTO);

}
