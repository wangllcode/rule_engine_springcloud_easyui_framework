package com.petecat.ruleengine.basedata.rbchinfo.controller;

import io.swagger.annotations.Api;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basedata.rbchinfo.service.IRbchInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.vo.SafrbchVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

/**
 * 分机构信息表操作Controller
 * 
 * @author
 */
@RestController
@RequestMapping("/rbch")
public class RbchInfoController extends BaseController {

	@Autowired
	private IRbchInfoService rbchInfoService;

	/**
	 * 
	 * @Title: queryRbchByPage
	 * @Description: 分页查询分机构信息列表
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<PageVO<SafrbchVO>> 返回类型
	 * @throws
	 */
	@GetMapping("/queryRbchByPage")
	public Result<PageVO<SafrbchVO>> queryRbchByPage(
			@ModelAttribute SafrbchDTO safrbchDTO) {
		PageVO<SafrbchVO> rbchList = rbchInfoService.listRbchByPage(safrbchDTO);
		return Result.success(rbchList);
	}

	/**
	 * 
	 * @Title: saveRbch
	 * @Description: 新增分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/saveRbch")
	public Result<?> saveRbch(@RequestBody SafrbchDTO safrbchDTO) {
		this.setLastUserName(safrbchDTO);
		rbchInfoService.saveRbch(safrbchDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: getRbchById
	 * @Description: 通过机构ID查询分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<SafrbchVO> 返回类型
	 * @throws
	 */
	@GetMapping("/getRbchById")
	public Result<SafrbchVO> getRbchById(@ModelAttribute SafrbchDTO safrbchDTO) {
		SafrbchVO safrBchVo = rbchInfoService.getRbchById(safrbchDTO);
		return Result.success(safrBchVo);
	}

	/**
	 * 
	 * @Title: removeRbchById
	 * @Description: 修改机构状态为禁用
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@DeleteMapping("/removeRbchById")
	public Result<?> removeRbchById(@ModelAttribute SafrbchDTO safrbchDTO) {
		this.setLastUserName(safrbchDTO);
		rbchInfoService.removeRbchById(safrbchDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: updateRbch
	 * @Description: 修改分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/updateRbch")
	public Result<?> updateRbch(@RequestBody SafrbchDTO safrbchDTO) {
		this.setLastUserName(safrbchDTO);
		rbchInfoService.updateRbch(safrbchDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(SafrbchDTO safrbchDTO) {
		String userId = this.getCurrentUser().getUserId();
		safrbchDTO.setLastModUser(userId);
		safrbchDTO.setLastModDate(DatesUtils.getStringDate());
	}

}
