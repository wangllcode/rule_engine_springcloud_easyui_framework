package com.petecat.ruleengine.basedata.rbchinfo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * 分机构信息类
 * @author 王来利
 *
 */
@Data
public class SafrbchM implements Serializable {

	/**
	 * 机构代号
	 */
	private String bchId;

	/**
	 * 机构名称
	 */
	private String bchDesc;

	/**
	 * 机构全称
	 */
	private String bchFullDesc;

	/**
	 * 机构所在区域代码
	 */
	private String areaCode;

	/**
	 * 状态【0:禁用/1:启用】
	 */
	private String isDisble;

	/**
	 * 最后操作用户
	 */
	private String lastModUser;

	/**
	 * 最后操作时间
	 */
	private String lastModDate;

	private static final long serialVersionUID = 1L;
}
