package com.petecat.ruleengine.basedata.rbchinfo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchM;
import com.petecat.ruleengine.basedata.rbchinfo.mapper.IRbchInfoMapper;
import com.petecat.ruleengine.basedata.rbchinfo.service.IRbchInfoService;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.vo.SafrbchVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Service
public class RbchInfoServiceImpl extends BaseService<SafrbchM, String>
		implements IRbchInfoService {

	@Autowired
	private IRbchInfoMapper rbchInfoMapper;

	/**
	 * 分页查询分机构信息
	 */
	public PageVO<SafrbchVO> listRbchByPage(SafrbchDTO safrbchDTO) {
		Integer count = rbchInfoMapper.countRbchByPage(safrbchDTO);
		PageVO<SafrbchVO> listVo = null;
		if (null != count && count > 0) {
			List<SafrbchM> safrList = rbchInfoMapper.listRbchByPage(safrbchDTO);
			listVo = new PageVO<SafrbchVO>();
			listVo.setTotal(count);
			listVo.setRows(DataCopy.copyList(safrList, SafrbchVO.class));
		}
		return listVo;
	}

	/**
	 * 增加分机构信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveRbch(SafrbchDTO safrbchDTO) {
		SafrbchM safrbchM = new SafrbchM();
		DataCopy.copyData(safrbchDTO, safrbchM);
		safrbchM.setBchId(String.valueOf(this.getDefaultPrimaryKey()));
		rbchInfoMapper.insertRbch(safrbchM);
	}

	/**
	 * 通过机构ID查询机构信息
	 */
	public SafrbchVO getRbchById(SafrbchDTO safrbchDTO) {
		List<SafrbchM> safrbchM = rbchInfoMapper.getRbchById(safrbchDTO);
		if(null != safrbchM && safrbchM.size() > 1){
			throw new CommonException(1000, null, null);
		}
		SafrbchVO safrbchVo = new SafrbchVO();
		DataCopy.copyData(safrbchM.get(0), safrbchVo);
		return safrbchVo;
	}

	/**
	 * 修改机构状态为禁用
	 */
	public void removeRbchById(SafrbchDTO safrbchDTO) {
		rbchInfoMapper.deleteRbchById(safrbchDTO);
	}

	/**
	 * 修改分机构信息
	 */
	public void updateRbch(SafrbchDTO safrbchDTO) {
		rbchInfoMapper.updateRbch(safrbchDTO);
	}

}
