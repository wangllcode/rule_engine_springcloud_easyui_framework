package com.petecat.ruleengine.basedata.rbchinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchDTO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Mapper
public interface IRbchInfoMapper{
	/**
	 * 
	 * @Title: listRbchByPage
	 * @Description: 分页查询分机构信息列表
	 * @param @param SafrbchM
	 * @param @return 参数
	 * @return List 返回类型
	 * @throws
	 */
	List<SafrbchM> listRbchByPage(SafrbchDTO safrbchm);

	/**
	 * 
	 * @Title: countRbchByPage
	 * @Description: 查询总行数
	 * @param @param s
	 * @param @return 参数
	 * @return Integer 返回类型
	 * @throws
	 */
	Integer countRbchByPage(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: insertRbch
	 * @Description: 增加分机构信息
	 * @param @param safrbchM 参数
	 * @return void 返回类型
	 * @throws
	 */
	void insertRbch(SafrbchM safrbchM);

	/**
	 * 
	 * @Title: getRbchById
	 * @Description: 通过机构ID查询机构信息
	 * @param @param bchId
	 * @param @return 参数
	 * @return SafrbchM 返回类型
	 * @throws
	 */
	List<SafrbchM> getRbchById(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: deleteRbchById
	 * @Description: 修改机构状态为禁用
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void deleteRbchById(SafrbchDTO safrbchDTO);

	/**
	 * 
	 * @Title: updateRbch
	 * @Description: 修改分机构信息
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateRbch(SafrbchDTO safrbchDTO);
}
