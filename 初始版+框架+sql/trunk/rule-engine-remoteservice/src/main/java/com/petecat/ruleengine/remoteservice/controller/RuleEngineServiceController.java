/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  RuleEngineServiceController.java   
 * @Package com.petecat.ruleengine.remoteservice.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 上午12:50:14   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.remoteservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.global.Result;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * @ClassName:  RuleEngineServiceController   
 * @Description:规则引擎服务类
 * @author: admin
 * @date:   2017年12月6日 上午12:50:14   
 */
@RestController
@RequestMapping("/remoteService")
@Api(value = "规则引擎对外接口",tags="规则引擎对外接口")
public class RuleEngineServiceController extends BaseController{

    //option的value的内容是这个method的描述，notes是详细描述，response是最终返回的json model。其他可以忽略
    @ApiOperation(value = "根据不同的规则代码返回不同的结果")
    @ApiImplicitParams({  
        @ApiImplicitParam(paramType = "rulecode", name = "rulecode", dataType = "String", 
        		required = true, value = "规则代码")})
	@GetMapping("/callRuleEngine/{rulecode}")
	public Result<?> callRuleEngine(@PathVariable("rulecode") String rulecode){
		return Result.success();
	}
	
}
