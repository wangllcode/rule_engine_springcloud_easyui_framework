create database rule_engine;
CREATE USER 'rule_user'@'localhost' IDENTIFIED BY '1';
CREATE USER 'rule_user'@'%' IDENTIFIED BY '1';
GRANT ALL ON rule_engine.* TO 'rule_user'@'localhost';
GRANT ALL ON rule_engine.* TO 'rule_user'@'%';
flush privileges;