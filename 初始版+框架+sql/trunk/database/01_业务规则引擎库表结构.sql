/*--Excel Generate SQL Script
--for Mysql 
--==========2017/12/08 10:53:45==========
--==========20==========*/
DROP TABLE IF EXISTS SAFRBCHM; 
CREATE TABLE SAFRBCHM  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '机构代号' , 
      `BCHDESC`  VARCHAR(30)  NOT NULL comment '机构名称' , 
      `BCHFULLDESC`  VARCHAR(120) DEFAULT ''  comment '机构全称' , 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '机构所在区域代码' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息' ;



DROP TABLE IF EXISTS SAFRBCHH; 
CREATE TABLE SAFRBCHH  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '机构代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `BCHDESC`  VARCHAR(30)  NOT NULL comment '机构名称' , 
      `BCHFULLDESC`  VARCHAR(120) DEFAULT ''  comment '机构全称' , 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '机构所在区域代码' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息H表' ;



DROP TABLE IF EXISTS SAFASYSM; 
CREATE TABLE SAFASYSM  ( 
      `SYSUUID`  VARCHAR(64)  NOT NULL comment '系统标识' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SYSNAME`  VARCHAR(20)  NOT NULL comment '系统名称' , 
      `SYSFULLNAME`  VARCHAR(50) DEFAULT ''  comment '系统全称' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `SYSUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息' ;



DROP TABLE IF EXISTS SAFASYSH; 
CREATE TABLE SAFASYSH  ( 
      `SYSUUID`  VARCHAR(64)  NOT NULL comment '系统标识' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SYSNAME`  VARCHAR(20)  NOT NULL comment '系统名称' , 
      `SYSFULLNAME`  VARCHAR(50) DEFAULT ''  comment '系统全称' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `SYSUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息H表' ;



DROP TABLE IF EXISTS BPSAAREM; 
CREATE TABLE BPSAAREM  ( 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '地区代号' , 
      `AREADESC`  VARCHAR(120)  NOT NULL comment '地区描述' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用[0-否,1-是]' , 
      `LASTMODUSER`  VARCHAR(20)   comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `AREACODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区信息表' ;



DROP TABLE IF EXISTS BPSASYSM; 
CREATE TABLE BPSASYSM  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PARAMNAME`  VARCHAR(180)  NOT NULL comment '参数名称' , 
      `PARAMVALUE`  VARCHAR(400) DEFAULT ''  comment '参数值' , 
      `PARAMTYPE`  VARCHAR(20)  NOT NULL comment '参数类型' , 
      `PARAMDESC`  VARCHAR(180) DEFAULT ''  comment '参数描述' , 
      `PARAMSTATUS`  CHAR(1) DEFAULT '1' NOT NULL comment '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]' , 
      `PARAMGROUP`  VARCHAR(1) DEFAULT ''  comment '参数所属组' , 
      `PARAMSQE`  INT   comment '参数组序列' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `PARAMNAME` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表' ;



DROP TABLE IF EXISTS BPSASYSH; 
CREATE TABLE BPSASYSH  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PARAMNAME`  VARCHAR(180)  NOT NULL comment '参数名称' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PARAMVALUE`  VARCHAR(400) DEFAULT ''  comment '参数值' , 
      `PARAMTYPE`  INT  NOT NULL comment '参数类型' , 
      `PARAMDESC`  VARCHAR(180) DEFAULT '1'  comment '参数描述' , 
      `PARAMSTATUS`  CHAR(1)  NOT NULL comment '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]' , 
      `PARAMGROUP`  VARCHAR(1) DEFAULT ''  comment '参数所属组' , 
      `PARAMSQE`  INT DEFAULT 1  comment '参数组序列' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `PARAMNAME`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表H表' ;



DROP TABLE IF EXISTS BPSAEXGM; 
CREATE TABLE BPSAEXGM  ( 
      `PARMTYPE`  VARCHAR(20)  NOT NULL comment '参数类别' , 
      `PARMNAME`  VARCHAR(100)  NOT NULL comment '参数代号' , 
      `PARMVALUE`  VARCHAR(200) DEFAULT ''  comment '参数值' , 
      `PARMDATATYPE`  VARCHAR(20) DEFAULT '0' NOT NULL comment '参数资料类型' , 
      `PARMDESC`  VARCHAR(120) DEFAULT ''  comment '参数说明' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序号' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `PARMTYPE`  ,  `PARMNAME` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表' ;



DROP TABLE IF EXISTS BPSAEXGH; 
CREATE TABLE BPSAEXGH  ( 
      `PARMTYPE`  VARCHAR(20)  NOT NULL comment '参数类别' , 
      `PARMNAME`  VARCHAR(100)  NOT NULL comment '参数代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PARMVALUE`  VARCHAR(200) DEFAULT ''  comment '参数值' , 
      `PARMDATATYPE`  VARCHAR(20) DEFAULT '0' NOT NULL comment '参数资料类型' , 
      `PARMDESC`  VARCHAR(120) DEFAULT ''  comment '参数说明' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序号' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `PARMTYPE`  ,  `PARMNAME`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表H表' ;



DROP TABLE IF EXISTS RLEFMDLM; 
CREATE TABLE RLEFMDLM  ( 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `FMDLCODE`  VARCHAR(20)  NOT NULL comment '字段模块代号' , 
      `FMDLNAME`  VARCHAR(50)  NOT NULL comment '字段模块名称' , 
      `FMDLDESC`  VARCHAR(500) DEFAULT ''  comment '字段模块描述' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FMDLUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表' ;



DROP TABLE IF EXISTS RLEFMDLH; 
CREATE TABLE RLEFMDLH  ( 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `FMDLCODE`  VARCHAR(20)  NOT NULL comment '字段模块代号' , 
      `FMDLNAME`  VARCHAR(50)  NOT NULL comment '字段模块名称' , 
      `FMDLDESC`  VARCHAR(500) DEFAULT ''  comment '字段模块描述' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FMDLUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表H表' ;



DROP TABLE IF EXISTS RLEFILDM; 
CREATE TABLE RLEFILDM  ( 
      `FILDMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `FILDCODE`  VARCHAR(30)  NOT NULL comment '字段代号（英文字段）' , 
      `FILDNAME`  VARCHAR(20)  NOT NULL comment '字段名称' , 
      `FILDDESC`  VARCHAR(120) DEFAULT ''  comment '字段用途描述' , 
      `FILDTYPE`  CHAR(1) DEFAULT '2' NOT NULL comment '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】' , 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '字段所属模块' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FILDMUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表' ;



DROP TABLE IF EXISTS RLEFILDH; 
CREATE TABLE RLEFILDH  ( 
      `FILDMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `FILDCODE`  VARCHAR(30)  NOT NULL comment '字段代号（英文字段）' , 
      `FILDNAME`  VARCHAR(20)  NOT NULL comment '字段名称' , 
      `FILDDESC`  VARCHAR(120) DEFAULT ''  comment '字段用途描述' , 
      `FILDTYPE`  CHAR(1) DEFAULT '2' NOT NULL comment '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】' , 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '字段所属模块' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FILDMUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表H表' ;



DROP TABLE IF EXISTS RLEATCTM; 
CREATE TABLE RLEATCTM  ( 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `ATCTNAME`  VARCHAR(20)  NOT NULL comment '附件目录名称' , 
      `ATCTDESC`  VARCHAR(120) DEFAULT ''  comment '附件目录描述' , 
      `PARENTATCTUUID`  VARCHAR(64)   comment '上级目录UUID' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATCTUUID`  ,  `SYSCODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件目录规则（附件分属模块）' ;



DROP TABLE IF EXISTS RLEATCTM; 
CREATE TABLE RLEATCTM  ( 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `ATCTNAME`  VARCHAR(20)  NOT NULL comment '附件目录名称' , 
      `ATCTDESC`  VARCHAR(120) DEFAULT ''  comment '附件目录描述' , 
      `PARENTATCTUUID`  VARCHAR(64)   comment '上级目录UUID' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATCTUUID`  ,  `SYSCODE`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件目录规则H表（附件分属模块）' ;



DROP TABLE IF EXISTS RLEATNMM; 
CREATE TABLE RLEATNMM  ( 
      `ATNMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '附件目录UUID' , 
      `ATCTNAME`  VARCHAR(30)  NOT NULL comment '附件命名名称' , 
      `ATCTTYPE`  CHAR(1) DEFAULT '0' NOT NULL comment '是否输入/输出【0：输入/1：输出】' , 
      `ATCTINPUYTYPE`  CHAR(1)   comment '输入规则【0：包含/1：必须一致】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATNMUUID`  ,  `ATCTUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则' ;



DROP TABLE IF EXISTS RLEATNMH; 
CREATE TABLE RLEATNMH  ( 
      `ATNMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '附件目录UUID' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `ATCTNAME`  VARCHAR(30)  NOT NULL comment '附件命名名称' , 
      `ATCTTYPE`  CHAR(1) DEFAULT '0' NOT NULL comment '是否输入/输出【0：输入/1：输出】' , 
      `ATCTINPUYTYPE`  CHAR(1)   comment '输入规则【0：包含/1：必须一致】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATNMUUID`  ,  `ATCTUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则H表' ;



DROP TABLE IF EXISTS RLEPINTM; 
CREATE TABLE RLEPINTM  ( 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `BHCID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `PINTNAME`  VARCHAR(100) DEFAULT ''  comment '类型名称' , 
      `PINTDESC`  VARCHAR(500) DEFAULT ''  comment '类型描述' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `SYSCODE`  ,  `BHCID`  ,  `PINTCODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）' ;



DROP TABLE IF EXISTS RLEPINTH; 
CREATE TABLE RLEPINTH  ( 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `BHCID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `PINTNAME`  VARCHAR(100) DEFAULT ''  comment '类型名称' , 
      `PINTDESC`  VARCHAR(500) DEFAULT ''  comment '类型描述' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `SYSCODE`  ,  `BHCID`  ,  `SEQUUID`  ,  `PINTCODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）H表' ;



