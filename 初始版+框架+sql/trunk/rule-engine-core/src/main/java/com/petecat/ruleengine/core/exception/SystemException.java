/**   
 * @Title: SystemException.java 
 * @Package com.hryx.common.exception 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author zhxh 
 * @date 2015年9月29日
 * @version V0.0.1   
 */
package com.petecat.ruleengine.core.exception;

/**
 * @ClassName: SystemException
 * @Description: 已知的错误，比较严重的异常，需要通知管理人员处理
 * @author zhxh
 * @date 2015年9月29日
 * 
 */
public class SystemException extends CommonException {

	/**   
	 * @Title:  SystemException   
	 * @Description:TODO(这里用一句话描述这个方法的作用)   
	 * @param code
	 * @param message
	 * @param type
	 * @param messageType
	 * @param throwable 
	 */
	public SystemException(int code, String message, CommonExceptionType type, int messageType, Throwable throwable) {
		super(code, message, type, messageType, throwable);
	}

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message, Throwable throwable) {
		super(code,message,throwable);
	}

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param values
	 *            消息体中的占位符
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message, String values[],
			Throwable throwable) {
		super(code,message,values,throwable);
	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message,
			CommonExceptionType type, Throwable throwable) {
		super(code,message,type,throwable);
	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param values
	 *            [] 消息体中的占位符的值
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message, String values[],
			CommonExceptionType type, Throwable throwable) {
		super(code,message,values,type,throwable);
	}
	
	
	
	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message,int messageType, Throwable throwable) {
		super(code,message,messageType,throwable);
	}

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 SystemException(@see
	 * SystemException)则记录originalException 如果throwable为 null(@see
	 * SystemException)则记录originalException为当前的SystemException
	 * 多次设置只有第一次的SystemException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param values
	 *            消息体中的占位符
	 * @param throwable
	 *            引起异常
	 */
	public SystemException(int code, String message,int messageType, String values[],
			Throwable throwable) {
		super(code,message,messageType,values,throwable);
	}

	
	/**
	 * @Fields serialVersionUID :序列化值
	 */
	private static final long serialVersionUID = 1L;

}
