/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  BaseService.java   
 * @Package com.petecat.formdesigner.service   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年11月27日 下午3:28:55   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.service;

import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.core.util.IdWorker;

/**   
 * @ClassName:  BaseService   
 * @Description:基础服务
 * @author: admin
 * @date:   2017年11月27日 下午3:28:55   
 */
@Transactional(readOnly=true)
public class BaseService <T,ID>{
	/**   
	 * @Title: getDefaultPrimaryKey
	 * @Description:获取默认主键   
	 * @return long     
	 */
	public long getDefaultPrimaryKey() {
		return new IdWorker(1,1).nextId();
	}
}
