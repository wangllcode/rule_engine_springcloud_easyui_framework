/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  LogData.java   
 * @Package com.petecat.ruleengine.core.interceptor.entity   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午2:35:44   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.aop.log.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**   
 * @ClassName:  LogData   
 * @Description:日志信息
 * @author: admin
 * @date:   2017年12月5日 下午2:35:44   
 */
@Getter
@Setter
public class LogDataModel implements java.io.Serializable{

	/**   
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)   
	 */   
	private static final long serialVersionUID = 1L;

	/**   
	 * @Fields requestData : 请求的数据
	 */   
	private Map<String,String> requestData = new HashMap<>();
	
	/**   
	 * @Fields cls : 执行的类
	 */   
	private String cls;
	
	
	
	
	/**   
	 * @Fields method : 执行的方法 
	 */   
	private String method ;
	

	/**   
	 * @Fields type : 请求方式 
	 */   
	private String type ;
	
	/**   
	 * @Fields method : 执行的开始时间 毫秒
	 */   
	private long execStartTime;
	
	
	/**   
	 * @Fields result : 执行的结果
	 */   
	private String result;
	
	
	/**   
	 * @Fields exceptionContent : 异常内容
	 */   
	private String exceptionContent;
	

	/**   
	 * @Fields endTime : 执行的结束时间 毫秒
	 */   
	private long execEndTime;
	

	/**   
	 * @Fields urlParam : url地址上参数
	 */   
	private String urlParam;
	
	/**   
	 * @Fields warpRequestData : 封装的请求参数
	 */   
	private String warpRequestData;
	

	/**   
	 * @Fields execTime : 执行的时间 毫秒
	 */
	@Setter
	private long execTime;
	
	private String ip;
	
	
	public long getExecTime() {
		return execEndTime-execStartTime;
	}

	
}
