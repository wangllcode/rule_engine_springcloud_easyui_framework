package com.petecat.ruleengine.core.exception;

/**
 * @ClassName: CommonExceptionType
 * @Description: 异常种类定义
 * @author zhxh
 * @date 2015年9月29日
 * 
 */
public enum CommonExceptionType {
	/**
	 * @Fields JSONERROR_200 : 使用json返回错误，但是请求的状态为成功状态 （200）
	 */
	JSONERROR_200("0"),
	/**
	 * @Fields JSONERROR_500 : 使用json返回错误，但是请求的状态为错误（500）
	 */
	JSONERROR_500("1"),

	/**
	 * @Fields JSONERROR_402  : 使用json返回错误，但是请求的状态为错误（402 ）
	 */
	JSONERROR_402("2"),
	
	/**
	 * @Fields ERROR_OUTPUT_500 :直接输出错误信息（文本），并且请求状态为错误（500）
	 */
	ERROR_OUTPUT_500("3"),
	
	/**
	 * @Fields ERROR_OUTPUT_200 : 直接输出错误信息（文本），但是请求状态为成功（200）
	 */
	ERROR_OUTPUT_200("4"),
	/**
	 * @Fields ERROR_OUTPUT_402 : 直接输出错误信息（文本），但是请求状态为402（402）
	 */
	ERROR_OUTPUT_402("5"),
	/**
	 * @Fields ERROR_REDIRECT_PAGE : 要求重定向到系统定义的错误界面
	 */
	ERROR_REDIRECT_PAGE("6"),

	/**
	 * @Fields ERROR_SYSTEM_INNER_RUNTIME : 系统内部运行错误，由其他错误决定应答类型
	 */
	ERROR_SYSTEM_INNER_RUNTIME("-1");

	private String code = "-1";

	CommonExceptionType() {
	}

	CommonExceptionType(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return code.toString();
	}

}
