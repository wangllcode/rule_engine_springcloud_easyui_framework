/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  BaseRepository.java   
 * @Package com.petecat.ruleengine.core.repository   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午9:11:33   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.mapper;

import java.io.Serializable;

/**   
 * @ClassName:  BaseMapper   
 * @Description:基础仓库
 * @author: admin
 * @date:   2017年12月5日 下午9:11:33   
 */
public interface  BaseMapper<T,ID extends  Serializable> {
	
	/**   
	 * @Title: updateByPrimaryKey   
	 * @Description:根据组件更新
	 * @param entity
	 * @return void     
	 */
	void updateByPrimaryKey(T entity);
	
	/**   
	 * @Title: updateByPrimaryKeySelective   
	 * @Description: 根据组件选择更新 
	 * @param entity
	 * @return void     
	 */
	void updateByPrimaryKeySelective(T entity);
	
	/**   
	 * @Title: insert   
	 * @Description: 插入数据  
	 * @param entity
	 * @return void     
	 */
	void insert(T entity);
	
	/**   
	 * @Title: deleteByPrimaryKey   
	 * @Description:删除数据
	 * @param ID
	 * @return void     
	 */
	void deleteByPrimaryKey(ID id);
	

	/**   
	 * @Title: getByPrimaryKey   
	 * @Description:通过主键查询
	 * @param ID
	 * @return void     
	 */
	T getByPrimaryKey(ID id);
	
	
}

