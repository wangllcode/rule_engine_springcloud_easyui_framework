/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  SystemWebMvcRegistrationsAdapter.java   
 * @Package com.petecat.riskmanage.exception.handler   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月5日 上午10:13:38   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.exception.handler.ExceptionHandlerResolver;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.sso.vo.SsoUserInfoVO;

import net.hryx.sso.result.util.UserTokenUtils;
import net.hryx.sso.vo.SSOUserTokenInfo;

/**   
 * @ClassName:  SystemWebMvcRegistrationsAdapter   
 * @Description:系统web注册适配
 * @author: admin
 * @date:   2017年9月5日 上午10:13:38   
 */
@Component
public class SystemWebMvcRegistrationsAdapter extends WebMvcConfigurerAdapter  {

	/**   
	 * <p>Title: configureHandlerExceptionResolvers</p>   
	 * <p>Description: </p>   
	 * @param exceptionResolvers   
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureHandlerExceptionResolvers(java.util.List)   
	 */  
	@Override
	public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		exceptionResolvers.add(0,new ExceptionHandlerResolver());
		super.configureHandlerExceptionResolvers(exceptionResolvers);
	}

	/**   
	 * <p>Title: addInterceptors</p>   
	 * <p>Description: </p>   
	 * @param registry   
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry)   
	 */  
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new HandlerInterceptor() {
			@Override
			public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
					throws Exception {
				String authorization = request.getHeader("Authorization");
				if(!request.getRequestURI().contains("user/ssoLogin")) {
					boolean innerPackage = false;
					if( handler!=null) {
						if(handler.getClass().getPackage().getName().contains("com.petecat")) {
							innerPackage = true;
						}else if(handler instanceof HandlerMethod) {
							HandlerMethod realMethod = (HandlerMethod) handler;
							if(realMethod.getBeanType().getPackage().getName().contains("com.petecat")){
			                  innerPackage = true;
		                   }
						}
					}
					if(innerPackage && StringUtils.isBlank(authorization)) {
						throw new CommonException(-10,null,null);
					}
					if(StringUtils.isNotBlank(authorization)) {
						net.hryx.sso.result.Result<SSOUserTokenInfo> tokenResult = UserTokenUtils.getTokenUserInfo(authorization);
						if(tokenResult.isSuccess()) {
							SsoUserInfoVO infoVO = new SsoUserInfoVO();
							DataCopy.copyData(tokenResult.getValue(), infoVO);
							request.setAttribute("CURRENT_USER_INFO",infoVO);
						}else {
							if( -200006 == tokenResult.getError().getCode()) {
								throw new CommonException(-9,null,null);
							
							} 
						}
				 }
				}
				return true;
			}

			@Override
			public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
					ModelAndView modelAndView) throws Exception {
			}

			@Override
			public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
					Exception ex) throws Exception {
			}
			
		});
		super.addInterceptors(registry);
	}
	
}
