/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  Snippet.java   
 * @Package com.petecat.formdesigner.config.redis   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年11月27日 下午12:54:52   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.petecat.ruleengine.core.config.json.JsonConfigProperties;

/**   
 * @ClassName:  SystemRedisTemplate   
 * @Description:redis配置
 * @author: admin
 * @date:   2017年11月27日 下午12:54:52   
 */
@Component
public class SystemRedisTemplate {
	 
	@Autowired
	private  JsonConfigProperties properties;
	
	@Autowired
	private RedisTemplate<Object,Object> redisTemplate;
	
	
	private StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
	
	/**   
	 * @Title: getFastJsonConfig   
	 * @Description:获取fastJsonConfig
	 * @return FastJsonConfig     
	 */
	public FastJsonConfig getFastJsonConfig() {
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
    	SerializeConfig serializeConfig = new SerializeConfig(properties.isFieldBase());
    	fastJsonConfig.setSerializeConfig(serializeConfig);
    	fastJsonConfig.setDateFormat(properties.getDateFormat());
    	String [] features = properties.getSerializerFeatures();
    	if( features != null) {
        	fastJsonConfig.setSerializerFeatures(getSerializerFeature());	
    	}
		return fastJsonConfig;
	}
	/**   
	 * @Title: getSerializerFeature   
	 * @Description:获取getSerializerFeature
	 * @return SerializerFeature     
	 */
	public SerializerFeature[] getSerializerFeature() {
    	int count = 0;
    	String [] features = properties.getSerializerFeatures();
    	SerializerFeature [] sFeatures = null;
    	if( features != null) {
        	 sFeatures = new SerializerFeature[features.length];
    		for(String feature:features){
        		sFeatures[count++] = SerializerFeature.valueOf(feature);
        	}
    	}
		return sFeatures;
	}
	
	
	/**   
	 * @Title: setStringValue   
	 * @Description:设置字符串值   
	 * @param key
	 * @param value
	 * @return void     
	 */
	public void setStringValue(String key,String value) {
		this.redisTemplate.execute((RedisConnection conn)->{
			conn.set(stringRedisSerializer.serialize(key), stringRedisSerializer.serialize(value));
			return null;
		});
	}


	/**   
	 * @Title: hGetString   
	 * @Description: 获取hashtable里面的信息 
	 * @param key
	 * @param field
	 * @return void     
	 */
	public void hGetString(String key, String field) {
		this.redisTemplate.execute((RedisConnection conn)->{
			byte[] datas = conn.hGet(stringRedisSerializer.serialize(key),
					stringRedisSerializer.serialize(field));
			if(datas!=null) {
				return new String(datas);
			}
			return null;
		});
	}


	/**   
	 * @Title: hGetObject   
	 * @Description: 获取hashTable里面的对象
	 * @param key
	 * @param field
	 * @param class
	 * @return T     
	 */
	public <T> T hGetObject(String key, String field, Class<T> cls) {
		return this.redisTemplate.execute((RedisConnection conn)->{
			byte[] datas = conn.hGet(stringRedisSerializer.serialize(key),
					stringRedisSerializer.serialize(field));
			if(datas!=null) {
				return JSON.parseObject(datas, cls);
			}
			return null;
		});
	}
	

	/**   
	 * @Title: hSetObject   
	 * @Description: 设置hashTable里面的对象
	 * @param key
	 * @param field
	 * @param obj
	 */
	public void hSetObject(String key, String field,Object obj) {
		 this.redisTemplate.execute((RedisConnection conn)->{
			byte[] realKey = stringRedisSerializer.serialize(key);
			byte[] realField= stringRedisSerializer.serialize(field);
			byte[] value = null;
			if(obj != null) {
				value = JSON.toJSONBytes(obj, this.getSerializerFeature());
			}
			conn.hSet(realKey, realField, value);
			return null;
		});
	}
	/**   
	 * @Title: hIncrByLong   
	 * @Description: 设置并获取值 
	 * @param key
	 * @return void     
	 */
	public long hIncrByLong(String key,String field) {
		return this.redisTemplate.execute((RedisConnection conn)->{
			    byte[] realKey = stringRedisSerializer.serialize(key);
				byte[] realField= stringRedisSerializer.serialize(field);
				return conn.hIncrBy(realKey, realField, 1);
			});
	}
}

