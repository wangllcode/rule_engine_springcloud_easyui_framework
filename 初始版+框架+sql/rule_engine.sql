/*
Navicat MySQL Data Transfer

Source Server         : rule-engine
Source Server Version : 50703
Source Host           : 192.168.1.200:3309
Source Database       : rule_engine

Target Server Type    : MYSQL
Target Server Version : 50703
File Encoding         : 65001

Date: 2017-12-10 16:12:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bpsaarem
-- ----------------------------
DROP TABLE IF EXISTS `bpsaarem`;
CREATE TABLE `bpsaarem` (
  `AREACODE` varchar(20) NOT NULL COMMENT '地区代号',
  `AREADESC` varchar(120) NOT NULL COMMENT '地区描述',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用[0-否,1-是]',
  `LASTMODUSER` varchar(20) DEFAULT NULL COMMENT '操作者',
  `LASTMODDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`AREACODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区信息表';

-- ----------------------------
-- Records of bpsaarem
-- ----------------------------
INSERT INTO `bpsaarem` VALUES ('110000', '北京市', '0', null, '2017-11-05 21:04:49');
INSERT INTO `bpsaarem` VALUES ('110100', '市辖区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110101', '东城区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110102', '西城区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110105', '朝阳区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110106', '丰台区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110107', '石景山区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110108', '海淀区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110109', '门头沟区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110111', '房山区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110112', '通州区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110113', '顺义区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110114', '昌平区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110115', '大兴区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110116', '怀柔区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110117', '平谷区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110118', '密云区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('110119', '延庆区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120000', '天津市', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120100', '市辖区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120101', '和平区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120102', '河东区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120103', '河西区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120104', '南开区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120105', '河北区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120106', '红桥区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120110', '东丽区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120111', '西青区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120112', '津南区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120113', '北辰区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120114', '武清区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120115', '宝坻区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120116', '滨海新区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120117', '宁河区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120118', '静海区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('120119', '蓟州区', '0', null, '2017-11-05 21:04:50');
INSERT INTO `bpsaarem` VALUES ('130000', '河北省', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130100', '石家庄市', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130101', '市辖区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130102', '长安区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130104', '桥西区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130105', '新华区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130107', '井陉矿区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130108', '裕华区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130109', '藁城区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130110', '鹿泉区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130111', '栾城区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130121', '井陉县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130123', '正定县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130125', '行唐县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130126', '灵寿县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130127', '高邑县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130128', '深泽县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130129', '赞皇县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130130', '无极县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130131', '平山县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130132', '元氏县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130133', '赵县', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130183', '晋州市', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130184', '新乐市', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130200', '唐山市', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130201', '市辖区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130202', '路南区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130203', '路北区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130204', '古冶区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130205', '开平区', '0', null, '2017-11-05 21:04:51');
INSERT INTO `bpsaarem` VALUES ('130207', '丰南区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130208', '丰润区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130209', '曹妃甸区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130223', '滦县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130224', '滦南县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130225', '乐亭县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130227', '迁西县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130229', '玉田县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130281', '遵化市', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130283', '迁安市', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130300', '秦皇岛市', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130301', '市辖区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130302', '海港区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130303', '山海关区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130304', '北戴河区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130306', '抚宁区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130321', '青龙满族自治县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130322', '昌黎县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130324', '卢龙县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130400', '邯郸市', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130401', '市辖区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130402', '邯山区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130403', '丛台区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130404', '复兴区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130406', '峰峰矿区', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130421', '邯郸县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130423', '临漳县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130424', '成安县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130425', '大名县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130426', '涉县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130427', '磁县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130428', '肥乡县', '0', null, '2017-11-05 21:04:52');
INSERT INTO `bpsaarem` VALUES ('130429', '永年县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130430', '邱县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130431', '鸡泽县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130432', '广平县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130433', '馆陶县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130434', '魏县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130435', '曲周县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130481', '武安市', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130500', '邢台市', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130501', '市辖区', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130502', '桥东区', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130503', '桥西区', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130521', '邢台县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130522', '临城县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130523', '内丘县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130524', '柏乡县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130525', '隆尧县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130526', '任县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130527', '南和县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130528', '宁晋县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130529', '巨鹿县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130530', '新河县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130531', '广宗县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130532', '平乡县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130533', '威县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130534', '清河县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130535', '临西县', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130581', '南宫市', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130582', '沙河市', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130600', '保定市', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130601', '市辖区', '0', null, '2017-11-05 21:04:53');
INSERT INTO `bpsaarem` VALUES ('130602', '竞秀区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130606', '莲池区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130607', '满城区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130608', '清苑区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130609', '徐水区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130623', '涞水县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130624', '阜平县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130626', '定兴县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130627', '唐县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130628', '高阳县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130629', '容城县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130630', '涞源县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130631', '望都县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130632', '安新县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130633', '易县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130634', '曲阳县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130635', '蠡县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130636', '顺平县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130637', '博野县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130638', '雄县', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130681', '涿州市', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130683', '安国市', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130684', '高碑店市', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130700', '张家口市', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130701', '市辖区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130702', '桥东区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130703', '桥西区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130705', '宣化区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130706', '下花园区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130708', '万全区', '0', null, '2017-11-05 21:04:54');
INSERT INTO `bpsaarem` VALUES ('130709', '崇礼区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130722', '张北县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130723', '康保县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130724', '沽源县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130725', '尚义县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130726', '蔚县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130727', '阳原县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130728', '怀安县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130730', '怀来县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130731', '涿鹿县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130732', '赤城县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130800', '承德市', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130801', '市辖区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130802', '双桥区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130803', '双滦区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130804', '鹰手营子矿区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130821', '承德县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130822', '兴隆县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130823', '平泉县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130824', '滦平县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130825', '隆化县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130826', '丰宁满族自治县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130827', '宽城满族自治县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130828', '围场满族蒙古族自治县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130900', '沧州市', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130901', '市辖区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130902', '新华区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130903', '运河区', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130921', '沧县', '0', null, '2017-11-05 21:04:55');
INSERT INTO `bpsaarem` VALUES ('130922', '青县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130923', '东光县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130924', '海兴县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130925', '盐山县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130926', '肃宁县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130927', '南皮县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130928', '吴桥县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130929', '献县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130930', '孟村回族自治县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130981', '泊头市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130982', '任丘市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130983', '黄骅市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('130984', '河间市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131000', '廊坊市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131001', '市辖区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131002', '安次区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131003', '广阳区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131022', '固安县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131023', '永清县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131024', '香河县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131025', '大城县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131026', '文安县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131028', '大厂回族自治县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131081', '霸州市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131082', '三河市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131100', '衡水市', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131101', '市辖区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131102', '桃城区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131103', '冀州区', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131121', '枣强县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131122', '武邑县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131123', '武强县', '0', null, '2017-11-05 21:04:56');
INSERT INTO `bpsaarem` VALUES ('131124', '饶阳县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('131125', '安平县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('131126', '故城县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('131127', '景县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('131128', '阜城县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('131182', '深州市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('139000', '省直辖县级行政区划', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('139001', '定州市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('139002', '辛集市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140000', '山西省', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140100', '太原市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140101', '市辖区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140105', '小店区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140106', '迎泽区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140107', '杏花岭区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140108', '尖草坪区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140109', '万柏林区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140110', '晋源区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140121', '清徐县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140122', '阳曲县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140123', '娄烦县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140181', '古交市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140200', '大同市', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140201', '市辖区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140202', '城区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140203', '矿区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140211', '南郊区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140212', '新荣区', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140221', '阳高县', '0', null, '2017-11-05 21:04:57');
INSERT INTO `bpsaarem` VALUES ('140222', '天镇县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140223', '广灵县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140224', '灵丘县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140225', '浑源县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140226', '左云县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140227', '大同县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140300', '阳泉市', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140301', '市辖区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140302', '城区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140303', '矿区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140311', '郊区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140321', '平定县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140322', '盂县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140400', '长治市', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140401', '市辖区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140402', '城区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140411', '郊区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140421', '长治县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140423', '襄垣县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140424', '屯留县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140425', '平顺县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140426', '黎城县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140427', '壶关县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140428', '长子县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140429', '武乡县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140430', '沁县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140431', '沁源县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140481', '潞城市', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140500', '晋城市', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140501', '市辖区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140502', '城区', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140521', '沁水县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140522', '阳城县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140524', '陵川县', '0', null, '2017-11-05 21:04:58');
INSERT INTO `bpsaarem` VALUES ('140525', '泽州县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140581', '高平市', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140600', '朔州市', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140601', '市辖区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140602', '朔城区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140603', '平鲁区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140621', '山阴县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140622', '应县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140623', '右玉县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140624', '怀仁县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140700', '晋中市', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140701', '市辖区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140702', '榆次区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140721', '榆社县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140722', '左权县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140723', '和顺县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140724', '昔阳县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140725', '寿阳县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140726', '太谷县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140727', '祁县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140728', '平遥县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140729', '灵石县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140781', '介休市', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140800', '运城市', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140801', '市辖区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140802', '盐湖区', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140821', '临猗县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140822', '万荣县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140823', '闻喜县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140824', '稷山县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140825', '新绛县', '0', null, '2017-11-05 21:04:59');
INSERT INTO `bpsaarem` VALUES ('140826', '绛县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140827', '垣曲县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140828', '夏县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140829', '平陆县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140830', '芮城县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140881', '永济市', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140882', '河津市', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140900', '忻州市', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140901', '市辖区', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140902', '忻府区', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140921', '定襄县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140922', '五台县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140923', '代县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140924', '繁峙县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140925', '宁武县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140926', '静乐县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140927', '神池县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140928', '五寨县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140929', '岢岚县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140930', '河曲县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140931', '保德县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140932', '偏关县', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('140981', '原平市', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('141000', '临汾市', '0', null, '2017-11-05 21:05:00');
INSERT INTO `bpsaarem` VALUES ('141001', '市辖区', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141002', '尧都区', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141021', '曲沃县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141022', '翼城县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141023', '襄汾县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141024', '洪洞县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141025', '古县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141026', '安泽县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141027', '浮山县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141028', '吉县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141029', '乡宁县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141030', '大宁县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141031', '隰县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141032', '永和县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141033', '蒲县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141034', '汾西县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141081', '侯马市', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141082', '霍州市', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141100', '吕梁市', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141101', '市辖区', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141102', '离石区', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141121', '文水县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141122', '交城县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141123', '兴县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141124', '临县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141125', '柳林县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141126', '石楼县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141127', '岚县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141128', '方山县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141129', '中阳县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141130', '交口县', '0', null, '2017-11-05 21:05:01');
INSERT INTO `bpsaarem` VALUES ('141181', '孝义市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('141182', '汾阳市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150000', '内蒙古自治区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150100', '呼和浩特市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150101', '市辖区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150102', '新城区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150103', '回民区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150104', '玉泉区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150105', '赛罕区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150121', '土默特左旗', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150122', '托克托县', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150123', '和林格尔县', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150124', '清水河县', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150125', '武川县', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150200', '包头市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150201', '市辖区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150202', '东河区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150203', '昆都仑区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150204', '青山区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150205', '石拐区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150206', '白云鄂博矿区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150207', '九原区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150221', '土默特右旗', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150222', '固阳县', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150223', '达尔罕茂明安联合旗', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150300', '乌海市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150301', '市辖区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150302', '海勃湾区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150303', '海南区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150304', '乌达区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150400', '赤峰市', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150401', '市辖区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150402', '红山区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150403', '元宝山区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150404', '松山区', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150421', '阿鲁科尔沁旗', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150422', '巴林左旗', '0', null, '2017-11-05 21:05:02');
INSERT INTO `bpsaarem` VALUES ('150423', '巴林右旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150424', '林西县', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150425', '克什克腾旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150426', '翁牛特旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150428', '喀喇沁旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150429', '宁城县', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150430', '敖汉旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150500', '通辽市', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150501', '市辖区', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150502', '科尔沁区', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150521', '科尔沁左翼中旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150522', '科尔沁左翼后旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150523', '开鲁县', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150524', '库伦旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150525', '奈曼旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150526', '扎鲁特旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150581', '霍林郭勒市', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150600', '鄂尔多斯市', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150601', '市辖区', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150602', '东胜区', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150603', '康巴什区', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150621', '达拉特旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150622', '准格尔旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150623', '鄂托克前旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150624', '鄂托克旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150625', '杭锦旗', '0', null, '2017-11-05 21:05:03');
INSERT INTO `bpsaarem` VALUES ('150626', '乌审旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150627', '伊金霍洛旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150700', '呼伦贝尔市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150701', '市辖区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150702', '海拉尔区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150703', '扎赉诺尔区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150721', '阿荣旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150722', '莫力达瓦达斡尔族自治旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150723', '鄂伦春自治旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150724', '鄂温克族自治旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150725', '陈巴尔虎旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150726', '新巴尔虎左旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150727', '新巴尔虎右旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150781', '满洲里市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150782', '牙克石市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150783', '扎兰屯市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150784', '额尔古纳市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150785', '根河市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150800', '巴彦淖尔市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150801', '市辖区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150802', '临河区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150821', '五原县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150822', '磴口县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150823', '乌拉特前旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150824', '乌拉特中旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150825', '乌拉特后旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150826', '杭锦后旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150900', '乌兰察布市', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150901', '市辖区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150902', '集宁区', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150921', '卓资县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150922', '化德县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150923', '商都县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150924', '兴和县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150925', '凉城县', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150926', '察哈尔右翼前旗', '0', null, '2017-11-05 21:05:04');
INSERT INTO `bpsaarem` VALUES ('150927', '察哈尔右翼中旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('150928', '察哈尔右翼后旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('150929', '四子王旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('150981', '丰镇市', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152200', '兴安盟', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152201', '乌兰浩特市', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152202', '阿尔山市', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152221', '科尔沁右翼前旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152222', '科尔沁右翼中旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152223', '扎赉特旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152224', '突泉县', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152500', '锡林郭勒盟', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152501', '二连浩特市', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152502', '锡林浩特市', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152522', '阿巴嘎旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152523', '苏尼特左旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152524', '苏尼特右旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152525', '东乌珠穆沁旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152526', '西乌珠穆沁旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152527', '太仆寺旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152528', '镶黄旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152529', '正镶白旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152530', '正蓝旗', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152531', '多伦县', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152900', '阿拉善盟', '0', null, '2017-11-05 21:05:05');
INSERT INTO `bpsaarem` VALUES ('152921', '阿拉善左旗', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('152922', '阿拉善右旗', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('152923', '额济纳旗', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210000', '辽宁省', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210100', '沈阳市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210101', '市辖区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210102', '和平区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210103', '沈河区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210104', '大东区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210105', '皇姑区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210106', '铁西区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210111', '苏家屯区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210112', '浑南区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210113', '沈北新区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210114', '于洪区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210115', '辽中区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210123', '康平县', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210124', '法库县', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210181', '新民市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210200', '大连市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210201', '市辖区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210202', '中山区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210203', '西岗区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210204', '沙河口区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210211', '甘井子区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210212', '旅顺口区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210213', '金州区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210214', '普兰店区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210224', '长海县', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210281', '瓦房店市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210283', '庄河市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210300', '鞍山市', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210301', '市辖区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210302', '铁东区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210303', '铁西区', '0', null, '2017-11-05 21:05:06');
INSERT INTO `bpsaarem` VALUES ('210304', '立山区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210311', '千山区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210321', '台安县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210323', '岫岩满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210381', '海城市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210400', '抚顺市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210401', '市辖区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210402', '新抚区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210403', '东洲区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210404', '望花区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210411', '顺城区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210421', '抚顺县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210422', '新宾满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210423', '清原满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210500', '本溪市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210501', '市辖区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210502', '平山区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210503', '溪湖区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210504', '明山区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210505', '南芬区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210521', '本溪满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210522', '桓仁满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210600', '丹东市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210601', '市辖区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210602', '元宝区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210603', '振兴区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210604', '振安区', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210624', '宽甸满族自治县', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210681', '东港市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210682', '凤城市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210700', '锦州市', '0', null, '2017-11-05 21:05:07');
INSERT INTO `bpsaarem` VALUES ('210701', '市辖区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210702', '古塔区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210703', '凌河区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210711', '太和区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210726', '黑山县', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210727', '义县', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210781', '凌海市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210782', '北镇市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210800', '营口市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210801', '市辖区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210802', '站前区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210803', '西市区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210804', '鲅鱼圈区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210811', '老边区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210881', '盖州市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210882', '大石桥市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210900', '阜新市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210901', '市辖区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210902', '海州区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210903', '新邱区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210904', '太平区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210905', '清河门区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210911', '细河区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210921', '阜新蒙古族自治县', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('210922', '彰武县', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211000', '辽阳市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211001', '市辖区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211002', '白塔区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211003', '文圣区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211004', '宏伟区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211005', '弓长岭区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211011', '太子河区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211021', '辽阳县', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211081', '灯塔市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211100', '盘锦市', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211101', '市辖区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211102', '双台子区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211103', '兴隆台区', '0', null, '2017-11-05 21:05:08');
INSERT INTO `bpsaarem` VALUES ('211104', '大洼区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211122', '盘山县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211200', '铁岭市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211201', '市辖区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211202', '银州区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211204', '清河区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211221', '铁岭县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211223', '西丰县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211224', '昌图县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211281', '调兵山市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211282', '开原市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211300', '朝阳市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211301', '市辖区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211302', '双塔区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211303', '龙城区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211321', '朝阳县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211322', '建平县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211324', '喀喇沁左翼蒙古族自治县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211381', '北票市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211382', '凌源市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211400', '葫芦岛市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211401', '市辖区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211402', '连山区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211403', '龙港区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211404', '南票区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211421', '绥中县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211422', '建昌县', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('211481', '兴城市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220000', '吉林省', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220100', '长春市', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220101', '市辖区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220102', '南关区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220103', '宽城区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220104', '朝阳区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220105', '二道区', '0', null, '2017-11-05 21:05:09');
INSERT INTO `bpsaarem` VALUES ('220106', '绿园区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220112', '双阳区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220113', '九台区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220122', '农安县', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220182', '榆树市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220183', '德惠市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220200', '吉林市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220201', '市辖区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220202', '昌邑区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220203', '龙潭区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220204', '船营区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220211', '丰满区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220221', '永吉县', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220281', '蛟河市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220282', '桦甸市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220283', '舒兰市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220284', '磐石市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220300', '四平市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220301', '市辖区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220302', '铁西区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220303', '铁东区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220322', '梨树县', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220323', '伊通满族自治县', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220381', '公主岭市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220382', '双辽市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220400', '辽源市', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220401', '市辖区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220402', '龙山区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220403', '西安区', '0', null, '2017-11-05 21:05:10');
INSERT INTO `bpsaarem` VALUES ('220421', '东丰县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220422', '东辽县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220500', '通化市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220501', '市辖区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220502', '东昌区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220503', '二道江区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220521', '通化县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220523', '辉南县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220524', '柳河县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220581', '梅河口市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220582', '集安市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220600', '白山市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220601', '市辖区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220602', '浑江区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220605', '江源区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220621', '抚松县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220622', '靖宇县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220623', '长白朝鲜族自治县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220681', '临江市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220700', '松原市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220701', '市辖区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220702', '宁江区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220721', '前郭尔罗斯蒙古族自治县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220722', '长岭县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220723', '乾安县', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220781', '扶余市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220800', '白城市', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220801', '市辖区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220802', '洮北区', '0', null, '2017-11-05 21:05:11');
INSERT INTO `bpsaarem` VALUES ('220821', '镇赉县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('220822', '通榆县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('220881', '洮南市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('220882', '大安市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222400', '延边朝鲜族自治州', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222401', '延吉市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222402', '图们市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222403', '敦化市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222404', '珲春市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222405', '龙井市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222406', '和龙市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222424', '汪清县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('222426', '安图县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230000', '黑龙江省', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230100', '哈尔滨市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230101', '市辖区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230102', '道里区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230103', '南岗区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230104', '道外区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230108', '平房区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230109', '松北区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230110', '香坊区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230111', '呼兰区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230112', '阿城区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230113', '双城区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230123', '依兰县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230124', '方正县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230125', '宾县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230126', '巴彦县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230127', '木兰县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230128', '通河县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230129', '延寿县', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230183', '尚志市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230184', '五常市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230200', '齐齐哈尔市', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230201', '市辖区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230202', '龙沙区', '0', null, '2017-11-05 21:05:12');
INSERT INTO `bpsaarem` VALUES ('230203', '建华区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230204', '铁锋区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230205', '昂昂溪区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230206', '富拉尔基区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230207', '碾子山区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230208', '梅里斯达斡尔族区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230221', '龙江县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230223', '依安县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230224', '泰来县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230225', '甘南县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230227', '富裕县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230229', '克山县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230230', '克东县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230231', '拜泉县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230281', '讷河市', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230300', '鸡西市', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230301', '市辖区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230302', '鸡冠区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230303', '恒山区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230304', '滴道区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230305', '梨树区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230306', '城子河区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230307', '麻山区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230321', '鸡东县', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230381', '虎林市', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230382', '密山市', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230400', '鹤岗市', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230401', '市辖区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230402', '向阳区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230403', '工农区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230404', '南山区', '0', null, '2017-11-05 21:05:13');
INSERT INTO `bpsaarem` VALUES ('230405', '兴安区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230406', '东山区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230407', '兴山区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230421', '萝北县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230422', '绥滨县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230500', '双鸭山市', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230501', '市辖区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230502', '尖山区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230503', '岭东区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230505', '四方台区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230506', '宝山区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230521', '集贤县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230522', '友谊县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230523', '宝清县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230524', '饶河县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230600', '大庆市', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230601', '市辖区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230602', '萨尔图区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230603', '龙凤区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230604', '让胡路区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230605', '红岗区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230606', '大同区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230621', '肇州县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230622', '肇源县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230623', '林甸县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230624', '杜尔伯特蒙古族自治县', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230700', '伊春市', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230701', '市辖区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230702', '伊春区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230703', '南岔区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230704', '友好区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230705', '西林区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230706', '翠峦区', '0', null, '2017-11-05 21:05:14');
INSERT INTO `bpsaarem` VALUES ('230707', '新青区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230708', '美溪区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230709', '金山屯区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230710', '五营区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230711', '乌马河区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230712', '汤旺河区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230713', '带岭区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230714', '乌伊岭区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230715', '红星区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230716', '上甘岭区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230722', '嘉荫县', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230781', '铁力市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230800', '佳木斯市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230801', '市辖区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230803', '向阳区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230804', '前进区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230805', '东风区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230811', '郊区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230822', '桦南县', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230826', '桦川县', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230828', '汤原县', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230881', '同江市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230882', '富锦市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230883', '抚远市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230900', '七台河市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230901', '市辖区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230902', '新兴区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230903', '桃山区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230904', '茄子河区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('230921', '勃利县', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('231000', '牡丹江市', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('231001', '市辖区', '0', null, '2017-11-05 21:05:15');
INSERT INTO `bpsaarem` VALUES ('231002', '东安区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231003', '阳明区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231004', '爱民区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231005', '西安区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231025', '林口县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231081', '绥芬河市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231083', '海林市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231084', '宁安市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231085', '穆棱市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231086', '东宁市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231100', '黑河市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231101', '市辖区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231102', '爱辉区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231121', '嫩江县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231123', '逊克县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231124', '孙吴县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231181', '北安市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231182', '五大连池市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231200', '绥化市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231201', '市辖区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231202', '北林区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231221', '望奎县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231222', '兰西县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231223', '青冈县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231224', '庆安县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231225', '明水县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231226', '绥棱县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231281', '安达市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231282', '肇东市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('231283', '海伦市', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('232700', '大兴安岭地区', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('232721', '呼玛县', '0', null, '2017-11-05 21:05:16');
INSERT INTO `bpsaarem` VALUES ('232722', '塔河县', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('232723', '漠河县', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310000', '上海市', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310100', '市辖区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310101', '黄浦区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310104', '徐汇区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310105', '长宁区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310106', '静安区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310107', '普陀区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310109', '虹口区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310110', '杨浦区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310112', '闵行区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310113', '宝山区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310114', '嘉定区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310115', '浦东新区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310116', '金山区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310117', '松江区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310118', '青浦区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310120', '奉贤区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('310151', '崇明区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320000', '江苏省', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320100', '南京市', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320101', '市辖区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320102', '玄武区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320104', '秦淮区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320105', '建邺区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320106', '鼓楼区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320111', '浦口区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320113', '栖霞区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320114', '雨花台区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320115', '江宁区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320116', '六合区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320117', '溧水区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320118', '高淳区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320200', '无锡市', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320201', '市辖区', '0', null, '2017-11-05 21:05:17');
INSERT INTO `bpsaarem` VALUES ('320205', '锡山区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320206', '惠山区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320211', '滨湖区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320213', '梁溪区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320214', '新吴区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320281', '江阴市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320282', '宜兴市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320300', '徐州市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320301', '市辖区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320302', '鼓楼区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320303', '云龙区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320305', '贾汪区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320311', '泉山区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320312', '铜山区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320321', '丰县', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320322', '沛县', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320324', '睢宁县', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320381', '新沂市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320382', '邳州市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320400', '常州市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320401', '市辖区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320402', '天宁区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320404', '钟楼区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320411', '新北区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320412', '武进区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320413', '金坛区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320481', '溧阳市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320500', '苏州市', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320501', '市辖区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320505', '虎丘区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320506', '吴中区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320507', '相城区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320508', '姑苏区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320509', '吴江区', '0', null, '2017-11-05 21:05:18');
INSERT INTO `bpsaarem` VALUES ('320581', '常熟市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320582', '张家港市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320583', '昆山市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320585', '太仓市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320600', '南通市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320601', '市辖区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320602', '崇川区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320611', '港闸区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320612', '通州区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320621', '海安县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320623', '如东县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320681', '启东市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320682', '如皋市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320684', '海门市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320700', '连云港市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320701', '市辖区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320703', '连云区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320706', '海州区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320707', '赣榆区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320722', '东海县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320723', '灌云县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320724', '灌南县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320800', '淮安市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320801', '市辖区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320803', '淮安区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320804', '淮阴区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320812', '清江浦区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320813', '洪泽区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320826', '涟水县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320830', '盱眙县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320831', '金湖县', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320900', '盐城市', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320901', '市辖区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320902', '亭湖区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320903', '盐都区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320904', '大丰区', '0', null, '2017-11-05 21:05:19');
INSERT INTO `bpsaarem` VALUES ('320921', '响水县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('320922', '滨海县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('320923', '阜宁县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('320924', '射阳县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('320925', '建湖县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('320981', '东台市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321000', '扬州市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321001', '市辖区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321002', '广陵区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321003', '邗江区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321012', '江都区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321023', '宝应县', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321081', '仪征市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321084', '高邮市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321100', '镇江市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321101', '市辖区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321102', '京口区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321111', '润州区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321112', '丹徒区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321181', '丹阳市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321182', '扬中市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321183', '句容市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321200', '泰州市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321201', '市辖区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321202', '海陵区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321203', '高港区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321204', '姜堰区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321281', '兴化市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321282', '靖江市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321283', '泰兴市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321300', '宿迁市', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321301', '市辖区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321302', '宿城区', '0', null, '2017-11-05 21:05:20');
INSERT INTO `bpsaarem` VALUES ('321311', '宿豫区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('321322', '沭阳县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('321323', '泗阳县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('321324', '泗洪县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330000', '浙江省', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330100', '杭州市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330101', '市辖区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330102', '上城区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330103', '下城区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330104', '江干区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330105', '拱墅区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330106', '西湖区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330108', '滨江区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330109', '萧山区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330110', '余杭区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330111', '富阳区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330122', '桐庐县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330127', '淳安县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330182', '建德市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330185', '临安市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330200', '宁波市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330201', '市辖区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330203', '海曙区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330204', '江东区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330205', '江北区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330206', '北仑区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330211', '镇海区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330212', '鄞州区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330225', '象山县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330226', '宁海县', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330281', '余姚市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330282', '慈溪市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330283', '奉化市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330300', '温州市', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330301', '市辖区', '0', null, '2017-11-05 21:05:21');
INSERT INTO `bpsaarem` VALUES ('330302', '鹿城区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330303', '龙湾区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330304', '瓯海区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330305', '洞头区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330324', '永嘉县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330326', '平阳县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330327', '苍南县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330328', '文成县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330329', '泰顺县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330381', '瑞安市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330382', '乐清市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330400', '嘉兴市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330401', '市辖区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330402', '南湖区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330411', '秀洲区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330421', '嘉善县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330424', '海盐县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330481', '海宁市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330482', '平湖市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330483', '桐乡市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330500', '湖州市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330501', '市辖区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330502', '吴兴区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330503', '南浔区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330521', '德清县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330522', '长兴县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330523', '安吉县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330600', '绍兴市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330601', '市辖区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330602', '越城区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330603', '柯桥区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330604', '上虞区', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330624', '新昌县', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330681', '诸暨市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330683', '嵊州市', '0', null, '2017-11-05 21:05:22');
INSERT INTO `bpsaarem` VALUES ('330700', '金华市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330701', '市辖区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330702', '婺城区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330703', '金东区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330723', '武义县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330726', '浦江县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330727', '磐安县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330781', '兰溪市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330782', '义乌市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330783', '东阳市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330784', '永康市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330800', '衢州市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330801', '市辖区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330802', '柯城区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330803', '衢江区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330822', '常山县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330824', '开化县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330825', '龙游县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330881', '江山市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330900', '舟山市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330901', '市辖区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330902', '定海区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330903', '普陀区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330921', '岱山县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('330922', '嵊泗县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331000', '台州市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331001', '市辖区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331002', '椒江区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331003', '黄岩区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331004', '路桥区', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331021', '玉环县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331022', '三门县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331023', '天台县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331024', '仙居县', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331081', '温岭市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331082', '临海市', '0', null, '2017-11-05 21:05:23');
INSERT INTO `bpsaarem` VALUES ('331100', '丽水市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331101', '市辖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331102', '莲都区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331121', '青田县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331122', '缙云县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331123', '遂昌县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331124', '松阳县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331125', '云和县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331126', '庆元县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331127', '景宁畲族自治县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('331181', '龙泉市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340000', '安徽省', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340100', '合肥市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340101', '市辖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340102', '瑶海区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340103', '庐阳区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340104', '蜀山区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340111', '包河区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340121', '长丰县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340122', '肥东县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340123', '肥西县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340124', '庐江县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340181', '巢湖市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340200', '芜湖市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340201', '市辖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340202', '镜湖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340203', '弋江区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340207', '鸠江区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340208', '三山区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340221', '芜湖县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340222', '繁昌县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340223', '南陵县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340225', '无为县', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340300', '蚌埠市', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340301', '市辖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340302', '龙子湖区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340303', '蚌山区', '0', null, '2017-11-05 21:05:24');
INSERT INTO `bpsaarem` VALUES ('340304', '禹会区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340311', '淮上区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340321', '怀远县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340322', '五河县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340323', '固镇县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340400', '淮南市', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340401', '市辖区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340402', '大通区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340403', '田家庵区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340404', '谢家集区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340405', '八公山区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340406', '潘集区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340421', '凤台县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340422', '寿县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340500', '马鞍山市', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340501', '市辖区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340503', '花山区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340504', '雨山区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340506', '博望区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340521', '当涂县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340522', '含山县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340523', '和县', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340600', '淮北市', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340601', '市辖区', '0', null, '2017-11-05 21:05:25');
INSERT INTO `bpsaarem` VALUES ('340602', '杜集区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340603', '相山区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340604', '烈山区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340621', '濉溪县', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340700', '铜陵市', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340701', '市辖区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340705', '铜官区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340706', '义安区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340711', '郊区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340722', '枞阳县', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340800', '安庆市', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340801', '市辖区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340802', '迎江区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340803', '大观区', '0', null, '2017-11-05 21:05:26');
INSERT INTO `bpsaarem` VALUES ('340811', '宜秀区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340822', '怀宁县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340824', '潜山县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340825', '太湖县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340826', '宿松县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340827', '望江县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340828', '岳西县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('340881', '桐城市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341000', '黄山市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341001', '市辖区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341002', '屯溪区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341003', '黄山区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341004', '徽州区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341021', '歙县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341022', '休宁县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341023', '黟县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341024', '祁门县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341100', '滁州市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341101', '市辖区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341102', '琅琊区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341103', '南谯区', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341122', '来安县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341124', '全椒县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341125', '定远县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341126', '凤阳县', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341181', '天长市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341182', '明光市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341200', '阜阳市', '0', null, '2017-11-05 21:05:27');
INSERT INTO `bpsaarem` VALUES ('341201', '市辖区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341202', '颍州区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341203', '颍东区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341204', '颍泉区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341221', '临泉县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341222', '太和县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341225', '阜南县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341226', '颍上县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341282', '界首市', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341300', '宿州市', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341301', '市辖区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341302', '埇桥区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341321', '砀山县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341322', '萧县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341323', '灵璧县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341324', '泗县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341500', '六安市', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341501', '市辖区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341502', '金安区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341503', '裕安区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341504', '叶集区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341522', '霍邱县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341523', '舒城县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341524', '金寨县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341525', '霍山县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341600', '亳州市', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341601', '市辖区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341602', '谯城区', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341621', '涡阳县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341622', '蒙城县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341623', '利辛县', '0', null, '2017-11-05 21:05:28');
INSERT INTO `bpsaarem` VALUES ('341700', '池州市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341701', '市辖区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341702', '贵池区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341721', '东至县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341722', '石台县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341723', '青阳县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341800', '宣城市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341801', '市辖区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341802', '宣州区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341821', '郎溪县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341822', '广德县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341823', '泾县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341824', '绩溪县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341825', '旌德县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('341881', '宁国市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350000', '福建省', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350100', '福州市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350101', '市辖区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350102', '鼓楼区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350103', '台江区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350104', '仓山区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350105', '马尾区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350111', '晋安区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350121', '闽侯县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350122', '连江县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350123', '罗源县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350124', '闽清县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350125', '永泰县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350128', '平潭县', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350181', '福清市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350182', '长乐市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350200', '厦门市', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350201', '市辖区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350203', '思明区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350205', '海沧区', '0', null, '2017-11-05 21:05:29');
INSERT INTO `bpsaarem` VALUES ('350206', '湖里区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350211', '集美区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350212', '同安区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350213', '翔安区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350300', '莆田市', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350301', '市辖区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350302', '城厢区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350303', '涵江区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350304', '荔城区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350305', '秀屿区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350322', '仙游县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350400', '三明市', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350401', '市辖区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350402', '梅列区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350403', '三元区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350421', '明溪县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350423', '清流县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350424', '宁化县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350425', '大田县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350426', '尤溪县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350427', '沙县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350428', '将乐县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350429', '泰宁县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350430', '建宁县', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350481', '永安市', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350500', '泉州市', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350501', '市辖区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350502', '鲤城区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350503', '丰泽区', '0', null, '2017-11-05 21:05:30');
INSERT INTO `bpsaarem` VALUES ('350504', '洛江区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350505', '泉港区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350521', '惠安县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350524', '安溪县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350525', '永春县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350526', '德化县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350527', '金门县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350581', '石狮市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350582', '晋江市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350583', '南安市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350600', '漳州市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350601', '市辖区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350602', '芗城区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350603', '龙文区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350622', '云霄县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350623', '漳浦县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350624', '诏安县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350625', '长泰县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350626', '东山县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350627', '南靖县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350628', '平和县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350629', '华安县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350681', '龙海市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350700', '南平市', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350701', '市辖区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350702', '延平区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350703', '建阳区', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350721', '顺昌县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350722', '浦城县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350723', '光泽县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350724', '松溪县', '0', null, '2017-11-05 21:05:31');
INSERT INTO `bpsaarem` VALUES ('350725', '政和县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350781', '邵武市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350782', '武夷山市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350783', '建瓯市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350800', '龙岩市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350801', '市辖区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350802', '新罗区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350803', '永定区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350821', '长汀县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350823', '上杭县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350824', '武平县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350825', '连城县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350881', '漳平市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350900', '宁德市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350901', '市辖区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350902', '蕉城区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350921', '霞浦县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350922', '古田县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350923', '屏南县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350924', '寿宁县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350925', '周宁县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350926', '柘荣县', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350981', '福安市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('350982', '福鼎市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360000', '江西省', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360100', '南昌市', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360101', '市辖区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360102', '东湖区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360103', '西湖区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360104', '青云谱区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360105', '湾里区', '0', null, '2017-11-05 21:05:32');
INSERT INTO `bpsaarem` VALUES ('360111', '青山湖区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360112', '新建区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360121', '南昌县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360123', '安义县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360124', '进贤县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360200', '景德镇市', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360201', '市辖区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360202', '昌江区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360203', '珠山区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360222', '浮梁县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360281', '乐平市', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360300', '萍乡市', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360301', '市辖区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360302', '安源区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360313', '湘东区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360321', '莲花县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360322', '上栗县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360323', '芦溪县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360400', '九江市', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360401', '市辖区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360402', '濂溪区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360403', '浔阳区', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360421', '九江县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360423', '武宁县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360424', '修水县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360425', '永修县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360426', '德安县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360428', '都昌县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360429', '湖口县', '0', null, '2017-11-05 21:05:33');
INSERT INTO `bpsaarem` VALUES ('360430', '彭泽县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360481', '瑞昌市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360482', '共青城市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360483', '庐山市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360500', '新余市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360501', '市辖区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360502', '渝水区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360521', '分宜县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360600', '鹰潭市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360601', '市辖区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360602', '月湖区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360622', '余江县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360681', '贵溪市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360700', '赣州市', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360701', '市辖区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360702', '章贡区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360703', '南康区', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360721', '赣县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360722', '信丰县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360723', '大余县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360724', '上犹县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360725', '崇义县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360726', '安远县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360727', '龙南县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360728', '定南县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360729', '全南县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360730', '宁都县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360731', '于都县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360732', '兴国县', '0', null, '2017-11-05 21:05:34');
INSERT INTO `bpsaarem` VALUES ('360733', '会昌县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360734', '寻乌县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360735', '石城县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360781', '瑞金市', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360800', '吉安市', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360801', '市辖区', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360802', '吉州区', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360803', '青原区', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360821', '吉安县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360822', '吉水县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360823', '峡江县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360824', '新干县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360825', '永丰县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360826', '泰和县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360827', '遂川县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360828', '万安县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360829', '安福县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360830', '永新县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360881', '井冈山市', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360900', '宜春市', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360901', '市辖区', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360902', '袁州区', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360921', '奉新县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360922', '万载县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360923', '上高县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360924', '宜丰县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360925', '靖安县', '0', null, '2017-11-05 21:05:35');
INSERT INTO `bpsaarem` VALUES ('360926', '铜鼓县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('360981', '丰城市', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('360982', '樟树市', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('360983', '高安市', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361000', '抚州市', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361001', '市辖区', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361002', '临川区', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361021', '南城县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361022', '黎川县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361023', '南丰县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361024', '崇仁县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361025', '乐安县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361026', '宜黄县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361027', '金溪县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361028', '资溪县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361029', '东乡县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361030', '广昌县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361100', '上饶市', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361101', '市辖区', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361102', '信州区', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361103', '广丰区', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361121', '上饶县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361123', '玉山县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361124', '铅山县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361125', '横峰县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361126', '弋阳县', '0', null, '2017-11-05 21:05:36');
INSERT INTO `bpsaarem` VALUES ('361127', '余干县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('361128', '鄱阳县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('361129', '万年县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('361130', '婺源县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('361181', '德兴市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370000', '山东省', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370100', '济南市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370101', '市辖区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370102', '历下区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370103', '市中区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370104', '槐荫区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370105', '天桥区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370112', '历城区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370113', '长清区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370124', '平阴县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370125', '济阳县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370126', '商河县', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370181', '章丘市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370200', '青岛市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370201', '市辖区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370202', '市南区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370203', '市北区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370211', '黄岛区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370212', '崂山区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370213', '李沧区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370214', '城阳区', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370281', '胶州市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370282', '即墨市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370283', '平度市', '0', null, '2017-11-05 21:05:37');
INSERT INTO `bpsaarem` VALUES ('370285', '莱西市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370300', '淄博市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370301', '市辖区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370302', '淄川区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370303', '张店区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370304', '博山区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370305', '临淄区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370306', '周村区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370321', '桓台县', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370322', '高青县', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370323', '沂源县', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370400', '枣庄市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370401', '市辖区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370402', '市中区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370403', '薛城区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370404', '峄城区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370405', '台儿庄区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370406', '山亭区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370481', '滕州市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370500', '东营市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370501', '市辖区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370502', '东营区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370503', '河口区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370505', '垦利区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370522', '利津县', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370523', '广饶县', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370600', '烟台市', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370601', '市辖区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370602', '芝罘区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370611', '福山区', '0', null, '2017-11-05 21:05:38');
INSERT INTO `bpsaarem` VALUES ('370612', '牟平区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370613', '莱山区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370634', '长岛县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370681', '龙口市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370682', '莱阳市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370683', '莱州市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370684', '蓬莱市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370685', '招远市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370686', '栖霞市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370687', '海阳市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370700', '潍坊市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370701', '市辖区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370702', '潍城区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370703', '寒亭区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370704', '坊子区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370705', '奎文区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370724', '临朐县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370725', '昌乐县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370781', '青州市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370782', '诸城市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370783', '寿光市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370784', '安丘市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370785', '高密市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370786', '昌邑市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370800', '济宁市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370801', '市辖区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370811', '任城区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370812', '兖州区', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370826', '微山县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370827', '鱼台县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370828', '金乡县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370829', '嘉祥县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370830', '汶上县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370831', '泗水县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370832', '梁山县', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370881', '曲阜市', '0', null, '2017-11-05 21:05:39');
INSERT INTO `bpsaarem` VALUES ('370883', '邹城市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370900', '泰安市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370901', '市辖区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370902', '泰山区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370911', '岱岳区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370921', '宁阳县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370923', '东平县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370982', '新泰市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('370983', '肥城市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371000', '威海市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371001', '市辖区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371002', '环翠区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371003', '文登区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371082', '荣成市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371083', '乳山市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371100', '日照市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371101', '市辖区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371102', '东港区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371103', '岚山区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371121', '五莲县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371122', '莒县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371200', '莱芜市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371201', '市辖区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371202', '莱城区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371203', '钢城区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371300', '临沂市', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371301', '市辖区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371302', '兰山区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371311', '罗庄区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371312', '河东区', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371321', '沂南县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371322', '郯城县', '0', null, '2017-11-05 21:05:40');
INSERT INTO `bpsaarem` VALUES ('371323', '沂水县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371324', '兰陵县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371325', '费县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371326', '平邑县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371327', '莒南县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371328', '蒙阴县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371329', '临沭县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371400', '德州市', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371401', '市辖区', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371402', '德城区', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371403', '陵城区', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371422', '宁津县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371423', '庆云县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371424', '临邑县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371425', '齐河县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371426', '平原县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371427', '夏津县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371428', '武城县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371481', '乐陵市', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371482', '禹城市', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371500', '聊城市', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371501', '市辖区', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371502', '东昌府区', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371521', '阳谷县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371522', '莘县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371523', '茌平县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371524', '东阿县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371525', '冠县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371526', '高唐县', '0', null, '2017-11-05 21:05:41');
INSERT INTO `bpsaarem` VALUES ('371581', '临清市', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371600', '滨州市', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371601', '市辖区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371602', '滨城区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371603', '沾化区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371621', '惠民县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371622', '阳信县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371623', '无棣县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371625', '博兴县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371626', '邹平县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371700', '菏泽市', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371701', '市辖区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371702', '牡丹区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371703', '定陶区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371721', '曹县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371722', '单县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371723', '成武县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371724', '巨野县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371725', '郓城县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371726', '鄄城县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('371728', '东明县', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410000', '河南省', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410100', '郑州市', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410101', '市辖区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410102', '中原区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410103', '二七区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410104', '管城回族区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410105', '金水区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410106', '上街区', '0', null, '2017-11-05 21:05:42');
INSERT INTO `bpsaarem` VALUES ('410108', '惠济区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410122', '中牟县', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410181', '巩义市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410182', '荥阳市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410183', '新密市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410184', '新郑市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410185', '登封市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410200', '开封市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410201', '市辖区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410202', '龙亭区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410203', '顺河回族区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410204', '鼓楼区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410205', '禹王台区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410211', '金明区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410212', '祥符区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410221', '杞县', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410222', '通许县', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410223', '尉氏县', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410225', '兰考县', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410300', '洛阳市', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410301', '市辖区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410302', '老城区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410303', '西工区', '0', null, '2017-11-05 21:05:43');
INSERT INTO `bpsaarem` VALUES ('410304', '瀍河回族区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410305', '涧西区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410306', '吉利区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410311', '洛龙区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410322', '孟津县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410323', '新安县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410324', '栾川县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410325', '嵩县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410326', '汝阳县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410327', '宜阳县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410328', '洛宁县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410329', '伊川县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410381', '偃师市', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410400', '平顶山市', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410401', '市辖区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410402', '新华区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410403', '卫东区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410404', '石龙区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410411', '湛河区', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410421', '宝丰县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410422', '叶县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410423', '鲁山县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410425', '郏县', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410481', '舞钢市', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410482', '汝州市', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410500', '安阳市', '0', null, '2017-11-05 21:05:44');
INSERT INTO `bpsaarem` VALUES ('410501', '市辖区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410502', '文峰区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410503', '北关区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410505', '殷都区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410506', '龙安区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410522', '安阳县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410523', '汤阴县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410526', '滑县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410527', '内黄县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410581', '林州市', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410600', '鹤壁市', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410601', '市辖区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410602', '鹤山区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410603', '山城区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410611', '淇滨区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410621', '浚县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410622', '淇县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410700', '新乡市', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410701', '市辖区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410702', '红旗区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410703', '卫滨区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410704', '凤泉区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410711', '牧野区', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410721', '新乡县', '0', null, '2017-11-05 21:05:45');
INSERT INTO `bpsaarem` VALUES ('410724', '获嘉县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410725', '原阳县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410726', '延津县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410727', '封丘县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410728', '长垣县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410781', '卫辉市', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410782', '辉县市', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410800', '焦作市', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410801', '市辖区', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410802', '解放区', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410803', '中站区', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410804', '马村区', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410811', '山阳区', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410821', '修武县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410822', '博爱县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410823', '武陟县', '0', null, '2017-11-05 21:05:46');
INSERT INTO `bpsaarem` VALUES ('410825', '温县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410882', '沁阳市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410883', '孟州市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410900', '濮阳市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410901', '市辖区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410902', '华龙区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410922', '清丰县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410923', '南乐县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410926', '范县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410927', '台前县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('410928', '濮阳县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411000', '许昌市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411001', '市辖区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411002', '魏都区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411023', '许昌县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411024', '鄢陵县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411025', '襄城县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411081', '禹州市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411082', '长葛市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411100', '漯河市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411101', '市辖区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411102', '源汇区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411103', '郾城区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411104', '召陵区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411121', '舞阳县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411122', '临颍县', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411200', '三门峡市', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411201', '市辖区', '0', null, '2017-11-05 21:05:47');
INSERT INTO `bpsaarem` VALUES ('411202', '湖滨区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411203', '陕州区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411221', '渑池县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411224', '卢氏县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411281', '义马市', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411282', '灵宝市', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411300', '南阳市', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411301', '市辖区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411302', '宛城区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411303', '卧龙区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411321', '南召县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411322', '方城县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411323', '西峡县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411324', '镇平县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411325', '内乡县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411326', '淅川县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411327', '社旗县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411328', '唐河县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411329', '新野县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411330', '桐柏县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411381', '邓州市', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411400', '商丘市', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411401', '市辖区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411402', '梁园区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411403', '睢阳区', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411421', '民权县', '0', null, '2017-11-05 21:05:48');
INSERT INTO `bpsaarem` VALUES ('411422', '睢县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411423', '宁陵县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411424', '柘城县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411425', '虞城县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411426', '夏邑县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411481', '永城市', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411500', '信阳市', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411501', '市辖区', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411502', '浉河区', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411503', '平桥区', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411521', '罗山县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411522', '光山县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411523', '新县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411524', '商城县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411525', '固始县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411526', '潢川县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411527', '淮滨县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411528', '息县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411600', '周口市', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411601', '市辖区', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411602', '川汇区', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411621', '扶沟县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411622', '西华县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411623', '商水县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411624', '沈丘县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411625', '郸城县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411626', '淮阳县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411627', '太康县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411628', '鹿邑县', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411681', '项城市', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411700', '驻马店市', '0', null, '2017-11-05 21:05:49');
INSERT INTO `bpsaarem` VALUES ('411701', '市辖区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411702', '驿城区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411721', '西平县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411722', '上蔡县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411723', '平舆县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411724', '正阳县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411725', '确山县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411726', '泌阳县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411727', '汝南县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411728', '遂平县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('411729', '新蔡县', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('419000', '省直辖县级行政区划', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('419001', '济源市', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420000', '湖北省', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420100', '武汉市', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420101', '市辖区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420102', '江岸区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420103', '江汉区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420104', '硚口区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420105', '汉阳区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420106', '武昌区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420107', '青山区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420111', '洪山区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420112', '东西湖区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420113', '汉南区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420114', '蔡甸区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420115', '江夏区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420116', '黄陂区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420117', '新洲区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420200', '黄石市', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420201', '市辖区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420202', '黄石港区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420203', '西塞山区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420204', '下陆区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420205', '铁山区', '0', null, '2017-11-05 21:05:50');
INSERT INTO `bpsaarem` VALUES ('420222', '阳新县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420281', '大冶市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420300', '十堰市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420301', '市辖区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420302', '茅箭区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420303', '张湾区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420304', '郧阳区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420322', '郧西县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420323', '竹山县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420324', '竹溪县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420325', '房县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420381', '丹江口市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420500', '宜昌市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420501', '市辖区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420502', '西陵区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420503', '伍家岗区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420504', '点军区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420505', '猇亭区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420506', '夷陵区', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420525', '远安县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420526', '兴山县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420527', '秭归县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420528', '长阳土家族自治县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420529', '五峰土家族自治县', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420581', '宜都市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420582', '当阳市', '0', null, '2017-11-05 21:05:51');
INSERT INTO `bpsaarem` VALUES ('420583', '枝江市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420600', '襄阳市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420601', '市辖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420602', '襄城区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420606', '樊城区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420607', '襄州区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420624', '南漳县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420625', '谷城县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420626', '保康县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420682', '老河口市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420683', '枣阳市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420684', '宜城市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420700', '鄂州市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420701', '市辖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420702', '梁子湖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420703', '华容区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420704', '鄂城区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420800', '荆门市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420801', '市辖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420802', '东宝区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420804', '掇刀区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420821', '京山县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420822', '沙洋县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420881', '钟祥市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420900', '孝感市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420901', '市辖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420902', '孝南区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420921', '孝昌县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420922', '大悟县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420923', '云梦县', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420981', '应城市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420982', '安陆市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('420984', '汉川市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('421000', '荆州市', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('421001', '市辖区', '0', null, '2017-11-05 21:05:52');
INSERT INTO `bpsaarem` VALUES ('421002', '沙市区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421003', '荆州区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421022', '公安县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421023', '监利县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421024', '江陵县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421081', '石首市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421083', '洪湖市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421087', '松滋市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421100', '黄冈市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421101', '市辖区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421102', '黄州区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421121', '团风县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421122', '红安县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421123', '罗田县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421124', '英山县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421125', '浠水县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421126', '蕲春县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421127', '黄梅县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421181', '麻城市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421182', '武穴市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421200', '咸宁市', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421201', '市辖区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421202', '咸安区', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421221', '嘉鱼县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421222', '通城县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421223', '崇阳县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421224', '通山县', '0', null, '2017-11-05 21:05:53');
INSERT INTO `bpsaarem` VALUES ('421281', '赤壁市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('421300', '随州市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('421301', '市辖区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('421303', '曾都区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('421321', '随县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('421381', '广水市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422800', '恩施土家族苗族自治州', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422801', '恩施市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422802', '利川市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422822', '建始县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422823', '巴东县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422825', '宣恩县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422826', '咸丰县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422827', '来凤县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('422828', '鹤峰县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('429000', '省直辖县级行政区划', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('429004', '仙桃市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('429005', '潜江市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('429006', '天门市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('429021', '神农架林区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430000', '湖南省', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430100', '长沙市', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430101', '市辖区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430102', '芙蓉区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430103', '天心区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430104', '岳麓区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430105', '开福区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430111', '雨花区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430112', '望城区', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430121', '长沙县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430124', '宁乡县', '0', null, '2017-11-05 21:05:54');
INSERT INTO `bpsaarem` VALUES ('430181', '浏阳市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430200', '株洲市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430201', '市辖区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430202', '荷塘区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430203', '芦淞区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430204', '石峰区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430211', '天元区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430221', '株洲县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430223', '攸县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430224', '茶陵县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430225', '炎陵县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430281', '醴陵市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430300', '湘潭市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430301', '市辖区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430302', '雨湖区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430304', '岳塘区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430321', '湘潭县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430381', '湘乡市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430382', '韶山市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430400', '衡阳市', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430401', '市辖区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430405', '珠晖区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430406', '雁峰区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430407', '石鼓区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430408', '蒸湘区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430412', '南岳区', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430421', '衡阳县', '0', null, '2017-11-05 21:05:55');
INSERT INTO `bpsaarem` VALUES ('430422', '衡南县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430423', '衡山县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430424', '衡东县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430426', '祁东县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430481', '耒阳市', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430482', '常宁市', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430500', '邵阳市', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430501', '市辖区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430502', '双清区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430503', '大祥区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430511', '北塔区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430521', '邵东县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430522', '新邵县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430523', '邵阳县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430524', '隆回县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430525', '洞口县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430527', '绥宁县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430528', '新宁县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430529', '城步苗族自治县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430581', '武冈市', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430600', '岳阳市', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430601', '市辖区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430602', '岳阳楼区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430603', '云溪区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430611', '君山区', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430621', '岳阳县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430623', '华容县', '0', null, '2017-11-05 21:05:56');
INSERT INTO `bpsaarem` VALUES ('430624', '湘阴县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430626', '平江县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430681', '汨罗市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430682', '临湘市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430700', '常德市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430701', '市辖区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430702', '武陵区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430703', '鼎城区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430721', '安乡县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430722', '汉寿县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430723', '澧县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430724', '临澧县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430725', '桃源县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430726', '石门县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430781', '津市市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430800', '张家界市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430801', '市辖区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430802', '永定区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430811', '武陵源区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430821', '慈利县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430822', '桑植县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430900', '益阳市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430901', '市辖区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430902', '资阳区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430903', '赫山区', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430921', '南县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430922', '桃江县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430923', '安化县', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('430981', '沅江市', '0', null, '2017-11-05 21:05:57');
INSERT INTO `bpsaarem` VALUES ('431000', '郴州市', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431001', '市辖区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431002', '北湖区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431003', '苏仙区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431021', '桂阳县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431022', '宜章县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431023', '永兴县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431024', '嘉禾县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431025', '临武县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431026', '汝城县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431027', '桂东县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431028', '安仁县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431081', '资兴市', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431100', '永州市', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431101', '市辖区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431102', '零陵区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431103', '冷水滩区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431121', '祁阳县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431122', '东安县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431123', '双牌县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431124', '道县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431125', '江永县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431126', '宁远县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431127', '蓝山县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431128', '新田县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431129', '江华瑶族自治县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431200', '怀化市', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431201', '市辖区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431202', '鹤城区', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431221', '中方县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431222', '沅陵县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431223', '辰溪县', '0', null, '2017-11-05 21:05:58');
INSERT INTO `bpsaarem` VALUES ('431224', '溆浦县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431225', '会同县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431226', '麻阳苗族自治县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431227', '新晃侗族自治县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431228', '芷江侗族自治县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431229', '靖州苗族侗族自治县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431230', '通道侗族自治县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431281', '洪江市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431300', '娄底市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431301', '市辖区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431302', '娄星区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431321', '双峰县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431322', '新化县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431381', '冷水江市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('431382', '涟源市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433100', '湘西土家族苗族自治州', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433101', '吉首市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433122', '泸溪县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433123', '凤凰县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433124', '花垣县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433125', '保靖县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433126', '古丈县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433127', '永顺县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('433130', '龙山县', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440000', '广东省', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440100', '广州市', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440101', '市辖区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440103', '荔湾区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440104', '越秀区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440105', '海珠区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440106', '天河区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440111', '白云区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440112', '黄埔区', '0', null, '2017-11-05 21:05:59');
INSERT INTO `bpsaarem` VALUES ('440113', '番禺区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440114', '花都区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440115', '南沙区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440117', '从化区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440118', '增城区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440200', '韶关市', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440201', '市辖区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440203', '武江区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440204', '浈江区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440205', '曲江区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440222', '始兴县', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440224', '仁化县', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440229', '翁源县', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440232', '乳源瑶族自治县', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440233', '新丰县', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440281', '乐昌市', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440282', '南雄市', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440300', '深圳市', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440301', '市辖区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440303', '罗湖区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440304', '福田区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440305', '南山区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440306', '宝安区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440307', '龙岗区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440308', '盐田区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440400', '珠海市', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440401', '市辖区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440402', '香洲区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440403', '斗门区', '0', null, '2017-11-05 21:06:00');
INSERT INTO `bpsaarem` VALUES ('440404', '金湾区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440500', '汕头市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440501', '市辖区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440507', '龙湖区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440511', '金平区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440512', '濠江区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440513', '潮阳区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440514', '潮南区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440515', '澄海区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440523', '南澳县', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440600', '佛山市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440601', '市辖区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440604', '禅城区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440605', '南海区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440606', '顺德区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440607', '三水区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440608', '高明区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440700', '江门市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440701', '市辖区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440703', '蓬江区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440704', '江海区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440705', '新会区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440781', '台山市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440783', '开平市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440784', '鹤山市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440785', '恩平市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440800', '湛江市', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440801', '市辖区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440802', '赤坎区', '0', null, '2017-11-05 21:06:01');
INSERT INTO `bpsaarem` VALUES ('440803', '霞山区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440804', '坡头区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440811', '麻章区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440823', '遂溪县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440825', '徐闻县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440881', '廉江市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440882', '雷州市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440883', '吴川市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440900', '茂名市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440901', '市辖区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440902', '茂南区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440904', '电白区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440981', '高州市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440982', '化州市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('440983', '信宜市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441200', '肇庆市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441201', '市辖区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441202', '端州区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441203', '鼎湖区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441204', '高要区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441223', '广宁县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441224', '怀集县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441225', '封开县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441226', '德庆县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441284', '四会市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441300', '惠州市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441301', '市辖区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441302', '惠城区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441303', '惠阳区', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441322', '博罗县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441323', '惠东县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441324', '龙门县', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441400', '梅州市', '0', null, '2017-11-05 21:06:02');
INSERT INTO `bpsaarem` VALUES ('441401', '市辖区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441402', '梅江区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441403', '梅县区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441422', '大埔县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441423', '丰顺县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441424', '五华县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441426', '平远县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441427', '蕉岭县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441481', '兴宁市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441500', '汕尾市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441501', '市辖区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441502', '城区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441521', '海丰县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441523', '陆河县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441581', '陆丰市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441600', '河源市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441601', '市辖区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441602', '源城区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441621', '紫金县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441622', '龙川县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441623', '连平县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441624', '和平县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441625', '东源县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441700', '阳江市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441701', '市辖区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441702', '江城区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441704', '阳东区', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441721', '阳西县', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441781', '阳春市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441800', '清远市', '0', null, '2017-11-05 21:06:03');
INSERT INTO `bpsaarem` VALUES ('441801', '市辖区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441802', '清城区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441803', '清新区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441821', '佛冈县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441823', '阳山县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441825', '连山壮族瑶族自治县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441826', '连南瑶族自治县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441881', '英德市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441882', '连州市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('441900', '东莞市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('442000', '中山市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445100', '潮州市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445101', '市辖区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445102', '湘桥区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445103', '潮安区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445122', '饶平县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445200', '揭阳市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445201', '市辖区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445202', '榕城区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445203', '揭东区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445222', '揭西县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445224', '惠来县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445281', '普宁市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445300', '云浮市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445301', '市辖区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445302', '云城区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445303', '云安区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445321', '新兴县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445322', '郁南县', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('445381', '罗定市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('450000', '广西壮族自治区', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('450100', '南宁市', '0', null, '2017-11-05 21:06:04');
INSERT INTO `bpsaarem` VALUES ('450101', '市辖区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450102', '兴宁区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450103', '青秀区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450105', '江南区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450107', '西乡塘区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450108', '良庆区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450109', '邕宁区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450110', '武鸣区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450123', '隆安县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450124', '马山县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450125', '上林县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450126', '宾阳县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450127', '横县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450200', '柳州市', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450201', '市辖区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450202', '城中区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450203', '鱼峰区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450204', '柳南区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450205', '柳北区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450206', '柳江区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450222', '柳城县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450223', '鹿寨县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450224', '融安县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450225', '融水苗族自治县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450226', '三江侗族自治县', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450300', '桂林市', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450301', '市辖区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450302', '秀峰区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450303', '叠彩区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450304', '象山区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450305', '七星区', '0', null, '2017-11-05 21:06:05');
INSERT INTO `bpsaarem` VALUES ('450311', '雁山区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450312', '临桂区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450321', '阳朔县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450323', '灵川县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450324', '全州县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450325', '兴安县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450326', '永福县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450327', '灌阳县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450328', '龙胜各族自治县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450329', '资源县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450330', '平乐县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450331', '荔浦县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450332', '恭城瑶族自治县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450400', '梧州市', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450401', '市辖区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450403', '万秀区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450405', '长洲区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450406', '龙圩区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450421', '苍梧县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450422', '藤县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450423', '蒙山县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450481', '岑溪市', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450500', '北海市', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450501', '市辖区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450502', '海城区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450503', '银海区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450512', '铁山港区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450521', '合浦县', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450600', '防城港市', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450601', '市辖区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450602', '港口区', '0', null, '2017-11-05 21:06:06');
INSERT INTO `bpsaarem` VALUES ('450603', '防城区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450621', '上思县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450681', '东兴市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450700', '钦州市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450701', '市辖区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450702', '钦南区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450703', '钦北区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450721', '灵山县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450722', '浦北县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450800', '贵港市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450801', '市辖区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450802', '港北区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450803', '港南区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450804', '覃塘区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450821', '平南县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450881', '桂平市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450900', '玉林市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450901', '市辖区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450902', '玉州区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450903', '福绵区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450921', '容县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450922', '陆川县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450923', '博白县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450924', '兴业县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('450981', '北流市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451000', '百色市', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451001', '市辖区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451002', '右江区', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451021', '田阳县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451022', '田东县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451023', '平果县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451024', '德保县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451026', '那坡县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451027', '凌云县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451028', '乐业县', '0', null, '2017-11-05 21:06:07');
INSERT INTO `bpsaarem` VALUES ('451029', '田林县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451030', '西林县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451031', '隆林各族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451081', '靖西市', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451100', '贺州市', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451101', '市辖区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451102', '八步区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451103', '平桂区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451121', '昭平县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451122', '钟山县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451123', '富川瑶族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451200', '河池市', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451201', '市辖区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451202', '金城江区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451221', '南丹县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451222', '天峨县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451223', '凤山县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451224', '东兰县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451225', '罗城仫佬族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451226', '环江毛南族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451227', '巴马瑶族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451228', '都安瑶族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451229', '大化瑶族自治县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451281', '宜州市', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451300', '来宾市', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451301', '市辖区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451302', '兴宾区', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451321', '忻城县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451322', '象州县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451323', '武宣县', '0', null, '2017-11-05 21:06:08');
INSERT INTO `bpsaarem` VALUES ('451324', '金秀瑶族自治县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451381', '合山市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451400', '崇左市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451401', '市辖区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451402', '江州区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451421', '扶绥县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451422', '宁明县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451423', '龙州县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451424', '大新县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451425', '天等县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('451481', '凭祥市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460000', '海南省', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460100', '海口市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460101', '市辖区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460105', '秀英区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460106', '龙华区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460107', '琼山区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460108', '美兰区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460200', '三亚市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460201', '市辖区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460202', '海棠区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460203', '吉阳区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460204', '天涯区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460205', '崖州区', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460300', '三沙市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('460400', '儋州市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469000', '省直辖县级行政区划', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469001', '五指山市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469002', '琼海市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469005', '文昌市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469006', '万宁市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469007', '东方市', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469021', '定安县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469022', '屯昌县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469023', '澄迈县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469024', '临高县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469025', '白沙黎族自治县', '0', null, '2017-11-05 21:06:09');
INSERT INTO `bpsaarem` VALUES ('469026', '昌江黎族自治县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('469027', '乐东黎族自治县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('469028', '陵水黎族自治县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('469029', '保亭黎族苗族自治县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('469030', '琼中黎族苗族自治县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500000', '重庆市', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500100', '市辖区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500101', '万州区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500102', '涪陵区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500103', '渝中区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500104', '大渡口区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500105', '江北区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500106', '沙坪坝区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500107', '九龙坡区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500108', '南岸区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500109', '北碚区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500110', '綦江区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500111', '大足区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500112', '渝北区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500113', '巴南区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500114', '黔江区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500115', '长寿区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500116', '江津区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500117', '合川区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500118', '永川区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500119', '南川区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500120', '璧山区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500151', '铜梁区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500152', '潼南区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500153', '荣昌区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500154', '开州区', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500200', '县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500228', '梁平县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500229', '城口县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500230', '丰都县', '0', null, '2017-11-05 21:06:10');
INSERT INTO `bpsaarem` VALUES ('500231', '垫江县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500232', '武隆县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500233', '忠县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500235', '云阳县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500236', '奉节县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500237', '巫山县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500238', '巫溪县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500240', '石柱土家族自治县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500241', '秀山土家族苗族自治县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500242', '酉阳土家族苗族自治县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('500243', '彭水苗族土家族自治县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510000', '四川省', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510100', '成都市', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510101', '市辖区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510104', '锦江区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510105', '青羊区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510106', '金牛区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510107', '武侯区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510108', '成华区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510112', '龙泉驿区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510113', '青白江区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510114', '新都区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510115', '温江区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510116', '双流区', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510121', '金堂县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510124', '郫县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510129', '大邑县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510131', '蒲江县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510132', '新津县', '0', null, '2017-11-05 21:06:11');
INSERT INTO `bpsaarem` VALUES ('510181', '都江堰市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510182', '彭州市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510183', '邛崃市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510184', '崇州市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510185', '简阳市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510300', '自贡市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510301', '市辖区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510302', '自流井区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510303', '贡井区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510304', '大安区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510311', '沿滩区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510321', '荣县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510322', '富顺县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510400', '攀枝花市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510401', '市辖区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510402', '东区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510403', '西区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510411', '仁和区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510421', '米易县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510422', '盐边县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510500', '泸州市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510501', '市辖区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510502', '江阳区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510503', '纳溪区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510504', '龙马潭区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510521', '泸县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510522', '合江县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510524', '叙永县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510525', '古蔺县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510600', '德阳市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510601', '市辖区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510603', '旌阳区', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510623', '中江县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510626', '罗江县', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510681', '广汉市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510682', '什邡市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510683', '绵竹市', '0', null, '2017-11-05 21:06:12');
INSERT INTO `bpsaarem` VALUES ('510700', '绵阳市', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510701', '市辖区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510703', '涪城区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510704', '游仙区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510705', '安州区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510722', '三台县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510723', '盐亭县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510725', '梓潼县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510726', '北川羌族自治县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510727', '平武县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510781', '江油市', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510800', '广元市', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510801', '市辖区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510802', '利州区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510811', '昭化区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510812', '朝天区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510821', '旺苍县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510822', '青川县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510823', '剑阁县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510824', '苍溪县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510900', '遂宁市', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510901', '市辖区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510903', '船山区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510904', '安居区', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510921', '蓬溪县', '0', null, '2017-11-05 21:06:13');
INSERT INTO `bpsaarem` VALUES ('510922', '射洪县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('510923', '大英县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511000', '内江市', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511001', '市辖区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511002', '市中区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511011', '东兴区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511024', '威远县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511025', '资中县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511028', '隆昌县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511100', '乐山市', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511101', '市辖区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511102', '市中区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511111', '沙湾区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511112', '五通桥区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511113', '金口河区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511123', '犍为县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511124', '井研县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511126', '夹江县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511129', '沐川县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511132', '峨边彝族自治县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511133', '马边彝族自治县', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511181', '峨眉山市', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511300', '南充市', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511301', '市辖区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511302', '顺庆区', '0', null, '2017-11-05 21:06:14');
INSERT INTO `bpsaarem` VALUES ('511303', '高坪区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511304', '嘉陵区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511321', '南部县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511322', '营山县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511323', '蓬安县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511324', '仪陇县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511325', '西充县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511381', '阆中市', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511400', '眉山市', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511401', '市辖区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511402', '东坡区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511403', '彭山区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511421', '仁寿县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511423', '洪雅县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511424', '丹棱县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511425', '青神县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511500', '宜宾市', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511501', '市辖区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511502', '翠屏区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511503', '南溪区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511521', '宜宾县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511523', '江安县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511524', '长宁县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511525', '高县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511526', '珙县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511527', '筠连县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511528', '兴文县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511529', '屏山县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511600', '广安市', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511601', '市辖区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511602', '广安区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511603', '前锋区', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511621', '岳池县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511622', '武胜县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511623', '邻水县', '0', null, '2017-11-05 21:06:15');
INSERT INTO `bpsaarem` VALUES ('511681', '华蓥市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511700', '达州市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511701', '市辖区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511702', '通川区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511703', '达川区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511722', '宣汉县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511723', '开江县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511724', '大竹县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511725', '渠县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511781', '万源市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511800', '雅安市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511801', '市辖区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511802', '雨城区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511803', '名山区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511822', '荥经县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511823', '汉源县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511824', '石棉县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511825', '天全县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511826', '芦山县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511827', '宝兴县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511900', '巴中市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511901', '市辖区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511902', '巴州区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511903', '恩阳区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511921', '通江县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511922', '南江县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('511923', '平昌县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('512000', '资阳市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('512001', '市辖区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('512002', '雁江区', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('512021', '安岳县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('512022', '乐至县', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('513200', '阿坝藏族羌族自治州', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('513201', '马尔康市', '0', null, '2017-11-05 21:06:16');
INSERT INTO `bpsaarem` VALUES ('513221', '汶川县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513222', '理县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513223', '茂县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513224', '松潘县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513225', '九寨沟县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513226', '金川县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513227', '小金县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513228', '黑水县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513230', '壤塘县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513231', '阿坝县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513232', '若尔盖县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513233', '红原县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513300', '甘孜藏族自治州', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513301', '康定市', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513322', '泸定县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513323', '丹巴县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513324', '九龙县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513325', '雅江县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513326', '道孚县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513327', '炉霍县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513328', '甘孜县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513329', '新龙县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513330', '德格县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513331', '白玉县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513332', '石渠县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513333', '色达县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513334', '理塘县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513335', '巴塘县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513336', '乡城县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513337', '稻城县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513338', '得荣县', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513400', '凉山彝族自治州', '0', null, '2017-11-05 21:06:17');
INSERT INTO `bpsaarem` VALUES ('513401', '西昌市', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513422', '木里藏族自治县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513423', '盐源县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513424', '德昌县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513425', '会理县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513426', '会东县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513427', '宁南县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513428', '普格县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513429', '布拖县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513430', '金阳县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513431', '昭觉县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513432', '喜德县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513433', '冕宁县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513434', '越西县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513435', '甘洛县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513436', '美姑县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('513437', '雷波县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520000', '贵州省', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520100', '贵阳市', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520101', '市辖区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520102', '南明区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520103', '云岩区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520111', '花溪区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520112', '乌当区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520113', '白云区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520115', '观山湖区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520121', '开阳县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520122', '息烽县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520123', '修文县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520181', '清镇市', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520200', '六盘水市', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520201', '钟山区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520203', '六枝特区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520221', '水城县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520222', '盘县', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520300', '遵义市', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520301', '市辖区', '0', null, '2017-11-05 21:06:18');
INSERT INTO `bpsaarem` VALUES ('520302', '红花岗区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520303', '汇川区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520304', '播州区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520322', '桐梓县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520323', '绥阳县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520324', '正安县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520325', '道真仡佬族苗族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520326', '务川仡佬族苗族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520327', '凤冈县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520328', '湄潭县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520329', '余庆县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520330', '习水县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520381', '赤水市', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520382', '仁怀市', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520400', '安顺市', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520401', '市辖区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520402', '西秀区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520403', '平坝区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520422', '普定县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520423', '镇宁布依族苗族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520424', '关岭布依族苗族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520425', '紫云苗族布依族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520500', '毕节市', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520501', '市辖区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520502', '七星关区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520521', '大方县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520522', '黔西县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520523', '金沙县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520524', '织金县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520525', '纳雍县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520526', '威宁彝族回族苗族自治县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520527', '赫章县', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520600', '铜仁市', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520601', '市辖区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520602', '碧江区', '0', null, '2017-11-05 21:06:19');
INSERT INTO `bpsaarem` VALUES ('520603', '万山区', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520621', '江口县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520622', '玉屏侗族自治县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520623', '石阡县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520624', '思南县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520625', '印江土家族苗族自治县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520626', '德江县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520627', '沿河土家族自治县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('520628', '松桃苗族自治县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522300', '黔西南布依族苗族自治州', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522301', '兴义市', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522322', '兴仁县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522323', '普安县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522324', '晴隆县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522325', '贞丰县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522326', '望谟县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522327', '册亨县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522328', '安龙县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522600', '黔东南苗族侗族自治州', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522601', '凯里市', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522622', '黄平县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522623', '施秉县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522624', '三穗县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522625', '镇远县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522626', '岑巩县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522627', '天柱县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522628', '锦屏县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522629', '剑河县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522630', '台江县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522631', '黎平县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522632', '榕江县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522633', '从江县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522634', '雷山县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522635', '麻江县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522636', '丹寨县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522700', '黔南布依族苗族自治州', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522701', '都匀市', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522702', '福泉市', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522722', '荔波县', '0', null, '2017-11-05 21:06:20');
INSERT INTO `bpsaarem` VALUES ('522723', '贵定县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522725', '瓮安县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522726', '独山县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522727', '平塘县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522728', '罗甸县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522729', '长顺县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522730', '龙里县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522731', '惠水县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('522732', '三都水族自治县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530000', '云南省', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530100', '昆明市', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530101', '市辖区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530102', '五华区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530103', '盘龙区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530111', '官渡区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530112', '西山区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530113', '东川区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530114', '呈贡区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530122', '晋宁县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530124', '富民县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530125', '宜良县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530126', '石林彝族自治县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530127', '嵩明县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530128', '禄劝彝族苗族自治县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530129', '寻甸回族彝族自治县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530181', '安宁市', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530300', '曲靖市', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530301', '市辖区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530302', '麒麟区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530303', '沾益区', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530321', '马龙县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530322', '陆良县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530323', '师宗县', '0', null, '2017-11-05 21:06:21');
INSERT INTO `bpsaarem` VALUES ('530324', '罗平县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530325', '富源县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530326', '会泽县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530381', '宣威市', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530400', '玉溪市', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530401', '市辖区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530402', '红塔区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530403', '江川区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530422', '澄江县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530423', '通海县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530424', '华宁县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530425', '易门县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530426', '峨山彝族自治县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530427', '新平彝族傣族自治县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530428', '元江哈尼族彝族傣族自治县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530500', '保山市', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530501', '市辖区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530502', '隆阳区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530521', '施甸县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530523', '龙陵县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530524', '昌宁县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530581', '腾冲市', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530600', '昭通市', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530601', '市辖区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530602', '昭阳区', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530621', '鲁甸县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530622', '巧家县', '0', null, '2017-11-05 21:06:22');
INSERT INTO `bpsaarem` VALUES ('530623', '盐津县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530624', '大关县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530625', '永善县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530626', '绥江县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530627', '镇雄县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530628', '彝良县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530629', '威信县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530630', '水富县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530700', '丽江市', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530701', '市辖区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530702', '古城区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530721', '玉龙纳西族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530722', '永胜县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530723', '华坪县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530724', '宁蒗彝族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530800', '普洱市', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530801', '市辖区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530802', '思茅区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530821', '宁洱哈尼族彝族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530822', '墨江哈尼族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530823', '景东彝族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530824', '景谷傣族彝族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530826', '江城哈尼族彝族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530827', '孟连傣族拉祜族佤族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530828', '澜沧拉祜族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530829', '西盟佤族自治县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530900', '临沧市', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530901', '市辖区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530902', '临翔区', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530921', '凤庆县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530922', '云县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530923', '永德县', '0', null, '2017-11-05 21:06:23');
INSERT INTO `bpsaarem` VALUES ('530924', '镇康县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('530926', '耿马傣族佤族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('530927', '沧源佤族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532300', '楚雄彝族自治州', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532301', '楚雄市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532322', '双柏县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532323', '牟定县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532324', '南华县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532325', '姚安县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532326', '大姚县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532327', '永仁县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532328', '元谋县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532329', '武定县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532331', '禄丰县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532500', '红河哈尼族彝族自治州', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532501', '个旧市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532502', '开远市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532503', '蒙自市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532504', '弥勒市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532523', '屏边苗族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532524', '建水县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532525', '石屏县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532527', '泸西县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532528', '元阳县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532529', '红河县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532530', '金平苗族瑶族傣族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532531', '绿春县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532532', '河口瑶族自治县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532600', '文山壮族苗族自治州', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532601', '文山市', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532622', '砚山县', '0', null, '2017-11-05 21:06:24');
INSERT INTO `bpsaarem` VALUES ('532623', '西畴县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532624', '麻栗坡县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532625', '马关县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532626', '丘北县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532627', '广南县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532628', '富宁县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532800', '西双版纳傣族自治州', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532801', '景洪市', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532822', '勐海县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532823', '勐腊县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532900', '大理白族自治州', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532901', '大理市', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532922', '漾濞彝族自治县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532923', '祥云县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532924', '宾川县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532925', '弥渡县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532926', '南涧彝族自治县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532927', '巍山彝族回族自治县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532928', '永平县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532929', '云龙县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532930', '洱源县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532931', '剑川县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('532932', '鹤庆县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533100', '德宏傣族景颇族自治州', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533102', '瑞丽市', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533103', '芒市', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533122', '梁河县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533123', '盈江县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533124', '陇川县', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533300', '怒江傈僳族自治州', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533301', '泸水市', '0', null, '2017-11-05 21:06:25');
INSERT INTO `bpsaarem` VALUES ('533323', '福贡县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533324', '贡山独龙族怒族自治县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533325', '兰坪白族普米族自治县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533400', '迪庆藏族自治州', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533401', '香格里拉市', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533422', '德钦县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('533423', '维西傈僳族自治县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540000', '西藏自治区', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540100', '拉萨市', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540101', '市辖区', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540102', '城关区', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540103', '堆龙德庆区', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540121', '林周县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540122', '当雄县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540123', '尼木县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540124', '曲水县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540126', '达孜县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540127', '墨竹工卡县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540200', '日喀则市', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540202', '桑珠孜区', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540221', '南木林县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540222', '江孜县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540223', '定日县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540224', '萨迦县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540225', '拉孜县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540226', '昂仁县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540227', '谢通门县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540228', '白朗县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540229', '仁布县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540230', '康马县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540231', '定结县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540232', '仲巴县', '0', null, '2017-11-05 21:06:26');
INSERT INTO `bpsaarem` VALUES ('540233', '亚东县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540234', '吉隆县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540235', '聂拉木县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540236', '萨嘎县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540237', '岗巴县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540300', '昌都市', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540302', '卡若区', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540321', '江达县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540322', '贡觉县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540323', '类乌齐县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540324', '丁青县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540325', '察雅县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540326', '八宿县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540327', '左贡县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540328', '芒康县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540329', '洛隆县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540330', '边坝县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540400', '林芝市', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540402', '巴宜区', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540421', '工布江达县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540422', '米林县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540423', '墨脱县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540424', '波密县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540425', '察隅县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540426', '朗县', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540500', '山南市', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540501', '市辖区', '0', null, '2017-11-05 21:06:27');
INSERT INTO `bpsaarem` VALUES ('540502', '乃东区', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540521', '扎囊县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540522', '贡嘎县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540523', '桑日县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540524', '琼结县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540525', '曲松县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540526', '措美县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540527', '洛扎县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540528', '加查县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540529', '隆子县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540530', '错那县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('540531', '浪卡子县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542400', '那曲地区', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542421', '那曲县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542422', '嘉黎县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542423', '比如县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542424', '聂荣县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542425', '安多县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542426', '申扎县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542427', '索县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542428', '班戈县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542429', '巴青县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542430', '尼玛县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542431', '双湖县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542500', '阿里地区', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542521', '普兰县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542522', '札达县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542523', '噶尔县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542524', '日土县', '0', null, '2017-11-05 21:06:28');
INSERT INTO `bpsaarem` VALUES ('542525', '革吉县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('542526', '改则县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('542527', '措勤县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610000', '陕西省', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610100', '西安市', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610101', '市辖区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610102', '新城区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610103', '碑林区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610104', '莲湖区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610111', '灞桥区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610112', '未央区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610113', '雁塔区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610114', '阎良区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610115', '临潼区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610116', '长安区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610117', '高陵区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610122', '蓝田县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610124', '周至县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610125', '户县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610200', '铜川市', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610201', '市辖区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610202', '王益区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610203', '印台区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610204', '耀州区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610222', '宜君县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610300', '宝鸡市', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610301', '市辖区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610302', '渭滨区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610303', '金台区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610304', '陈仓区', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610322', '凤翔县', '0', null, '2017-11-05 21:06:29');
INSERT INTO `bpsaarem` VALUES ('610323', '岐山县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610324', '扶风县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610326', '眉县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610327', '陇县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610328', '千阳县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610329', '麟游县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610330', '凤县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610331', '太白县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610400', '咸阳市', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610401', '市辖区', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610402', '秦都区', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610403', '杨陵区', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610404', '渭城区', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610422', '三原县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610423', '泾阳县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610424', '乾县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610425', '礼泉县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610426', '永寿县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610427', '彬县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610428', '长武县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610429', '旬邑县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610430', '淳化县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610431', '武功县', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610481', '兴平市', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610500', '渭南市', '0', null, '2017-11-05 21:06:30');
INSERT INTO `bpsaarem` VALUES ('610501', '市辖区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610502', '临渭区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610503', '华州区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610522', '潼关县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610523', '大荔县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610524', '合阳县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610525', '澄城县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610526', '蒲城县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610527', '白水县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610528', '富平县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610581', '韩城市', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610582', '华阴市', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610600', '延安市', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610601', '市辖区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610602', '宝塔区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610603', '安塞区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610621', '延长县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610622', '延川县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610623', '子长县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610625', '志丹县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610626', '吴起县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610627', '甘泉县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610628', '富县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610629', '洛川县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610630', '宜川县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610631', '黄龙县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610632', '黄陵县', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610700', '汉中市', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610701', '市辖区', '0', null, '2017-11-05 21:06:31');
INSERT INTO `bpsaarem` VALUES ('610702', '汉台区', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610721', '南郑县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610722', '城固县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610723', '洋县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610724', '西乡县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610725', '勉县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610726', '宁强县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610727', '略阳县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610728', '镇巴县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610729', '留坝县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610730', '佛坪县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610800', '榆林市', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610801', '市辖区', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610802', '榆阳区', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610803', '横山区', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610821', '神木县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610822', '府谷县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610824', '靖边县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610825', '定边县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610826', '绥德县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610827', '米脂县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610828', '佳县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610829', '吴堡县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610830', '清涧县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610831', '子洲县', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610900', '安康市', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610901', '市辖区', '0', null, '2017-11-05 21:06:32');
INSERT INTO `bpsaarem` VALUES ('610902', '汉滨区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610921', '汉阴县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610922', '石泉县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610923', '宁陕县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610924', '紫阳县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610925', '岚皋县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610926', '平利县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610927', '镇坪县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610928', '旬阳县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('610929', '白河县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611000', '商洛市', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611001', '市辖区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611002', '商州区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611021', '洛南县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611022', '丹凤县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611023', '商南县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611024', '山阳县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611025', '镇安县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('611026', '柞水县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620000', '甘肃省', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620100', '兰州市', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620101', '市辖区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620102', '城关区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620103', '七里河区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620104', '西固区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620105', '安宁区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620111', '红古区', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620121', '永登县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620122', '皋兰县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620123', '榆中县', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620200', '嘉峪关市', '0', null, '2017-11-05 21:06:33');
INSERT INTO `bpsaarem` VALUES ('620201', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620300', '金昌市', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620301', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620302', '金川区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620321', '永昌县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620400', '白银市', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620401', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620402', '白银区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620403', '平川区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620421', '靖远县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620422', '会宁县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620423', '景泰县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620500', '天水市', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620501', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620502', '秦州区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620503', '麦积区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620521', '清水县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620522', '秦安县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620523', '甘谷县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620524', '武山县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620525', '张家川回族自治县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620600', '武威市', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620601', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620602', '凉州区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620621', '民勤县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620622', '古浪县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620623', '天祝藏族自治县', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620700', '张掖市', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620701', '市辖区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620702', '甘州区', '0', null, '2017-11-05 21:06:34');
INSERT INTO `bpsaarem` VALUES ('620721', '肃南裕固族自治县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620722', '民乐县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620723', '临泽县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620724', '高台县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620725', '山丹县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620800', '平凉市', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620801', '市辖区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620802', '崆峒区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620821', '泾川县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620822', '灵台县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620823', '崇信县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620824', '华亭县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620825', '庄浪县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620826', '静宁县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620900', '酒泉市', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620901', '市辖区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620902', '肃州区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620921', '金塔县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620922', '瓜州县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620923', '肃北蒙古族自治县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620924', '阿克塞哈萨克族自治县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620981', '玉门市', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('620982', '敦煌市', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621000', '庆阳市', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621001', '市辖区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621002', '西峰区', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621021', '庆城县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621022', '环县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621023', '华池县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621024', '合水县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621025', '正宁县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621026', '宁县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621027', '镇原县', '0', null, '2017-11-05 21:06:35');
INSERT INTO `bpsaarem` VALUES ('621100', '定西市', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621101', '市辖区', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621102', '安定区', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621121', '通渭县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621122', '陇西县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621123', '渭源县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621124', '临洮县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621125', '漳县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621126', '岷县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621200', '陇南市', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621201', '市辖区', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621202', '武都区', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621221', '成县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621222', '文县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621223', '宕昌县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621224', '康县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621225', '西和县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621226', '礼县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621227', '徽县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('621228', '两当县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622900', '临夏回族自治州', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622901', '临夏市', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622921', '临夏县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622922', '康乐县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622923', '永靖县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622924', '广河县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622925', '和政县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622926', '东乡族自治县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623000', '甘南藏族自治州', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623001', '合作市', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623021', '临潭县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623022', '卓尼县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623023', '舟曲县', '0', null, '2017-11-05 21:06:36');
INSERT INTO `bpsaarem` VALUES ('623024', '迭部县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('623025', '玛曲县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('623026', '碌曲县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('623027', '夏河县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630000', '青海省', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630100', '西宁市', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630101', '市辖区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630102', '城东区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630103', '城中区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630104', '城西区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630105', '城北区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630121', '大通回族土族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630122', '湟中县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630123', '湟源县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630200', '海东市', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630202', '乐都区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630203', '平安区', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630222', '民和回族土族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630223', '互助土族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630224', '化隆回族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('630225', '循化撒拉族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632200', '海北藏族自治州', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632221', '门源回族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632222', '祁连县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632223', '海晏县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632224', '刚察县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632300', '黄南藏族自治州', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632321', '同仁县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632322', '尖扎县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632323', '泽库县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632324', '河南蒙古族自治县', '0', null, '2017-11-05 21:06:37');
INSERT INTO `bpsaarem` VALUES ('632500', '海南藏族自治州', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632521', '共和县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632522', '同德县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632523', '贵德县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632524', '兴海县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632525', '贵南县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632600', '果洛藏族自治州', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632621', '玛沁县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632622', '班玛县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632623', '甘德县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632624', '达日县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632625', '久治县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632626', '玛多县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632700', '玉树藏族自治州', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632701', '玉树市', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632722', '杂多县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632723', '称多县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632724', '治多县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632725', '囊谦县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632726', '曲麻莱县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632800', '海西蒙古族藏族自治州', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632801', '格尔木市', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632802', '德令哈市', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632821', '乌兰县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632822', '都兰县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('632823', '天峻县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640000', '宁夏回族自治区', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640100', '银川市', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640101', '市辖区', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640104', '兴庆区', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640105', '西夏区', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640106', '金凤区', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640121', '永宁县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640122', '贺兰县', '0', null, '2017-11-05 21:06:38');
INSERT INTO `bpsaarem` VALUES ('640181', '灵武市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640200', '石嘴山市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640201', '市辖区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640202', '大武口区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640205', '惠农区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640221', '平罗县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640300', '吴忠市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640301', '市辖区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640302', '利通区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640303', '红寺堡区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640323', '盐池县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640324', '同心县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640381', '青铜峡市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640400', '固原市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640401', '市辖区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640402', '原州区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640422', '西吉县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640423', '隆德县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640424', '泾源县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640425', '彭阳县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640500', '中卫市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640501', '市辖区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640502', '沙坡头区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640521', '中宁县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('640522', '海原县', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650000', '新疆维吾尔自治区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650100', '乌鲁木齐市', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650101', '市辖区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650102', '天山区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650103', '沙依巴克区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650104', '新市区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650105', '水磨沟区', '0', null, '2017-11-05 21:06:39');
INSERT INTO `bpsaarem` VALUES ('650106', '头屯河区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650107', '达坂城区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650109', '米东区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650121', '乌鲁木齐县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650200', '克拉玛依市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650201', '市辖区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650202', '独山子区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650203', '克拉玛依区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650204', '白碱滩区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650205', '乌尔禾区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650400', '吐鲁番市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650402', '高昌区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650421', '鄯善县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650422', '托克逊县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650500', '哈密市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650502', '伊州区', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650521', '巴里坤哈萨克自治县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('650522', '伊吾县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652300', '昌吉回族自治州', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652301', '昌吉市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652302', '阜康市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652323', '呼图壁县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652324', '玛纳斯县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652325', '奇台县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652327', '吉木萨尔县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652328', '木垒哈萨克自治县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652700', '博尔塔拉蒙古自治州', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652701', '博乐市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652702', '阿拉山口市', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652722', '精河县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652723', '温泉县', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652800', '巴音郭楞蒙古自治州', '0', null, '2017-11-05 21:06:40');
INSERT INTO `bpsaarem` VALUES ('652801', '库尔勒市', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652822', '轮台县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652823', '尉犁县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652824', '若羌县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652825', '且末县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652826', '焉耆回族自治县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652827', '和静县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652828', '和硕县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652829', '博湖县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652900', '阿克苏地区', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652901', '阿克苏市', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652922', '温宿县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652923', '库车县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652924', '沙雅县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652925', '新和县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652926', '拜城县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652927', '乌什县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652928', '阿瓦提县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('652929', '柯坪县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653000', '克孜勒苏柯尔克孜自治州', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653001', '阿图什市', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653022', '阿克陶县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653023', '阿合奇县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653024', '乌恰县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653100', '喀什地区', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653101', '喀什市', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653121', '疏附县', '0', null, '2017-11-05 21:06:41');
INSERT INTO `bpsaarem` VALUES ('653122', '疏勒县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653123', '英吉沙县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653124', '泽普县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653125', '莎车县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653126', '叶城县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653127', '麦盖提县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653128', '岳普湖县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653129', '伽师县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653130', '巴楚县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653131', '塔什库尔干塔吉克自治县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653200', '和田地区', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653201', '和田市', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653221', '和田县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653222', '墨玉县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653223', '皮山县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653224', '洛浦县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653225', '策勒县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653226', '于田县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('653227', '民丰县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654000', '伊犁哈萨克自治州', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654002', '伊宁市', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654003', '奎屯市', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654004', '霍尔果斯市', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654021', '伊宁县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654022', '察布查尔锡伯自治县', '0', null, '2017-11-05 21:06:42');
INSERT INTO `bpsaarem` VALUES ('654023', '霍城县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654024', '巩留县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654025', '新源县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654026', '昭苏县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654027', '特克斯县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654028', '尼勒克县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654200', '塔城地区', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654201', '塔城市', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654202', '乌苏市', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654221', '额敏县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654223', '沙湾县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654224', '托里县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654225', '裕民县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654226', '和布克赛尔蒙古自治县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654300', '阿勒泰地区', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654301', '阿勒泰市', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654321', '布尔津县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654322', '富蕴县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654323', '福海县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654324', '哈巴河县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654325', '青河县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('654326', '吉木乃县', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('659000', '自治区直辖县级行政区划', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('659001', '石河子市', '0', null, '2017-11-05 21:06:43');
INSERT INTO `bpsaarem` VALUES ('659002', '阿拉尔市', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('659003', '图木舒克市', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('659004', '五家渠市', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('659006', '铁门关市', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('710000', '台湾省', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('810000', '香港特别行政区', '0', null, '2017-11-05 21:06:44');
INSERT INTO `bpsaarem` VALUES ('820000', '澳门特别行政区', '0', null, '2017-11-05 21:06:44');

-- ----------------------------
-- Table structure for bpsaexgh
-- ----------------------------
DROP TABLE IF EXISTS `bpsaexgh`;
CREATE TABLE `bpsaexgh` (
  `PARMTYPE` varchar(20) NOT NULL COMMENT '参数类别',
  `PARMNAME` varchar(100) NOT NULL COMMENT '参数代号',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `PARMVALUE` varchar(200) DEFAULT '' COMMENT '参数值',
  `PARMDATATYPE` varchar(20) NOT NULL DEFAULT '0' COMMENT '参数资料类型',
  `PARMDESC` varchar(120) DEFAULT '' COMMENT '参数说明',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序号',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用【0：禁用/1：启用】',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `CHANGEPASE` varchar(500) DEFAULT '' COMMENT '改变原因',
  `MARKFLAG` char(1) NOT NULL DEFAULT '0' COMMENT '审核标记[1=等待审核/2=审核通过/3=不通过]',
  `APPROVEUSER` varchar(20) DEFAULT '' COMMENT '审核人',
  `APPROVEDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
  `APPDESC` varchar(500) DEFAULT '' COMMENT '审核意见',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`PARMTYPE`,`PARMNAME`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表H表';

-- ----------------------------
-- Records of bpsaexgh
-- ----------------------------

-- ----------------------------
-- Table structure for bpsaexgm
-- ----------------------------
DROP TABLE IF EXISTS `bpsaexgm`;
CREATE TABLE `bpsaexgm` (
  `PARMTYPE` varchar(20) NOT NULL COMMENT '参数类别',
  `PARMNAME` varchar(100) NOT NULL COMMENT '参数代号',
  `PARMVALUE` varchar(200) DEFAULT '' COMMENT '参数值',
  `PARMDATATYPE` varchar(20) NOT NULL DEFAULT '0' COMMENT '参数资料类型',
  `PARMDESC` varchar(120) DEFAULT '' COMMENT '参数说明',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序号',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用【0：禁用/1：启用】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`PARMTYPE`,`PARMNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表';

-- ----------------------------
-- Records of bpsaexgm
-- ----------------------------

-- ----------------------------
-- Table structure for bpsasysh
-- ----------------------------
DROP TABLE IF EXISTS `bpsasysh`;
CREATE TABLE `bpsasysh` (
  `BCHID` varchar(20) NOT NULL COMMENT '所属机构',
  `PARAMNAME` varchar(180) NOT NULL COMMENT '参数名称',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `PARAMVALUE` varchar(400) DEFAULT '' COMMENT '参数值',
  `PARAMTYPE` int(11) NOT NULL COMMENT '参数类型',
  `PARAMDESC` varchar(180) DEFAULT '1' COMMENT '参数描述',
  `PARAMSTATUS` char(1) NOT NULL COMMENT '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]',
  `PARAMGROUP` varchar(1) DEFAULT '' COMMENT '参数所属组',
  `PARAMSQE` int(11) DEFAULT '1' COMMENT '参数组序列',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `CHANGEPASE` varchar(500) DEFAULT '' COMMENT '改变原因',
  `MARKFLAG` char(1) NOT NULL DEFAULT '0' COMMENT '审核标记[1=等待审核/2=审核通过/3=不通过]',
  `APPROVEUSER` varchar(20) DEFAULT '' COMMENT '审核人',
  `APPROVEDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
  `APPDESC` varchar(500) DEFAULT '' COMMENT '审核意见',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`BCHID`,`PARAMNAME`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表H表';

-- ----------------------------
-- Records of bpsasysh
-- ----------------------------

-- ----------------------------
-- Table structure for bpsasysm
-- ----------------------------
DROP TABLE IF EXISTS `bpsasysm`;
CREATE TABLE `bpsasysm` (
  `BCHID` varchar(20) NOT NULL COMMENT '所属机构',
  `PARAMNAME` varchar(180) NOT NULL COMMENT '参数名称',
  `PARAMVALUE` varchar(400) DEFAULT '' COMMENT '参数值',
  `PARAMTYPE` varchar(20) NOT NULL COMMENT '参数类型',
  `PARAMDESC` varchar(180) DEFAULT '' COMMENT '参数描述',
  `PARAMSTATUS` char(1) NOT NULL DEFAULT '1' COMMENT '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]',
  `PARAMGROUP` varchar(1) DEFAULT '' COMMENT '参数所属组',
  `PARAMSQE` int(11) DEFAULT NULL COMMENT '参数组序列',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`BCHID`,`PARAMNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表';

-- ----------------------------
-- Records of bpsasysm
-- ----------------------------

-- ----------------------------
-- Table structure for form_field_dict
-- ----------------------------
DROP TABLE IF EXISTS `form_field_dict`;
CREATE TABLE `form_field_dict` (
  `REFCODE` bigint(11) NOT NULL COMMENT '主键值',
  `FIELDCODE` varchar(100) NOT NULL COMMENT '属性代码',
  `FIELDNAME` varchar(100) NOT NULL COMMENT '属性名称',
  `FIELDTYPE` varchar(100) NOT NULL COMMENT '字段类型（0 字符串，1 数字，2 日期，3 日期带时间）',
  `FITTYPE` varchar(2) DEFAULT NULL COMMENT '适合的种类0 input 1 textarea  2 select 3 radio 9 其他',
  `TABLENAME` varchar(100) DEFAULT NULL COMMENT '表名称',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
  `TABLEDESC` varchar(100) DEFAULT NULL COMMENT '表说明',
  `LASTMODUSER` varchar(30) DEFAULT NULL COMMENT '最后修改人',
  `LASTMODDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`REFCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of form_field_dict
-- ----------------------------
INSERT INTO `form_field_dict` VALUES ('2', 'APLPROIDS', '申请产品', '0', '2', 'BPFAAPLM', null, '申请上产品名称', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('3', 'PAYSTAGES', '借款期限', '1', '0', 'BPFAAPLM', null, '申请上借款期限', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('4', 'APLDATE', '申请日期', '2', '0', 'BPFAAPLM', null, '申请上申请日期', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('5', 'PAYMETHOD', '还款方式', '1', '2', 'BPFAAPLM', null, '申请上还款方式', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('6', 'EXT_DATE', '期望用款时间', '2', '0', 'BPFAAPLM', null, '申请上期望用款时间', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('7', 'EXTPAY_DAY', '期望还利息日', '1', '0', 'BPFAAPLM', null, '申请上期望还利息日', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('8', 'INFOCHANNEL', '客户渠道', '2', '2', 'BPFAAPLM', null, '申请上客户渠道', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('9', 'MANAGERID', '客户经理', '0', '0', 'BPFAAPLM', null, '申请上客户经理', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('10', 'APP_AMT', '申请金额', '1', '0', 'BPFAAPLM', null, '申请上申请金额', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('11', 'CCYID', '币别', '2', '2', 'BPFAAPLM', null, '申请上的币别', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('12', 'MONTH_PAY', '月还款能力', '1', '0', 'BPFAAPLM', null, '申请上的月还款能力', null, '2017-11-27 17:02:46');
INSERT INTO `form_field_dict` VALUES ('938252674460160000', '1', '2', '3', '4', '123', null, '123', null, null);
INSERT INTO `form_field_dict` VALUES ('938288145315794944', '1', '2', '3', '4', '6', null, '5', null, null);
INSERT INTO `form_field_dict` VALUES ('939008603686309888', '1212', '1212', '11', null, null, null, null, null, null);
INSERT INTO `form_field_dict` VALUES ('939008628952797184', '1212', '121211111', '11', null, null, null, null, null, null);
INSERT INTO `form_field_dict` VALUES ('939050869872267264', '1212', '1212', '11', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for rleatctm
-- ----------------------------
DROP TABLE IF EXISTS `rleatctm`;
CREATE TABLE `rleatctm` (
  `ATCTUUID` varchar(64) NOT NULL COMMENT '主键',
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `ATCTNAME` varchar(20) NOT NULL COMMENT '附件目录名称',
  `ATCTDESC` varchar(120) DEFAULT '' COMMENT '附件目录描述',
  `PARENTATCTUUID` varchar(64) DEFAULT NULL COMMENT '上级目录UUID',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`ATCTUUID`,`SYSCODE`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件目录规则H表（附件分属模块）';

-- ----------------------------
-- Records of rleatctm
-- ----------------------------

-- ----------------------------
-- Table structure for rleatnmh
-- ----------------------------
DROP TABLE IF EXISTS `rleatnmh`;
CREATE TABLE `rleatnmh` (
  `ATNMUUID` varchar(64) NOT NULL COMMENT '主键',
  `ATCTUUID` varchar(64) NOT NULL COMMENT '附件目录UUID',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `ATCTNAME` varchar(30) NOT NULL COMMENT '附件命名名称',
  `ATCTTYPE` char(1) NOT NULL DEFAULT '0' COMMENT '是否输入/输出【0：输入/1：输出】',
  `ATCTINPUYTYPE` char(1) DEFAULT NULL COMMENT '输入规则【0：包含/1：必须一致】',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`ATNMUUID`,`ATCTUUID`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则H表';

-- ----------------------------
-- Records of rleatnmh
-- ----------------------------

-- ----------------------------
-- Table structure for rleatnmm
-- ----------------------------
DROP TABLE IF EXISTS `rleatnmm`;
CREATE TABLE `rleatnmm` (
  `ATNMUUID` varchar(64) NOT NULL COMMENT '主键',
  `ATCTUUID` varchar(64) NOT NULL COMMENT '附件目录UUID',
  `ATCTNAME` varchar(30) NOT NULL COMMENT '附件命名名称',
  `ATCTTYPE` char(1) NOT NULL DEFAULT '0' COMMENT '是否输入/输出【0：输入/1：输出】',
  `ATCTINPUYTYPE` char(1) DEFAULT NULL COMMENT '输入规则【0：包含/1：必须一致】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`ATNMUUID`,`ATCTUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则';

-- ----------------------------
-- Records of rleatnmm
-- ----------------------------

-- ----------------------------
-- Table structure for rlefildh
-- ----------------------------
DROP TABLE IF EXISTS `rlefildh`;
CREATE TABLE `rlefildh` (
  `FILDMUUID` varchar(64) NOT NULL COMMENT '主键',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `FILDCODE` varchar(30) NOT NULL COMMENT '字段代号（英文字段）',
  `FILDNAME` varchar(20) NOT NULL COMMENT '字段名称',
  `FILDDESC` varchar(120) DEFAULT '' COMMENT '字段用途描述',
  `FILDTYPE` char(1) NOT NULL DEFAULT '2' COMMENT '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】',
  `FMDLUUID` varchar(64) NOT NULL COMMENT '字段所属模块',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`FILDMUUID`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表H表';

-- ----------------------------
-- Records of rlefildh
-- ----------------------------

-- ----------------------------
-- Table structure for rlefildm
-- ----------------------------
DROP TABLE IF EXISTS `rlefildm`;
CREATE TABLE `rlefildm` (
  `FILDMUUID` varchar(64) NOT NULL COMMENT '主键',
  `FILDCODE` varchar(30) NOT NULL COMMENT '字段代号（英文字段）',
  `FILDNAME` varchar(20) NOT NULL COMMENT '字段名称',
  `FILDDESC` varchar(120) DEFAULT '' COMMENT '字段用途描述',
  `FILDTYPE` char(1) NOT NULL DEFAULT '2' COMMENT '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】',
  `FMDLUUID` varchar(64) NOT NULL COMMENT '字段所属模块',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`FILDMUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表';

-- ----------------------------
-- Records of rlefildm
-- ----------------------------

-- ----------------------------
-- Table structure for rlefmdlh
-- ----------------------------
DROP TABLE IF EXISTS `rlefmdlh`;
CREATE TABLE `rlefmdlh` (
  `FMDLUUID` varchar(64) NOT NULL COMMENT '主键',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `FMDLCODE` varchar(20) NOT NULL COMMENT '字段模块代号',
  `FMDLNAME` varchar(50) NOT NULL COMMENT '字段模块名称',
  `FMDLDESC` varchar(500) DEFAULT '' COMMENT '字段模块描述',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`FMDLUUID`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表H表';

-- ----------------------------
-- Records of rlefmdlh
-- ----------------------------

-- ----------------------------
-- Table structure for rlefmdlm
-- ----------------------------
DROP TABLE IF EXISTS `rlefmdlm`;
CREATE TABLE `rlefmdlm` (
  `FMDLUUID` varchar(64) NOT NULL COMMENT '主键',
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `FMDLCODE` varchar(20) NOT NULL COMMENT '字段模块代号',
  `FMDLNAME` varchar(50) NOT NULL COMMENT '字段模块名称',
  `FMDLDESC` varchar(500) DEFAULT '' COMMENT '字段模块描述',
  `ORDERNUM` int(11) DEFAULT '0' COMMENT '排序',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`FMDLUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表';

-- ----------------------------
-- Records of rlefmdlm
-- ----------------------------

-- ----------------------------
-- Table structure for rlepinth
-- ----------------------------
DROP TABLE IF EXISTS `rlepinth`;
CREATE TABLE `rlepinth` (
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `BHCID` varchar(20) NOT NULL COMMENT '所属机构',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `PINTCODE` varchar(100) NOT NULL COMMENT '类型代号（接口代号）',
  `PINTNAME` varchar(100) DEFAULT '' COMMENT '类型名称',
  `PINTDESC` varchar(500) DEFAULT '' COMMENT '类型描述',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`SYSCODE`,`BHCID`,`SEQUUID`,`PINTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）H表';

-- ----------------------------
-- Records of rlepinth
-- ----------------------------

-- ----------------------------
-- Table structure for rlepintm
-- ----------------------------
DROP TABLE IF EXISTS `rlepintm`;
CREATE TABLE `rlepintm` (
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `BHCID` varchar(20) NOT NULL COMMENT '所属机构',
  `PINTCODE` varchar(100) NOT NULL COMMENT '类型代号（接口代号）',
  `PINTNAME` varchar(100) DEFAULT '' COMMENT '类型名称',
  `PINTDESC` varchar(500) DEFAULT '' COMMENT '类型描述',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`SYSCODE`,`BHCID`,`PINTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）';

-- ----------------------------
-- Records of rlepintm
-- ----------------------------

-- ----------------------------
-- Table structure for safasysh
-- ----------------------------
DROP TABLE IF EXISTS `safasysh`;
CREATE TABLE `safasysh` (
  `SYSUUID` varchar(64) NOT NULL COMMENT '系统标识',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `SYSNAME` varchar(20) NOT NULL COMMENT '系统名称',
  `SYSFULLNAME` varchar(50) DEFAULT '' COMMENT '系统全称',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `CHANGEPASE` varchar(500) DEFAULT '' COMMENT '改变原因',
  `MARKFLAG` char(1) NOT NULL DEFAULT '0' COMMENT '审核标记[1=等待审核/2=审核通过/3=不通过]',
  `APPROVEUSER` varchar(20) DEFAULT '' COMMENT '审核人',
  `APPROVEDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
  `APPDESC` varchar(500) DEFAULT '' COMMENT '审核意见',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`SYSUUID`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息H表';

-- ----------------------------
-- Records of safasysh
-- ----------------------------

-- ----------------------------
-- Table structure for safasysm
-- ----------------------------
DROP TABLE IF EXISTS `safasysm`;
CREATE TABLE `safasysm` (
  `SYSUUID` varchar(64) NOT NULL COMMENT '系统标识',
  `SYSCODE` varchar(20) NOT NULL COMMENT '系统代号',
  `SYSNAME` varchar(20) NOT NULL COMMENT '系统名称',
  `SYSFULLNAME` varchar(50) DEFAULT '' COMMENT '系统全称',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`SYSUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息';

-- ----------------------------
-- Records of safasysm
-- ----------------------------

-- ----------------------------
-- Table structure for safrbchh
-- ----------------------------
DROP TABLE IF EXISTS `safrbchh`;
CREATE TABLE `safrbchh` (
  `BCHID` varchar(20) NOT NULL COMMENT '机构代号',
  `SEQUUID` varchar(64) NOT NULL COMMENT '历史序列',
  `BCHDESC` varchar(30) NOT NULL COMMENT '机构名称',
  `BCHFULLDESC` varchar(120) DEFAULT '' COMMENT '机构全称',
  `AREACODE` varchar(20) NOT NULL COMMENT '机构所在区域代码',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `EDTID` char(3) NOT NULL COMMENT '改变动作【ADD，MOD，DEL】',
  `CHANGEPASE` varchar(500) DEFAULT '' COMMENT '改变原因',
  `MARKFLAG` char(1) NOT NULL DEFAULT '0' COMMENT '审核标记[1=等待审核/2=审核通过/3=不通过]',
  `APPROVEUSER` varchar(20) DEFAULT '' COMMENT '审核人',
  `APPROVEDATE` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
  `APPDESC` varchar(500) DEFAULT '' COMMENT '审核意见',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`BCHID`,`SEQUUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息H表';

-- ----------------------------
-- Records of safrbchh
-- ----------------------------

-- ----------------------------
-- Table structure for safrbchm
-- ----------------------------
DROP TABLE IF EXISTS `safrbchm`;
CREATE TABLE `safrbchm` (
  `BCHID` varchar(20) NOT NULL COMMENT '机构代号',
  `BCHDESC` varchar(30) NOT NULL COMMENT '机构名称',
  `BCHFULLDESC` varchar(120) DEFAULT '' COMMENT '机构全称',
  `AREACODE` varchar(20) NOT NULL COMMENT '机构所在区域代码',
  `ISDISBLE` char(1) NOT NULL DEFAULT '1' COMMENT '状态【0：禁用/1：启用】',
  `LASTMODUSER` varchar(20) DEFAULT '' COMMENT '操作者',
  `LASTMODDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`BCHID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息';

-- ----------------------------
-- Records of safrbchm
-- ----------------------------
INSERT INTO `safrbchm` VALUES ('1', 'AS', 'ADAS', '1', '1', '王来利', '2017-12-08 16:14:44');
INSERT INTO `safrbchm` VALUES ('2', 'AS', 'QWEW', '1', '1', '王来利', '2017-12-08 16:14:44');
INSERT INTO `safrbchm` VALUES ('939759333661609984', 'ASASQ', 'WWW', 'qwe123', '1', '', '2017-12-10 15:36:22');
INSERT INTO `safrbchm` VALUES ('939766413227331584', 'DDDDDDDDD', 'AAAAAAAA', 'AAAAAAAAA', '0', '', '2017-12-10 15:59:05');
INSERT INTO `safrbchm` VALUES ('939767217258631168', 'DDDDDDDDD', 'AAAAAAAA', 'AAAAAAAAA', '0', '13688045624', '2017-12-10 16:02:17');
