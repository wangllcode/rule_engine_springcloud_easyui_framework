/*--Excel Generate SQL Script
--for Mysql 
--==========2017/12/08 10:53:45==========
--==========20==========*/
DROP TABLE IF EXISTS SAFRBCHM; 
CREATE TABLE SAFRBCHM  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '机构代号' , 
      `BCHDESC`  VARCHAR(30)  NOT NULL comment '机构名称' , 
      `BCHFULLDESC`  VARCHAR(120) DEFAULT ''  comment '机构全称' , 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '机构所在区域代码' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息' ;



DROP TABLE IF EXISTS SAFRBCHH; 
CREATE TABLE SAFRBCHH  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '机构代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `BCHDESC`  VARCHAR(30)  NOT NULL comment '机构名称' , 
      `BCHFULLDESC`  VARCHAR(120) DEFAULT ''  comment '机构全称' , 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '机构所在区域代码' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分机构信息H表' ;



DROP TABLE IF EXISTS SAFASYSM; 
CREATE TABLE SAFASYSM  ( 
      `SYSUUID`  VARCHAR(64)  NOT NULL comment '系统标识' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SYSNAME`  VARCHAR(20)  NOT NULL comment '系统名称' , 
      `SYSFULLNAME`  VARCHAR(50) DEFAULT ''  comment '系统全称' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `SYSUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息' ;



DROP TABLE IF EXISTS SAFASYSH; 
CREATE TABLE SAFASYSH  ( 
      `SYSUUID`  VARCHAR(64)  NOT NULL comment '系统标识' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SYSNAME`  VARCHAR(20)  NOT NULL comment '系统名称' , 
      `SYSFULLNAME`  VARCHAR(50) DEFAULT ''  comment '系统全称' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `SYSUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息H表' ;



DROP TABLE IF EXISTS BPSAAREM; 
CREATE TABLE BPSAAREM  ( 
      `AREACODE`  VARCHAR(20)  NOT NULL comment '地区代号' , 
      `AREADESC`  VARCHAR(120)  NOT NULL comment '地区描述' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用[0-否,1-是]' , 
      `LASTMODUSER`  VARCHAR(20)   comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `AREACODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区信息表' ;



DROP TABLE IF EXISTS BPSASYSM; 
CREATE TABLE BPSASYSM  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PARAMNAME`  VARCHAR(180)  NOT NULL comment '参数名称' , 
      `PARAMVALUE`  VARCHAR(400) DEFAULT ''  comment '参数值' , 
      `PARAMTYPE`  VARCHAR(20)  NOT NULL comment '参数类型' , 
      `PARAMDESC`  VARCHAR(180) DEFAULT ''  comment '参数描述' , 
      `PARAMSTATUS`  CHAR(1) DEFAULT '1' NOT NULL comment '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]' , 
      `PARAMGROUP`  VARCHAR(1) DEFAULT ''  comment '参数所属组' , 
      `PARAMSQE`  INT   comment '参数组序列' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `PARAMNAME` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表' ;



DROP TABLE IF EXISTS BPSASYSH; 
CREATE TABLE BPSASYSH  ( 
      `BCHID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PARAMNAME`  VARCHAR(180)  NOT NULL comment '参数名称' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PARAMVALUE`  VARCHAR(400) DEFAULT ''  comment '参数值' , 
      `PARAMTYPE`  INT  NOT NULL comment '参数类型' , 
      `PARAMDESC`  VARCHAR(180) DEFAULT '1'  comment '参数描述' , 
      `PARAMSTATUS`  CHAR(1)  NOT NULL comment '参数状态[1=页面显示且可修改(default)/2=页面显示且不可修改/3=页面不显示仅能在数据库修改]' , 
      `PARAMGROUP`  VARCHAR(1) DEFAULT ''  comment '参数所属组' , 
      `PARAMSQE`  INT DEFAULT 1  comment '参数组序列' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '状态【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `BCHID`  ,  `PARAMNAME`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表H表' ;



DROP TABLE IF EXISTS BPSAEXGM; 
CREATE TABLE BPSAEXGM  ( 
      `PARMTYPE`  VARCHAR(20)  NOT NULL comment '参数类别' , 
      `PARMNAME`  VARCHAR(100)  NOT NULL comment '参数代号' , 
      `PARMVALUE`  VARCHAR(200) DEFAULT ''  comment '参数值' , 
      `PARMDATATYPE`  VARCHAR(20) DEFAULT '0' NOT NULL comment '参数资料类型' , 
      `PARMDESC`  VARCHAR(120) DEFAULT ''  comment '参数说明' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序号' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用【0：禁用/1：启用】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `PARMTYPE`  ,  `PARMNAME` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表' ;



DROP TABLE IF EXISTS BPSAEXGH; 
CREATE TABLE BPSAEXGH  ( 
      `PARMTYPE`  VARCHAR(20)  NOT NULL comment '参数类别' , 
      `PARMNAME`  VARCHAR(100)  NOT NULL comment '参数代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PARMVALUE`  VARCHAR(200) DEFAULT ''  comment '参数值' , 
      `PARMDATATYPE`  VARCHAR(20) DEFAULT '0' NOT NULL comment '参数资料类型' , 
      `PARMDESC`  VARCHAR(120) DEFAULT ''  comment '参数说明' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序号' , 
      `ISDISBLE`  CHAR(1) DEFAULT '1' NOT NULL comment '是否启用【0：禁用/1：启用】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `CHANGEPASE`  VARCHAR(500) DEFAULT ''  comment '改变原因' , 
      `MARKFLAG`  CHAR(1) DEFAULT '0' NOT NULL comment '审核标记[1=等待审核/2=审核通过/3=不通过]' , 
      `APPROVEUSER`  VARCHAR(20) DEFAULT ''  comment '审核人' , 
      `APPROVEDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '审核时间' , 
      `APPDESC`  VARCHAR(500) DEFAULT ''  comment '审核意见' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `PARMTYPE`  ,  `PARMNAME`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表H表' ;



DROP TABLE IF EXISTS RLEFMDLM; 
CREATE TABLE RLEFMDLM  ( 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `FMDLCODE`  VARCHAR(20)  NOT NULL comment '字段模块代号' , 
      `FMDLNAME`  VARCHAR(50)  NOT NULL comment '字段模块名称' , 
      `FMDLDESC`  VARCHAR(500) DEFAULT ''  comment '字段模块描述' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FMDLUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表' ;



DROP TABLE IF EXISTS RLEFMDLH; 
CREATE TABLE RLEFMDLH  ( 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `FMDLCODE`  VARCHAR(20)  NOT NULL comment '字段模块代号' , 
      `FMDLNAME`  VARCHAR(50)  NOT NULL comment '字段模块名称' , 
      `FMDLDESC`  VARCHAR(500) DEFAULT ''  comment '字段模块描述' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FMDLUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段模块分类表H表' ;



DROP TABLE IF EXISTS RLEFILDM; 
CREATE TABLE RLEFILDM  ( 
      `FILDMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `FILDCODE`  VARCHAR(30)  NOT NULL comment '字段代号（英文字段）' , 
      `FILDNAME`  VARCHAR(20)  NOT NULL comment '字段名称' , 
      `FILDDESC`  VARCHAR(120) DEFAULT ''  comment '字段用途描述' , 
      `FILDTYPE`  CHAR(1) DEFAULT '2' NOT NULL comment '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】' , 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '字段所属模块' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FILDMUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表' ;



DROP TABLE IF EXISTS RLEFILDH; 
CREATE TABLE RLEFILDH  ( 
      `FILDMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `FILDCODE`  VARCHAR(30)  NOT NULL comment '字段代号（英文字段）' , 
      `FILDNAME`  VARCHAR(20)  NOT NULL comment '字段名称' , 
      `FILDDESC`  VARCHAR(120) DEFAULT ''  comment '字段用途描述' , 
      `FILDTYPE`  CHAR(1) DEFAULT '2' NOT NULL comment '字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】' , 
      `FMDLUUID`  VARCHAR(64)  NOT NULL comment '字段所属模块' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `FILDMUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段表H表' ;



DROP TABLE IF EXISTS RLEATCTM; 
CREATE TABLE RLEATCTM  ( 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `ATCTNAME`  VARCHAR(20)  NOT NULL comment '附件目录名称' , 
      `ATCTDESC`  VARCHAR(120) DEFAULT ''  comment '附件目录描述' , 
      `PARENTATCTUUID`  VARCHAR(64)   comment '上级目录UUID' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATCTUUID`  ,  `SYSCODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件目录规则（附件分属模块）' ;



DROP TABLE IF EXISTS RLEATCTM; 
CREATE TABLE RLEATCTM  ( 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `ATCTNAME`  VARCHAR(20)  NOT NULL comment '附件目录名称' , 
      `ATCTDESC`  VARCHAR(120) DEFAULT ''  comment '附件目录描述' , 
      `PARENTATCTUUID`  VARCHAR(64)   comment '上级目录UUID' , 
      `ORDERNUM`  INT DEFAULT 0  comment '排序' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATCTUUID`  ,  `SYSCODE`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件目录规则H表（附件分属模块）' ;



DROP TABLE IF EXISTS RLEATNMM; 
CREATE TABLE RLEATNMM  ( 
      `ATNMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '附件目录UUID' , 
      `ATCTNAME`  VARCHAR(30)  NOT NULL comment '附件命名名称' , 
      `ATCTTYPE`  CHAR(1) DEFAULT '0' NOT NULL comment '是否输入/输出【0：输入/1：输出】' , 
      `ATCTINPUYTYPE`  CHAR(1)   comment '输入规则【0：包含/1：必须一致】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATNMUUID`  ,  `ATCTUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则' ;



DROP TABLE IF EXISTS RLEATNMH; 
CREATE TABLE RLEATNMH  ( 
      `ATNMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `ATCTUUID`  VARCHAR(64)  NOT NULL comment '附件目录UUID' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `ATCTNAME`  VARCHAR(30)  NOT NULL comment '附件命名名称' , 
      `ATCTTYPE`  CHAR(1) DEFAULT '0' NOT NULL comment '是否输入/输出【0：输入/1：输出】' , 
      `ATCTINPUYTYPE`  CHAR(1)   comment '输入规则【0：包含/1：必须一致】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '操作时间' , 
 PRIMARY KEY ( `ATNMUUID`  ,  `ATCTUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件命名规则H表' ;


DROP TABLE IF EXISTS RLEPINTM; 
CREATE TABLE RLEPINTM  ( 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `BHCID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PROID`  VARCHAR(20)  NOT NULL comment '所属产品' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `PINTUUID`  VARCHAR(64)  NOT NULL comment '序列' , 
      `PINTNAME`  VARCHAR(100) DEFAULT ''  comment '类型名称' , 
      `PINTDESC`  VARCHAR(500) DEFAULT ''  comment '类型描述' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `SYSCODE`  ,  `BHCID`  ,  `PROID`  ,  `PINTCODE` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）' ;



DROP TABLE IF EXISTS RLEPINTH; 
CREATE TABLE RLEPINTH  ( 
      `SYSCODE`  VARCHAR(20)  NOT NULL comment '系统代号' , 
      `BHCID`  VARCHAR(20)  NOT NULL comment '所属机构' , 
      `PROID`  VARCHAR(20)  NOT NULL comment '所属产品' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PINTNAME`  VARCHAR(100) DEFAULT ''  comment '类型名称' , 
      `PINTDESC`  VARCHAR(500) DEFAULT ''  comment '类型描述' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `SYSCODE`  ,  `BHCID`  ,  `PROID`  ,  `PINTCODE`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则类型用途（接口）H表' ;



DROP TABLE IF EXISTS RLEATFAM; 
CREATE TABLE RLEATFAM  ( 
      `ATFAUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `PINTUUID`  VARCHAR(100)  NOT NULL comment '规则类型用途UUID' , 
      `FILDMUUID`  VARCHAR(64) DEFAULT '' NOT NULL comment '字段主键序列' , 
      `FILDMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '字段历史序列' , 
      `FILDCODE`  VARCHAR(30) DEFAULT '' NOT NULL comment '字段代号' , 
      `ATCTUUID`  VARCHAR(64) DEFAULT '' NOT NULL comment '附件命名主键序列' , 
      `ATCTSEQNOUUID`  VARCHAR(64)  NOT NULL comment '附件命名历史序列' , 
      `ATTISEXISTS`  CHAR(1) DEFAULT '0' NOT NULL comment '必须存在【0：否/1：是】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `ATFAUUID`  ,  `PINTUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段与附件名称关联' ;



DROP TABLE IF EXISTS RLEATFAH; 
CREATE TABLE RLEATFAH  ( 
      `ATFAUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `PINTUUID`  VARCHAR(100)  NOT NULL comment '规则类型用途UUID' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `FILDMUUID`  VARCHAR(64) DEFAULT '' NOT NULL comment '字段主键序列' , 
      `FILDMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '字段历史序列' , 
      `FILDCODE`  VARCHAR(30) DEFAULT '' NOT NULL comment '字段代号' , 
      `ATCTUUID`  VARCHAR(64) DEFAULT '' NOT NULL comment '附件命名主键序列' , 
      `ATCTSEQNOUUID`  VARCHAR(64)  NOT NULL comment '附件命名历史序列' , 
      `ATTISEXISTS`  CHAR(1) DEFAULT '0' NOT NULL comment '必须存在【0：否/1：是】' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `ATFAUUID`  ,  `PINTUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则字段与附件名称关联' ;



DROP TABLE IF EXISTS RLESTTMM; 
CREATE TABLE RLESTTMM  ( 
      `STTMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `PINTUUID`  VARCHAR(64)  NOT NULL comment '规则类型序列' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `STTMNAME`  VARCHAR(100) DEFAULT ''  comment '字段业务规则模板名称' , 
      `RLEFILDMUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号UUID' , 
      `RLEFILDMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号历史序列UUID' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `STTMUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务规则设置模板表' ;



DROP TABLE IF EXISTS RLESTTMH; 
CREATE TABLE RLESTTMH  ( 
      `STTMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '历史序列' , 
      `PINTUUID`  VARCHAR(64)  NOT NULL comment '规则类型序列' , 
      `PINTCODE`  VARCHAR(100)  NOT NULL comment '类型代号（接口代号）' , 
      `STTMNAME`  VARCHAR(100) DEFAULT ''  comment '字段业务规则模板名称' , 
      `RLEFILDMUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号UUID' , 
      `RLEFILDMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号历史序列UUID' , 
      `EDTID`  CHAR(3)  NOT NULL comment '改变动作【ADD，MOD，DEL】' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `STTMUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务规则设置模板表' ;



DROP TABLE IF EXISTS RLESTSUM; 
CREATE TABLE RLESTSUM  ( 
      `STSUUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `STTMUUID`  VARCHAR(64)  NOT NULL comment '业务规则设置主键' , 
      `RLEOPREATYPE`  VARCHAR(10)  NOT NULL comment '规则操作【见字典表type：RLEOPREATYPE】' , 
      `OPREATARGET`  VARCHAR(4)  NOT NULL comment '操作来源【0：字段参数/1：字典表/3：数字/4：固定字符】' , 
      `TGTFILDMUUID`  VARCHAR(64)   comment '目标字段代号UUID，操作来源为0时不为空' , 
      `AEXGTYPE`  VARCHAR(20)   comment '字典表类型，操作来源为1时不为空' , 
      `AEXGVALUE`  VARCHAR(100)   comment '字典表值，可保存多个，用英文逗号分隔，操作来源为1时不为空' , 
      `NUMVALUE1`  DECIMAL(16,6)   comment '数字1（范围数字使用，最小值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE2`  DECIMAL(16,6)   comment '数字2（范围数字使用，最大值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE3`  DECIMAL(16,6)   comment '数字3，操作来源为3时不为空' , 
      `NUMVALUE4`  DECIMAL(16,6)   comment '数字4，备用' , 
      `NUMVALUE5`  DECIMAL(16,6)   comment '数字5，备用' , 
      `NUMVALUE6`  DECIMAL(16,6)   comment '数字6，备用' , 
      `NUMVALUE7`  DECIMAL(16,6)   comment '数字7，备用' , 
      `NUMVALUE8`  DECIMAL(16,6)   comment '数字8，备用' , 
      `NUMVALUE9`  DECIMAL(16,6)   comment '数字9，备用' , 
      `NUMVALUE10`  DECIMAL(16,6)   comment '数字10，备用' , 
      `STRVALUE1`  VARCHAR(100)   comment '固定字符1，操作来源为4时使用' , 
      `STRVALUE2`  VARCHAR(100)   comment '固定字符2，操作来源为4时使用' , 
      `STRVALUE3`  VARCHAR(100)   comment '固定字符3，备用' , 
      `STRVALUE4`  VARCHAR(100)   comment '固定字符4，备用' , 
      `STRVALUE5`  VARCHAR(100)   comment '固定字符5，备用' , 
      `STRVALUE6`  VARCHAR(100)   comment '固定字符6，备用' , 
      `STRVALUE7`  VARCHAR(100)   comment '固定字符7，备用' , 
      `STRVALUE8`  VARCHAR(100)   comment '固定字符8，备用' , 
      `STRVALUE9`  VARCHAR(100)   comment '固定字符9，备用' , 
      `STRVALUE10`  VARCHAR(100)   comment '固定字符10，备用' , 
      `ISOUT`  CHAR(1)   comment '规则输出类型【0：无/1：拒绝/2：提示】
不满足规则的情况下使用' , 
      `OUTSTYPE`  CHAR(1)   comment '输出源类型【1：附件/2：描述】' , 
      `OUTSDESC`  TEXT   comment '输出源描述' , 
      `OUTSATTUUID`  TEXT   comment '输出源附件UUID，可保存多个，用英文逗号分隔' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `STSUUUID`  ,  `STTMUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务规则设置模板表' ;



DROP TABLE IF EXISTS RLESTSUH; 
CREATE TABLE RLESTSUH  ( 
      `STSUUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `STTMUUID`  VARCHAR(64)  NOT NULL comment '业务规则设置主键' , 
      `SEQUUID`  VARCHAR(64)  NOT NULL comment '业务规则设置历史序列' , 
      `RLEOPREATYPE`  VARCHAR(10)  NOT NULL comment '规则操作【见字典表type：RLEOPREATYPE】' , 
      `OPREATARGET`  VARCHAR(4)  NOT NULL comment '操作来源【0：字段参数/1：字典表/3：数字/4：固定字符】' , 
      `TGTFILDMUUID`  VARCHAR(64)   comment '目标字段代号UUID，操作来源为0时不为空' , 
      `AEXGTYPE`  VARCHAR(20)   comment '字典表类型，操作来源为1时不为空' , 
      `AEXGVALUE`  VARCHAR(100)   comment '字典表值，可保存多个，用英文逗号分隔，操作来源为1时不为空' , 
      `NUMVALUE1`  DECIMAL(16,6)   comment '数字1（范围数字使用，最小值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE2`  DECIMAL(16,6)   comment '数字2（范围数字使用，最大值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE3`  DECIMAL(16,6)   comment '数字3，操作来源为3时不为空' , 
      `NUMVALUE4`  DECIMAL(16,6)   comment '数字4，备用' , 
      `NUMVALUE5`  DECIMAL(16,6)   comment '数字5，备用' , 
      `NUMVALUE6`  DECIMAL(16,6)   comment '数字6，备用' , 
      `NUMVALUE7`  DECIMAL(16,6)   comment '数字7，备用' , 
      `NUMVALUE8`  DECIMAL(16,6)   comment '数字8，备用' , 
      `NUMVALUE9`  DECIMAL(16,6)   comment '数字9，备用' , 
      `NUMVALUE10`  DECIMAL(16,6)   comment '数字10，备用' , 
      `STRVALUE1`  VARCHAR(100)   comment '固定字符1，操作来源为4时使用' , 
      `STRVALUE2`  VARCHAR(100)   comment '固定字符2，操作来源为4时使用' , 
      `STRVALUE3`  VARCHAR(100)   comment '固定字符3，备用' , 
      `STRVALUE4`  VARCHAR(100)   comment '固定字符4，备用' , 
      `STRVALUE5`  VARCHAR(100)   comment '固定字符5，备用' , 
      `STRVALUE6`  VARCHAR(100)   comment '固定字符6，备用' , 
      `STRVALUE7`  VARCHAR(100)   comment '固定字符7，备用' , 
      `STRVALUE8`  VARCHAR(100)   comment '固定字符8，备用' , 
      `STRVALUE9`  VARCHAR(100)   comment '固定字符9，备用' , 
      `STRVALUE10`  VARCHAR(100)   comment '固定字符10，备用' , 
      `ISOUT`  CHAR(1)   comment '规则输出类型【0：无/1：拒绝/2：提示】
不满足规则的情况下使用' , 
      `OUTSTYPE`  CHAR(1)   comment '输出源类型【1：附件/2：描述】' , 
      `OUTSDESC`  TEXT   comment '输出源描述' , 
      `OUTSATTUUID`  TEXT   comment '输出源附件UUID，可保存多个，用英文逗号分隔' , 
      `LASTMODUSER`  VARCHAR(20) DEFAULT ''  comment '操作者' , 
      `LASTMODDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP   comment '操作时间' , 
 PRIMARY KEY ( `STSUUUID`  ,  `STTMUUID`  ,  `SEQUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务规则设置模板表' ;



DROP TABLE IF EXISTS USEPINTM; 
CREATE TABLE USEPINTM  ( 
      `USEPINTUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `PINTUUID`  VARCHAR(64)  NOT NULL comment '规则类型用途序列' , 
      `PINTSEQNOUUID`  VARCHAR(64)  NOT NULL comment '规则类型用途历史表序列' , 
      `BUSSNO`  VARCHAR(100)  NOT NULL comment '业务标识号' , 
      `UNIQUENO`  VARCHAR(30)  NOT NULL comment '请求唯一标识号' , 
      `BUSSDATE`  TIMESTAMP   comment '业务时间（根据系统标识，判断是否根据业务时间查找历史规则）' , 
      `REQSTATE`  CHAR(1)  NOT NULL comment '请求状态【0：异常/1：成功/2：处理中】' , 
      `REQCONTENT`  TEXT  NOT NULL comment '请求内容' , 
      `REQDATE`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL comment '请求时间' , 
      `DEALSTATE`  CHAR(1)  NOT NULL comment '处理状态【0：处理失败/1：处理成功】' , 
      `DEALERRCONTENT`  TEXT  NOT NULL comment '处理异常内容' , 
      `RECCONTENT`  TEXT  NOT NULL comment '回执内容' , 
      `RECSTATE`  CHAR(1)  NOT NULL comment '回执接收状态【0：回执失败/1：回执成功】' , 
 PRIMARY KEY ( `USEPINTUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则调用记录主表' ;



DROP TABLE IF EXISTS USEATFAM; 
CREATE TABLE USEATFAM  ( 
      `USEATFAUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `USEPINTUUID`  VARCHAR(64)  NOT NULL comment '规则调用记录主键' , 
      `ATFAUUID`  VARCHAR(64)  NOT NULL comment '规则字段与附件名称关联主键' , 
      `ATFASEQNOUUID`  VARCHAR(64)  NOT NULL comment '规则字段与附件名称历史序列' , 
      `FILDCODE`  VARCHAR(30)  NOT NULL comment '字段代号' , 
      `ATCTNAME`  VARCHAR(100)  NOT NULL comment '规则附件命名' , 
      `USEATTNAME`  VARCHAR(100)   comment '入参附件名称' , 
      `ATTISEXISTS`  CHAR(1)  NOT NULL comment '必须存在【0：否/1：是】' , 
      `ISEXISTS`  CHAR(1)  NOT NULL comment '是否存在【0：否/1：是】' , 
 PRIMARY KEY ( `USEATFAUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件规则记录表' ;



DROP TABLE IF EXISTS USESTTMM; 
CREATE TABLE USESTTMM  ( 
      `USESTTMUUID`  VARCHAR(64)  NOT NULL comment '主键' , 
      `USEPINTUUID`  VARCHAR(64)  NOT NULL comment '规则调用记录主键' , 
      `STTMUUID`  VARCHAR(64)  NOT NULL comment '业务规则设置主键' , 
      `STTMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '业务规则设置历史序列' , 
      `STTMNAME`  VARCHAR(100)   comment '字段业务规则模板名称' , 
      `RLEFILDMUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号UUID' , 
      `RLEFILDMSEQNOUUID`  VARCHAR(64)  NOT NULL comment '规则字段代号历史序列UUID' , 
      `OPREATARGET`  VARCHAR(4)  NOT NULL comment '操作来源【0：字段参数/1：字典表/3：数字/4：固定字符】' , 
      `TGTFILDMUUID`  VARCHAR(64)   comment '目标字段代号UUID，操作来源为0时不为空' , 
      `AEXGTYPE`  VARCHAR(20)   comment '字典表类型，操作来源为1时不为空' , 
      `AEXGVALUE`  VARCHAR(100)   comment '字典表值，可保存多个，用英文逗号分隔，操作来源为1时不为空' , 
      `NUMVALUE1`  DECIMAL(16,6)   comment '数字1（范围数字使用，最小值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE2`  DECIMAL(16,6)   comment '数字2（范围数字使用，最大值）规则来源是数字且规则操作是范围使用' , 
      `NUMVALUE3`  DECIMAL(16,6)   comment '数字3，操作来源为3时不为空' , 
      `NUMVALUE4`  DECIMAL(16,6)   comment '数字4，备用' , 
      `NUMVALUE5`  DECIMAL(16,6)   comment '数字5，备用' , 
      `NUMVALUE6`  DECIMAL(16,6)   comment '数字6，备用' , 
      `NUMVALUE7`  DECIMAL(16,6)   comment '数字7，备用' , 
      `NUMVALUE8`  DECIMAL(16,6)   comment '数字8，备用' , 
      `NUMVALUE9`  DECIMAL(16,6)   comment '数字9，备用' , 
      `NUMVALUE10`  DECIMAL(16,6)   comment '数字10，备用' , 
      `STRVALUE1`  VARCHAR(100)   comment '固定字符1，操作来源为4时使用' , 
      `STRVALUE2`  VARCHAR(100)   comment '固定字符2，操作来源为4时使用' , 
      `STRVALUE3`  VARCHAR(100)   comment '固定字符3，备用' , 
      `STRVALUE4`  VARCHAR(100)   comment '固定字符4，备用' , 
      `STRVALUE5`  VARCHAR(100)   comment '固定字符5，备用' , 
      `STRVALUE6`  VARCHAR(100)   comment '固定字符6，备用' , 
      `STRVALUE7`  VARCHAR(100)   comment '固定字符7，备用' , 
      `STRVALUE8`  VARCHAR(100)   comment '固定字符8，备用' , 
      `STRVALUE9`  VARCHAR(100)   comment '固定字符9，备用' , 
      `STRVALUE10`  VARCHAR(100)   comment '固定字符10，备用' , 
      `ISOUT`  CHAR(1)   comment '规则输出类型【0：无/1：拒绝/2：提示】
不满足规则的情况下使用' , 
      `OUTSTYPE`  CHAR(1)   comment '输出源类型【1：附件/2：描述】' , 
      `OUTSDESC`  TEXT   comment '输出源描述' , 
      `OUTSATTUUID`  TEXT   comment '输出源附件UUID，可保存多个，用英文逗号分隔' , 
 PRIMARY KEY ( `USESTTMUUID`  ,  `USEPINTUUID` ) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务规则设置记录表' ;

