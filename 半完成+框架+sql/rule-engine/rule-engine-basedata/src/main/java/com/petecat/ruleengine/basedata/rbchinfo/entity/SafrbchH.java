package com.petecat.ruleengine.basedata.rbchinfo.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class SafrbchH implements Serializable {
	/**
	 * 序列化ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 机构代号
	 */
	private String bchId;
	
	/**
	 * 历史序列
	 */
	private String sequuId;
	
	/**
	 * 机构名称
	 */
	private String bchDesc;
	
	/**
	 * 机构全称
	 */
	private String bchFullDesc;
	
	/**
	 * 机构所在区域代码
	 */
	private String areaCode;
	
	/**
	 * 状态【0:禁用/1:启用】
	 */
	private String isDisble;
	
	/**
	 * 改变动作【Add,Mod,Del】
	 */
	private String edtId;
	
	/**
	 * 改变原因
	 */
	private String changePase;
	
	/**
	 * 审核标记【1=等待审核/2=审核通过/3=不通过】
	 */
	private String markFlag;
	
	/**
	 * 审核人
	 */
	private String appRoveUser;
	
	/**
	 * 审核时间
	 */
	private String appRoveDate;
	
	/**
	 * 审核意见
	 */
	private String appDesc;
	
	/**
	 * 最后操作人
	 */
	private String lastModUser;
	
	/**
	 * 最后操作时间
	 */
	private String lastModDate;
	
}
