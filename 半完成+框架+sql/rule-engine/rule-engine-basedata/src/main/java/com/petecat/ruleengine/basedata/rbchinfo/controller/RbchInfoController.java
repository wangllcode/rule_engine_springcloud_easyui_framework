package com.petecat.ruleengine.basedata.rbchinfo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basedata.rbchinfo.service.IRbchInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDelDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.vo.SafrbchVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

/**
 * 分机构信息表操作Controller
 * 
 * @author
 */
@RestController
@RequestMapping("/rbchinfo")
@Api(tags="分机构信息管理")
public class RbchInfoController extends BaseController {

	@Autowired
	private IRbchInfoService rbchInfoService;

	/**
	 * 
	 * @Title: queryRbchByPage
	 * @Description: 分页查询分机构信息列表
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<PageVO<SafrbchVO>> 返回类型
	 * @throws
	 */
	@GetMapping("/queryRbchByPage")
	@ApiOperation(value="分页查询分机构信息列表")
	public Result<PageVO<SafrbchVO>> queryRbchByPage(SafrbchMDTO safrbchMDTO) {
		PageVO<SafrbchVO> rbchList = rbchInfoService.listRbchByPage(safrbchMDTO);
		return Result.success(rbchList);
	}

	/**
	 * 
	 * @Title: saveRbch
	 * @Description: 新增分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/saveRbch")
	@ApiOperation(value="新增分机构信息")
	public Result<?> saveRbch(@RequestBody SafrbchMDTO safrbchMDTO) {
		this.setLastUserName(safrbchMDTO);
		rbchInfoService.saveRbch(safrbchMDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: getRbchById
	 * @Description: 通过机构ID查询分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<SafrbchVO> 返回类型
	 * @throws
	 */
	@GetMapping("/getRbchById")
	@ApiImplicitParams(value={@ApiImplicitParam(name="bchId",value="机构代号",paramType="query",dataType="string")})
	@ApiOperation(value="通过机构ID查询分机构信息")
	public Result<SafrbchVO> getRbchById(String bchId) {
		SafrbchVO safrBchVo = rbchInfoService.getRbchById(bchId);
		return Result.success(safrBchVo);
	}

	/**
	 * 
	 * @Title: removeRbchById
	 * @Description: 修改机构状态为禁用
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@DeleteMapping("/removeRbchById")
	@ApiOperation(value="修改机构状态为禁用")
	public Result<?> removeRbchById(@ModelAttribute SafrbchMDelDTO safrbchMDelDTO) {
		String userId = this.getCurrentUser().getUserId();
		safrbchMDelDTO.setLastModUser(userId);
		safrbchMDelDTO.setLastModDate(DatesUtils.getStringDate());
		rbchInfoService.removeRbchById(safrbchMDelDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: updateRbch
	 * @Description: 修改分机构信息
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/updateRbch")
	@ApiOperation(value="修改分机构信息")
	public Result<?> updateRbch(@RequestBody SafrbchMDTO safrbchMDTO) {
		this.setLastUserName(safrbchMDTO);
		rbchInfoService.updateRbch(safrbchMDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(SafrbchMDTO safrbchMDTO) {
		String userId = this.getCurrentUser().getUserId();
		safrbchMDTO.setLastModUser(userId);
		safrbchMDTO.setLastModDate(DatesUtils.getStringDate());
	}
	@RequestMapping("/queryRbch")
	public Result<List<SafrbchVO>> queryRbch(){
		List<SafrbchVO> safrBchVo = rbchInfoService.queryRbch();
		return Result.success(safrBchVo);
	}

}
