package com.petecat.ruleengine.basedata.asysinfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class SafasysH implements Serializable {
	
	
	/**
	 * 系统标示
	 */
	private String sysUuId;
	
	/**
	 * 历史序列
	 */
	private String seqUuId;
	
	 /**
     * 系统代号
     */
    private String sysCode;

    /**
     * 系统名称
     */
    private String sysName;

    /**
     * 系统全称
     */
    private String sysFullName;

    /**
     * 状态【0：禁用/1：启用】
     */
    private String isDisble;

    /**
     * 改变动作【ADD，MOD，DEL】
     */
    private String edtId;

    /**
     * 改变原因
     */
    private String changePase;

    /**
     * 审核标记[1=等待审核/2=审核通过/3=不通过]
     */
    private String markFlag;

    /**
     * 审核人
     */
    private String appRoveUser;

    /**
     * 审核时间
     */
    private Date appRoveDate;

    /**
     * 审核意见
     */
    private String appDesc;

    /**
     * 操作者
     */
    private String lastModUser;

    /**
     * 操作时间
     */
    private String lastModDate;

    /**
     * safasysh
     */
    private static final long serialVersionUID = 1L;
}