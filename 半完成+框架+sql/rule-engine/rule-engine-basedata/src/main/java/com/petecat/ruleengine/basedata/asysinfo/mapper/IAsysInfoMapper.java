package com.petecat.ruleengine.basedata.asysinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basedata.asysinfo.entity.SafasysH;
import com.petecat.ruleengine.basedata.asysinfo.entity.SafasysM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDelDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.AsysNameVO;

@Mapper
public interface IAsysInfoMapper extends BaseMapper<SafasysM, String> {

	/**
	 * 
	 * @Title: queryAsysByPage
	 * @Description: 分页查询系统信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return List<SafasysM> 返回类型
	 * @throws
	 */
	List<SafasysM> queryAsysByPage(SafasysMDTO safasysMDTO);

	/**
	 * 
	 * @Title: insertRbchH
	 * @Description: 添加系统历史表
	 * @param @param safasysH
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	int insertRbchH(SafasysH safasysH);

	/**
	 * 
	 * @Title: getAsysById
	 * @Description: 通过ID查询信息
	 * @param @param safrbchMDTO
	 * @param @return 参数
	 * @return List<SafasysM> 返回类型
	 * @throws
	 */
	List<SafasysM> getAsysById(@Param("sysCode") String sysCode);

	/**
	 * 
	 * @Title: updateAsys
	 * @Description: 修改系統信息
	 * @param @param safasysMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAsys(SafasysMDTO safasysMDTO);

	/**
	 * 
	 * @Title: deleteAsysById
	 * @Description: 修改系統为禁用
	 * @param @param safasysMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void deleteAsysById(SafasysMDelDTO safasysMDTO);

	/**
	 * 
	 * @Title: queryAsysName
	 * @Description: 查询系统代码名称
	 * @param @return 参数
	 * @return List<AsysNameVO> 返回类型
	 * @throws
	 */
	List<AsysNameVO> queryAsysName();

}
