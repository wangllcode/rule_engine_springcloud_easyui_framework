package com.petecat.ruleengine.basedata.rbchinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchH;
import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDelDTO;

@Mapper
public interface IRbchMInfoMapper extends BaseMapper<SafrbchM, String>{
	/**
	 * 
	 * @Title: listRbchByPage
	 * @Description: 分页查询分机构信息列表
	 * @param @param SafrbchM
	 * @param @return 参数
	 * @return List 返回类型
	 * @throws
	 */
	List<SafrbchM> listRbchByPage(SafrbchMDTO safrbchm);

	/**
	 * 
	 * @Title: countRbchByPage
	 * @Description: 查询总行数
	 * @param @param s
	 * @param @return 参数
	 * @return Integer 返回类型
	 * @throws
	 */
	Integer countRbchByPage(SafrbchMDTO safrbchMDTO);

	/**
	 * 
	 * @Title: insertRbch
	 * @Description: 增加分机构信息
	 * @param @param safrbchM 参数
	 * @return void 返回类型
	 * @throws
	 */
	int insertRbch(SafrbchM safrbchM);

	/**
	 * 
	 * @Title: getRbchById
	 * @Description: 通过机构ID查询机构信息
	 * @param @param bchId
	 * @param @return 参数
	 * @return SafrbchM 返回类型
	 * @throws
	 */
	List<SafrbchM> getRbchById(@Param("bchId") String bchId);

	/**
	 * 
	 * @Title: deleteRbchById
	 * @Description: 修改机构状态为禁用
	 * @param @param safrbchMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void deleteRbchById(SafrbchMDelDTO safrbchMDTO);

	/**
	 * 
	 * @Title: updateRbch
	 * @Description: 修改分机构信息
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateRbch(SafrbchMDTO safrbchMDTO);
	
	/**
	 * 
	 * @Title: insertRbch
	 * @Description: 增加历史表分机构信息
	 * @param @param safrbchM 参数
	 * @return void 返回类型
	 * @throws
	 */
	int insertRbchH(SafrbchH safrbchM);
	
	List<SafrbchM> queryRbch();
}
