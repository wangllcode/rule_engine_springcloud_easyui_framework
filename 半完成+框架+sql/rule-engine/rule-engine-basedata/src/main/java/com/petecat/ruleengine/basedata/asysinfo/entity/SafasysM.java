package com.petecat.ruleengine.basedata.asysinfo.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SafasysM implements Serializable {
	/**
	 * 系统标识
	 */
	private String sysUuId;

	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 系统名称
	 */
	private String sysName;

	/**
	 * 系统全称
	 */
	private String sysFullName;

	/**
	 * 状态【0：禁用/1：启用】
	 */
	private String isDisble;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * safasysm
	 */
	private static final long serialVersionUID = 1L;
}