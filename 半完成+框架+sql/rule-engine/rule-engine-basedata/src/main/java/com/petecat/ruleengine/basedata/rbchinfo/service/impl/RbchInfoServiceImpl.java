package com.petecat.ruleengine.basedata.rbchinfo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchH;
import com.petecat.ruleengine.basedata.rbchinfo.entity.SafrbchM;
import com.petecat.ruleengine.basedata.rbchinfo.mapper.IRbchMInfoMapper;
import com.petecat.ruleengine.basedata.rbchinfo.service.IRbchInfoService;
import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.dto.SafrbchMDelDTO;
import com.petecat.ruleengine.protocol.basedata.rbchinfo.vo.SafrbchVO;
import com.petecat.ruleengine.protocol.dictionary.areainfo.dto.BpsaaremDTO;
import com.petecat.ruleengine.protocol.global.dto.DictionaryDTO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Service
public class RbchInfoServiceImpl extends BaseService<SafrbchM, String>
		implements IRbchInfoService {

	@Autowired
	private IRbchMInfoMapper rbchMInfoMapper;
	
	@Autowired
	private SystemRedisTemplate systemRedisTemplate;

	/**
	 * 分页查询分机构信息
	 */
	public PageVO<SafrbchVO> listRbchByPage(SafrbchMDTO safrbchMDTO) {
		Integer count = rbchMInfoMapper.countRbchByPage(safrbchMDTO);
		PageVO<SafrbchVO> listVo = new PageVO<SafrbchVO>();
		if (null != count && count > 0) {
			StringBuilder builder = new StringBuilder();
			List<SafrbchM> safrList = rbchMInfoMapper.listRbchByPage(safrbchMDTO);
			listVo.setTotal(count);
			List<SafrbchVO> safrbchList = DataCopy.copyList(safrList, SafrbchVO.class);
			List<DictionaryDTO> dtos = systemRedisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, "ISDISABLED", DictionaryDTO.class);
			safrbchList.parallelStream().forEachOrdered(t->{
				String areaCode = t.getAreaCode();
				if(areaCode.length() < 4){
					new CommonException(1003, null, null);
				}
				String proId = areaCode.substring(0, 2).concat("0000");
				String cityId = areaCode.substring(0, 4).concat("00");
				 Optional<DictionaryDTO> dictOpt = dtos.parallelStream().filter(dto->dto.getParmname().equals(t.getIsDisble())).findFirst();
				 if(dictOpt.isPresent()){
					 t.setIsDisbleName(dictOpt.get().getParmvalue());
				 }
				 BpsaaremDTO areaDto = systemRedisTemplate.hGetSingleObject(SystemConstant.ALL_AARE_DATA_KEY, proId, BpsaaremDTO.class);
				 if(areaDto!=null){
					 builder.append(areaDto.getAreadesc()).append(",");
				 }
				 BpsaaremDTO areaDto1 = systemRedisTemplate.hGetSingleObject(SystemConstant.ALL_AARE_DATA_KEY, cityId, BpsaaremDTO.class);
				 if(areaDto!=null){
					 builder.append(areaDto1.getAreadesc()).append(",");
				 }
				 BpsaaremDTO areaDto2 = systemRedisTemplate.hGetSingleObject(SystemConstant.ALL_AARE_DATA_KEY, t.getAreaCode(), BpsaaremDTO.class);
				 if(areaDto!=null){
					 builder.append(areaDto2.getAreadesc());
				 }
				 t.setAreaCodeDesc(builder.toString());
				 builder.delete( 0, builder.length() );
			});
			
			listVo.setRows(safrbchList);
		}else{
			listVo.setTotal(0);
		}
		return listVo;
	}

	/**
	 * 增加分机构信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveRbch(SafrbchMDTO safrbchMDTO) {
		List<SafrbchM> safrbchMList = rbchMInfoMapper.getRbchById(safrbchMDTO.getBchId());
		if (null != safrbchMList && safrbchMList.size() > 0) {
			throw new CommonException(1002, null, null);
		}
		SafrbchM safrbchM = new SafrbchM();
		DataCopy.copyData(safrbchMDTO, safrbchM);
		safrbchM.setIsDisble("1");
		rbchMInfoMapper.insertRbch(safrbchM);
		this.saveRbchHInfo(safrbchM, "ADD", "1");
	}

	/**
	 * 通过机构ID查询机构信息
	 */
	public SafrbchVO getRbchById(String bchId) {
		List<SafrbchM> safrbchM = rbchMInfoMapper.getRbchById(bchId);
		if (null != safrbchM && safrbchM.size() > 1) {
			throw new CommonException(1000, null, null);
		}
		SafrbchVO safrbchVo = new SafrbchVO();
		DataCopy.copyData(safrbchM.get(0), safrbchVo);
		List<DictionaryDTO> dtos = systemRedisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, "ISDISABLED", DictionaryDTO.class);
		 Optional<DictionaryDTO> dictOpt = dtos.parallelStream().filter(dto->dto.getParmname().equals(safrbchVo.getIsDisble())).findFirst();
		 if(dictOpt.isPresent()){
			 safrbchVo.setIsDisbleName(dictOpt.get().getParmvalue());
		 }
		return safrbchVo;
	}

	/**
	 * 修改机构状态为禁用
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeRbchById(SafrbchMDelDTO safrbchMDelDTO) {
		List<SafrbchM> safrbchM = rbchMInfoMapper.getRbchById(safrbchMDelDTO.getBchId());
		if (null == safrbchM || safrbchM.size() == 0) {
			throw new CommonException(1001, null, null);
		} else if (safrbchM.size() > 1) {
			throw new CommonException(1000, null, null);
		} else {
			rbchMInfoMapper.deleteRbchById(safrbchMDelDTO);
			this.saveRbchHInfo(safrbchM.get(0), "DEL", "1");
		}
	}

	/**
	 * 修改分机构信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateRbch(SafrbchMDTO safrbchMDTO) {
		List<SafrbchM> safrbchM = rbchMInfoMapper.getRbchById(safrbchMDTO.getBchId());
		if (null == safrbchM) {
			throw new CommonException(1001, null, null);
		} else if (safrbchM.size() > 1) {
			throw new CommonException(1000, null, null);
		} else {
			rbchMInfoMapper.updateRbch(safrbchMDTO);
			this.saveRbchHInfo(safrbchM.get(0), "MOD", "1");
		}
	}

	/**
	 * 
	 * @Title: saveRbchHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param safrbchDTO
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	public int saveRbchHInfo(SafrbchM safrbchM, String edtId, String markFlag) {
		SafrbchH safrbchH = new SafrbchH();
		DataCopy.copyData(safrbchM, safrbchH);
		safrbchH.setSequuId(String.valueOf(this.getDefaultPrimaryKey()));
		// 改变动作【ADD，MOD，DEL】
		safrbchH.setEdtId(edtId);
		// 审核标记[1=等待审核/2=审核通过/3=不通过]
		safrbchH.setMarkFlag(markFlag);
		return rbchMInfoMapper.insertRbchH(safrbchH);
	}
	/**
	 * 查询机构
	 */
	@Override
	public List<SafrbchVO> queryRbch() {
		List<SafrbchM> safrList = rbchMInfoMapper.queryRbch();
		List<SafrbchVO> safrbchList = DataCopy.copyList(safrList, SafrbchVO.class);
		return safrbchList;
	}

}
