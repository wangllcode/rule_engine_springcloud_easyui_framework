package com.petecat.ruleengine.basedata.asysinfo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basedata.asysinfo.service.IAsysInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDelDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.AsysNameVO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.SafasysMVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

/**
 * 系统信息表操作Controller
 * 
 * @author
 */
@RestController
@RequestMapping("/asysinfo")
@Api(tags="系统信息管理")
public class AsysInfoController extends BaseController {

	@Autowired
	private IAsysInfoService asysInfoService;

	/**
	 * 
	 * @Title: queryAsysByPage
	 * @Description: 分页查询系统信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return Result<PageVO<SafasysMVO>> 返回类型
	 * @throws
	 */
	@GetMapping("/queryAsysByPage")
	@ApiOperation(value = "分页查询系统信息",tags="系统信息管理")
	public Result<PageVO<SafasysMVO>> queryAsysByPage(
			@ModelAttribute SafasysMDTO safasysMDTO) {
		PageVO<SafasysMVO> asysList = asysInfoService
				.listAsysByPage(safasysMDTO);
		return Result.success(asysList);
	}
	
	
	@GetMapping("/queryAsysName")
	@ApiOperation(value = "查询系统代号名称")
	public Result<List<AsysNameVO>> queryAsysName() {
		List<AsysNameVO> asysList = asysInfoService.queryAsysName();
		return Result.success(asysList);
	}

	
	/**
	 * 
	 * @Title: saveAsys
	 * @Description: 保存系统信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/saveAsys")
	@ApiOperation(value = "保存系统信息",tags="系统信息管理")
	public Result<?> saveAsys(@RequestBody SafasysMDTO safasysMDTO) {
		this.setLastUserName(safasysMDTO);
		asysInfoService.saveAsys(safasysMDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: updateAsys
	 * @Description: 修改系统信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PutMapping("/updateAsys")
	@ApiOperation(value = "修改系统信息",tags="系统信息管理")
	public Result<?> updateAsys(@Validated @RequestBody SafasysMDTO safasysMDTO) {
		this.setLastUserName(safasysMDTO);
		asysInfoService.updateAsys(safasysMDTO);
		return Result.success();
	}
	
	@DeleteMapping("/removeAsysById")
	@ApiOperation(value = "刪除系统信息",tags="系统信息管理")
	public Result<?> removeAsysById(SafasysMDelDTO safasysMDelDTO) {
		String userId = this.getCurrentUser().getUserId();
		safasysMDelDTO.setLastModUser(userId);
		safasysMDelDTO.setLastModDate(DatesUtils.getStringDate());
		asysInfoService.removeAsysById(safasysMDelDTO);
		return Result.success();
	}
	
	/**
	 * 
	 * @Title: getAsysById
	 * @Description: 查询信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return Result<SafasysMVO> 返回类型
	 * @throws
	 */
	@GetMapping("/getAsysById")
	@ApiImplicitParams(value={@ApiImplicitParam(name="sysCode",value="系统代号",paramType="query",dataType="string")})
	@ApiOperation("通过系统代号查询系统信息")
	public Result<SafasysMVO> getAsysById(String sysCode) {
		SafasysMVO safasysMVO = asysInfoService.getAsysById(sysCode);
		return Result.success(safasysMVO);
	}

	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param safrbchDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(SafasysMDTO safasysMDTO) {
		String userId = this.getCurrentUser().getUserId();
		safasysMDTO.setLastModUser(userId);
		safasysMDTO.setLastModDate(DatesUtils.getStringDate());
	}

}
