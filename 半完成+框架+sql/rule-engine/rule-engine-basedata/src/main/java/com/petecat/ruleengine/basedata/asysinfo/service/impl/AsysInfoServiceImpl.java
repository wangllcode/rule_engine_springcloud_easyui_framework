package com.petecat.ruleengine.basedata.asysinfo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basedata.asysinfo.entity.SafasysH;
import com.petecat.ruleengine.basedata.asysinfo.entity.SafasysM;
import com.petecat.ruleengine.basedata.asysinfo.mapper.IAsysInfoMapper;
import com.petecat.ruleengine.basedata.asysinfo.service.IAsysInfoService;
import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDelDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.AsysNameVO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.SafasysMVO;
import com.petecat.ruleengine.protocol.global.AexgConstant;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.dto.DictionaryDTO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Service
public class AsysInfoServiceImpl extends BaseService<SafasysM, String>
		implements IAsysInfoService {

	@Autowired
	private IAsysInfoMapper asysInfoMapper;
	
	@Autowired
	private SystemRedisTemplate redisTemplate;
	/**
	 * 分页查询系统信息
	 */
	public PageVO<SafasysMVO> listAsysByPage(SafasysMDTO safasysMDTO) {
		SafasysM safasysM = new SafasysM();
		DataCopy.copyData(safasysMDTO, safasysM);
		Integer count = asysInfoMapper.count(safasysM);
		PageVO<SafasysMVO> pageVO = new PageVO<SafasysMVO>();
		if (null != count && count > 0) {
			List<SafasysM> asysList = asysInfoMapper.queryAsysByPage(safasysMDTO);
			pageVO.setTotal(count);
			List<DictionaryDTO> hGetLists = redisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.ISDISABLED, DictionaryDTO.class);
			List<SafasysMVO> safasysMVO = DataCopy.copyList(asysList, SafasysMVO.class);
			safasysMVO.parallelStream().forEachOrdered(s->{
				Optional<DictionaryDTO> findFirst = hGetLists.parallelStream().filter(h->h.getParmname().equals(s.getIsDisble())).findFirst();
				if(findFirst.isPresent()){
					s.setIsDisbleName(findFirst.get().getParmvalue());
				}
			});
			pageVO.setRows(safasysMVO);
		} else {
			pageVO.setTotal(0);
		}
		return pageVO;
	}
	
	/**
	 * 通过机构ID查询机构信息
	 */
	public SafasysMVO getAsysById(String sysCode) {
		List<SafasysM> safasysM = asysInfoMapper.getAsysById(sysCode);
		if (null != safasysM && safasysM.size() > 1) {
			throw new CommonException(2000, null, null);
		}
		SafasysMVO safasysMVO = new SafasysMVO();
		DataCopy.copyData(safasysM.get(0), safasysMVO);
		List<DictionaryDTO> hGetLists = redisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.ISDISABLED, DictionaryDTO.class);
		for (DictionaryDTO dictionaryDTO : hGetLists) {
			if(safasysMVO.getIsDisble().equals(dictionaryDTO.getParmname())){
				safasysMVO.setIsDisbleName(dictionaryDTO.getParmvalue());
			}
		}
		return safasysMVO;
	}

	/**
	 * 添加系统信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveAsys(SafasysMDTO safasysMDTO) {
		List<SafasysM> safasysMList = asysInfoMapper.getAsysById(safasysMDTO.getSysCode());
		if (null != safasysMList && safasysMList.size() > 0) {
			throw new CommonException(2002, null, null);
		}
		SafasysM safasysM = new SafasysM();
		DataCopy.copyData(safasysMDTO, safasysM);
		safasysM.setIsDisble("1");
		safasysM.setSysUuId(this.getDefaultPrimaryKeyForChar());
		asysInfoMapper.insert(safasysM);
		this.saveAsysHInfo(safasysM, "ADD", "1");
	}
	/**
	 * 修改系统状态为禁用
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeAsysById(SafasysMDelDTO safasysMDTO) {
		List<SafasysM> safasysM = asysInfoMapper.getAsysById(safasysMDTO.getSysCode());
		if (null == safasysM || safasysM.size() == 0) {
			throw new CommonException(2001, null, null);
		} else if (safasysM.size() > 1) {
			throw new CommonException(2000, null, null);
		} else {
			asysInfoMapper.deleteAsysById(safasysMDTO);
			this.saveAsysHInfo(safasysM.get(0), "DEL", "1");
		}
	}

	/**
	 * 修改系统信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateAsys(SafasysMDTO safasysMDTO) {
		List<SafasysM> safasysM = asysInfoMapper.getAsysById(safasysMDTO.getSysCode());
		if (null != safasysM && safasysM.size() > 1) {
			throw new CommonException(2001, null, null);
		} else if (safasysM.size() > 1) {
			throw new CommonException(2000, null, null);
		} else {
			asysInfoMapper.updateAsys(safasysMDTO);
			this.saveAsysHInfo(safasysM.get(0), "MOD", "1");
		}
	}
	/**
	 * 
	 * @Title: saveAsysHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param safrbchM
	 * @param @param edtId
	 * @param @param markFlag
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public int saveAsysHInfo(SafasysM safrbchM, String edtId, String markFlag) {
		SafasysH safasysH = new SafasysH();
		safasysH.setLastModDate(DatesUtils.getStringDate());
		DataCopy.copyData(safrbchM, safasysH);
		safasysH.setSeqUuId(String.valueOf(this.getDefaultPrimaryKey()));
		// 改变动作【ADD，MOD，DEL】
		safasysH.setEdtId(edtId);
		// 审核标记[1=等待审核/2=审核通过/3=不通过]
		safasysH.setMarkFlag(markFlag);
		return asysInfoMapper.insertRbchH(safasysH);
	}

	/**
	 * 查询系统代码名称
	 */
	public List<AsysNameVO> queryAsysName() {
		List<AsysNameVO> asysNameVO = asysInfoMapper.queryAsysName();
		if(null != asysNameVO && asysNameVO.size() > 0){
			return asysNameVO;
		}
		return null;
	}

}
