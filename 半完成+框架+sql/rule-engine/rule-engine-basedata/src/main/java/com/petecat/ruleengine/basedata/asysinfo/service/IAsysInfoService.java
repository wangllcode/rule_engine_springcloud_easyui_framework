package com.petecat.ruleengine.basedata.asysinfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.dto.SafasysMDelDTO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.AsysNameVO;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.SafasysMVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

public interface IAsysInfoService {

	/**
	 * 
	 * @Title: listAsysByPage
	 * @Description: 分页查询系统信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return PageVO<SafasysMVO> 返回类型
	 * @throws
	 */
	PageVO<SafasysMVO> listAsysByPage(SafasysMDTO safasysMDTO);

	/**
	 * 
	 * @Title: saveAsys
	 * @Description: 添加系统信息
	 * @param @param safasysMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveAsys(SafasysMDTO safasysMDTO);

	/**
	 * 
	 * @Title: getAsysById
	 * @Description: 查询系统单个信息
	 * @param @param safasysMDTO
	 * @param @return 参数
	 * @return SafasysMVO 返回类型
	 * @throws
	 */
	SafasysMVO getAsysById(String sysCode);

	/**
	 * 
	 * @Title: updateAsys
	 * @Description: 修改系统信息
	 * @param @param safasysMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAsys(SafasysMDTO safasysMDTO);

	/**
	 * 
	 * @Title: removeAsys
	 * @Description: 一處系統信息
	 * @param @param safasysMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeAsysById(SafasysMDelDTO safasysMDTO);

	/**
	 * 
	 * @Title: queryAsysName
	 * @Description: 查询系统代号名称
	 * @param @return 参数
	 * @return List<AsysNameVO> 返回类型
	 * @throws
	 */
	List<AsysNameVO> queryAsysName();

}
