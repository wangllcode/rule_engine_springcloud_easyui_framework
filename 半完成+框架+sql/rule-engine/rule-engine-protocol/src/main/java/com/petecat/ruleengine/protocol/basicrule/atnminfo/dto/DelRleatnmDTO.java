package com.petecat.ruleengine.protocol.basicrule.atnminfo.dto;

import java.util.Date;

import lombok.Data;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

@Data
public class DelRleatnmDTO{

	/**
	 * 主键
	 */
	private String atnmUuId;
	/**
	 * 附件目录UUID
	 */
	private String atctUuId;
	/**
	 * 操作者
	 */
	private String lastModUser;
}
