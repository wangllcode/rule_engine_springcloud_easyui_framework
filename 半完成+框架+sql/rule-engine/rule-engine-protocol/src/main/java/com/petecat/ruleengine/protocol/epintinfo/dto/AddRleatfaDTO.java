package com.petecat.ruleengine.protocol.epintinfo.dto;

import lombok.Data;

@Data
public class AddRleatfaDTO {
	/**
	 * 主键
	 */
	private String atfaUuid;
	/**
	 * 规则类型用途UUID
	 */
	private String pintUuid;
	/**
	 * 字段主键序列
	 */
	private String fildmUuid;
	/**
	 * 字段历史序列
	 */
	private String fildmSeqnoUuid;
	/**
	 * 字段代号
	 */
	private String fildCode;
	/**
	 * 附件命名主键序列
	 */
	private String atctUuid;
	/**
	 * 附件命名历史序列
	 */
	private String atctSeqnoUuid;
	/**
	 * 必须存在【0：否/1：是】
	 */
	private String attisExists;
	/**
	 * 操作者
	 */
	private String lastModUser;
	/**
	 * 操作时间
	 */
	private String lastModDate;
}
