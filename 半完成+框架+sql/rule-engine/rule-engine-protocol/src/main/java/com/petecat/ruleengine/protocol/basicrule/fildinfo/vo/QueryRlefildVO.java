package com.petecat.ruleengine.protocol.basicrule.fildinfo.vo;

import lombok.Data;

@Data
public class QueryRlefildVO{
	
	/**
     * 主键
     */
    private String fildmuuid;
    
    /**
     * 字段代号（英文字段）
     */
    private String fildcode;

    /**
     * 字段名称
     */
    private String fildname;

    /**
     * 字段用途描述
     */
    private String filddesc;

    /**
     * 字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】
     */
    private String fildtype;
    
    /**
     * 字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】
     */
    private String fildtypeName;

    /**
     * 字段所属模块
     */
    private String fmdluuid;
    
    /**
     * 字段所属模块Name
     */
    private String fmdlName;

    /**
     * 排序
     */
    private Integer ordernum;

}