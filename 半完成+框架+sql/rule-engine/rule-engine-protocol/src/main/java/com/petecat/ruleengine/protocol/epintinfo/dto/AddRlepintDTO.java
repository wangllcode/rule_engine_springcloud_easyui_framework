package com.petecat.ruleengine.protocol.epintinfo.dto;

import lombok.Data;

@Data
public class AddRlepintDTO {
	/**
	 * 系统代号
	 */
	private String sysCode;
	/**
	 * 所属机构
	 */
	private String bhcId;
	/**
	 * 所属产品
	 */
	private String proId;
	/**
	 * 类型代号（接口代号）
	 */
	private String pintCode;
	/**
	 * 序列
	 */
	private String pintUuid;
	/**
	 * 类型名称
	 */
	private String pintName;
	/**
	 * 类型描述
	 */
	private String pintDesc;
	/**
	 * 操作者
	 */
	private String lastModUser;
	/**
	 * 操作时间
	 */
	private String lastModDate;
}
