package com.petecat.ruleengine.protocol.basedata.asysinfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

import lombok.Data;

@Data
public class SafasysMDelDTO implements Serializable {

	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 状态【0：禁用/1：启用】
	 */
	private String isDisble;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * safasysm
	 */
	private static final long serialVersionUID = 1L;
}