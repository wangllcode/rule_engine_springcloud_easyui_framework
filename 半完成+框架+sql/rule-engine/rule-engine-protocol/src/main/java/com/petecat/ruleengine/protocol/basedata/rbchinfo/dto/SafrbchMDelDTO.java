package com.petecat.ruleengine.protocol.basedata.rbchinfo.dto;

import java.io.Serializable;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

import lombok.Data;

/**
 * 分机构信息类
 * 
 * @author 王来利
 *
 */
@Data
public class SafrbchMDelDTO extends PageQryDTO implements Serializable {

	/**
	 * 机构代号
	 */
	private String bchId;

	/**
	 * 状态【0:禁用/1:启用】
	 */
	private String isDisble;

	/**
	 * 最后操作用户
	 */
	private String lastModUser;
	/**
	 * 最后操作时间
	 */
	private String lastModDate;

	private static final long serialVersionUID = 1L;
}
