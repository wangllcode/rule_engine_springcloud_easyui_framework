package com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo;

import lombok.Data;
@Data
public class QueryRlefmdlVO{
	 /**
     * 主键
     */
    private String fmdluuid;

    /**
     * 系统代号
     */
    private String syscode;
    
    /**
     * 系统代号
     */
    private String sysName;
    
    /**
     * 字段模块代号
     */
    private String fmdlcode;

    /**
     * 字段模块名称
     */
    private String fmdlname;

    /**
     * 字段模块描述
     */
    private String fmdldesc;

    /**
     * 排序
     */
    private Integer ordernum;
    /**
	 * ------------------------ 状态
	 */
	private String state;
	
	/**
     * 字段类型
     */
    private String fildType;
	
	/**
     * 字段类型
     */
    private String fildTypeName;
}
