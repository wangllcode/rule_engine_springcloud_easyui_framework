package com.petecat.ruleengine.protocol.global.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreeNodeVO {
	private String id;
	private String text;
	private String iconCls;
	private String state;
	private boolean checked;
	private String parentId;
	private Map attributes = new HashMap();
	private List<TreeNodeVO> children;
	private String flag;
	private String BCHID;
}
