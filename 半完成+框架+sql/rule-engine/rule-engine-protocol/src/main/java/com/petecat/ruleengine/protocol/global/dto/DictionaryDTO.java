package com.petecat.ruleengine.protocol.global.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author 
 */
@Data
public class DictionaryDTO  implements Serializable {
    /**
     * 参数值
     */
    private String parmvalue;

    /**
     * 参数资料类型
     */
    private String parmdatatype;

    /**
     * 参数说明
     */
    private String parmdesc;

    /**
     * 排序号
     */
    private Integer ordernum;

    /**
     * 是否启用【0：禁用/1：启用】
     */
    private String isdisble;

    /**
     * 操作者
     */
    private String lastmoduser;

    /**
     * 操作时间
     */
    private Date lastmoddate;
    
    /**
     * 参数类别
     */
    private String parmtype;

    /**
     * 参数代号
     */
    private String parmname;

    private static final long serialVersionUID = 1L;

}