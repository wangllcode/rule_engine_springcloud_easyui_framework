package com.petecat.ruleengine.protocol.basedata.asysinfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotBlank;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

import lombok.Data;

@Data
@ApiModel(value="修改系统")
public class SafasysMDTO extends PageQryDTO implements Serializable {
	/**
	 * 系统标识
	 */
	private String sysUuId;

	/**
	 * 系统代号
	 */
	@NotBlank(message="{SafasysMDTO.sysCode.NotBlank}")
	@ApiModelProperty(value="系统代号",required=true)
	private String sysCode;

	/**
	 * 系统名称
	 */
	private String sysName;

	/**
	 * 系统全称
	 */
	private String sysFullName;

	/**
	 * 状态【0：禁用/1：启用】
	 */
	private String isDisble;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * safasysm
	 */
	private static final long serialVersionUID = 1L;
}