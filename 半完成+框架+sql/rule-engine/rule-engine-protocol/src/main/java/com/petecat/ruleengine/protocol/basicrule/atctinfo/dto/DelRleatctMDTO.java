package com.petecat.ruleengine.protocol.basicrule.atctinfo.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DelRleatctMDTO implements Serializable {

	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 操作者
	 */
	private String lastModUser;
	
	/**
	 * ID
	 */
	private String atctUuId;
	
	/**
	 * safasysm
	 */
	private static final long serialVersionUID = 1L;
}