package com.petecat.ruleengine.protocol.dictionary.areainfo.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author 
 */
@Data
public class BpsaaremDTO implements Serializable {
    /**
     * 地区代号
     */
    private String areacode;

    /**
     * 地区描述
     */
    private String areadesc;

    /**
     * 是否启用[0-否,1-是]
     */
    private String isdisble;

    /**
     * 操作者
     */
    private String lastmoduser;

    /**
     * 操作时间
     */
    private Date lastmoddate;

    private static final long serialVersionUID = 1L;

}