package com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class AddRlefmdlDTO implements Serializable {

	/**
	 * 主键
	 */
	private String fmdluuid;

	/**
	 * 系统代号
	 */
	private String syscode;

	/**
	 * 字段模块代号
	 */
	private String fmdlcode;

	/**
	 * 字段模块名称
	 */
	private String fmdlname;

	/**
	 * 字段模块描述
	 */
	private String fmdldesc;

	/**
	 * 排序
	 */
	private Integer ordernum;

	/**
	 * 操作者
	 */
	private String lastmoduser;

	/**
	 * 操作时间
	 */
	private String lastmoddate;

}
