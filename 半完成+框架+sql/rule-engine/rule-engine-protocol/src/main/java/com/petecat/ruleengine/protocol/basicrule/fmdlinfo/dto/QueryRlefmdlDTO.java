package com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto;

import lombok.Data;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

@Data
public class QueryRlefmdlDTO extends PageQryDTO{
    /**
     * 系统代号
     */
    private String syscode;

    /**
     * 字段模块代号
     */
    private String fmdlcode;

    /**
     * 字段模块名称
     */
    private String fmdlname;

    /**
     * 字段模块描述
     */
    private String fmdldesc;

    /**
     * 树ID
     */
    private String id;
    
    
    private String state;

}
