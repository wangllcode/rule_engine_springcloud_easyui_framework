package com.petecat.ruleengine.protocol.basicrule.atctinfo.vo;

import lombok.Data;

@Data
public class QueryRleatctVO {
	/**
	 * 主键
	 */
	private String atctUuId;

	/**
	 * 系统代号
	 */
	private String sysCode;
	
	/**
	 * 系统mingc 
	 */
	private String sysName;
	/**
	 * 附件目录名称
	 */
	private String atctName;

	/**
	 * 附件目录描述
	 */
	private String atctDesc;

	/**
	 * 上级目录UUID
	 */
	private String parentAtctUuId;

	/**
	 * 排序
	 */
	private Integer orderNum;

	/**
	 * 操作者
	 */
	private String lastModUser;
	/**
	 * 上级目名称
	 */
	private String parentAtctName;
	/**
	 * 目录状态
	 */
	private String state;
	
	/**
	 * 操作时间
	 */
	private String lastModDate;
}
