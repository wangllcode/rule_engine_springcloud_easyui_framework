package com.petecat.ruleengine.protocol.basicrule.fildinfo.dto;

import lombok.Data;

@Data
public class DelRlefildDTO {

    /**
     * 字段代号（英文字段）
     */
    private String fildcode;
    
    /**
     * 主键
     */
    private String fildmuuid;
    
    /**
     * 操作者
     */
    private String lastmoduser;

}