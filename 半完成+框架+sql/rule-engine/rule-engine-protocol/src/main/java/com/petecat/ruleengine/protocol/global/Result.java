/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  Result.java   
 * @Package com.petecat.riskmanage.global   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月4日 下午4:51:43   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.protocol.global;

import lombok.Data;

/**   
 * @ClassName:  Result   
 * @Description:返回的结果
 * @author: admin
 * @date:   2017年9月4日 下午4:51:43   
 */
@Data
public class Result<T> {

	/**   
	 * @Fields SUCCESS : 成功
	 */   
	public final static int SUCCESS=MessageType.SUCCESS;
	
	/**   
	 * @Fields RUNTIME_ERROR : 运行错误
	 */   
	public final static int RUNTIME_ERROR=MessageType.RUNTIME_ERROR;
	
	/**   
	 * @Fields DATA_ERROR : 数据错误
	 */   
	public final static int DATA_ERROR=MessageType.DATA_ERROR;
	
	/**   
	 * @Fields data :返回的数据结果
	 */   
	private T data;
	
	/**   
	 * @Fields code : 返回的代码
	 */   
	private int code;
	
	/**   
	 * @Fields message : 返回的消息
	 */   
	private String message;
	
	/**   
	 * @Title:  Result   
	 * @Description:结果集
	 */
	public Result() {
		super();
	}

	
	/**   
	 * @Title: error   
	 * @Description: 错误方法
	 * @param code
	 * @return Result<T>     
	 */
	public static <T>Result<T> error(int code,String message) {
		Result<T> result = new Result<>();
		result.setCode(code);
		result.setMessage(message);
		return result;
	}
	
	/**   
	 * @Title: runtimeError   
	 * @Description: 错误方法
	 * @param code
	 * @return Result<T>     
	 */
	public static <T>Result<T> runtimeError(String message) {
		Result<T> result = new Result<>();
		result.setCode(RUNTIME_ERROR);
		result.setMessage(message);
		return result;
	}
	
	/**   
	 * @Title: dataError   
	 * @Description: 数据错误
	 * @param code
	 * @return Result<T>     
	 */
	public static <T>Result<T> dataError(String message) {
		Result<T> result = new Result<>();
		result.setCode(DATA_ERROR);
		result.setMessage(message);
		return result;
	}
	
	/**   
	 * @Title: success   
	 * @Description: 成功消息
	 * @param data
	 * @return Result<T>     
	 */
	public static <T> Result<T> success(T data) {
		Result<T> result = new Result<>();
		result.setCode(SUCCESS);
		result.setData(data);
		result.setMessage("操作成功");
		return result;
	}
	
	/**   
	 * @Title: success   
	 * @Description: 成功消息
	 * @return Result<T>     
	 */
	public static <T> Result<T> success() {
		Result<T> result = new Result<>();
		result.setCode(SUCCESS);
		result.setMessage("操作成功");
		return result;
	}
}
