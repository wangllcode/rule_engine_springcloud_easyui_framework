package com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto;

import lombok.Data;
@Data
public class DelRlefmdlDTO{
	 /**
     * 主键
     */
    private String fmdluuid;

    /**
     * 操作者
     */
    private String lastmoduser;

}
