package com.petecat.ruleengine.protocol.global;

public class AexgConstant {
	
	/**
	 * 0:禁用 1:启用
	 */
	public final static String ISDISABLED = "ISDISABLED";
	
	/**
	 * 0:输入 1:输出
	 */
	public final static String ATCTTYPE = "ATCTTYPE";
	
	/**
	 * 0:包含 1:必须一致
	 */
	public final static String ATCTINPUTTYPE = "ATCTINPUTTYPE";
	
	/**
	 * 0:Int 1:Number 2:String 3:Date
	 */
	public final static String FILDTYPE = "FILDTYPE";
}
