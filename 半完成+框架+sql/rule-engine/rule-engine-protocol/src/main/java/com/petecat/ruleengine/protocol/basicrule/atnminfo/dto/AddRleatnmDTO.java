package com.petecat.ruleengine.protocol.basicrule.atnminfo.dto;

import java.util.Date;

import lombok.Data;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

@Data
public class AddRleatnmDTO{

	/**
	 * 主键
	 */
	private String atnmUuId;

	/**
	 * 附件目录UUID
	 */
	private String atctUuId;

	/**
	 * 附件命名名称
	 */
	private String atctName;

	/**
	 * 是否输入/输出【0：输入/1：输出】
	 */
	private String atctType;

	/**
	 * 输入规则【0：包含/1：必须一致】
	 */
	private String atctInputType;
	
	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;
}
