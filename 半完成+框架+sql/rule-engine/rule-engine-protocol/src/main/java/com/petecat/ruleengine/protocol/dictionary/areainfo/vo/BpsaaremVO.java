package com.petecat.ruleengine.protocol.dictionary.areainfo.vo;

import java.io.Serializable;

import lombok.Data;

/**
 * @author 
 */
@Data
public class BpsaaremVO implements Serializable {
    /**
     * 地区代号
     */
    private String areacode;

    /**
     * 地区描述
     */
    private String areadesc;

    /**
     * 是否启用[0-否,1-是]
     */
    private String isdisble;

    private static final long serialVersionUID = 1L;

}