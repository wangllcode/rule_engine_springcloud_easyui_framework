package com.petecat.ruleengine.protocol.basicrule.atctinfo.dto;

import lombok.Data;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

@Data
public class AddRleatctDTO{
	
	/**
	 * ID
	 */
	private String atctUuId;
	/**
	 * 系统代号
	 */
	private String sysCode;
	/**
	 * 附件目录名称
	 */
	private String atctName;

	/**
	 * 附件目录描述
	 */
	private String atctDesc;

	/**
	 * 上级目录UUID
	 */
	private String parentAtctUuId;
	
	/**
	 * 排序
	 */
	private Integer orderNum;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

}
