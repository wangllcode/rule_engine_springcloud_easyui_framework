/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  FormFieldPageQryDTO.java   
 * @Package com.petecat.ruleengine.protocol.business.businessrule   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 上午12:28:37   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.protocol.business.businessrule.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

import lombok.Data;

/**   
 * @ClassName:  FormFieldPageQryDTO   
 * @Description:分页查询DTO
 * @author: admin
 * @date:   2017年12月6日 上午12:28:37   
 */
@Data
public class FormFieldPageQryDTO extends PageQryDTO{
	 /**
     * 属性代码
     */
	@NotBlank(message="{FormFieldPageQryDTO.fieldcode.NULL}")
	@Size(min = 2, max = 2, message = "{FormFieldPageQryDTO.fieldcode.SIZEMINMAX}")
    private String fieldcode;
    /**
     * 属性名称
     */
    private String fieldname;

    /**
     * 字段类型（0 字符串，1 数字，2 日期，3 日期带时间）
     */
    private String fieldtype;

    /**
     * 适合的种类0 input 1 textarea  2 select 3 radio 9 其他
     */
    private String fittype;

    /**
     * 表名称
     */
    private String tablename;


    /**
     * 表说明
     */
    private String tabledesc;
}
