package com.petecat.ruleengine.protocol.basicrule.atnminfo.vo;

import lombok.Data;

@Data
public class QueryRleatnmVO {
	/**
	 * 附件目录UUID
	 */
	private String atctUuId;

	/**
	 * 主键
	 */
	private String atnmUuId;

	/**
	 * 附件命名名称
	 */
	private String atctName;

	/**
	 * 是否输入/输出【0：输入/1：输出】
	 */
	private String atctType;
	
	/**
	 * 是否输入/输出【0：输入/1：输出】
	 */
	private String atctTypeName;

	/**
	 * 输入规则【0：包含/1：必须一致】
	 */
	private String atctInputType;
	
	/**
	 * 输入规则【0：包含/1：必须一致】
	 */
	private String atctInputTypeName;
	/**
	 * 附件目录名称
	 */
	private String atctParentName;
}
