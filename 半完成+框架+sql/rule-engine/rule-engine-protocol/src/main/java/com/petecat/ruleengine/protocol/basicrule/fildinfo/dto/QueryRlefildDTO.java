package com.petecat.ruleengine.protocol.basicrule.fildinfo.dto;

import java.io.Serializable;
import java.util.Date;

import com.petecat.ruleengine.protocol.global.dto.PageQryDTO;

import lombok.Data;

@Data
public class QueryRlefildDTO extends PageQryDTO {

	/**
	 * 字段代号（英文字段）
	 */
	private String fildcode;

	/**
	 * 字段名称
	 */
	private String fildname;

	/**
	 * 字段用途描述
	 */
	private String filddesc;
	
	/**
     * 字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】
     */
    private String fildtype;
    
	/**
	 * 字段所属模块
	 */
	private String fmdluuid;

}