package com.petecat.ruleengine.protocol.basedata.asysinfo.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class SafasysMVO implements Serializable {
	/**
	 * 系统标识
	 */
	private String sysUuId;

	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 系统名称
	 */
	private String sysName;

	/**
	 * 系统全称
	 */
	private String sysFullName;

	/**
	 * 状态【0：禁用/1：启用】
	 */
	private String isDisble;
	
	/**
	 * 状态【0：禁用/1：启用】
	 */
	private String isDisbleName;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * safasysm
	 */
	private static final long serialVersionUID = 1L;
}