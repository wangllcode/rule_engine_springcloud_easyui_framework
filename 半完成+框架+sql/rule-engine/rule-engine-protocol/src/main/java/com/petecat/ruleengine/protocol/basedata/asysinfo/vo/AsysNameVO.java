package com.petecat.ruleengine.protocol.basedata.asysinfo.vo;

import lombok.Data;

@Data
public class AsysNameVO{
	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 系统名称
	 */
	private String sysName;

}