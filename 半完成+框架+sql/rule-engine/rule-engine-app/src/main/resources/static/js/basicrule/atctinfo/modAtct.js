var sysCode = '';
var flag = false;
$("#sysCodeUpd").combobox(
		{
			url : BUS_URL + 'asysinfo/queryAsysName',
			valueField : 'sysCode',
			textField : 'sysName',
			method : 'get',
			beforeSend : function(request) {
				request.setRequestHeader('Authorization',
						getSessionData('userToken'));
			},
			onLoadSuccess : function() {
				$('#modForm').form('load', op_row);
			},
			onSelect : function(data) {
				debugger
				if (!flag) {
					sysCode = data.sysCode;
					flag = true;
				}
				$("#sysCodeHidden").val(data.sysCode);
				$('#superId').combotree(
						{
							valueField : 'id',
							textField : 'text',
							method : 'get',
							beforeSend : function(request) {
								request.setRequestHeader('Authorization',
										getSessionData('userToken'));
							},
							url : BUS_URL + "atctinfo/queryAtctTree?sysCode="
									+ data.sysCode,
							onLoadSuccess : function(node, data) {
								debugger
								var sysCodeHidden = $("#sysCodeHidden").val();
								if (sysCodeHidden == sysCode) {
									$('#superId').combotree('setValue',
											op_row.parentAtctUuId);
									$('#superId').combotree('setText',
											op_row.parentAtctName);
								} else {
									$('#superId').combotree("clear");
								}
							}
						});

			}
		});
var modForm = new initCommonForm({
	formId : "modForm",
	url : BUS_URL + "atctinfo/updateAtct",
	submitBtnId : 'updBtn',
	method : 'put',
	successBackFunction : function(obj) {
		$("#win").window('close');
		$("#wxConfigManage").datagrid("load");
	}
});
$('#modForm').form('load', op_row);