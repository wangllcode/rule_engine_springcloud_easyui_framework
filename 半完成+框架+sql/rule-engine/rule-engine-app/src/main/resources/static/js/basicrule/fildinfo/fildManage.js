//检查权限
checkPowerAndJump("QRYFILD");
$(function() {
	var items = {
		id : "#fildType",
		name : "FILDTYPE"
	}
	initSel(items);
	$("#fmdlUuId").combobox(
			{
				url : BUS_URL + 'fmdlinfo/queryFmdlName',
				valueField : 'id',
				textField : 'text',
				method : 'get',
				beforeSend : function(request) {
					request.setRequestHeader('Authorization',
							getSessionData('userToken'));
				}
			});
	$("#wxConfigManage")
			.datagrid(
					{
						pagination : true,
						url : BUS_URL + "fildinfo/queryFildByPage",
						loadMsg : '加载中...',
						beforeSend : function(request) {
							request.setRequestHeader('Authorization',
									getSessionData('userToken'));
						},
						method : 'get',
						singleSelect : true,
						fitColumns : true,
						striped : true,
						border : false,
						fit : true,
						columns : [ [
								{
									title : '字段代号',
									field : 'fildcode',
									width : 100
								},
								{
									title : '字段名称',
									field : 'fildname',
									width : 100
								},
								{
									title : '字段描述',
									field : 'filddesc',
									width : 100
								},
								{
									title : '字段类型',
									field : 'fildtypeName',
									width : 100
								},/*
									 * { title : '所属模块', field : 'fmdlName',
									 * width : 100 },
									 */
								{
									title : '操作',
									field : 'operate',
									width : 80,
									formatter : function(value, row, index) {
										var str = "";
										if (checkPower("UPDFILD")) {
											str += '<a href="javascript:mod()">修改</a><span class="btnSplit"></span>';
										}
										if (checkPower("DELFILD")) {
											str += '<a href="javascript:del()">删除</a><span class="btnSplit"></span>';
										}
										if (checkPower("GETFILD")) {
											str += '<a href="javascript:qry()">查看</a><span class="btnSplit"></span>';
										}
										return str;
									}
								} ] ]
					});

	$("#btnAdd").click(function() {
		if (!win_show_flag) {
			initWin();
			win_show_flag = true;
		}
		$('#win').window('setTitle', '新增规则字段信息');
		$('#win').window('open');
		$('#win').window('refresh', "fildadd.html");
	})
})

var win_show_flag = false;
var op_refcode = "";
var op_row = null;
function initWin() {
	$('#win').window({
		width : 800,
		height : 330,
		collapsible : false,
		minimizable : false,
		maximizable : false,
		resizable : false,
		closed : true,
		modal : true
	})
}

// 修改
function mod() {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '修改规则字段信息');
	$('#win').window('open');
	$('#win').window('refresh', 'fildmod.html');
}

// 删除
function del(stu) {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '删除规则字段信息');
	$('#win').window('open');
	$('#win').window('refresh', 'filddel.html');
}

/**
 * 查看
 */
function qry() {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '查看规则字段信息');
	$('#win').window('open');
	$('#win').window('refresh', 'fildqry.html');
}

function queryDg() {
	$("#wxConfigManage").datagrid("options").queryParams = $("#queryForm")
			.serializeObject();
	$("#wxConfigManage").datagrid("load");
}

function resetDg() {
	$("#queryForm").form('reset');
	$("#wxConfigManage").datagrid("options").queryParams = {};
	$("#wxConfigManage").datagrid("load");
}
function retBack() {
	$("#returnBtn").click(function() {
		$('#win').window('close');
	});
}
function queryFmdlTree() {
	parent.addTab("fmdlTree", "/basicrule/fmdlinfo/fmdltree.html", "查看字段目录树");
	parent.$('#tabs').tabs('select', "查看字段目录树");
}