$(function() {
				var items = {
						id : "#fildTypeUpd",
						name : "FILDTYPE"
					}
					initSel(items);
				
				$("#fmdlUuIdUpd").combobox({
					url : BUS_URL + 'fmdlinfo/queryFmdlName',
					valueField : 'id',
					textField : 'text',
					method : 'get',
					beforeSend : function(request) {
						request.setRequestHeader('Authorization',
								getSessionData('userToken'));
					},
					onLoadSuccess:function(){
						$('#modForm').form('load',op_row);
					}
				});
			var modForm = new initCommonForm({
				formId : "modForm",
				url : BUS_URL + "fildinfo/updateFild",
				submitBtnId : 'updBtn',
				method:'put',
				successBackFunction : function(obj) {
					$("#win").window('close');
					$("#wxConfigManage").datagrid("load");
				}
			});
			$('#modForm').form('load', op_row);
			$("input[readonly='readonly']").css("backgroundColor","rgb(235, 235, 228)");
		});