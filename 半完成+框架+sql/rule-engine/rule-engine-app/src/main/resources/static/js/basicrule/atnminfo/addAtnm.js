$("#atctUuId").combobox({
	url : BUS_URL + 'atctinfo/queryAtctName',
	valueField : 'id',
	textField : 'text',
	method : 'get',
	beforeSend : function(request) {
		request.setRequestHeader('Authorization', getSessionData('userToken'));
	}
});
// 加载类型
var items = {
	id : "#atctTypeAdd",
	name : "ATCTTYPE"
}
initSel(items);
// 加载输入规则
var items1 = {
	id : "#atctInputTypeAdd",
	name : "ATCTINPUTTYPE"
}
initSel(items1);
var addForm = new initCommonForm({
	formId : "addForm",
	url : BUS_URL + "atnminfo/saveAtnm",
	submitBtnId : 'addBtn',
	successBackFunction : function(obj) {
		$("#win").window('close');
		$("#wxConfigManage").datagrid("load");
	}
});