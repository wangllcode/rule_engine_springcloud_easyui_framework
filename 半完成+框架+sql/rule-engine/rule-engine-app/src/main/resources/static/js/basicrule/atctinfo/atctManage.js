//检查权限
checkPowerAndJump("QRYATCT");
$(function() {
	addSysName("sysCode");
	$("#wxConfigManage")
			.datagrid(
					{
						pagination : true,
						url : BUS_URL + "atctinfo/queryAtctByPage",
						loadMsg : '加载中...',
						beforeSend : function(request) {
							request.setRequestHeader('Authorization',
									getSessionData('userToken'));
						},
						method : 'get',
						singleSelect : true,
						fitColumns : true,
						striped : true,
						border : false,
						fit : true,
						columns : [ [
								{
									title : '附件目录名称',
									field : 'atctName',
									width : 100
								},
								{
									title : '所属系统',
									field : 'sysName',
									width : 100
								},
								{
									title : '附件目录描述',
									field : 'atctDesc',
									width : 100
								},
								{
									title : '上级目录名称',
									field : 'parentAtctName',
									width : 100
								},
								{
									title : '排序',
									field : 'orderNum',
									width : 100
								},
								{
									title : '操作',
									field : 'operate',
									width : 80,
									formatter : function(value, row, index) {
										var str = "";
										if (checkPower("UPDATCT")) {
											str += '<a href="javascript:mod()">修改</a><span class="btnSplit"></span>';
										}
										if (checkPower("DELATCT")) {
											str += '<a href="javascript:del()">删除</a><span class="btnSplit"></span>';
										}
										if (checkPower("GETATCT")) {
											str += '<a href="javascript:qry()">查看</a><span class="btnSplit"></span>';
										}
										return str;
									}
								} ] ]
					});

	$("#btnAdd").click(function() {
		if (!win_show_flag) {
			initWin();
			win_show_flag = true;
		}
		$('#win').window('setTitle', '新增附件目录规则');
		$('#win').window('open');
		$('#win').window('refresh', "atctadd.html");
	})
})

var win_show_flag = false;
var op_refcode = "";
var op_row = null;
function initWin() {
	$('#win').window({
		width : 800,
		height : 280,
		collapsible : false,
		minimizable : false,
		maximizable : false,
		resizable : false,
		closed : true,
		modal : true
	})
}

// 修改
function mod() {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '修改附件目录规则信息');
	$('#win').window('open');
	$('#win').window('refresh', 'atctmod.html');
}

// 删除
function del(stu) {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '删除附件目录规则信息');
	$('#win').window('open');
	$('#win').window('refresh', 'atctdel.html');
}

/**
 * 查看
 */
function qry() {
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if (!win_show_flag) {
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle', '查看系统信息');
	$('#win').window('open');
	$('#win').window('refresh', 'atctqry.html');
}

function queryDg() {
	$("#wxConfigManage").datagrid("options").queryParams = $("#queryForm")
			.serializeObject();
	$("#wxConfigManage").datagrid("load");
}

function resetDg() {
	$("#queryForm").form('reset');
	$("#wxConfigManage").datagrid("options").queryParams = {};
	$("#wxConfigManage").datagrid("load");
}
function retBack() {
	$("#returnBtn").click(function() {
		$('#win').window('close');
	});
}
function queryAtctTree() {
	parent.addTab("atctTree", "/basicrule/atctinfo/atcttree.html", "查看附件目录树");
	parent.$('#tabs').tabs('select', "查看附件目录树");
}