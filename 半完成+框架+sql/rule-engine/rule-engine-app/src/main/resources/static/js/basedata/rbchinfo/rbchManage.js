   //检查权限
    checkPowerAndJump("QRYRBCH");
    $(function(){
    	var items ={
    			id:"#isDisble",
    			name:'ISDISABLED'
    	};
    	initSel(items);
    	$("#wxConfigManage").datagrid({
    			  pagination:true,
    			  url:BUS_URL+"rbchinfo/queryRbchByPage",
    			  loadMsg:'加载中...',
   			      beforeSend: function(request) {
   			    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
                  },
                  method:'get',
    			  singleSelect:true,
    			  fitColumns:true,
    			  striped:true,
    			  border:false,
    			  fit:true,
    			  columns:[[   
    	                     {title:'机构代号',field:'bchId',width:100},
    	                     {title:'机构名称',field:'bchDesc',width:100},
    	                     {title:'机构全称',field:'bchFullDesc',width:100},
    	                     {title:'区域名称',field:'areaCodeDesc',width:100},
    	                     {title:'机构状态',field:'isDisbleName',width:100},
    	                     {title:'操作',field:'operate',width:80,
    			              formatter: function(value,row,index){
    			 				 var str = "";
    			 				 var span = "启用";
    			 				 var sta = row['isDisble']==1?"0":"1";
    			 				  if(row['isDisble'] == 1){
    			 					 span = "禁用";
    			 					  if(checkPower("UPDRBCH")){
      			            		  	  str+= '<a href="javascript:mod()">修改</a><span class="btnSplit"></span>';
	      			            	  }
    			 				  }
    			 				  if(checkPower("DELRBCH")){
     			            		  str+= '<a href="javascript:del('+sta+')">'+span+'</a><span class="btnSplit"></span>';
     			            	  }
    			            	  if(checkPower("GETRBCH")){
    			            		   str+= '<a href="javascript:qry()">查看</a><span class="btnSplit"></span>';
    			            	  }
    			 				return str;  
    						}}
    			 ]] 
    		});

    	$("#btnAdd").click(function(){
    		if(!win_show_flag){
    			initWin();
    			win_show_flag = true;
    		}
    		$('#win').window('setTitle','新增分机构信息');
    		$('#win').window('open');
    		$('#win').window('refresh', "rbchadd.html");
    	})
    })
    
var win_show_flag = false;   
var op_refcode="";
var op_row = null;
function initWin(){
	$('#win').window({
		width:500,
		height:280,
		collapsible : false,
		minimizable : false,
		maximizable:false,
		resizable:false,
		closed:true,
		modal:true
	})
}

//修改
function mod(){
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if(!win_show_flag){
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle','修改分机构信息');
	$('#win').window('open');
	$('#win').window('refresh', 'rbchmod.html');
}

//删除
function del(sta){
	op_row = $("#wxConfigManage").datagrid('getSelected');
	statusText = sta == 1 ?"启用":"禁用";
	status = sta;
	if(!win_show_flag){
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle',statusText+'分机构信息');
	$('#win').window('open');
	$('#win').window('refresh', 'rbchdel.html');
}

/**
 * 查看
 */
function qry(){
	op_row = $("#wxConfigManage").datagrid('getSelected');
	if(!win_show_flag){
		initWin();
		win_show_flag = true;
	}
	$('#win').window('setTitle','查看分机构信息');
	$('#win').window('open');
	$('#win').window('refresh', 'rbchqry.html');
}


function queryDg(){
	 $("#wxConfigManage").datagrid("options").queryParams=$("#queryForm").serializeObject();
  	 $("#wxConfigManage").datagrid("load");
}

function resetDg(){
	 $("#queryForm").form('reset');
	 $("#wxConfigManage").datagrid("options").queryParams={
  	 };
  	 $("#wxConfigManage").datagrid("load");
}
function retBack(){
	$("#returnBtn").click(function(){
		$('#win').window('close'); 
	});
}