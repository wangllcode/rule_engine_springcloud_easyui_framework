var items = {
	id : "#fildTypeAdd",
	name : "FILDTYPE"
}
initSel(items);
$("#fmdlUuIdAdd").combobox({
	url : BUS_URL + 'fmdlinfo/queryFmdlName',
	valueField : 'id',
	textField : 'text',
	method : 'get',
	beforeSend : function(request) {
		request.setRequestHeader('Authorization', getSessionData('userToken'));
	}
});
var addForm = new initCommonForm({
	formId : "addForm",
	url : BUS_URL + "fildinfo/saveFild",
	submitBtnId : 'addBtn',
	successBackFunction : function(obj) {
		$("#win").window('close');
		$("#wxConfigManage").datagrid("load");
	}
});