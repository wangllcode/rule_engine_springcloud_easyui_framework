//检查权限
		checkPowerAndJump("QRYASYS");
		$(function() {
			var items = {
				id : "#isDisble",
				name : "ISDISABLED"
			}
			initSel(items);
			$("#wxConfigManage")
					.datagrid(
							{
								pagination : true,
								url : BUS_URL + "asysinfo/queryAsysByPage",
								loadMsg : '加载中...',
								beforeSend : function(request) {
									request.setRequestHeader('Authorization',
											getSessionData('userToken'));
								},
								method : 'get',
								singleSelect : true,
								fitColumns : true,
								striped : true,
								border : false,
								fit : true,
								columns : [ [
										{
											title : '系统代号',
											field : 'sysCode',
											width : 100
										},
										{
											title : '系统名称',
											field : 'sysName',
											width : 100
										},
										{
											title : '系统全称',
											field : 'sysFullName',
											width : 100
										},
										{
											title : '系统状态',
											field : 'isDisbleName',
											width : 100
										},
										{
											title : '操作',
											field : 'operate',
											width : 80,
											formatter : function(value, row,
													index) {
												var str = "";
												var span = "启用";
												var sta = row['isDisble'] == 1 ? "0"
														: "1";
												if (row['isDisble'] == 1) {
													span = "禁用";
													if (checkPower("UPDASYS")) {
														str += '<a href="javascript:mod()">修改</a><span class="btnSplit"></span>';
													}
												}
												if (checkPower("DELASYS")) {
													str += '<a href="javascript:del('
															+ sta
															+ ')">'
															+ span
															+ '</a><span class="btnSplit"></span>';
												}
												if (checkPower("GETASYS")) {
													str += '<a href="javascript:qry()">查看</a><span class="btnSplit"></span>';
												}
												return str;
											}
										} ] ]
							});

			$("#btnAdd").click(function() {
				if (!win_show_flag) {
					initWin();
					win_show_flag = true;
				}
				$('#win').window('setTitle', '新增分机构信息');
				$('#win').window('open');
				$('#win').window('refresh', "asysadd.html");
			})
		})

		var win_show_flag = false;
		var op_refcode = "";
		var op_row = null;
		function initWin() {
			$('#win').window({
				width : 500,
				height:220,
				collapsible : false,
				minimizable : false,
				maximizable : false,
				resizable : false,
				closed : true,
				modal : true
			})
		}

		//修改
		function mod() {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', '修改系统信息');
			$('#win').window('open');
			$('#win').window('refresh', 'asysmod.html');
		}

		//删除
		function del(stu) {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			statusText = stu == 1 ? "启用" : "禁用";
			status = stu;
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', statusText + '系统信息');
			$('#win').window('open');
			$('#win').window('refresh', 'asysdel.html');
		}

		/**
		 * 查看
		 */
		function qry() {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', '查看系统信息');
			$('#win').window('open');
			$('#win').window('refresh', 'asysqry.html');
		}

		function queryDg() {
			$("#wxConfigManage").datagrid("options").queryParams = $(
					"#queryForm").serializeObject();
			$("#wxConfigManage").datagrid("load");
		}

		function resetDg() {
			$("#queryForm").form('reset');
			$("#wxConfigManage").datagrid("options").queryParams = {};
			$("#wxConfigManage").datagrid("load");
		}
		function retBack(){
			$("#returnBtn").click(function(){
				$('#win').window('close'); 
			});
		}