$(function() {
	commonViewLoadForm('deleteForm', op_row)
	$("#delBtn").click(
			function() {
				var req = {
					useToken : true
				};
				req['reqJSON'] = {
					url : BUS_URL + 'fmdlinfo/removeFmdlById?fmdluuid='
							+ op_row.fmdluuid,
					type : 'delete'
				};
				req['successFn'] = function(respData) {
					if (respData.code == SUCCESS_CODE) {
						$("#win").window('close');
						$("#wxConfigManage").datagrid("load");
					} else {
						$.messager.alert('提示', respData.message);
					}
				};
				ajaxRequest(req);
			})
});
