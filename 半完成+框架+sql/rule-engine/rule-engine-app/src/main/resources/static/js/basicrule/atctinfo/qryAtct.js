$(function() {
	commonViewLoadForm('deleteForm', op_row)
	$('#superId').combotree(
			{
				required : true,
				valueField : 'id',
				textField : 'text',
				method : 'get',
				beforeSend : function(request) {
					request.setRequestHeader('Authorization',
							getSessionData('userToken'));
				},
				url : BUS_URL + "atctinfo/queryAtctTree",
				onLoadSuccess : function() {
					$('#superId').combotree('setValue', op_row.parentAtctUuId);
					$('#superId').combotree('setText', op_row.parentAtctName);
				}
			});
	// loadFormData();
})
function loadFormData() {
	var req = {
		useToken : true
	};
	req['reqJSON'] = {
		url : BUS_URL + 'atctinfo/getAtctById',
		type : 'get',
		data : {
			sysCode : op_row.sysCode
		}
	};
	req['successFn'] = function(respData) {
		console.dir(respData)
		if (respData.code == SUCCESS_CODE) {
			$('#qryForm').form('load', respData.data);
		} else {
			$.messager.alert('提示', respData.message);
		}
	};
	ajaxRequest(req);
}