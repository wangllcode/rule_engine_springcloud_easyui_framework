commonViewLoadForm('qryForm',op_row);
			$("#delBtn").text(statusText);
			$("#delBtn").click(function() {
				var req = {
					useToken : true
				};
				req['reqJSON'] = {
					url : BUS_URL + 'asysinfo/removeAsysById?sysCode='+op_row.sysCode+'&isDisble='+status,
					type : 'delete'
				};
				req['successFn'] = function(respData) {
					if (respData.code == SUCCESS_CODE) {
						$("#win").window('close');
						$("#wxConfigManage").datagrid("load");
					} else {
						$.messager.alert('提示', respData.message);
					}
				};
				ajaxRequest(req);
			})