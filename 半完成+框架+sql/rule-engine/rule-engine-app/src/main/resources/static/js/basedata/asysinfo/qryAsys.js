commonViewLoadForm('qryForm', op_row)
// loadFormData();
function loadFormData() {
	var req = {
		useToken : true
	};
	req['reqJSON'] = {
		url : BUS_URL + 'asysinfo/getAsysById',
		type : 'get',
		data : {
			sysCode : op_row.sysCode
		}
	};
	req['successFn'] = function(respData) {
		console.dir(respData)
		if (respData.code == SUCCESS_CODE) {
			$('#qryForm').form('load', respData.data);
		} else {
			$.messager.alert('提示', respData.message);
		}
	};
	ajaxRequest(req);
}