$(function() {
	$("#atctUuId").combobox(
			{
				url : BUS_URL + 'atctinfo/queryAtctName',
				valueField : 'id',
				textField : 'text',
				method : 'get',
				beforeSend : function(request) {
					request.setRequestHeader('Authorization',
							getSessionData('userToken'));
				},
				onLoadSuccess : function() {
					$('#modForm').form('load', op_row);
				}
			});
	// 加载类型
	var items = {
		id : "#atctTypeUpd",
		name : "ATCTTYPE"
	}
	initSel(items);
	// 加载输入规则
	var items1 = {
		id : "#atctInputTypeUpd",
		name : "ATCTINPUTTYPE"
	}
	initSel(items1);
})
var modForm = new initCommonForm({
	formId : "modForm",
	url : BUS_URL + "atnminfo/updateAtnm",
	submitBtnId : 'updBtn',
	method : 'put',
	successBackFunction : function(obj) {
		$("#win").window('close');
		$("#wxConfigManage").datagrid("load");
	}
});
$('#modForm').form('load', op_row);