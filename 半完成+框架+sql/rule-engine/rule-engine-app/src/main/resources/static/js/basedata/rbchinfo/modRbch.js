  initCityFlag = false;
	     initAreaFlag = false;
	     initProFlag = false;
		$(function() {
			var modForm = new initCommonForm({
				formId : "modForm",
				url : BUS_URL + "rbchinfo/updateRbch",
				submitBtnId : 'updBtn',
				successBackFunction : function(obj) {
					$("#win").window('close');
					$("#wxConfigManage").datagrid("load");
				}
			});
			var proId = op_row.areaCode.substring(0,2)+"0000";
			var cityId = op_row.areaCode.substring(0,4)+"00";
			var areaCode = op_row.areaCode;
			$('#modForm').form('load', op_row);
			$('#CITYID').combobox({
			    valueField:'areacode',
			    textField:'areadesc',
			    method:'get',
			    onLoadSuccess:function(){
			    	if(!initCityFlag){
			    		$("#CITYID").combobox('setValue',cityId);
			    		initCityFlag = true;
			    	}
			    },
			    beforeSend: function(request) {
				    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
			    },onSelect : function(rec) {
			    		$('#ACCAREACODE').combobox('clear');
			    		$('#ACCAREACODE').combobox('reload',BUS_URL+'areainfo/queryAareCountyList?areacode='+ rec.areacode);
			    }
			});
			
    		$('#ACCAREACODE').combobox({
			    valueField:'areacode',
			    textField:'areadesc',
			    method:'get',
			    onLoadSuccess:function(){
			    	if(!initAreaFlag){
			    		$("#ACCAREACODE").combobox('setValue',areaCode);
			    		initAreaFlag = true;
			    	}
			    },
			    beforeSend: function(request) {
			    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
		    	}
	    	});
    		
			$("#PROID").combobox({
				url:BUS_URL+'areainfo/queryAareProvList',
			    valueField:'areacode',
			    textField:'areadesc',
			    method:'get',
			    beforeSend: function(request) {
				    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
			    },
			    onLoadSuccess:function(){
					$("#PROID").combobox('setValue',proId);
			    },
				onSelect : function(rec) {
					    $('#CITYID').combobox('clear');
					    $('#ACCAREACODE').combobox('clear');
			    		$('#CITYID').combobox('reload',BUS_URL+'areainfo/queryAareCityList?areacode='+ rec.areacode);
				}
			});
			$("input[readonly='readonly']").css("backgroundColor","rgb(235, 235, 228)");
		});