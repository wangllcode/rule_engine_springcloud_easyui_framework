	commonViewLoadForm('qryForm', op_row)
	// loadFormData();
function loadFormData() {
	var req = {
		useToken : true
	};
	req['reqJSON'] = {
		url : BUS_URL + 'rbchinfo/getRbchById',
		type : 'get',
		data : {
			bchId : op_row.bchId
		}
	};
	req['successFn'] = function(respData) {
		console.dir(respData)
		if (respData.code == SUCCESS_CODE) {
			$('#qryForm').form('load', respData.data);
		} else {
			$.messager.alert('提示', respData.message);
		}
	};
	ajaxRequest(req);
}