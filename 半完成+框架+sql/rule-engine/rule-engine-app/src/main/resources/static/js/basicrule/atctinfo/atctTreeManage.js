$(function() {
	addSysName("sysCode");
	$("#wxConfigManage1").treegrid(
			{
				url : BUS_URL + "atctinfo/queryAtctByPage",
				loadMsg : '加载中...',
				beforeSend : function(request) {
					request.setRequestHeader('Authorization',
							getSessionData('userToken'));
				},
				method : 'get',
				singleSelect : true,
				fitColumns : true,
				striped : true,
				border : false,
				fit : true,
				animate : true, // 是否开启动画
				collapsible : true, // 是否可以折叠
				idField : 'atctUuId', // 指示那个字段是标识字段
				treeField : 'atctName', // 定义树节点的字段
				showFooter : true, // 定义是否显示行的底部
				rownumbers : true, // 设置为true，则显示带有行号的列
				columns : [ [ {
					title : '附件目录名称',
					field : 'atctName',
					width : 100
				}, {
					title : '所属系统',
					field : 'sysName',
					width : 100
				}, {
					title : '附件目录描述',
					field : 'atctDesc',
					width : 100
				}, {
					title : '上级目录名称',
					field : 'parentAtctName',
					width : 100
				}, {
					title : '排序',
					field : 'orderNum',
					width : 100
				} ] ]
			});
	// 返回
	$("#returnBtn").click(function() {
		parent.$('#tabs').tabs('close', "查看附件目录树");
	});
})

function queryDg() {
	debugger
	$("#wxConfigManage1").treegrid("options").queryParams = $("#queryForm")
			.serializeObject();
	$("#wxConfigManage1").treegrid("load");
}