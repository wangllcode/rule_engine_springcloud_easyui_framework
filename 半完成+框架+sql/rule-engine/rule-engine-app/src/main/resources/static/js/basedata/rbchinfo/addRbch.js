	
	$("#PROID").combobox({
		url:BUS_URL+'areainfo/queryAareProvList',
	    valueField:'areacode',
	    textField:'areadesc',
	    method:'get',
	    beforeSend: function(request) {
		    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
	    },
		onSelect : function(rec) {
			$('#CITYID').combobox({
				url:BUS_URL+'areainfo/queryAareCityList?areacode='+ rec.areacode,
			    valueField:'areacode',
			    textField:'areadesc',
			    method:'get',
			    beforeSend: function(request) {
				    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
			    },onSelect : function(rec) {
			    	$('#ACCAREACODE').combobox({
						url:BUS_URL+'areainfo/queryAareCountyList?areacode='+ rec.areacode,
					    valueField:'areacode',
					    textField:'areadesc',
					    method:'get',
					    beforeSend: function(request) {
					    	 request.setRequestHeader('Authorization',getSessionData('userToken'));
				    	}
			    	});
			    }
			});
			$('#CITYID').combobox('reload');
			$('#CITYID').combobox('clear');
			$('#ACCAREACODE').combobox('clear');
		}
	});
	
	
		var addForm = new initCommonForm({
			formId : "addForm",
			url : BUS_URL + "rbchinfo/saveRbch",
			submitBtnId : 'addBtn',
			successBackFunction : function(obj) {
				$("#win").window('close');
				$("#wxConfigManage").datagrid("load");
			}
		});