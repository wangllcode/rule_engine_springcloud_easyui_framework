$("#sysCodeAdd").combobox(
		{
			url : BUS_URL + 'asysinfo/queryAsysName',
			valueField : 'sysCode',
			textField : 'sysName',
			method : 'get',
			beforeSend : function(request) {
				request.setRequestHeader('Authorization',
						getSessionData('userToken'));
			},
			onSelect : function(data) {
				$('#superId').combotree(
						{
							valueField : 'id',
							textField : 'text',
							method : 'get',
							beforeSend : function(request) {
								request.setRequestHeader('Authorization',
										getSessionData('userToken'));
							},
							url : BUS_URL + "atctinfo/queryAtctTree?sysCode="
									+ data.sysCode
						});
			}
		});
var addForm = new initCommonForm({
	formId : "addForm",
	url : BUS_URL + "atctinfo/saveAtct",
	submitBtnId : 'addBtn',
	successBackFunction : function(obj) {
		$("#win").window('close');
		$("#wxConfigManage").datagrid("load");
	}
});