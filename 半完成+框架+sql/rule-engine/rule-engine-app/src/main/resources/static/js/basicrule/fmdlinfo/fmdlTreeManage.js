$(function() {
	addSysName("sysCode");
	$("#wxConfigManage1").treegrid(
			{
				url : BUS_URL + "fmdlinfo/queryFmdlByPage?state=1",
				loadMsg : '加载中...',
				beforeSend : function(request) {
					request.setRequestHeader('Authorization',
							getSessionData('userToken'));
				},
				method : 'get',
				singleSelect : true,
				fitColumns : true,
				striped : true,
				border : false,
				fit : true,
				animate : true, // 是否开启动画
				collapsible : true, // 是否可以折叠
				idField : 'fmdluuid', // 指示那个字段是标识字段
				treeField : 'fmdlname', // 定义树节点的字段
				showFooter : true, // 定义是否显示行的底部
				rownumbers : true, // 设置为true，则显示带有行号的列
				columns : [ [ {
					title : '所属系统',
					field : 'sysName',
					width : 100
				}, {
					title : '模块/字段名称',
					field : 'fmdlname',
					width : 100
				}, {
					title : '模块/字段代号',
					field : 'fmdlcode',
					width : 100
				}, {
					title : '模块/字段描述',
					field : 'fmdldesc',
					width : 100
				}, {
					title : '排序',
					field : 'ordernum',
					width : 100
				}, {
					title : '字段类型',
					field : 'fildTypeName',
					width : 100
				} ] ]
			});
	// 返回
	$("#returnBtn").click(function() {
		parent.$('#tabs').tabs('close', "查看字段目录树");
	});
})

function queryDg() {
	$("#wxConfigManage1").treegrid("options").queryParams = $("#queryForm")
			.serializeObject();
	$("#wxConfigManage1").treegrid("load");
}