//检查权限
		checkPowerAndJump("QRYFMDL");
		$(function() {
			addSysName("sysCode");
			$("#wxConfigManage")
					.datagrid(
							{
								 pagination:true,
								url : BUS_URL + "fmdlinfo/queryFmdlByPage",
								loadMsg : '加载中...',
								beforeSend : function(request) {
									request.setRequestHeader('Authorization',
											getSessionData('userToken'));
								},
								method : 'get',
								singleSelect : true,
								fitColumns : true,
								striped : true,
								border : false,
								fit : true,
								columns : [ [
										{
											title : '所属系统',
											field : 'sysName',
											width : 100
										},
										{
											title : '字段名称',
											field : 'fmdlname',
											width : 100
										},
										{
											title : '字段代号',
											field : 'fmdlcode',
											width : 100
										},
										{
											title : '字段描述',
											field : 'fmdldesc',
											width : 100
										},
										{
											title : '排序',
											field : 'ordernum',
											width : 100
										},
										{
											title : '操作',
											field : 'operate',
											width : 80,
											formatter : function(value, row,
													index) {
												var str = "";
												if (checkPower("UPDFMDL")) {
													str += '<a href="javascript:mod()">修改</a><span class="btnSplit"></span>';
												}
												if (checkPower("DELFMDL")) {
													str += '<a href="javascript:del()">删除</a><span class="btnSplit"></span>';
												}
												if (checkPower("GETFMDL")) {
													str += '<a href="javascript:qry()">查看</a><span class="btnSplit"></span>';
												}
												return str;
											}
										} ] ]
							});

			$("#btnAdd").click(function() {
				if (!win_show_flag) {
					initWin();
					win_show_flag = true;
				}
				$('#win').window('setTitle', '新增附件目录规则');
				$('#win').window('open');
				$('#win').window('refresh', "fmdladd.html");
			})
		})

		var win_show_flag = false;
		var op_refcode = "";
		var op_row = null;
		function initWin() {
			$('#win').window({
				width : 600,
				height : 300,
				collapsible : false,
				minimizable : false,
				maximizable : false,
				resizable : false,
				closed : true,
				modal : true
			})
		}

		//修改
		function mod() {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', '修改附件目录规则信息');
			$('#win').window('open');
			$('#win').window('refresh', 'fmdlmod.html');
		}

		//删除
		function del(stu) {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', '删除附件目录规则信息');
			$('#win').window('open');
			$('#win').window('refresh', 'fmdldel.html');
		}

		/**
		 * 查看
		 */
		function qry() {
			op_row = $("#wxConfigManage").datagrid('getSelected');
			if (!win_show_flag) {
				initWin();
				win_show_flag = true;
			}
			$('#win').window('setTitle', '查看系统信息');
			$('#win').window('open');
			$('#win').window('refresh', 'fmdlqry.html');
		}

		function queryDg() {
			$("#wxConfigManage").datagrid("options").queryParams = $(
					"#queryForm").serializeObject();
			$("#wxConfigManage").datagrid("load");
		}

		function resetDg() {
			$("#queryForm").form('reset');
			$("#wxConfigManage").datagrid("options").queryParams = {};
			$("#wxConfigManage").datagrid("load");
		}
		function retBack() {
			$("#returnBtn").click(function() {
				$('#win').window('close');
			});
		}
		function queryFmdlTree() {
			parent.addTab("fmdlTree","/basicrule/fmdlinfo/fmdltree.html","查看字段目录树");
			parent.$('#tabs').tabs('select', "查看字段目录树");
		}