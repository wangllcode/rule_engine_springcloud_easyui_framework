$(function() {
			commonViewLoadForm('deleteForm',op_row)
			var statusText = status == 1 ?"启用":"禁用";
			$("#delBtn").text(statusText);
			$("#delBtn").click(function() {
				var req = {
					useToken : true
				};
				req['reqJSON'] = {
					url : BUS_URL + 'rbchinfo/removeRbchById?bchId='+op_row.bchId+'&isDisble='+status,
					type : 'delete'
				};
				req['successFn'] = function(respData) {
					if (respData.code == SUCCESS_CODE) {
						$("#win").window('close');
						$("#wxConfigManage").datagrid("load");
					} else {
						$.messager.alert('提示', respData.message);
					}
				};
				ajaxRequest(req);
			})
		});