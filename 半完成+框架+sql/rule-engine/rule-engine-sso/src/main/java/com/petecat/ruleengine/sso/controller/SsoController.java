/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  SsoController.java   
 * @Package com.petecat.ruleengine.sso.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 下午8:35:17   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.sso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.sso.dto.LoginDTO;
import com.petecat.ruleengine.protocol.sso.vo.SsoUserAuthInfoVO;
import com.petecat.ruleengine.protocol.sso.vo.SsoUserInfoVO;
import com.petecat.ruleengine.sso.comp.SSOComp;

import net.hryx.sso.bean.TokenCheck;
import net.hryx.sso.result.util.AESUtils;
import net.hryx.sso.vo.SSOUserAuth;
import net.hryx.sso.vo.SSOUserVo;

/**   
 * @ClassName:  SsoController   
 * @Description:用户登录  
 * @author: admin
 * @date:   2017年12月6日 下午8:35:17   
 */
@RestController
@RequestMapping("/user")
public class SsoController extends BaseController{


	@Autowired
	private SystemRedisTemplate systemRedisTemplate;
	
	@Autowired
	private SSOComp ssoComp;
	
	@Autowired
	private Environment environment;
	
	/**   
	 * @Title: ssoLogin   
	 * @Description:sso登录  
	 * @param dto
	 * @return Result<SsoUserInfoVO>     
	 * @throws Exception 
	 */
	@PostMapping("/ssoLogin")
	public Result<SsoUserInfoVO> ssoLogin(@RequestBody LoginDTO dto) throws Exception{
		TokenCheck tokenCheck = this.ssoComp.getTokenCheck(dto.getMobilephone());
		net.hryx.sso.result.Result<SSOUserVo> loginResult = ssoComp.getAburSSOService().
				loginUserForJwt(tokenCheck,dto.getMobilephone(), 
				AESUtils.encrypt(environment.getProperty("SSO_PASSWORD_ENCRYPT"),
				dto.getPassword()),environment.getProperty("SSO_SYSTEMCODE"));
		if(loginResult.isSuccess()) {
			SsoUserInfoVO userVo = new SsoUserInfoVO();
			DataCopy.copyData(loginResult.getValue(), userVo);
			//加载用户权限
			net.hryx.sso.result.Result<List<SSOUserAuth>> authResult = ssoComp.getAburSSOService().
					getUserPermissionSystem(tokenCheck, userVo.getUserId(), environment.getProperty("SSO_SYSTEMCODE"));
			if(authResult.isSuccess()) {
				List<SSOUserAuth> auths = authResult.getValue();
				List<SsoUserAuthInfoVO> authInfos = DataCopy.copFixList(auths,SsoUserAuthInfoVO.class);
				userVo.setAuthInfos(authInfos);
			}
			net.hryx.sso.result.Result<List<String>> authVistUrlResults = ssoComp.getAburSSOService().queryUserOnlyUniqueVistUrls(tokenCheck, userVo.getUserId(), environment.getProperty("SSO_SYSTEMCODE"));
			if(authVistUrlResults.isSuccess()) {
				systemRedisTemplate.hSetListObject(SystemConstant.USER_VISTS_AUTH_LIST_KEY, userVo.getUserId(), authVistUrlResults.getValue());
			}
			return Result.success(userVo);
		}else {
			return Result.error(loginResult.getError().getCode(), loginResult.getError().getMessage());
		}
	}
	
}
