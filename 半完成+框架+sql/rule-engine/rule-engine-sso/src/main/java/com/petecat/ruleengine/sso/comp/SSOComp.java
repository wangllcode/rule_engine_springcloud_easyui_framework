package com.petecat.ruleengine.sso.comp;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.caucho.hessian.client.HessianProxyFactory;

import net.hryx.sso.bean.TokenCheck;
import net.hryx.sso.intefaces.IAburSSOService;
import net.hryx.sso.intefaces.IToken;
import net.hryx.sso.result.util.MD5;

@Component("ssoComp")
public class SSOComp{
	private IAburSSOService aburSSOService;
	
	private  IToken iToken  = null;
	
	@Autowired
	private Environment environment;
	
	/**   
	 * @Title: getAburSSOService   
	 * @Description: 获取用户服务  
	 * @throws Exception
	 * @return IAburSSOService     
	 */
	public  IAburSSOService getAburSSOService() throws Exception{
			if(this.aburSSOService==null){
				synchronized(this){
					if(this.aburSSOService==null){
						HessianProxyFactory factory = new HessianProxyFactory();
				        factory.setOverloadEnabled(true);
				        factory.setDebug(false);
				        String tokenUrl = environment.getProperty("SSO_ABUR_URL");
				        aburSSOService = (IAburSSOService) factory.create(IAburSSOService.class, tokenUrl, SSOComp.class.getClassLoader());
					}
				}
			}
		return this.aburSSOService;
		
	}



	/**   
	 * @Title: getTokenCheck   
	 * @Description: 获取校验服务
	 * @param token
	 * @return TokenCheck     
	 * @throws Exception 
	 */
	public  TokenCheck getTokenCheck(String token) throws Exception{
		    TokenCheck check = new TokenCheck();
	        check.setToken(token);
			check.setNow(new Date().getTime());
			String tokenPassword = environment.getProperty("SSO_TOKEN_PASSWORD");
	        check.setKeycode(MD5.getMD5(token + check.getNow() +tokenPassword));
	        return check;
	}
	

	/**   
	 * @Title: getIToken   
	 * @Description: 获取token校验服务
	 * @throws Exception
	 * @return IToken     
	 */
	public IToken getIToken() throws Exception{
			if(this.iToken==null){
				synchronized(this){
					if(this.iToken==null){
						HessianProxyFactory factory = new HessianProxyFactory();
				        factory.setOverloadEnabled(true);
				        factory.setDebug(false);
				        String tokenUrl =  environment.getProperty("SSO_CHECK_TOKEN_URL");
				        iToken = (IToken) factory.create(IToken.class, tokenUrl);
					}
				}
			}
		return this.iToken;
		
	}
}
