/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  IAexgInfoService.java   
 * @Package com.petecat.ruleengine.dictionary.aexginfo.service.impl   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:51:55   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aareinfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.dictionary.areainfo.dto.BpsaaremDTO;

/**   
 * @ClassName:  IAareInfoService   
 * @Description:区域字典服务类 
 * @author: admin
 * @date:   2017年12月11日 上午10:51:55   
 */
public interface IAareInfoService {

	/**   
	 * @Title: reloadAare   
	 * @Description: 重新加载区域
	 * @return void     
	 */
	void reloadAare();

	/**   
	 * @Title: listAareProvList   
	 * @Description: 加载省
	 * @return List<BpsaaremDTO>     
	 */
	List<BpsaaremDTO> listAareProvList();

	/**   
	 * @Title: listAareCityList   
	 * @Description: 加载市
	 * @param areacode
	 * @return List<BpsaaremDTO>     
	 */
	List<BpsaaremDTO> listAareCityList(String areacode);

	/**   
	 * @Title: listAareCountyList   
	 * @Description: 加载区域
	 * @param areacode
	 * @return List<BpsaaremDTO>     
	 */
	List<BpsaaremDTO> listAareCountyList(String areacode);
    
}
