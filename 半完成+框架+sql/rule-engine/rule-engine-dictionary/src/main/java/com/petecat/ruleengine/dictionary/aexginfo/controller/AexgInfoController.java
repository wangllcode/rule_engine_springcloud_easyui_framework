/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  AexgInfoController.java   
 * @Package com.petecat.ruleengine.dictionary.aexginfo.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:58:56   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aexginfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.dictionary.aexginfo.service.IAexgInfoService;
import com.petecat.ruleengine.protocol.dictionary.aexginfo.dto.BpsaexgmQryDTO;
import com.petecat.ruleengine.protocol.dictionary.aexginfo.vo.BpsaexgmQryVO;
import com.petecat.ruleengine.protocol.global.Result;

/**   
 * @ClassName:  AexgInfoController   
 * @Description:数据字典控制
 * @author: admin
 * @date:   2017年12月11日 上午10:58:56   
 */
@RestController
@RequestMapping("/aexginfo")
public class AexgInfoController extends BaseController {

	@Autowired
	private IAexgInfoService aexgInfoService;
	
	/**   
	 * @Title: reloadDictory   
	 * @Description:重新加载数据字典
	 * @return Result<?>     
	 */
	@GetMapping("/reloadDictory")
	public Result<?> reloadDictory(){
		aexgInfoService.reloadDictory();
		return Result.success();
	}
	/**   
	 * @Title: queryAexginfoByType   
	 * @Description:加载数据字典
	 * @return Result<?>     
	 */
	@GetMapping("/queryAexginfoByType")
	public Result<?> queryAexginfoByType(String parmType){
		List<BpsaexgmQryDTO> dtos = aexgInfoService.listAexginfoByType(parmType);
		return Result.success(DataCopy.copFixList(dtos, BpsaexgmQryVO.class));
	}
}
