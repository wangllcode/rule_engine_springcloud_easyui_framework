package com.petecat.ruleengine.dictionary.aexginfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.dictionary.aexginfo.entity.Bpsaexgm;
import com.petecat.ruleengine.dictionary.aexginfo.entity.BpsaexgmKey;
@Mapper
public interface IAexgInfoMapper extends BaseMapper<Bpsaexgm,BpsaexgmKey>{

	/**   
	 * @Title: listAllAexg   
	 * @Description: 加载所有的aexg  
	 * @return List<Bpsaexgm>     
	 */
	List<Bpsaexgm> listAllAexg();

	/**   
	 * @Title: listAexginfoByType   
	 * @Description: 查询数据字典
	 * @param parmType
	 * @return List<Bpsaexgm>     
	 */
	List<Bpsaexgm> listAexginfoByType(String parmtype);
	
}