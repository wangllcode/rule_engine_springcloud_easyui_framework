package com.petecat.ruleengine.dictionary.aareinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.dictionary.aareinfo.entity.Bpsaarem;
import com.petecat.ruleengine.dictionary.aexginfo.entity.Bpsaexgm;
import com.petecat.ruleengine.dictionary.aexginfo.entity.BpsaexgmKey;
@Mapper
public interface IAareInfoMapper extends BaseMapper<Bpsaexgm,BpsaexgmKey>{


	/**   
	 * @Title: listAareinfo 
	 * @Description: 查询所有区域
	 * @return List<Bpsaarem>     
	 */
	List<Bpsaarem> listAareinfo();
	
}