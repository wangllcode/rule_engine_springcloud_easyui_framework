/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  IAexgInfoService.java   
 * @Package com.petecat.ruleengine.dictionary.aexginfo.service.impl   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:51:55   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aexginfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.dictionary.aexginfo.dto.BpsaexgmQryDTO;

/**   
 * @ClassName:  IAexgInfoService   
 * @Description:数据字典服务类 
 * @author: admin
 * @date:   2017年12月11日 上午10:51:55   
 */
public interface IAexgInfoService {

	/**   
	 * @Title: reloadDictory   
	 * @Description: 重新加载数据字典
	 * @return void     
	 */
	void reloadDictory();

	/**   
	 * @Title: listAexginfoByType   
	 * @Description: 
	 * @param parmType
	 * @return List<BpsaexgmQryDTO>     
	 */
	List<BpsaexgmQryDTO> listAexginfoByType(String parmType);
    
}
