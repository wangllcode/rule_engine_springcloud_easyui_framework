/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  AexgInfoService.java   
 * @Package com.petecat.ruleengine.dictionary.aexginfo.service.impl   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:54:57   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aexginfo.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.dictionary.aexginfo.entity.Bpsaexgm;
import com.petecat.ruleengine.dictionary.aexginfo.mapper.IAexgInfoMapper;
import com.petecat.ruleengine.dictionary.aexginfo.service.IAexgInfoService;
import com.petecat.ruleengine.protocol.dictionary.aexginfo.dto.BpsaexgmQryDTO;

/**   
 * @ClassName:  AexgInfoService   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: admin
 * @date:   2017年12月11日 上午10:54:57   
 */
@Service
public class AexgInfoService implements IAexgInfoService,InitializingBean{

	@Autowired
	private IAexgInfoMapper aexgInfoMapper;
	

	@Autowired
	private SystemRedisTemplate systemRedisTemplate;
	
	/**   
	 * <p>Title: reloadDictory</p>   
	 * <p>Description: </p>      
	 * @see com.petecat.ruleengine.dictionary.aexginfo.service.IAexgInfoService#reloadDictory()   
	 */  
	@Override
	public void reloadDictory() {
		List<Bpsaexgm> aexgms = aexgInfoMapper.listAllAexg();
		Map<String,List<Bpsaexgm>> paramTypes = aexgms.parallelStream().collect(Collectors.groupingBy(Bpsaexgm::getParmtype));
		paramTypes.forEach((key,value)->{
			systemRedisTemplate.hSetListObject(SystemConstant.SYS_DICTIONARY_KEY, key, value);
		});
	}

	/**   
	 * <p>Title: afterPropertiesSet</p>   
	 * <p>Description: </p>   
	 * @throws Exception   
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()   
	 */  
	@Override
	public void afterPropertiesSet() throws Exception {
		reloadDictory();
	}

	/**   
	 * <p>Title: listAexginfoByType</p>   
	 * <p>Description: </p>   
	 * @param parmType
	 * @return   
	 * @see com.petecat.ruleengine.dictionary.aexginfo.service.IAexgInfoService#listAexginfoByType(java.lang.String)   
	 */  
	@Override
	public List<BpsaexgmQryDTO> listAexginfoByType(String parmType) {
		List<Bpsaexgm> aexgms = aexgInfoMapper.listAexginfoByType(parmType);
		return systemRedisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, parmType, BpsaexgmQryDTO.class);
	}

}
