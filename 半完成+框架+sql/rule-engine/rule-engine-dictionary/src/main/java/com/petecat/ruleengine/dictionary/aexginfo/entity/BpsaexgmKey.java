package com.petecat.ruleengine.dictionary.aexginfo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * @author 
 */
@Data
public class BpsaexgmKey implements Serializable {
    /**
     * 参数类别
     */
    private String parmtype;

    /**
     * 参数代号
     */
    private String parmname;

    private static final long serialVersionUID = 1L;
}