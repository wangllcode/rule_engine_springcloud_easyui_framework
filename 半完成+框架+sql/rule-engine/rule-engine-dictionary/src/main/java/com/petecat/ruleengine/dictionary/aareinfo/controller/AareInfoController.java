/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  AexgInfoController.java   
 * @Package com.petecat.ruleengine.dictionary.aexginfo.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:58:56   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aareinfo.controller;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService;
import com.petecat.ruleengine.protocol.dictionary.areainfo.dto.BpsaaremDTO;
import com.petecat.ruleengine.protocol.dictionary.areainfo.vo.BpsaaremVO;
import com.petecat.ruleengine.protocol.global.Result;

/**   
 * @ClassName:  AareInfoController   
 * @Description:区域数据字典
 * @author: admin
 * @date:   2017年12月11日 上午10:58:56   
 */
@RestController
@RequestMapping("/areainfo")
@Validated
public class AareInfoController extends BaseController {

	@Autowired
	private IAareInfoService areaInfoService;
	
	/**   
	 * @Title: reloadAare  
	 * @Description:重新加载区域
	 * @return Result<?>     
	 */
	@GetMapping("/reloadAare")
	public Result<?> reloadAare(){
		areaInfoService.reloadAare();
		return Result.success();
	}
	/**   
	 * @Title: queryAareProvList  
	 * @Description:加载省
	 * @return Result<?>     
	 */
	@GetMapping("/queryAareProvList")
	public Result<List<BpsaaremVO>> queryAareProvList(){
		List<BpsaaremDTO> dtos = areaInfoService.listAareProvList();
		return Result.success(DataCopy.copFixList(dtos, BpsaaremVO.class));
	}
	
	/**   
	 * @Title: queryAareCityList  
	 * @Description:加载市
	 * @return Result<?>     
	 */
	@GetMapping("/queryAareCityList")
	public Result<List<BpsaaremVO>> queryAareCityList(String areacode){
		List<BpsaaremDTO> dtos = areaInfoService.listAareCityList(areacode);
		return Result.success(DataCopy.copFixList(dtos, BpsaaremVO.class));
	}
	

	/**   
	 * @Title: queryAareCityList  
	 * @Description:加载县
	 * @return Result<?>     
	 */
	@GetMapping("/queryAareCountyList")
	public Result<List<BpsaaremVO>> queryAareCountyList(@NotBlank String areacode){
		List<BpsaaremDTO> dtos = areaInfoService.listAareCountyList(areacode);
		return Result.success(DataCopy.copFixList(dtos, BpsaaremVO.class));
	}
	
}
