/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  AareInfoService.java   
 * @Package com.petecat.ruleengine.dictionary.areainfo.service.impl   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 上午10:54:57   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.dictionary.aareinfo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.dictionary.aareinfo.entity.Bpsaarem;
import com.petecat.ruleengine.dictionary.aareinfo.mapper.IAareInfoMapper;
import com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService;
import com.petecat.ruleengine.protocol.dictionary.areainfo.dto.BpsaaremDTO;

/**   
 * @ClassName:  AareInfoService   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: admin
 * @date:   2017年12月11日 上午10:54:57   
 */
@Service
public class AareInfoService implements IAareInfoService,InitializingBean{

	@Autowired
	private IAareInfoMapper areaInfoMapper;

	@Autowired
	private SystemRedisTemplate systemRedisTemplate;

	/**   
	 * <p>Title: afterPropertiesSet</p>   
	 * <p>Description: </p>   
	 * @throws Exception   
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()   
	 */  
	@Override
	public void afterPropertiesSet() throws Exception {
		reloadAare();
	}

	/**   
	 * <p>Title: reloadAare</p>   
	 * <p>Description: </p>      
	 * @see com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService#reloadAare()   
	 */  
	@Override
	public void reloadAare() {
		List<Bpsaarem>  aares = this.areaInfoMapper.listAareinfo();
		//组装省
		List<Bpsaarem> provs =  aares.parallelStream().filter(t->t.getAreacode().endsWith("0000")).
				sorted((t1,t2)->t1.getAreacode().compareTo(t2.getAreacode())).collect(Collectors.toList());
		this.systemRedisTemplate.hSetListObject(SystemConstant.AARE_DATA_KEY, "PROV_DATA", provs);
		//组装省市
		provs.parallelStream().forEach(t->{
			List<Bpsaarem> cities = aares.parallelStream().filter(aare->aare.getAreacode().
					startsWith(t.getAreacode().substring(0, 2)) 
					&& !aare.getAreacode().equals(t.getAreacode())
					&& aare.getAreacode().endsWith("00")
					).sorted((t1,t2)->t1.getAreacode().compareTo(t2.getAreacode())).collect(Collectors.toList());
			systemRedisTemplate.hSetListObject(SystemConstant.AARE_DATA_KEY,t.getAreacode(), cities);
			cities.parallelStream().forEach(city->{
				List<Bpsaarem> counties = aares.parallelStream().filter(aare->aare.getAreacode().
						startsWith(city.getAreacode().substring(0, 4)) 
						&& !aare.getAreacode().equals(city.getAreacode())
						).sorted((t1,t2)->t1.getAreacode().compareTo(t2.getAreacode())).collect(Collectors.toList());
			     systemRedisTemplate.hSetListObject(SystemConstant.AARE_DATA_KEY,city.getAreacode(), counties);
			});
		});
		aares.parallelStream().forEach(t->{
			 systemRedisTemplate.hSetSingleObject(SystemConstant.ALL_AARE_DATA_KEY,t.getAreacode(), t);
		});
	}

	/**   
	 * <p>Title: listAareProvList</p>   
	 * <p>Description: </p>   
	 * @return   
	 * @see com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService#listAareProvList()   
	 */  
	@Override
	public List<BpsaaremDTO> listAareProvList() {
		return this.systemRedisTemplate.hGetListObject(SystemConstant.AARE_DATA_KEY, "PROV_DATA", BpsaaremDTO.class);
	}

	/**   
	 * <p>Title: listAareCityList</p>   
	 * <p>Description: </p>   
	 * @return   
	 * @see com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService#listAareCityList()   
	 */  
	@Override
	public List<BpsaaremDTO> listAareCityList(String areacode) {
		return this.systemRedisTemplate.hGetListObject(SystemConstant.AARE_DATA_KEY,areacode, BpsaaremDTO.class);
	}

	/**   
	 * <p>Title: listAareCountyList</p>   
	 * <p>Description: </p>   
	 * @param areacode
	 * @return   
	 * @see com.petecat.ruleengine.dictionary.aareinfo.service.IAareInfoService#listAareCountyList(java.lang.String)   
	 */  
	@Override
	public List<BpsaaremDTO> listAareCountyList(String areacode) {
		return this.systemRedisTemplate.hGetListObject(SystemConstant.AARE_DATA_KEY,areacode, BpsaaremDTO.class);
	}
	

}
