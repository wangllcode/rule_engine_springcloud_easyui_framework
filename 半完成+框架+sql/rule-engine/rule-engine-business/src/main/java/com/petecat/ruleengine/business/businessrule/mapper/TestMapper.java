/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  TestMapper.java   
 * @Package com.petecat.ruleengine.business.businessrule.mapper   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 上午12:09:57   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.business.businessrule.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.petecat.ruleengine.business.businessrule.entity.FormFieldDict;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldPageQryDTO;

/**   
 * @ClassName:  TestMapper   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: admin
 * @date:   2017年12月6日 上午12:09:57   
 */
/**
A) Service/DAO 层方法命名规约   
 1） 获取单个对象的方法用 get 做前缀。    
 2） 获取多个对象的方法用 list 做前缀。    
 3） 获取统计值的方法用 count 做前缀。   
 4） 插入的方法用 save/insert 做前缀。   
 5） 删除的方法用 remove/delete 做前缀。   
 6） 修改的方法用 update 做前缀。 
B) 领域模型命名规约    
  1） 数据对象：xxxDO，xxx 即为数据表名。   
  2） 数据传输对象：xxxDTO，xxx 为业务领域相关的名称。   
  3） 展示对象：xxxVO。
 */
@Mapper
public interface TestMapper extends BaseMapper<FormFieldDict,Long>{

	/**   
	 * @Title: countTestByPage   
	 * @Description: 分页查询时候的总记录条数   
	 * @param queryDto
	 * @return int     
	 */
	int countTestByPage(FormFieldPageQryDTO queryDto);

	/**   
	 * @Title: listTestByPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param queryDto
	 * @return List<FormFieldDict>     
	 */
	List<FormFieldDict> listTestByPage(FormFieldPageQryDTO queryDto);

	/**   
	 * @Title: deleteTest   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param refcode
	 * @return void     
	 */
	void deleteTest(long refcode);

	/**   
	 * @Title: getTest   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param refcode
	 * @return
	 * @return FormFieldDict     
	 */
	FormFieldDict getTest(long refcode);


}
