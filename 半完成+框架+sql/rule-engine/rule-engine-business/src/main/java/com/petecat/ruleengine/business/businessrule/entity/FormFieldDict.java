package com.petecat.ruleengine.business.businessrule.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author 
 */
@Data
public class FormFieldDict implements Serializable {
    /**
     * 主键值
     */
    private Long refcode;

    /**
     * 属性代码
     */
    private String fieldcode;

    /**
     * 属性名称
     */
    private String fieldname;

    /**
     * 字段类型（0 字符串，1 数字，2 日期，3 日期带时间）
     */
    private String fieldtype;

    /**
     * 适合的种类0 input 1 textarea  2 select 3 radio 9 其他
     */
    private String fittype;

    /**
     * 表名称
     */
    private String tablename;

    /**
     * 备注
     */
    private String remark;

    /**
     * 表说明
     */
    private String tabledesc;

    /**
     * 最后修改人
     */
    private String lastmoduser;

    /**
     * 最后修改时间
     */
    private Date lastmoddate;

    private static final long serialVersionUID = 1L;

}