/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  TestService.java   
 * @Package com.petecat.ruleengine.business.businessrule.service   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月6日 上午12:07:38   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.business.businessrule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.business.businessrule.entity.FormFieldDict;
import com.petecat.ruleengine.business.businessrule.mapper.TestMapper;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldAddDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldModDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldPageQryDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldQryDTO;
import com.petecat.ruleengine.protocol.business.businessrule.vo.FormFieldPageQryResultVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

/**   
 * @ClassName:  TestService   
 * @Description:测试服务 
 * @author: admin
 * @date:   2017年12月6日 上午12:07:38   
 */
/**
A) Service/DAO 层方法命名规约   
 1） 获取单个对象的方法用 get 做前缀。    
 2） 获取多个对象的方法用 list 做前缀。    
 3） 获取统计值的方法用 count 做前缀。   
 4） 插入的方法用 save/insert 做前缀。   
 5） 删除的方法用 remove/delete 做前缀。   
 6） 修改的方法用 update 做前缀。 
B) 领域模型命名规约    
  1） 数据对象：xxxDO，xxx 即为数据表名。   
  2） 数据传输对象：xxxDTO，xxx 为业务领域相关的名称。   
  3） 展示对象：xxxVO。
 */
@Service
public class TestService extends BaseService<FormFieldDict, Long> {

	@Autowired
	private TestMapper testMapper;
	
	/**   
	 * @Title: listTestByPage   
	 * @Description: 分页查询案例
	 * @param queryDto
	 * @return PageVO<FormFieldPageQryResultVO>     
	 */
	public PageVO<FormFieldPageQryResultVO> listTestByPage(FormFieldPageQryDTO queryDto) {
		int size = testMapper.countTestByPage(queryDto);
		List<FormFieldDict> dicts = this.testMapper.listTestByPage(queryDto);
		PageVO<FormFieldPageQryResultVO> pageVo = new PageVO<>();
		pageVo.setTotal(size);
		pageVo.setRows(DataCopy.copFixList(dicts, FormFieldPageQryResultVO.class));
		return pageVo;
	}

	/**   
	 * @Title: saveTest   
	 * @Description: 保存
	 * @param addDto
	 * @return void     
	 * @throws Exception 
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveTest(FormFieldAddDTO addDto) throws Exception {
		FormFieldDict dict = new FormFieldDict();
		DataCopy.copyData(addDto,dict);
		dict.setRefcode(this.getDefaultPrimaryKey());
		this.testMapper.insert(dict);
	}

	/**   
	 * @Title: deleteTest   
	 * @Description: 
	 * @param refcode
	 * @return void     
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void deleteTest(long refcode) {
		this.testMapper.deleteTest(refcode);
	}

	/**   
	 * @Title: getTest   
	 * @Description: 查询数据
	 * @param refcode
	 * @return FormFieldQryDTO     
	 */
	public FormFieldQryDTO getTest(long refcode) {
		FormFieldDict dict  = this.testMapper.getTest(refcode);
		FormFieldQryDTO dto =  new FormFieldQryDTO();
		 DataCopy.copyData(dict,dto);
		 return dto;
	}

	/**   
	 * @Title: updateTest   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param modDto
	 * @return void     
	 */
	@Transactional(propagation=Propagation.REQUIRED)
	public void updateTest(FormFieldModDTO modDto) {
		FormFieldDict dict = new FormFieldDict();
		DataCopy.copyData(modDto,dict);
		this.testMapper.updateByPrimaryKeySelective(dict);
	}
   
}
