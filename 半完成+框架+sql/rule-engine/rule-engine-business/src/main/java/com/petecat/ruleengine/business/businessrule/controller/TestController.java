/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  TestController.java   
 * @Package com.petecat.ruleengine.business.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午8:51:29   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.business.businessrule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.business.businessrule.service.TestService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldAddDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldModDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldPageQryDTO;
import com.petecat.ruleengine.protocol.business.businessrule.dto.FormFieldQryDTO;
import com.petecat.ruleengine.protocol.business.businessrule.vo.FormFieldPageQryResultVO;
import com.petecat.ruleengine.protocol.business.businessrule.vo.FormFieldQryVO;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

/**   
 * @ClassName:  TestController   
 * @Description:测试controller
 * @author: admin
 * @date:   2017年12月5日 下午8:51:29   
 */
/**
 * 
B) 领域模型命名规约    
1） 数据对象：xxxDO，xxx 即为数据表名。   
2） 数据传输对象：xxxDTO，xxx 为业务领域相关的名称。   
3） 展示对象：xxxVO。
*
* control层中
* 分页查询方法使用query***ByPage
* 跳转页面使用load***Page
* 查询单条记录使用get**(By**)
* 查询多条记录使用find***(By**)或者是query
*/
@RestController
@RequestMapping("/test")
@Validated
public class TestController extends BaseController{
	
	@Autowired
	private TestService testService;
	
	/**   
	 * @Title: queryTestByPage   
	 * @Description: 分页查询案例 
	 * @param queryDto
	 * @return Result<PageVO<FormFieldPageQryResultVO>>     
	 */
	@GetMapping("/queryTestByPage")
	public Result<PageVO<FormFieldPageQryResultVO>> queryTestByPage(@Validated @ModelAttribute FormFieldPageQryDTO queryDto) {
		PageVO<FormFieldPageQryResultVO> result = this.testService.listTestByPage(queryDto);
		return Result.success(result);
	}
	

	/**   
	 * @Title: saveTest   
	 * @Description: 保存案例
	 * @param queryDto
	 * @return result
	 * @throws Exception 
	 */
	@PostMapping("/saveTest")
	public Result<?> saveTest( @RequestBody FormFieldAddDTO addDto) throws Exception {
		addDto.setFieldtype("11");
		this.testService.saveTest(addDto);
		return Result.success();
	}
	
	/**   
	 * @Title: deleteTest   
	 * @Description: 保存案例
	 * @param refcode
	 * @return result
	 */
	@DeleteMapping("/deleteTest")
	public Result<?> deleteTest(long refcode) {
		this.testService.deleteTest(refcode);
		return Result.success();
	}
	
	
	/**   
	 * @Title: getTest   
	 * @Description: 保存案例
	 * @param refcode
	 * @return result
	 */
	@GetMapping("/getTest")
	public Result<FormFieldQryVO> getTest(long refcode) {
		FormFieldQryDTO queryDto = this.testService.getTest(refcode);
		FormFieldQryVO vo = new FormFieldQryVO();
		DataCopy.copyData(queryDto, vo);
		return Result.success(vo);
	}
	
	/**   
	 * @Title: updateTest   
	 * @Description: 保存案例
	 * @param queryDto
	 * @return result
	 */
	@PostMapping("/updateTest")
	public Result<?> updateTest(@RequestBody FormFieldModDTO modDto) {
		modDto.setFieldtype("11");
		this.testService.updateTest(modDto);
		return Result.success();
	}
}
