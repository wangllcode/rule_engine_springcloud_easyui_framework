package com.petecat.ruleengine.core.uuid;

import com.petecat.ruleengine.core.util.IdWorker;

public class WorkThread{
	private IdWorker idWorker;
	private long startTime;
	private boolean idle=true;
	/**  
	 * @Title:  getStartTime
	 * @Description:获取startTime的值
	 * @return: long
	 */
	public long getStartTime() {
		return startTime;
	}
	/**  
	 * @Title:  setStartTime
	 * @Description:设置startTime的值
	 * @return: long
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	/**  
	 * @Title:  getIdWorker
	 * @Description:获取idWorker的值
	 * @return: IdWorker
	 */
	public IdWorker getIdWorker() {
		return idWorker;
	}
	/**  
	 * @Title:  setIdWorker
	 * @Description:设置idWorker的值
	 * @return: IdWorker
	 */
	public void setIdWorker(IdWorker idWorker) {
		this.idWorker = idWorker;
	}
	/**  
	 * @Title:  isIdle
	 * @Description:获取idle的值
	 * @return: boolean
	 */
	public boolean isIdle() {
		return idle;
	}
	/**  
	 * @Title:  setIdle
	 * @Description:设置idle的值
	 * @return: boolean
	 */
	public void setIdle(boolean idle) {
		this.idle = idle;
	}
	
	
	
} 
