/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  RequestUtils.java   
 * @Package com.petecat.ruleengine.core.util   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午4:25:59   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

/**   
 * @ClassName:  RequestUtils   
 * @Description:请求工具
 * @author: admin
 * @date:   2017年12月5日 下午4:25:59   
 */
public class RequestUtils {

    /** 
     * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址, 
     * 参考文章： http://developer.51cto.com/art/201111/305181.htm 
     *  
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？ 
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。 
     *  
     * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130, 
     * 192.168.1.100 
     *  
     * 用户真实IP为： 192.168.1.110 
     *  
     * @param request 
     * @return 
     */  
    public static String getIpAddress(HttpServletRequest request) {  
        String ip = request.getHeader("x-forwarded-for");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        } 
        return "0:0:0:0:0:0:0:1".equals(ip)?"127.0.0.1":ip;
    }
    

	/**
	 * @Title: getRequestMap
	 * @Description: 获取request中基础数据参数
	 * @param request
	 * @return Map 返回类型
	 * @throws
	 */
	public static Map<String,String> getRequestMapForRequest(HttpServletRequest request) {
		Map<String,String> requestMap = new HashMap<String,String>();
		Map<String, String[]> map = request.getParameterMap();
		for (String key : map.keySet()) {
			String[] objs = map.get(key);
			if (objs != null) {
				if (objs.length == 1) {
					if (objs[0] == null || objs[0].trim().equals("")) {
						requestMap.put(key, null);
					} else {
						requestMap.put(key, objs[0]);
					}
				} else if (objs.length > 1) {
					StringBuffer sb = new StringBuffer();
					for (String value : objs) {
						if (StringUtils.isNotBlank(value)) {
							sb.append(value.trim()).append(",");
						}
					}
					if (sb.length() > 0) {
						requestMap.put(key, sb.substring(0, sb.length() - 1));
					} else {
						requestMap.put(key, "");
					}
				}
			} else {
				requestMap.put(key, null);
			}
		}
		return requestMap;
	}
 
}
