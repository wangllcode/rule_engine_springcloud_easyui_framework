/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  Snippet.java   
 * @Package com.petecat.riskmanage.config.spring   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月4日 下午6:49:14   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.petecat.ruleengine.core.message.ApplicationMessage;

/**   
 * @ClassName:  
 * @Description:系统初始化配置信息
 * @author: admin
 * @date:   2017年9月4日 下午6:49:14   
 */
@Component
public class SystemWebApplicationInitializer implements InitializingBean {
	
	@Autowired
	private Environment environment;

	
	private Log log = LogFactory.getLog(getClass());

	/**   
	 * <p>Title: afterPropertiesSet</p>   
	 * <p>Description: </p>   
	 * @throws Exception   
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()   
	 */  
	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("加载消息内容到ApplicationMessage中");
		ApplicationMessage.setEnvironment(environment);
	}
}
