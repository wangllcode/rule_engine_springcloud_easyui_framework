/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  DataCopy.java   
 * @Package com.petecat.formdesigner.util   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年11月27日 下午4:41:51   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;


/**   
 * @ClassName:  DataCopy   
 * @Description:数据copy
 * @author: admin
 * @date:   2017年11月27日 下午4:41:51   
 */
public class DataCopy {
	
   private static Logger logger = Logger.getLogger(DataCopy.class);
	
   public static <TD> List<TD> copyList(List<?> initDatas,Class<TD> cls){
	   List<TD> results = new ArrayList<>();
	   if(!CollectionUtils.isEmpty(initDatas)) {
		   initDatas.parallelStream().forEachOrdered((t)->{
			   TD obj = null;
				try {
					obj = cls.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					logger.error(e.getMessage(),e);
			   }
			   BeanUtils.copyProperties(t,obj);
			   results.add(obj);
		   });
	   }
	 return results;
   }

/**   
 * @Title: copyData   
 * @Description: 复制数据
 * @param source
 * @param dest
 * @return  t   
 */
public static void copyData(Object source, Object dest) {
	   List<Field> fields = FieldUtils.getAllFieldsList(source.getClass());
	   List<Field> destFields = FieldUtils.getAllFieldsList(dest.getClass());
	   fields.parallelStream().forEach(field->{
		   Optional<Field> setField =
				   destFields.parallelStream().filter(t->field.getName().equalsIgnoreCase(t.getName())).
				   findFirst();
		   if(setField.isPresent()) {
			   try {
				    field.setAccessible(true);
					FieldUtils.writeField(setField.get(),dest,field.get(source),true);
				} catch (Exception e) {
					try{
						field.setAccessible(true);
						FieldUtils.writeField(setField.get(),dest,
								DataConvert.changeValue(
										setField.get().getType().
										getSimpleName(),field.get(source),
										2, true));
					}catch(Exception e1){
						logger.warn(e1.getMessage());
					}
				}
		   }
	   });
}

/**   
 * @Title: copFixList   
 * @Description: 复制数组
 * @param auths
 * @param class1
 * @return List<SsoUserAuthInfoVO>     
 */
public static <T> List<T> copFixList(List<?> auths, Class<T> cls) {
	 List<T> results = new ArrayList<>();
	   if(!CollectionUtils.isEmpty(auths)) {
		   auths.parallelStream().forEachOrdered((t)->{
			   T obj = null;
				try {
					obj = cls.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					logger.error(e.getMessage(),e);
			   }
			   copyData(t,obj);
			   results.add(obj);
		   });
	   }
	 return results;
}

}
