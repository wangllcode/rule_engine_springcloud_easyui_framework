package com.petecat.ruleengine.core.config.db;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.zaxxer.hikari.HikariConfig;

@ConfigurationProperties("spring.datasource.hikari")
public class SystemHikariConfig extends HikariConfig{

}
