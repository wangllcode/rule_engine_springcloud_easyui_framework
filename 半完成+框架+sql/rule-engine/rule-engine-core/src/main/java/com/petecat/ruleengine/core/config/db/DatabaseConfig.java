/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  DBConfig.java   
 * @Package com.petecat.ruleengine.core.config.db   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午8:39:13   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.db;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;

/**   
 * @ClassName:  DatabaseConfig   
 * @Description:数据库配置
 * @author: admin
 * @date:   2017年12月5日 下午8:39:13   
 */
@Configuration
@EnableConfigurationProperties(SystemHikariConfig.class)
public class DatabaseConfig {
	@Bean
	@Primary
   @ConfigurationProperties(prefix="spring.datasource.hikari")
    public DataSource getDataSource(SystemHikariConfig config) {
		return	new HikariDataSource(config);
    }
	
}
