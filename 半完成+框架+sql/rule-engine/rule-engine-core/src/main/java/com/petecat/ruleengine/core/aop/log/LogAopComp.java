/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  LogInterceptor.java   
 * @Package com.petecat.ruleengine.core.interceptor   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月5日 下午2:30:38   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.aop.log;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.petecat.ruleengine.core.aop.log.handler.LogStoreHandler;
import com.petecat.ruleengine.core.aop.log.model.LogDataModel;
import com.petecat.ruleengine.core.util.RequestUtils;

/**   
 * @ClassName:  LogAopComp   
 * @Description:日志AOP
 * @author: admin
 * @date:   2017年12月5日 下午2:30:38   
 */
@Component
@Aspect
public class LogAopComp {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Pointcut("execution(* com.petecat..*.controller..*.*(..))")
	public void initLog() {
	}
  
	@Before(value="initLog()")
	public void beforeExec(JoinPoint joinPoint) {
		try {
			HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
			Object[] args =  joinPoint.getArgs();
			LogDataModel logDataModel = LogStoreHandler.gernateInitLog(joinPoint.getTarget().getClass().getName(), 
					joinPoint.getSignature().getName(), request);
			//组装输入参数
			if(ArrayUtils.isNotEmpty(args)){
				String inputObjectData = JSON.toJSONString(args);
				logDataModel.setWarpRequestData(inputObjectData);
			}
			LogStoreHandler.storeLog(logDataModel);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
	}
	 /**   
	 * @Title: afterExec   
	 * @Description: 正常退出
	 * @param joinPoint
	 * @param rvt
	 * @return void     
	 */
	 @AfterReturning(returning="rvt", pointcut="initLog()")
	 public void afterExec(JoinPoint joinPoint,Object rvt){
		 try {
			LogDataModel dataModel = LogStoreHandler.getLog();
			if (dataModel != null) {
				dataModel.setExecEndTime(System.nanoTime()/1000000);
				if (rvt != null && !rvt.getClass().isAssignableFrom(Void.class)) {
					dataModel.setResult(JSON.toJSONString(rvt));
				}
				LogStoreHandler.writerLog(dataModel);
			}
		}catch(Exception e) {
			logger.error(e.getMessage(),e);
		}
	 }
	
	/**   
	 * @Title: afterThrowingExec   
	 * @Description: 异常退出 
	 * @return void     
	 */
	 @AfterThrowing(pointcut="initLog()",throwing="ex")
	 public void afterThrowingExec(JoinPoint joinPoint,Throwable ex){
		 try {
				LogDataModel dataModel = LogStoreHandler.getLog();
				if (dataModel != null) {
					dataModel.setExecEndTime(System.nanoTime()/1000000);
					if (ex != null) {
						StringWriter writer = new StringWriter();
						ex.printStackTrace(new PrintWriter(writer));
						dataModel.setExceptionContent(writer.toString());
					}
					LogStoreHandler.writerLog(dataModel);
				}
			}catch(Exception e) {
				logger.error(e.getMessage(),e);
			}
	 }
	 
}
