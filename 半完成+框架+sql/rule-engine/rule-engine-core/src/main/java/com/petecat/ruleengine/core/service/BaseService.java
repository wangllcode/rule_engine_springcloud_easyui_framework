/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  BaseService.java   
 * @Package com.petecat.formdesigner.service   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年11月27日 下午3:28:55   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.core.exception.SystemException;
import com.petecat.ruleengine.core.uuid.UuidWorkComp;
import com.petecat.ruleengine.core.uuid.WorkThread;

/**   
 * @ClassName:  BaseService   
 * @Description:基础服务
 * @author: admin
 * @date:   2017年11月27日 下午3:28:55   
 */
@Transactional(readOnly=true)
public class BaseService <T,ID>{
	
	private Logger baseLogger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UuidWorkComp uuidWorkComp;
	/**   
	 * @Title: getDefaultPrimaryKey
	 * @Description:获取默认主键   
	 * @return long     
	 * @throws Exception 
	 */
	public long getDefaultPrimaryKey() {
		WorkThread thread = null;
		try {
			thread = uuidWorkComp.getIdWorker();
			baseLogger.debug("获取到的数据中心ID为：{}，工作号为：{}",thread.getIdWorker().getDatacenterId(),thread.getIdWorker().getWorkerId());
			return thread.getIdWorker().nextId();
		}catch(Exception e){
			throw new SystemException(9010,null,e);
		}
	}
	
	/**   
	 * @Title: getDefaultPrimaryKeyForChar
	 * @Description:获取默认主键   
	 * @return string     
	 * @throws Exception 
	 */
	public String getDefaultPrimaryKeyForChar() {
			return String.valueOf(getDefaultPrimaryKey());
	}
}
