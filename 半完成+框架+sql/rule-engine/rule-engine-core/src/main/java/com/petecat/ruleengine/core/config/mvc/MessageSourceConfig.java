/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  MessageSourceConfig.java   
 * @Package com.petecat.ruleengine.core.config.mvc   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月12日 下午3:25:00   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePropertySource;

/**   
 * @ClassName:  MessageSourceConfig   
 * @Description:
 * @author: admin
 * @date:   2017年12月12日 下午3:25:00   
 */
public class MessageSourceConfig implements SpringApplicationRunListener{
	
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public MessageSourceConfig(SpringApplication application, String[] args) {
		
	}
	/**   
	 * <p>Title: starting</p>   
	 * <p>Description: </p>      
	 * @see org.springframework.boot.SpringApplicationRunListener#starting()   
	 */  
	@Override
	public void starting() {
		
	}

	/**   
	 * <p>Title: environmentPrepared</p>   
	 * <p>Description: </p>   
	 * @param environment   
	 * @see org.springframework.boot.SpringApplicationRunListener#environmentPrepared(org.springframework.core.env.ConfigurableEnvironment)   
	 */  
	@Override
	public void environmentPrepared(ConfigurableEnvironment environment) {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] resources = resolver.getResources("classpath*:message/validate_*.properties");
			if(ArrayUtils.isNotEmpty(resources)) {
				List<String> validates = new ArrayList<>();
				for(Resource resource : resources) {
					logger.debug("加载验证配置文件："+resource.getURL().getPath());
					validates.add("message/".concat(resource.getFilename().substring(0,resource.getFilename().lastIndexOf("."))));
				}
				Properties data = new Properties();
				data.put("spring.messages.basename", StringUtils.join(validates,","));
				PropertiesPropertySource source = new PropertiesPropertySource("spring.validate.message.properties",data);
				environment.getPropertySources().addFirst(source);
			}
			
			resources = resolver.getResources("classpath*:message/message_*.properties");
			if(ArrayUtils.isNotEmpty(resources)) {
				for(Resource resource : resources) {
					logger.debug("加载系统消息码与消息配置文件："+resource.getURL().getPath());
					ResourcePropertySource source = new ResourcePropertySource(new EncodedResource(resource,"utf8"));
					environment.getPropertySources().addLast(source);
				}
			}
		} catch (Exception e) {
			logger.error("加载各种代码与消息配置文件失败",e);
			throw new ApplicationContextException("加载各种代码与消息配置文件失败");
		}
		
	}

	/**   
	 * <p>Title: contextPrepared</p>   
	 * <p>Description: </p>   
	 * @param context   
	 * @see org.springframework.boot.SpringApplicationRunListener#contextPrepared(org.springframework.context.ConfigurableApplicationContext)   
	 */  
	@Override
	public void contextPrepared(ConfigurableApplicationContext context) {
		
	}

	/**   
	 * <p>Title: contextLoaded</p>   
	 * <p>Description: </p>   
	 * @param context   
	 * @see org.springframework.boot.SpringApplicationRunListener#contextLoaded(org.springframework.context.ConfigurableApplicationContext)   
	 */  
	@Override
	public void contextLoaded(ConfigurableApplicationContext context) {
		
	}

	/**   
	 * <p>Title: finished</p>   
	 * <p>Description: </p>   
	 * @param context
	 * @param exception   
	 * @see org.springframework.boot.SpringApplicationRunListener#finished(org.springframework.context.ConfigurableApplicationContext, java.lang.Throwable)   
	 */  
	@Override
	public void finished(ConfigurableApplicationContext context, Throwable exception) {
		
	}

}
