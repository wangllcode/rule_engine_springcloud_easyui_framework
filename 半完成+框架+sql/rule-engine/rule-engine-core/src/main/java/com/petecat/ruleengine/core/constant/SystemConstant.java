/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  SystemConstant.java   
 * @Package com.petecat.ruleengine.core.constant   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 下午12:07:31   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.constant;

/**   
 * @ClassName:  SystemConstant   
 * @Description:系统常量
 * @author: admin
 * @date:   2017年12月11日 下午12:07:31   
 */
public class SystemConstant {
  public final static String SYS_DICTIONARY_KEY="SYS_DICTIONARY";
  public static final String NOT_LOGIN_URL_KEY = "NOT_LOGIN_URL";
  public static final String NOT_POWER_URL_KEY = "NOT_POWER_URL";
  public static final String USER_VISTS_AUTH_LIST_KEY = "USER_VISTS_AUTH_LIST";
  public static final String AARE_DATA_KEY = "AARE_DATA";
  public static final String ALL_AARE_DATA_KEY = "ALL_AARE_DATA";
}
