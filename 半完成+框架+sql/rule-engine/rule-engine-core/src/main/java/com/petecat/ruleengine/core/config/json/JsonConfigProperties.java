package com.petecat.ruleengine.core.config.json;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**   
 * @ClassName:  JsonConfigProperties   
 * @Description:json配置文件
 * @author: admin
 * @date:   2017年9月4日 下午2:42:45   
 */  
@Data
@ConfigurationProperties("fastjson")
public class JsonConfigProperties {
	
	/**   
	 * @Fields dateFormat :
	 */   
	private String dateFormat;
	
	private boolean fieldBase;
	
	private String[] serializerFeatures;
	
}