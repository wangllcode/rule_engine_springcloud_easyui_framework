package com.petecat.ruleengine.core.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DataConvert {
	
	 private static final Log logger = LogFactory.getLog(DataConvert.class);
	 
	 public static  Object changeValue(String datatype, Object object,int scale,boolean time) {
					if (null == object)
					{
					   return nullToObject(datatype);
					} else if ("string".equalsIgnoreCase(datatype))
					{
						return getObjectToString(object,time);
					}
		
					if("byte".equalsIgnoreCase(datatype)){
						return getObjectToByte(false,object);
					}
					else if("short".equalsIgnoreCase(datatype)){
						return getObjectToShort(false,object);
					}
					else if("int".equalsIgnoreCase(datatype)){
						return getObjectToInt(false,object);
					}
				    else if("long".equalsIgnoreCase(datatype)){
				    	return getObjectToLong(false,object);
				    }
					else if("float".equalsIgnoreCase(datatype)){
						return getObjectToFloat(false,object);
					}
					else if("double".equalsIgnoreCase(datatype)){
						return getObjectToDouble(false,object);
					}
					else if("char".equalsIgnoreCase(datatype)){
						return getObjectToChar(false,object);
					}
					else if("boolean".equalsIgnoreCase(datatype)){
						return getObjectToBoolean(false,object);
					}
					else if("Byte".equalsIgnoreCase(datatype)){
						return getObjectToByte(true,object);
					}
					else if("Short".equalsIgnoreCase(datatype)){
						return getObjectToShort(true,object);
					}
					else if("Integer".equalsIgnoreCase(datatype)){
						return getObjectToInt(true,object);
					}
					else if("Long".equalsIgnoreCase(datatype)){
						return getObjectToLong(true,object);
					}
					else if("Float".equalsIgnoreCase(datatype)){
						return getObjectToFloat(true,object);
					}
					else if("Double".equalsIgnoreCase(datatype)){
						return getObjectToDouble(true,object);
					}
					else if("Character".equalsIgnoreCase(datatype)){
						return getObjectToChar(true,object);
					}
					else if("Boolean".equalsIgnoreCase(datatype)){
						return getObjectToBoolean(true,object);
					}
					else if("BigDecimal".equalsIgnoreCase(datatype)){
						return getObjectToBigDecimal(object,scale);
					}
					else if("BigInteger".equalsIgnoreCase(datatype)){
						return getObjectToBigInteger(object);
					}else if("Date".equalsIgnoreCase(datatype)){
						return getObjectToDate(object,time);
					}else if("Timestamp".equalsIgnoreCase(datatype)){
						return getObjectToTimestamp(object,time);
					}else{
						if(object !=null){
							return object.toString();
						}else{
							return null;
						}
		}
	}

	
	private static String getObjectToString(Object object,boolean time) {
			if(object == null){
				return null;
			}
		try {
			 if(object instanceof Character || object.getClass()==Character.class){
				 Character character = (Character) object;
				 return character.toString();
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				return Boolean.toString((Boolean) object);
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				 Date value = (Date)object;
				 if(time){
					 String str = DateFormatUtils.format(value, "yyyy-MM-dd HH:mm:ss");
					 if(str.endsWith("00:00:00")){
						 return  DateFormatUtils.format(value, "yyyy-MM-dd");
					 }else{
						 return str;
					 }
				 }else{
					 return  DateFormatUtils.format(value, "yyyy-MM-dd");
				 }
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				Timestamp value = (Timestamp)object;
				 if(time){
					 return DateFormatUtils.format(value, "yyyy-MM-dd HH:mm:ss");
				 }else{
					 return  DateFormatUtils.format(value, "yyyy-MM-dd");
				 }
			}else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class ){
				BigDecimal value = (BigDecimal)object;
				return value.toPlainString();
			}
			else{
				return object.toString();
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Timestamp");
		}
		return "";
	}
	
	private static Timestamp getObjectToTimestamp(Object object,boolean time) {
		if(object == null){
			return null;
		}
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
			    	return null;
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				    return null;
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				return new Timestamp((Integer)object);
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				return new Timestamp((Long)object);
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				return new Timestamp((Long)object);
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				return new Timestamp((Long)object);
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				return null;
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				return null;
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
				return new Timestamp(value.longValue());
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
				return new Timestamp(value.longValue());
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				 Date value = (Date)object;
				 return  new Timestamp(value.getTime());
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				return (Timestamp) object;
			}
			else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 Date date = null;
				 try {
						date = DateUtils.parseDate(value, "yyyy-MM-dd HH:mm:ss");
						
					} catch (Exception e) {
						if(time){
							 date = DateUtils.parseDate(value, "yyyyMMdd HH:mm:ss");
						 }else{
							 date = DateUtils.parseDate(value, "yyyyMMdd");
						 }
				 }
				 return  new Timestamp(date.getTime());
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Timestamp");
		}
		return null;
	}


	private static Date getObjectToDate(Object object,boolean time) {
		if(object == null){
			return null;
		}
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
			    	return null;
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				    return null;
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				return new Date(Long.valueOf((Integer)object).longValue());
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				return new Date((Long)object);
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				return new Date((Long)object);
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				return new Date((Long)object);
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				return null;
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				return null;
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
				return new Date(value.longValue());
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
				return new Date(value.longValue());
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				return (Date) object;
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				 Timestamp value = (Timestamp)object;
				 return  new Date(value.getTime());
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 Date date = null;
				 try {
					 date = DateUtils.parseDate(value, "yyyy-MM-dd HH:mm:ss");
				} catch (Exception e) {
					if(time){
						 date = DateUtils.parseDate(value, "yyyyMMdd HH:mm:ss");
					 }else{
						 date = DateUtils.parseDate(value, "yyyyMMdd");
					 }
				}
				 return date;
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到date,"+e.getMessage());
		}
		return null;
	}


	private static BigInteger getObjectToBigInteger(Object object) {
		if(object == null){
			return null;
		}
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
			    	return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				return new BigInteger(String.valueOf(object));
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				Character charValue = (Character)object;
				int value = charValue;
				return new BigInteger(String.valueOf(value));
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				return null;
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
				return new BigInteger(String.valueOf(value.longValue()));
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				return (BigInteger) object;
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				Date value = (Date)object;
				return new BigInteger(String.valueOf(value.getTime()));
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				 Timestamp value = (Timestamp)object;
				 return new BigInteger(String.valueOf(value.getTime()));
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 return new BigInteger(value);
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到BigInteger");
		}
		return null;
	}


	private static BigDecimal getObjectToBigDecimal(Object object,int scale) {
		if(object == null){
			return null;
		}
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
				Byte value = (Byte)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				Short value = (Short)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				Integer value = (Integer)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				Long value = (Long)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				Float value = (Float)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				Double value = (Double)object;
			    BigDecimal decimal = new BigDecimal(value);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				  Character charValue = (Character)object;
				  int value = charValue;
				  BigDecimal decimal = new BigDecimal(value);
				  return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
				  
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				return null;
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
				return value;
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
				return new BigDecimal(value,scale);
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				Date value = (Date)object;
			    Long longValue = value.getTime();
			    BigDecimal decimal = new BigDecimal(longValue);
			    return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				  Timestamp value = (Timestamp)object;
				  Long longValue = value.getTime();
				  BigDecimal decimal = new BigDecimal(longValue);
				  return decimal.setScale(scale,BigDecimal.ROUND_HALF_UP);
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 return new BigDecimal(value);
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到BigDecimal");
		}
		return null;
	}

	private static Boolean getObjectToBoolean(boolean b, Object object) {
		if(object == null){
			if(!b){
				return false;
			}else{
				return new Boolean(false);
			}
		}
		try {
		    if(object instanceof Boolean || object.getClass()==Boolean.class){
				return (Boolean) object;
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 Boolean valueB =  new Boolean(value);
				 if(b){
					 return valueB;
				 }else{
					 return valueB.booleanValue();
				 }
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Boolean");
		}
		return false;
	}


	private static Object getObjectToChar(boolean b, Object object) {
		if(object == null){
			   return null;
		}
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
				byte value = (Byte) object;
				char valueChar = (char) value;
				Character charObj = new Character(valueChar);
				if(b){
					return charObj;
				}else{
					charObj.charValue();
				}
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				short value = (Short) object;
				char valueChar = (char) value;
				Character charObj = new Character(valueChar);
				if(b){
					return charObj;
				}else{
					charObj.charValue();
				}
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				int value = (Integer) object;
				char valueChar = (char) value;
				Character charObj = new Character(valueChar);
				if(b){
					return charObj;
				}else{
					charObj.charValue();
				}
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				Character value = (Character) object;
				if(b){
					return value;
				}else{
					value.charValue();
				}
				  
			}
			return null;
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Char");
		}
		return null;
	}

	private static Double getObjectToDouble(boolean b, Object object) {
		if(object == null){
			if(b){
			  return null;
			}else{
				return 0.0D;
			}
		}
		Double doubleValue = null;
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
				Byte value = (Byte)object;
				doubleValue = new Double(value);
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				Short value = (Short)object;
				doubleValue = new Double(value);
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				Integer value = (Integer)object;
				doubleValue = new Double(value);
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				Long value = (Long)object;
				doubleValue = new Double(value);
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				Float value = (Float)object;
				doubleValue = new Double(value);
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				doubleValue = (Double) object;
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				  Character charValue = (Character)object;
				  int value = charValue;
				  doubleValue =  new Double(value);
			}
			else if(object instanceof Boolean || object.getClass()==Boolean.class){
				  doubleValue =  new Double(0);
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
			    doubleValue =  new Double(value.doubleValue());
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
			    doubleValue =  new Double(value.doubleValue());
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 doubleValue =  new Double(value);
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				Date value = (Date)object;
				doubleValue =  new Double(value.getTime());
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				 Timestamp value = (Timestamp)object;
				 doubleValue =  new Double(value.getTime());
			}
			if(b){
				return doubleValue;
			}else{
				return doubleValue.doubleValue();
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Double");
		}
		return 0.0D;
	}

 private static Float getObjectToFloat(boolean b, Object object) {
		
    if(object == null){
	 if(b){
			  return null;
			}else{
				return 0.0F;
			}
		}
	    Float doubleValue = null;
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
				Byte value = (Byte)object;
				doubleValue = new Float(value);
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				Short value = (Short)object;
				doubleValue = new Float(value);
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				Integer value = (Integer)object;
				doubleValue = new Float(value);
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				Long value = (Long)object;
				doubleValue = new Float(value);
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				Float value = (Float)object;
				doubleValue = value;
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				Double value = (Double)object;
				doubleValue = new Float(value);
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				  Character charValue = (Character)object;
				  int value = charValue;
				  doubleValue =  new Float(value);
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				BigDecimal value = (BigDecimal)object;
			    doubleValue =  new Float(value.doubleValue());
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
			    doubleValue =  new Float(value.doubleValue());
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 doubleValue =  new Float(value);
			}
			else if(object instanceof Date || object.getClass()==Date.class){
				Date value = (Date)object;
				doubleValue =  new Float(value.getTime());
			}
			else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
				 Timestamp value = (Timestamp)object;
				 doubleValue =  new Float(value.getTime());
			}
			if(b){
				return doubleValue;
			}else{
				return doubleValue.floatValue();
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Float");
		}
		return 0.0F;
	}


	private static Long getObjectToLong(boolean b, Object object) {
		if(object == null){
		  if(b){
				 return null;
				 }else{
					return 0L;
			 }
			}
		    Long longValue = null;
			try {
				if(object instanceof Byte || object.getClass()==Byte.class){
					Byte value = (Byte)object;
					longValue = new Long(value);
				}
				else if(object instanceof Short || object.getClass()==Short.class){
					Short value = (Short)object;
					longValue = new Long(value);
				}
				else if(object instanceof Integer || object.getClass()==Integer.class){
					Integer value = (Integer)object;
					longValue = new Long(value);
				}
				else if(object instanceof Long || object.getClass()==Long.class){
					Long value = (Long)object;
					longValue = value;
				}
				else if(object instanceof Float || object.getClass()==Float.class){
					Float value = (Float)object;
					longValue = value.longValue();
				}
				else if(object instanceof Double || object.getClass()==Double.class){
					Double value = (Double)object;
					longValue = value.longValue();
				}
				else if(object instanceof Character || object.getClass()==Character.class){
					  Character charValue = (Character)object;
					  int value = charValue;
					  longValue =  new Long(value);
				}
				else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
					BigDecimal value = (BigDecimal)object;
					longValue =  value.longValue();
				}
				else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
					BigInteger value = (BigInteger)object;
					longValue =  value.longValue();
				}else if(object instanceof String || object.getClass()==String.class){
					 String value = (String)object;
					 longValue =  new Long(value);
				}else if(object instanceof Date || object.getClass()==Date.class){
					Date value = (Date)object;
					longValue =  new Long(value.getTime());
				}
				else if(object instanceof Timestamp || object.getClass()==Timestamp.class){
					 Timestamp value = (Timestamp)object;
					 longValue =  new Long(value.getTime());
				}
				if(b){
					return longValue;
				}else{
					return longValue.longValue();
				}
			} catch (Exception e) {
				logger.error("转换数据失败！"+object+"到Long");
			}
			return 0L;
	}


private static Integer getObjectToInt(boolean b, Object object) {
	        if(object == null){
				 if(b){
						  return null;
						}else{
							return 0;
						}
			}
		    Integer integerValue = null;
			try {
				if(object instanceof Byte || object.getClass()==Byte.class){
					Byte value = (Byte)object;
					integerValue = new Integer(value);
				}
				else if(object instanceof Short || object.getClass()==Short.class){
					Short value = (Short)object;
					integerValue = new Integer(value);
				}
				else if(object instanceof Integer || object.getClass()==Integer.class){
					integerValue = (Integer)object;
				}
				else if(object instanceof Long || object.getClass()==Long.class){
					Long value = (Long)object;
					integerValue = value.intValue();
				}
				else if(object instanceof Float || object.getClass()==Float.class){
					Float value = (Float)object;
					integerValue = value.intValue();
				}
				else if(object instanceof Double || object.getClass()==Double.class){
					Double value = (Double)object;
					integerValue = value.intValue();
				}
				else if(object instanceof Character || object.getClass()==Character.class){
					  Character charValue = (Character)object;
					  int value = charValue;
					  integerValue = value;
				}
				else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
					BigDecimal value = (BigDecimal)object;
					  integerValue = value.intValue();
				}
				else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
					BigInteger value = (BigInteger)object;
					integerValue = value.intValue();
				}else if(object instanceof String || object.getClass()==String.class){
					 String value = (String)object;
					 integerValue =  new Integer(value);
				}
				if(b){
					return integerValue;
				}else{
					return integerValue.intValue();
				}
			} catch (Exception e) {
				logger.error("转换数据失败！"+object+"到Integer");
      }
		   return 0;
	}


	private static Short getObjectToShort(boolean b, Object object) {
		  if(object == null){
				 if(b){
						  return null;
						}else{
							return 0;
						}
			}
		    Short integerValue = null;
			try {
				if(object instanceof Byte || object.getClass()==Byte.class){
					Byte value = (Byte)object;
					integerValue = new Short(value);
				}
				else if(object instanceof Short || object.getClass()==Short.class){
					integerValue = (Short)object;
				}
				else if(object instanceof Integer || object.getClass()==Integer.class){
					integerValue = ((Integer)object).shortValue();
				}
				else if(object instanceof Long || object.getClass()==Long.class){
					integerValue = ((Long)object).shortValue();
				}
				else if(object instanceof Float || object.getClass()==Float.class){
					integerValue = ((Float)object).shortValue();
				}
				else if(object instanceof Double || object.getClass()==Double.class){
					integerValue = ((Double)object).shortValue();
				}
				else if(object instanceof Character || object.getClass()==Character.class){
					  Character charValue = (Character)object;
					  int value = charValue;
					  integerValue = (short) value;
				}
				else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
					  BigDecimal value = (BigDecimal)object;
					  integerValue = value.shortValue();
				}
				else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
					BigInteger value = (BigInteger)object;
					integerValue = value.shortValue();
				}else if(object instanceof String || object.getClass()==String.class){
					 String value = (String)object;
					 integerValue =  new Short(value);
				}
				if(b){
					return integerValue;
				}else{
					return integerValue.shortValue();
				}
			} catch (Exception e) {
				logger.error("转换数据失败！"+object+"到short");
       }
		   return 0;
	}


	private static Byte getObjectToByte(boolean b, Object object) {
		 if(object == null){
			 if(b){
				  return null;
				 }
			  else{
				return 0;
			 }
		}
	    Byte integerValue = null;
		try {
			if(object instanceof Byte || object.getClass()==Byte.class){
				integerValue = (Byte)object;
			}
			else if(object instanceof Short || object.getClass()==Short.class){
				integerValue = ((Short)object).byteValue();
			}
			else if(object instanceof Integer || object.getClass()==Integer.class){
				integerValue = ((Integer)object).byteValue();
			}
			else if(object instanceof Long || object.getClass()==Long.class){
				integerValue = ((Long)object).byteValue();
			}
			else if(object instanceof Float || object.getClass()==Float.class){
				integerValue =((Float)object).byteValue();
			}
			else if(object instanceof Double || object.getClass()==Double.class){
				integerValue = ((Double)object).byteValue();
			}
			else if(object instanceof Character || object.getClass()==Character.class){
				  Character charValue = (Character)object;
				  int value = charValue;
				  integerValue = (byte) value;
			}
			else if(object instanceof BigDecimal || object.getClass()==BigDecimal.class){
				  BigDecimal value = (BigDecimal)object;
				  integerValue = ((BigDecimal)object).byteValue();
			}
			else if(object instanceof BigInteger || object.getClass()==BigInteger.class){
				BigInteger value = (BigInteger)object;
				 integerValue = ((BigInteger)object).byteValue();
			}else if(object instanceof String || object.getClass()==String.class){
				 String value = (String)object;
				 integerValue = new  Byte(value);
			}
			if(b){
				return integerValue;
			}else{
				return integerValue.byteValue();
			}
		} catch (Exception e) {
			logger.error("转换数据失败！"+object+"到Byte");
   }
	   return 0;
	}

	private static  Object nullToObject(String datatype)
	{

		if ("byte".equalsIgnoreCase(datatype))
		{
			return (byte) 0;
		} else if ("short".equalsIgnoreCase(datatype))
		{
			return (short)0;
		} else if ("int".equalsIgnoreCase(datatype))
		{
			return 0;
		} else if ("long".equalsIgnoreCase(datatype))
		{
			return 0L;
		} else if ("float".equalsIgnoreCase(datatype))
		{
			return 0F;
		} else if ("double".equalsIgnoreCase(datatype))
		{
			return 0D;
		}
		else if ("boolean".equalsIgnoreCase(datatype))
		{
			return false;
		}else if ("string".equals(datatype))
		{
			return "";
		}else if ("String".equals(datatype))
		{
			return null;
		}
		return null;
	}



	public static Object BlobToBytes(Blob blob) throws Exception {
		byte retArr[] = null;
		if(blob !=null){
		InputStream in = blob.getBinaryStream();
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		int c=in.read();//读取bis流中的下一个字节
		while(c!=-1){
		     baos.write(c);
		     c=in.read();
		}
		in.close();
		retArr = baos.toByteArray();
		}
		return retArr;
	}
}
