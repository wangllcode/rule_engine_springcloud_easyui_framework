package com.petecat.ruleengine.core.aop.log.filter;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.GenericFilterBean;

import com.petecat.ruleengine.core.aop.log.handler.LogStoreHandler;

/**   
 * @ClassName:  LogFilterConfig   
 * @Description:日志filter
 * @author: admin
 * @date:   2017年12月19日 上午11:12:23   
 */
@Configuration
public class LogFilter extends GenericFilterBean{

	/**   
	 * <p>Title: doFilter</p>   
	 * <p>Description: </p>   
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException   
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)   
	 */  
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			chain.doFilter(request, response);
		} finally {
			LogStoreHandler.remove();
		}
	}

}