package com.petecat.ruleengine.core.config.json;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageConverter;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;

@Configuration
@EnableConfigurationProperties(JsonConfigProperties.class)
public class JsonHttpMessageConvertConfiguration {
	
    /**   
     * @Title: jsonHttpConverter   
     * @Description: 配置自定义的jsonConvert
     * @return HttpMessageConverter<Object>     
     */
    @Bean
    @Order(0)
    public HttpMessageConverter<Object> jsonHttpConverter(JsonConfigProperties properties) {
    	FastJsonHttpMessageConverter convert = new FastJsonHttpMessageConverter();
    	FastJsonConfig fastJsonConfig = new FastJsonConfig();
    	SerializeConfig serializeConfig = new SerializeConfig(properties.isFieldBase());
    	fastJsonConfig.setSerializeConfig(serializeConfig);
    	fastJsonConfig.setDateFormat(properties.getDateFormat());
    	int count = 0;
    	String [] features = properties.getSerializerFeatures();
    	if( features != null) {
        	SerializerFeature [] sFeatures = new SerializerFeature[features.length];
    		for(String feature:features){
        		sFeatures[count++] = SerializerFeature.valueOf(feature);
        	}
        	fastJsonConfig.setSerializerFeatures(sFeatures);	
    	}
    	
    	convert.setFastJsonConfig(fastJsonConfig);
    	return convert;
    }
}