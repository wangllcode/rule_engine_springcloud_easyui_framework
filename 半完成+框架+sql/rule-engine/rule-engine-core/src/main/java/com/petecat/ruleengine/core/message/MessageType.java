/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  MessageType.java   
 * @Package com.petecat.riskmanage.exception   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月5日 上午10:54:25   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.message;

/**   
 * @ClassName:  MessageType   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: admin
 * @date:   2017年9月5日 上午10:54:25   
 */
public class MessageType {
	/**   
	 * @Fields SUCCESS : 成功
	 */   
	public static int SUCCESS=0;
	
	/**   
	 * @Fields RUNTIME_ERROR : 运行错误
	 */   
	public static int RUNTIME_ERROR=-1;
	
	/**   
	 * @Fields DATA_ERROR : 数据错误
	 */   
	public static int DATA_ERROR=-2;
}
