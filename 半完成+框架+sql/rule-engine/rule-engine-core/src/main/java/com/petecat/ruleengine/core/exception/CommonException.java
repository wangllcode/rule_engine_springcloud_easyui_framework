/**   
 * @Title: CommonException.java 
 * @Package com.hryx.common.exception 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author zhxh 
 * @date 2015年9月29日
 * @version V0.0.1   
 */
package com.petecat.ruleengine.core.exception;

import java.text.MessageFormat;

import org.springframework.util.Assert;

import com.petecat.ruleengine.core.message.ApplicationMessage;
import com.petecat.ruleengine.core.message.MessageType;


/**
 * @ClassName: CommonException
 * @Description: 系统通用异常处理
 * @author zhxh
 * @date 2015年9月29日
 * 
 */
public class CommonException extends RuntimeException {

	/**
	 * @Fields serialVersionUID :序列化值
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @Fields values : 值
	 */
	private String[] values = null;

	/**
	 * @Fields message : 错误消息
	 */
	private String message;

	/**
	 * @Fields code : 错误代码
	 */
	private int code;

	/**
	 * @Fields type : 错误类型
	 */
	private CommonExceptionType type = CommonExceptionType.ERROR_SYSTEM_INNER_RUNTIME;
	
	/**
	 * @Fields 错误种类 
	 */
	private int messageType = MessageType.RUNTIME_ERROR;

	/**
	 * @Fields originalException : 最原始的CommonException异常信息
	 */
	private CommonException originalException = null;

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
	}

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param values
	 *            消息体中的占位符
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message, String values[],
			Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.values = values;
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message,
			CommonExceptionType type, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		Assert.notNull(type, "CommonExceptionType arg is not be null");
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
		this.type = type;

	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param values
	 *            [] 消息体中的占位符的值
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message, String values[],
			CommonExceptionType type, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.values = values;
		Assert.notNull(type, "CommonExceptionType arg is not be null");
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
		this.type = type;

	}
	
	
	
	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message,int messageType, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.messageType=messageType;
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
	}

	/**
	 * <p>
	 * Title:异常构造器,异常类型默认为系统内部转化异常，不决定如何反馈给用户
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param values
	 *            消息体中的占位符
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message,int messageType, String values[],
			Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.values = values;
		this.messageType=messageType;
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message,
			CommonExceptionType type,int messageType, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.messageType = messageType;
		Assert.notNull(type, "CommonExceptionType arg is not be null");
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
		this.type = type;

	}

	/**
	 * <p>
	 * Title:异常构造器
	 * </p>
	 * <p>
	 * Description: 如果throwable为 CommonException(@see
	 * CommonException)则记录originalException 如果throwable为 null(@see
	 * CommonException)则记录originalException为当前的CommonException
	 * 多次设置只有第一次的CommonException异常记录到
	 * 如果message不为空，则整个异常消息来至于message，否则整个异常消息来至于code，code的消息请参考下面code参数解释
	 * </p>
	 * 
	 * @param code
	 *            异常代码，消息内容来至于@link CommonMessageUtils#getMessage() 的解析
	 * @param message
	 *            异常消息
	 * @param messageType 消息错误种类
	 * @param values
	 *            [] 消息体中的占位符的值
	 * @param type
	 *            异常类型
	 * @param throwable
	 *            引起异常
	 */
	public CommonException(int code, String message, String values[],
			CommonExceptionType type,int messageType, Throwable throwable) {
		super(throwable);
		this.code = code;
		this.message = message;
		this.values = values;
		this.messageType=messageType;
		Assert.notNull(type, "CommonExceptionType arg is not be null");
		if (throwable == null) {
			originalException = this;
		} else {
			if (throwable instanceof CommonException
					&& originalException == null) {
				originalException = (CommonException) throwable;
			}
		}
		this.type = type;

	}

	/*
	 * (非 Javadoc) <p>Title: getMessage</p> <p>Description: </p>
	 * 
	 * @return
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		if (this.message == null || "".equals(this.message.trim())) {
				this.message = ApplicationMessage.getMessage(String.valueOf(this.code));
		}
		if (values != null && values.length > 0) {
			this.message = MessageFormat.format(this.message, values);
		}
		return this.message;
	}

	/*
	 * (非 Javadoc) <p>Title: toString</p> <p>Description: </p>
	 * 
	 * @return
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		return super.toString();
	}

	/*
	 * (非 Javadoc) <p>Title: getCause</p> <p>Description: </p>
	 * 
	 * @return
	 * 
	 * @see java.lang.Throwable#getCause()
	 */
	@Override
	public Throwable getCause() {
		Throwable throwable = super.getCause();
		if (throwable == null) {
			if (originalException != null && this != originalException) {
				throwable = originalException;
			}
		} else {
			if (this == originalException) {
				throwable = null;
			}
		}
		return throwable;
	}

	/**
	 * @return the originalException
	 */
	public CommonException getOriginalException() {
		return originalException;
	}

	/**
	 * @return the type
	 */
	public CommonExceptionType getType() {
		return type;
	}

	public int getCode() {
		return code;
	}

	public String[] getValues() {
		return values;
	}

	/**  
	 * @Title:  getMessageType
	 * @Description:获取messageType的值
	 * @return: int
	 */
	public int getMessageType() {
		return messageType;
	}

}
