/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  StringToDateConvert.java   
 * @Package com.petecat.riskmanage.config.spring   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月6日 下午3:20:34   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.spring;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

/**   
 * @ClassName:  StringToDateConvert   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: admin
 * @date:   2017年9月6日 下午3:20:34   
 */
@ConfigurationPropertiesBinding
@Configuration
public class StringToDateConvert implements Converter<String, Date> {

	/**   
	 * <p>Title: convert</p>   
	 * <p>Description: </p>   
	 * @param source
	 * @return   
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)   
	 */  
	@Override
	public Date convert(String source) {
		 Date date = null; 
		 if(StringUtils.isNotBlank(source)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
	        try {  
	            //防止空数据出错  
	            if(StringUtils.isNotBlank(source)){  
	                date = format.parse(source);  
	            }  
	        } catch (ParseException e) {
	            format = new SimpleDateFormat("yyyy-MM-dd");  
	            try {  
	                date = format.parse(source);  
	            } catch (ParseException e1) {  
	                format = new SimpleDateFormat("yyyy/MM/dd");  
	                try{  
	                    date = format.parse(source);  
	                }catch (Exception e2) {  
	                	format = new SimpleDateFormat("yyyyMMdd");  
	 	                try{  
	 	                    date = format.parse(source);  
	 	                }catch (Exception e3) {  
	 	                   throw new IllegalArgumentException (e2);
	 	                } 
	                }  
	            }  
	        }  
		}
		return date;
	}

}
