/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  ApplicationMessage.java   
 * @Package com.petecat.riskmanage.message   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月4日 下午6:39:02   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.message;

import java.text.MessageFormat;

import org.springframework.core.env.Environment;

/**   
 * @ClassName:  ApplicationMessage   
 * @Description:系统消息
 * @author: admin
 * @date:   2017年9月4日 下午6:39:02   
 */
public class ApplicationMessage {
	
	private static Environment environment;
	
	
	/**  
	 * @Title:  setEnvironment
	 * @Description:设置environment的值
	 * @return: Environment
	 */
	public static void setEnvironment(Environment environment) {
		ApplicationMessage.environment = environment;
	}

	/**
	 * @return the message
	 */
	public static String getMessage(String code) {
		if (environment.getProperty(code) == null) {
			return code;
		}
		return environment.getProperty(code);
	}

	/**
	 * @return the message
	 */
	public static String getFormatMessage(String code, String... values) {
		if (environment.getProperty(code) == null) {
			return code;
		}
		String messageStr = environment.getProperty(code);
		if (values != null && values.length > 0) {
			return MessageFormat.format(messageStr, values);
		} else {
			return messageStr;
		}
	}
}
