/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  BaseController.java   
 * @Package com.petecat.riskmanage.controller   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年9月4日 下午5:17:20   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.exception.CommonExceptionType;
import com.petecat.ruleengine.protocol.sso.vo.SsoUserInfoVO;

/**   
 * @ClassName:  BaseController   
 * @Description:基本的Controller 
 * @author: admin
 * @date:   2017年9月4日 下午5:17:20   
 */
public class BaseController {
   
	public CommonException getOutputOpearteErrorException(Exception e,
			CommonExceptionType commonExceptionType) {
		return new CommonException(-1,null,commonExceptionType,e);
	}
	
	/**   
	 * @Title: getCurrentUser   
	 * @Description: 获取当前用户信息
	 * @return SsoUserInfoVO     
	 */
	public SsoUserInfoVO getCurrentUser() {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		return (SsoUserInfoVO) request.getAttribute("CURRENT_USER_INFO");
	}
}
