/**  
 * All rights Reserved, Designed By http://www.pete-cat.com/
 * @Title:  ValidationConfig.java   
 * @Package com.petecat.ruleengine.core.config.mvc   
 * @Description:TODO(用一句话描述该文件做什么)   
 * @author: 成都皮特猫科技     
 * @date:2017年12月11日 下午7:03:46   
 * @version V1.0 
 * @Copyright: 2017 www.pete-cat.com Inc. All rights reserved. 
 * 注意：本内容仅限于成都皮特猫信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.petecat.ruleengine.core.config.mvc;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**   
 * @ClassName:  ValidationConfig   
 * @Description:  
 * @author: admin
 * @date:   2017年12月11日 下午7:03:46   
 */
@Configuration
public class ValidationConfig {

	@Bean
	public LocalValidatorFactoryBean getLocalValidatorFactoryBean(MessageSource messageSource) {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource);
		return bean;
	}
	
}
