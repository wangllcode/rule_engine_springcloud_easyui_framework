package com.petecat.ruleengine.basicrule.atnminfo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctM;
import com.petecat.ruleengine.basicrule.atnminfo.entity.RleatnmH;
import com.petecat.ruleengine.basicrule.atnminfo.entity.RleatnmM;
import com.petecat.ruleengine.basicrule.atnminfo.mapper.IAtnmInfoMapper;
import com.petecat.ruleengine.basicrule.atnminfo.service.IAtnmInfoService;
import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basedata.asysinfo.vo.SafasysMVO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.AddRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.DelRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.QueryRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.vo.QueryRleatnmVO;
import com.petecat.ruleengine.protocol.global.AexgConstant;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.dto.DictionaryDTO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Service
public class AtnmInfoServiceImpl extends BaseService<RleatnmM, String>
		implements IAtnmInfoService {

	@Autowired
	private IAtnmInfoMapper atnmInfoMapper;

	@Autowired
	private SystemRedisTemplate redisTemplate;
	/**
	 * 附件命名规则分页查询
	 */
	public PageVO<QueryRleatnmVO> listAtnmByPage(QueryRleatnmDTO queryRleatnmDTO) {
		RleatnmM rleatnmM = new RleatnmM();
		DataCopy.copyData(queryRleatnmDTO, rleatnmM);
		Integer count = atnmInfoMapper.count(rleatnmM);
		PageVO<QueryRleatnmVO> pageVo = new PageVO<QueryRleatnmVO>();
		if (null != count && count > 0) {
			List<RleatnmM> atctList = atnmInfoMapper
					.queryAtnmByPage(queryRleatnmDTO);
			pageVo.setTotal(count);
			List<DictionaryDTO> hGetListObject = redisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.ATCTINPUTTYPE, DictionaryDTO.class);
			List<DictionaryDTO> hGetListObject1 = redisTemplate.hGetListObject(SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.ATCTTYPE, DictionaryDTO.class);
			List<QueryRleatnmVO> copyList = DataCopy.copyList(atctList, QueryRleatnmVO.class);
			copyList.parallelStream().forEachOrdered(nm->{
				Optional<DictionaryDTO> findFirst = hGetListObject.parallelStream().filter(h->h.getParmname().equals(nm.getAtctInputType())).findFirst();
				Optional<DictionaryDTO> findFirst1 = hGetListObject1.parallelStream().filter(h->h.getParmname().equals(nm.getAtctType())).findFirst();
				if(findFirst.isPresent())
					nm.setAtctInputTypeName(findFirst.get().getParmvalue());
				if(findFirst1.isPresent())
					nm.setAtctTypeName(findFirst1.get().getParmvalue());
			});
			pageVo.setRows(copyList);
		} else {
			pageVo.setTotal(0);
		}
		return pageVo;
	}

	/**
	 * 添加附件命名规则
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveAtnm(AddRleatnmDTO addRleatnmDTO) {
		RleatnmM rleatnmM = new RleatnmM();
		DataCopy.copyData(addRleatnmDTO, rleatnmM);
		rleatnmM.setAtnmUuId(getDefaultPrimaryKeyForChar());
		atnmInfoMapper.insert(rleatnmM);
		this.saveAtnmHInfo(rleatnmM, "ADD");
	}

	/**
	 * 修改附件命名规则
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateAtnm(AddRleatnmDTO addRleatnmDTO) {
		List<RleatnmM> rleatctMList = atnmInfoMapper.getAtnmById(addRleatnmDTO.getAtnmUuId());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(2003, null, null);
		} else {
			atnmInfoMapper.updateAtnm(addRleatnmDTO);
			this.saveAtnmHInfo(rleatctMList.get(0), "MOD");
		}
	}

	/**
	 * 刪除附件命名规则
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeAtnmById(DelRleatnmDTO delRleatnmDTO) {
		List<RleatnmM> rleatctMList = atnmInfoMapper.getAtnmById(delRleatnmDTO.getAtnmUuId());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(2003, null, null);
		} else if (rleatctMList.size() > 1) {
			throw new CommonException(2001, null, null);
		} else {
			atnmInfoMapper.removeAtnmById(delRleatnmDTO);
			this.saveAtnmHInfo(rleatctMList.get(0), "DEL");
		}
	}
	/**
	 * @Title: saveAtctHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param rleatctM
	 * @param @param edtId
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public int saveAtnmHInfo(RleatnmM rleatnmM, String edtId) {
		RleatnmH rleatnmH = new RleatnmH();
		rleatnmH.setLastModDate(DatesUtils.getStringDate());
		DataCopy.copyData(rleatnmM, rleatnmH);
		rleatnmH.setSeqUuId(getDefaultPrimaryKeyForChar());
		// 改变动作【ADD，MOD，DEL】
		rleatnmH.setEdtId(edtId);
		return atnmInfoMapper.insertAtnmH(rleatnmH);
	}
}
