package com.petecat.ruleengine.basicrule.fmdlinfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.AddRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.DelRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.QueryRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

public interface IFmdlInfoService {

	/**
	 * 
	 * @Title: listFmdlByPage
	 * @Description: 规则字段模块分页查询
	 * @param @param queryRlefmdlDTO
	 * @param @return 参数
	 * @return PageVO<QueryRlefmdlVO> 返回类型
	 * @throws
	 */
	List<QueryRlefmdlVO> listFmdlByPage(QueryRlefmdlDTO queryRlefmdlDTO);

	/**
	 * 
	 * @Title: saveFmdl
	 * @Description: 添加规则字段模块
	 * @param @param addRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveFmdl(AddRlefmdlDTO addRlefmdlDTO);

	/**
	 * 
	 * @Title: removeFmdlById
	 * @Description: 刪除规则字段模块
	 * @param @param delRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeFmdlById(DelRlefmdlDTO delRlefmdlDTO);

	/**
	 * 
	 * @Title: updateFmdl
	 * @Description: 修改规则字段模块
	 * @param @param addRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateFmdl(AddRlefmdlDTO addRlefmdlDTO);

	List<TreeNodeVO> queryFmdlName();

}
