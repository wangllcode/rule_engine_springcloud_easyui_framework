package com.petecat.ruleengine.basicrule.fildinfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class RlefildH implements Serializable {
	 /**
     * 主键
     */
    private String fildmuuid;

    /**
     * 历史序列
     */
    private String sequuid;
    /**
     * 字段代号（英文字段）
     */
    private String fildcode;

    /**
     * 字段名称
     */
    private String fildname;

    /**
     * 字段用途描述
     */
    private String filddesc;

    /**
     * 字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】
     */
    private String fildtype;

    /**
     * 字段所属模块
     */
    private String fmdluuid;

    /**
     * 排序
     */
    private Integer ordernum;

    /**
     * 改变动作【ADD，MOD，DEL】
     */
    private String edtid;

    /**
     * 操作者
     */
    private String lastmoduser;

    /**
     * 操作时间
     */
    private String lastmoddate;

    /**
     * rlefildh
     */
    private static final long serialVersionUID = 1L;
}