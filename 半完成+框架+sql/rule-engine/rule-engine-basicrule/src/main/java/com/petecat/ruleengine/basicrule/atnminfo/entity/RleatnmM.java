package com.petecat.ruleengine.basicrule.atnminfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RleatnmM implements Serializable {

	/**
	 * 主键
	 */
	private String atnmUuId;
	
	/**
	 * 附件目录UUID
	 */
	private String atctUuId;

	/**
	 * 附件命名名称
	 */
	private String atctName;

	/**
	 * 是否输入/输出【0：输入/1：输出】
	 */
	private String atctType;

	/**
	 * 输入规则【0：包含/1：必须一致】
	 */
	private String atctInputType;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;
	
	/**
	 * 附件目录名称
	 */
	private String atctParentName;
	
	/**
	 * rleatnmm
	 */
	private static final long serialVersionUID = 1L;
}