package com.petecat.ruleengine.basicrule.fmdlinfo.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basicrule.fmdlinfo.service.IFmdlInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.AddRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.AddRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.DelRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.QueryRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

@RestController
@RequestMapping("/fmdlinfo")
@Api(tags="规则字段模块管理")
public class FmdlInfoController extends BaseController{
	
	@Autowired
	private IFmdlInfoService fmdlInfoService;
	
	@GetMapping("/queryFmdlByPage")
	@ApiOperation("规则字段模块分页查询")
	public Result<List<QueryRlefmdlVO>> queryFmdlByPage(
			@ModelAttribute QueryRlefmdlDTO queryRlefmdlDTO) {
		List<QueryRlefmdlVO> atnmList = fmdlInfoService.listFmdlByPage(queryRlefmdlDTO);
		return Result.success(atnmList);
	}
	
	@GetMapping("/queryFmdlName")
	public Result<List<TreeNodeVO>> queryFmdlName() {
		List<TreeNodeVO> nodeVo = fmdlInfoService.queryFmdlName();
		return Result.success(nodeVo);
	}
	
	
	@PostMapping("/saveFmdl")
	@ApiOperation(value = "添加规则字段模块")
	public Result<?> saveFmdl(@RequestBody AddRlefmdlDTO addRlefmdlDTO) {
		this.setLastUserName(addRlefmdlDTO);
		fmdlInfoService.saveFmdl(addRlefmdlDTO);
		return Result.success();
	}
	
	@DeleteMapping("/removeFmdlById")
	@ApiOperation(value = "刪除规则字段模块")
	public Result<?> removeFmdlById(DelRlefmdlDTO delRlefmdlDTO) {
		String userId = this.getCurrentUser().getUserId();
		delRlefmdlDTO.setLastmoduser(userId);
		fmdlInfoService.removeFmdlById(delRlefmdlDTO);
		return Result.success();
	}
	
	
	@PutMapping("/updateFmdl")
	@ApiOperation(value = "修改规则字段模块")
	public Result<?> updateFmdl(@RequestBody AddRlefmdlDTO addRlefmdlDTO) {
		this.setLastUserName(addRlefmdlDTO);
		fmdlInfoService.updateFmdl(addRlefmdlDTO);
		return Result.success();
	}
	
	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param addRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(AddRlefmdlDTO addRlefmdlDTO) {
		String userId = this.getCurrentUser().getUserId();
		addRlefmdlDTO.setLastmoduser(userId);
		addRlefmdlDTO.setLastmoddate(DatesUtils.getStringDate());
	}
	
	
}
