package com.petecat.ruleengine.basicrule.epintinfo.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class RlepintM implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 系统代号
	 */
	private String sysCode;
	/**
	 * 所属机构
	 */
	private String bhcId;
	/**
	 * 所属产品
	 */
	private String proId;
	/**
	 * 类型代号（接口代号）
	 */
	private String pintCode;
	/**
	 * 序列
	 */
	private String pintUuid;
	/**
	 * 类型名称
	 */
	private String pintName;
	/**
	 * 类型描述
	 */
	private String pintDesc;
	/**
	 * 操作者
	 */
	private String lastModUser;
	/**
	 * 操作时间
	 */
	private String lastModDate;
	
	/**
	 * 机构
	 */
	private String bhcName;
	/**
	 * 产品
	 */
	private String proName;

}
