package com.petecat.ruleengine.basicrule.atctinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctH;
import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.AddRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.DelRleatctMDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.QueryRleatctDTO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

@Mapper
public interface IAtctInfoMapper extends BaseMapper<RleatctM, String> {

	/**
	 * 
	 * @Title: queryAtctByPage
	 * @Description: 分页查询附件目录
	 * @param @param queryRleatctDTO
	 * @param @return 参数
	 * @return List<RleatctM> 返回类型
	 * @throws
	 */
	List<RleatctM> queryAtctByPage(QueryRleatctDTO queryRleatctDTO);

	/**
	 * 
	 * @Title: getAtctById
	 * @Description: 查询附件目录信息
	 * @param @param sysCode
	 * @param @return 参数
	 * @return List<RleatctM> 返回类型
	 * @throws
	 */
	List<RleatctM> getAtctById(@Param("sysCode") String sysCode);

	/**
	 * 
	 * @Title: insertAtctH
	 * @Description: 添加目录附件历史表
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	int insertAtctH(RleatctH rleatctH);

	/**
	 * 
	 * @Title: removeAtctById
	 * @Description: 删除目录附件信息
	 * @param @param delRleatctMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeAtctById(DelRleatctMDTO delRleatctMDTO);

	/**
	 * 
	 * @Title: updateAtct
	 * @Description: 修改目录附件信息
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAtct(AddRleatctDTO addRleatctDTO);

	/**
	 * @param sysCode 
	 * @return 
	 * 
	 * @Title: queryAtctTreeBySuperId
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param id 参数
	 * @return void 返回类型
	 * @throws
	 */
	List<TreeNodeVO> queryAtctTreeBySuperId(@Param("parentAtctUuid") String id, @Param("sysCode") String sysCode);

	List<TreeNodeVO> queryAtctName();

}