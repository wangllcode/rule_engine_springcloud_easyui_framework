package com.petecat.ruleengine.basicrule.fmdlinfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
@Data
public class RlefmdlH implements Serializable{
	 /**
     * 主键
     */
    private String fmdluuid;

    /**
     * 历史序列
     */
    private String sequuid;
	 /**
     * 系统代号
     */
    private String syscode;

    /**
     * 字段模块代号
     */
    private String fmdlcode;

    /**
     * 字段模块名称
     */
    private String fmdlname;

    /**
     * 字段模块描述
     */
    private String fmdldesc;

    /**
     * 排序
     */
    private Integer ordernum;

    /**
     * 改变动作【ADD，MOD，DEL】
     */
    private String edtid;

    /**
     * 操作者
     */
    private String lastmoduser;

    /**
     * 操作时间
     */
    private String lastmoddate;

    /**
     * rlefmdlh
     */
    private static final long serialVersionUID = 1L;
}
