package com.petecat.ruleengine.basicrule.atnminfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class RleatnmH implements Serializable {
	/**
	 * 主键
	 */
	private String atnmUuId;
	
	/**
	 * 附件目录UUID
	 */
	private String atctUuId;

	/**
	 * 历史序列
	 */
	private String seqUuId;
	
	/**
	 * 附件命名名称
	 */
	private String atctName;

	/**
	 * 是否输入/输出【0：输入/1：输出】
	 */
	private String atctType;

	/**
	 * 输入规则【0：包含/1：必须一致】
	 */
	private String atctInputType;

	/**
	 * 改变动作【ADD，MOD，DEL】
	 */
	private String edtId;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * rleatnmh
	 */
	private static final long serialVersionUID = 1L;
}