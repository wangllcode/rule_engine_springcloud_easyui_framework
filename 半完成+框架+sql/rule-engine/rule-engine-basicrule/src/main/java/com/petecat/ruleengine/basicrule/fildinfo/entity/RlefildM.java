package com.petecat.ruleengine.basicrule.fildinfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RlefildM implements Serializable {
	/**
	 * 主键
	 */
	private String fildmuuid;

	/**
	 * 字段代号（英文字段）
	 */
	private String fildcode;

	/**
	 * 字段名称
	 */
	private String fildname;

	/**
	 * 字段用途描述
	 */
	private String filddesc;

	/**
	 * 字段类型【0：Int/1：Number/2：String/3：TIMESTAMP】
	 */
	private String fildtype;

	/**
	 * 字段所属模块
	 */
	private String fmdluuid;

	/**
	 * 排序
	 */
	private Integer ordernum;

	/**
	 * 操作者
	 */
	private String lastmoduser;

	/**
	 * 操作时间
	 */
	private String lastmoddate;
	
	/**
	 * 字段所属模块Name
	 */
	private String fmdlName;
	/**
	 * rlefildm
	 */
	private static final long serialVersionUID = 1L;
}