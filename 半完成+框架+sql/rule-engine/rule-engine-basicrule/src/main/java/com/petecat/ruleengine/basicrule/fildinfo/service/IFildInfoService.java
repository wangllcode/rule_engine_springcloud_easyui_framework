package com.petecat.ruleengine.basicrule.fildinfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.AddRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.DelRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.QueryRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.vo.QueryRlefildVO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

public interface IFildInfoService {

	/**
	 * 
	 * @Title: listFildByPage
	 * @Description: 规则字段分页查询
	 * @param @param queryRlefildDTO
	 * @param @return 参数
	 * @return PageVO<QueryRlefildVO> 返回类型
	 * @throws
	 */
	PageVO<QueryRlefildVO> listFildByPage(QueryRlefildDTO queryRlefildDTO);

	/**
	 * 
	 * @Title: saveFild
	 * @Description: 添加规则字段
	 * @param @param addRlefildDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveFild(AddRlefildDTO addRlefildDTO);

	/**
	 * 
	 * @Title: removeFildById
	 * @Description: 刪除规则字段
	 * @param @param delRlefildDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeFildById(DelRlefildDTO delRlefildDTO);

	/**
	 * 
	 * @Title: updateFild
	 * @Description: 修改规则字段
	 * @param @param addRlefildDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateFild(AddRlefildDTO addRlefildDTO);

	List<QueryRlefmdlVO> queryFildByFmdlUuId(String id);

}
