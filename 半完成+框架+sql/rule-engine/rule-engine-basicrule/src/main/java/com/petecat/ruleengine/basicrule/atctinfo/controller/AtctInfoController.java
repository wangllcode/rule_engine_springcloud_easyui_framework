package com.petecat.ruleengine.basicrule.atctinfo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basicrule.atctinfo.service.IAtctInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.AddRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.DelRleatctMDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.QueryRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.vo.QueryRleatctVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

/**
 * 
 * @ClassName: AtctInfoController
 * @Description: 附件目录规则
 * @author 王来利
 * @date 2017年12月12日
 *
 */
@RestController
@RequestMapping("/atctinfo")
@Api(tags = "附件目录规则")
public class AtctInfoController extends BaseController {

	@Autowired
	private IAtctInfoService atctInfoService;

	/**
	 * 
	 * @Title: queryAtctByPage
	 * @Description: 附件目录规则分页查询
	 * @param @param queryRleatctDTO
	 * @param @return 参数
	 * @return Result<PageVO<QueryRleatctVO>> 返回类型
	 * @throws
	 */
	@GetMapping("/queryAtctByPage")
	@ApiOperation("附件目录规则分页查询")
	public Result<List<QueryRleatctVO>> queryAtctByPage(
			@ModelAttribute QueryRleatctDTO queryRleatctDTO) {
		List<QueryRleatctVO> atctList = atctInfoService
				.listAtctByPage(queryRleatctDTO);
		return Result.success(atctList);
	}

	@GetMapping("/queryAtctTree")
	public Result<List<TreeNodeVO>> queryAtctTree(String id,String sysCode) {
		List<TreeNodeVO> nodeVo = atctInfoService.queryAtctTree(id,sysCode);
		return Result.success(nodeVo);
	}

	@GetMapping("/queryAtctName")
	public Result<List<TreeNodeVO>> queryAtctName() {
		List<TreeNodeVO> nodeVo = atctInfoService.queryAtctName();
		return Result.success(nodeVo);
	}

	/**
	 * 
	 * @Title: getAtctById
	 * @Description: 通过系统代号查询附件目录信息
	 * @param @param sysCode
	 * @param @return 参数
	 * @return Result<QueryRleatctVO> 返回类型
	 * @throws
	 */
	@GetMapping("/getAtctById")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "sysCode", value = "系统代号", paramType = "query", dataType = "string") })
	@ApiOperation("通过系统代号查询附件目录信息")
	public Result<QueryRleatctVO> getAtctById(String sysCode) {
		QueryRleatctVO queryRleatctVO = atctInfoService.getAtctById(sysCode);
		return Result.success(queryRleatctVO);
	}

	/**
	 * 
	 * @Title: saveAtct
	 * @Description: 添加目录附件信息
	 * @param @param addRleatctDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@PostMapping("/saveAtct")
	@ApiOperation(value = "添加目录附件信息")
	public Result<?> saveAtct(@RequestBody AddRleatctDTO addRleatctDTO) {
		this.setLastUserName(addRleatctDTO);
		atctInfoService.saveAtct(addRleatctDTO);
		return Result.success();
	}

	/**
	 * 
	 * @Title: removeAtctById
	 * @Description: 刪除目录附件信息
	 * @param @param delRleatctMDTO
	 * @param @return 参数
	 * @return Result<?> 返回类型
	 * @throws
	 */
	@DeleteMapping("/removeAtctById")
	@ApiOperation(value = "刪除目录附件信息")
	public Result<?> removeAtctById(DelRleatctMDTO delRleatctMDTO) {
		String userId = this.getCurrentUser().getUserId();
		delRleatctMDTO.setLastModUser(userId);
		atctInfoService.removeAtctById(delRleatctMDTO);
		return Result.success();
	}
	
	
	@PutMapping("/updateAtct")
	@ApiOperation(value = "修改目录附件信息")
	public Result<?> updateAtct(@RequestBody AddRleatctDTO addRleatctDTO) {
		this.setLastUserName(addRleatctDTO);
		atctInfoService.updateAtct(addRleatctDTO);
		return Result.success();
	}
	
	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(AddRleatctDTO addRleatctDTO) {
		String userId = this.getCurrentUser().getUserId();
		addRleatctDTO.setLastModUser(userId);
		addRleatctDTO.setLastModDate(DatesUtils.getStringDate());
	}
}
