package com.petecat.ruleengine.basicrule.fildinfo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basicrule.fildinfo.service.IFildInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.AddRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.DelRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.QueryRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.vo.QueryRlefildVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@RestController
@RequestMapping("/fildinfo")
@Api(tags="规则字段管理")
public class FildInfoController extends BaseController {
	
	@Autowired
	private IFildInfoService fildInfoService;
	
	@GetMapping("/queryFildByPage")
	@ApiOperation("规则字段分页查询")
	public Result<PageVO<QueryRlefildVO>> queryFildByPage(
			@ModelAttribute QueryRlefildDTO queryRlefildDTO) {
		PageVO<QueryRlefildVO> atnmList = fildInfoService.listFildByPage(queryRlefildDTO);
		return Result.success(atnmList);
	}
	
	
	
	@PostMapping("/saveFild")
	@ApiOperation(value = "添加规则字段")
	public Result<?> saveFild(@RequestBody AddRlefildDTO addRlefildDTO) {
		this.setLastUserName(addRlefildDTO);
		fildInfoService.saveFild(addRlefildDTO);
		return Result.success();
	}
	
	@DeleteMapping("/removeFildById")
	@ApiOperation(value = "刪除规则字段")
	public Result<?> removeFildById(DelRlefildDTO delRlefildDTO) {
		String userId = this.getCurrentUser().getUserId();
		delRlefildDTO.setLastmoduser(userId);
		fildInfoService.removeFildById(delRlefildDTO);
		return Result.success();
	}
	
	
	@PutMapping("/updateFild")
	@ApiOperation(value = "修改规则字段")
	public Result<?> updateFild(@RequestBody AddRlefildDTO addRlefildDTO) {
		this.setLastUserName(addRlefildDTO);
		fildInfoService.updateFild(addRlefildDTO);
		return Result.success();
	}
	
	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param addRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(AddRlefildDTO addRlefildDTO) {
		String userId = this.getCurrentUser().getUserId();
		addRlefildDTO.setLastmoduser(userId);
		addRlefildDTO.setLastmoddate(DatesUtils.getStringDate());
	}
	
}
