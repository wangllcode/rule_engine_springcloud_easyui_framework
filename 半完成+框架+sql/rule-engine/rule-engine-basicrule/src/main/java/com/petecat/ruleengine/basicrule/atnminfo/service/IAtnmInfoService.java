package com.petecat.ruleengine.basicrule.atnminfo.service;

import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.AddRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.DelRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.QueryRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.vo.QueryRleatnmVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

public interface IAtnmInfoService {

	/**
	 * 
	 * @Title: listAtnmByPage
	 * @Description: 附件命名规则分页查询
	 * @param @param queryRleatnmDTO
	 * @param @return 参数
	 * @return PageVO<QueryRleatnmVO> 返回类型
	 * @throws
	 */
	PageVO<QueryRleatnmVO> listAtnmByPage(QueryRleatnmDTO queryRleatnmDTO);

	/**
	 * 
	 * @Title: saveAtnm
	 * @Description: 添加附件命名规则
	 * @param @param addRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveAtnm(AddRleatnmDTO addRleatnmDTO);

	/**
	 * 
	 * @Title: updateAtnm
	 * @Description: 修改附件命名规则
	 * @param @param addRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAtnm(AddRleatnmDTO addRleatnmDTO);

	/**
	 * 
	 * @Title: removeAtnmById
	 * @Description: 刪除附件命名规则
	 * @param @param delRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeAtnmById(DelRleatnmDTO delRleatnmDTO);

}
