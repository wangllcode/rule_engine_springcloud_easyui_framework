package com.petecat.ruleengine.basicrule.atnminfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctM;
import com.petecat.ruleengine.basicrule.atnminfo.entity.RleatnmH;
import com.petecat.ruleengine.basicrule.atnminfo.entity.RleatnmM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.AddRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.DelRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.QueryRleatnmDTO;

@Mapper
public interface IAtnmInfoMapper extends BaseMapper<RleatnmM, String> {

	/**
	 * 
	 * @Title: queryAtnmByPage
	 * @Description: 附件命名规则分页查询
	 * @param @param queryRleatnmDTO
	 * @param @return 参数
	 * @return List<RleatnmM> 返回类型
	 * @throws
	 */
	List<RleatnmM> queryAtnmByPage(QueryRleatnmDTO queryRleatnmDTO);

	/**
	 * 
	 * @Title: getAtnmById
	 * @Description: 通过ID查询信息
	 * @param @param atnmUuId
	 * @param @return 参数
	 * @return List<RleatnmM> 返回类型
	 * @throws
	 */
	List<RleatnmM> getAtnmById(@Param("atnmUuId") String atnmUuId);

	/**
	 * 
	 * @Title: insertAtnmH
	 * @Description: 添加历史表
	 * @param @param rleatnmH
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	int insertAtnmH(RleatnmH rleatnmH);

	/**
	 * 
	 * @Title: removeAtnmById
	 * @Description: 删除规则
	 * @param @param delRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeAtnmById(DelRleatnmDTO delRleatnmDTO);

	/**
	 * 
	 * @Title: updateAtnm
	 * @Description: 修改附件命名规则
	 * @param @param addRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAtnm(AddRleatnmDTO addRleatnmDTO);

}
