package com.petecat.ruleengine.basicrule.atctinfo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctH;
import com.petecat.ruleengine.basicrule.atctinfo.entity.RleatctM;
import com.petecat.ruleengine.basicrule.atctinfo.mapper.IAtctInfoMapper;
import com.petecat.ruleengine.basicrule.atctinfo.service.IAtctInfoService;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.AddRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.DelRleatctMDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.QueryRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.vo.QueryRleatctVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.vo.PageVO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

@Service
public class AtctInfoServiceImpl extends BaseService<RleatctM, String>
		implements IAtctInfoService {

	@Autowired
	private IAtctInfoMapper atctInfoMapper;

	/**
	 * 附件目录规则分页查询
	 */
	public List<QueryRleatctVO> listAtctByPage(QueryRleatctDTO queryRleatctDTO) {
		if (null == queryRleatctDTO.getId()) {
			queryRleatctDTO.setId("");
		}
		List<RleatctM> atctList = atctInfoMapper
				.queryAtctByPage(queryRleatctDTO);
		List<QueryRleatctVO> list = DataCopy.copyList(atctList,
				QueryRleatctVO.class);
		return list;
	}

	/**
	 * 查询附件目录信息
	 */
	public QueryRleatctVO getAtctById(String sysCode) {
		List<RleatctM> rleatctM = atctInfoMapper.getAtctById(sysCode);
		if (null != rleatctM && rleatctM.size() > 1) {
			throw new CommonException(1001, null, null);
		}
		QueryRleatctVO queryRleatctVO = new QueryRleatctVO();
		DataCopy.copyData(rleatctM.get(0), queryRleatctVO);
		return queryRleatctVO;
	}

	/**
	 * 添加目录附件
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveAtct(AddRleatctDTO addRleatctDTO) {
		RleatctM rleatctM = new RleatctM();
		DataCopy.copyData(addRleatctDTO, rleatctM);
		rleatctM.setAtctUuId(this.getDefaultPrimaryKeyForChar());
		atctInfoMapper.insert(rleatctM);
		this.saveAtctHInfo(rleatctM, "ADD");
	}

	/**
	 * 删除目录附件信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeAtctById(DelRleatctMDTO delRleatctMDTO) {
		List<RleatctM> rleatctM = atctInfoMapper.getAtctById(delRleatctMDTO
				.getSysCode());
		if (null == rleatctM || rleatctM.size() == 0) {
			throw new CommonException(1001, null, null);
		} else {
			atctInfoMapper.removeAtctById(delRleatctMDTO);
			this.saveAtctHInfo(rleatctM.get(0), "DEL");
		}
	}

	/**
	 * 修改目录附件信息
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateAtct(AddRleatctDTO addRleatctDTO) {
		List<RleatctM> rleatctM = atctInfoMapper.getAtctById(addRleatctDTO
				.getSysCode());
		if (null == rleatctM || rleatctM.size() == 0) {
			throw new CommonException(1001, null, null);
		}else {
			atctInfoMapper.updateAtct(addRleatctDTO);
			this.saveAtctHInfo(rleatctM.get(0), "MOD");
		}
	}

	/**
	 * @Title: saveAtctHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param rleatctM
	 * @param @param edtId
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public int saveAtctHInfo(RleatctM rleatctM, String edtId) {
		RleatctH rleatctH = new RleatctH();
		rleatctH.setLastModDate(DatesUtils.getStringDate());
		DataCopy.copyData(rleatctM, rleatctH);
		rleatctH.setSeqUuId(getDefaultPrimaryKeyForChar());
		// 改变动作【ADD，MOD，DEL】
		rleatctH.setEdtId(edtId);
		return atctInfoMapper.insertAtctH(rleatctH);
	}

	@Override
	public List<TreeNodeVO> queryAtctTree(String id,String sysCode) {
		if(null == id){
			id="";
		}
		List<TreeNodeVO> vo = atctInfoMapper.queryAtctTreeBySuperId(id,sysCode);
		if(null != vo && vo.size() > 0){
			return vo;
		}else{
			vo = atctInfoMapper.queryAtctTreeBySuperId(null,sysCode);
			vo.parallelStream().forEachOrdered(t->{
				t.setState("open");
			});
		}
		return vo;
	}

	@Override
	public List<TreeNodeVO> queryAtctName() {
		return atctInfoMapper.queryAtctTreeBySuperId(null,null);
	}

}
