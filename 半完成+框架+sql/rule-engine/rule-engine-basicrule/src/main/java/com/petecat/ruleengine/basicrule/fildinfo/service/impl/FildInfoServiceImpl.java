package com.petecat.ruleengine.basicrule.fildinfo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basicrule.fildinfo.entity.RlefildH;
import com.petecat.ruleengine.basicrule.fildinfo.entity.RlefildM;
import com.petecat.ruleengine.basicrule.fildinfo.mapper.IFildInfoMapper;
import com.petecat.ruleengine.basicrule.fildinfo.service.IFildInfoService;
import com.petecat.ruleengine.core.constant.SystemConstant;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.redis.SystemRedisTemplate;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.AddRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.DelRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.QueryRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.vo.QueryRlefildVO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;
import com.petecat.ruleengine.protocol.global.AexgConstant;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.dto.DictionaryDTO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@Service
public class FildInfoServiceImpl extends BaseService<RlefildM, String>
		implements IFildInfoService {

	@Autowired
	private IFildInfoMapper fildInfoMapper;

	@Autowired
	private SystemRedisTemplate redisTemplate;

	@Override
	public PageVO<QueryRlefildVO> listFildByPage(QueryRlefildDTO queryRlefildDTO) {
		RlefildM rlefildM = new RlefildM();
		DataCopy.copyData(queryRlefildDTO, rlefildM);
		Integer count = fildInfoMapper.count(rlefildM);
		PageVO<QueryRlefildVO> pageVo = new PageVO<QueryRlefildVO>();
		if (null != count && count > 0) {
			List<RlefildM> atctList = fildInfoMapper
					.queryFildByPage(queryRlefildDTO);
			pageVo.setTotal(count);
			List<DictionaryDTO> hGetListObject = redisTemplate.hGetListObject(
					SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.FILDTYPE,
					DictionaryDTO.class);
			List<QueryRlefildVO> copyList = DataCopy.copyList(atctList,
					QueryRlefildVO.class);
			copyList.parallelStream().forEachOrdered(
					nm -> {
						Optional<DictionaryDTO> findFirst = hGetListObject
								.parallelStream()
								.filter(h -> h.getParmname().equals(
										nm.getFildtype())).findFirst();
						if (findFirst.isPresent())
							nm.setFildtypeName(findFirst.get().getParmvalue());
					});
			pageVo.setRows(copyList);
		} else {
			pageVo.setTotal(0);
		}
		return pageVo;
	}

	/**
	 * 
	 * @Title: queryFildByFmdlUuId
	 * @Description: 通过模块ID查询字段信息列表
	 * @param @param queryRlefildDTO
	 * @param @return 参数
	 * @return Result<List<QueryRlefildVO>> 返回类型
	 * @throws
	 */
	public List<QueryRlefmdlVO> queryFildByFmdlUuId(String id) {
		List<QueryRlefmdlVO> list = fildInfoMapper.queryFildByFmdlUuId(id);
		List<DictionaryDTO> hGetListObject = redisTemplate.hGetListObject(
				SystemConstant.SYS_DICTIONARY_KEY, AexgConstant.FILDTYPE,
				DictionaryDTO.class);
		list.parallelStream().forEachOrdered(nm -> {
					Optional<DictionaryDTO> findFirst = hGetListObject.parallelStream().filter(h -> h.getParmname().equals(nm.getFildType())).findFirst();
					if (findFirst.isPresent())
						nm.setFildTypeName(findFirst.get().getParmvalue());
				});
		return list;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveFild(AddRlefildDTO addRlefildDTO) {
		RlefildM rlefildM = new RlefildM();
		DataCopy.copyData(addRlefildDTO, rlefildM);
		rlefildM.setFildmuuid(getDefaultPrimaryKeyForChar());
		fildInfoMapper.insert(rlefildM);
		this.saveFildHInfo(rlefildM, "ADD");
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeFildById(DelRlefildDTO delRlefildDTO) {
		List<RlefildM> rleatctMList = fildInfoMapper.getFildById(delRlefildDTO
				.getFildmuuid());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(3003, null, null);
		} else if (rleatctMList.size() > 1) {
			throw new CommonException(3001, null, null);
		} else {
			fildInfoMapper.removeFildById(delRlefildDTO);
			this.saveFildHInfo(rleatctMList.get(0), "DEL");
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateFild(AddRlefildDTO addRlefildDTO) {
		List<RlefildM> rleatctMList = fildInfoMapper.getFildById(addRlefildDTO
				.getFildmuuid());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(3003, null, null);
		} else if (rleatctMList.size() > 1) {
			throw new CommonException(3001, null, null);
		} else {
			fildInfoMapper.updateFild(addRlefildDTO);
			this.saveFildHInfo(rleatctMList.get(0), "MOD");
		}
	}

	/**
	 * @Title: saveAtctHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param rleatctM
	 * @param @param edtId
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public int saveFildHInfo(RlefildM rlefildM, String edtId) {
		RlefildH rlefildH = new RlefildH();
		rlefildH.setLastmoddate(DatesUtils.getStringDate());
		DataCopy.copyData(rlefildM, rlefildH);
		rlefildH.setSequuid(getDefaultPrimaryKeyForChar());
		// 改变动作【ADD，MOD，DEL】
		rlefildH.setEdtid(edtId);
		return fildInfoMapper.insertFildH(rlefildH);
	}
}
