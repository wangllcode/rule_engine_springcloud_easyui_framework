package com.petecat.ruleengine.basicrule.epintinfo.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class RleatfaM implements  Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	private String atfaUuid;
	/**
	 * 规则类型用途UUID
	 */
	private String pintUuid;
	/**
	 * 字段主键序列
	 */
	private String fildmUuid;
	/**
	 * 字段历史序列
	 */
	private String fildmSeqnoUuid;
	/**
	 * 字段代号
	 */
	private String fildCode;
	/**
	 * 附件命名主键序列
	 */
	private String atctUuid;
	/**
	 * 附件命名历史序列
	 */
	private String atctSeqnoUuid;
	/**
	 * 必须存在【0：否/1：是】
	 */
	private String attisExists;
	/**
	 * 操作者
	 */
	private String lastModUser;
	/**
	 * 操作时间
	 */
	private String lastModDate;
	/**
	 * 附件名陈
	 */
	private String fileName;
	/**
	 * 字段名陈
	 */
	private String filedName;

}
