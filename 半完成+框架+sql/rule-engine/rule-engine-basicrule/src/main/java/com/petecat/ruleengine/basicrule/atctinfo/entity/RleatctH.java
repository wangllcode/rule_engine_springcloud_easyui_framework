package com.petecat.ruleengine.basicrule.atctinfo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RleatctH implements Serializable {

	/**
	 * 主键
	 */
	private String atctUuId;

	/**
	 * 历史序列
	 */
	private String seqUuId;

	/**
	 * 系统代号
	 */
	private String sysCode;

	/**
	 * 附件目录名称
	 */
	private String atctName;

	/**
	 * 附件目录描述
	 */
	private String atctDesc;

	/**
	 * 上级目录UUID
	 */
	private String parentAtctUuId;

	/**
	 * 排序
	 */
	private Integer orderNum;

	/**
	 * 改变动作【ADD,MOD,DEL】
	 */
	private String edtId;

	/**
	 * 操作者
	 */
	private String lastModUser;

	/**
	 * 操作时间
	 */
	private String lastModDate;

	/**
	 * rleatcth
	 */
	private static final long serialVersionUID = 1L;
}