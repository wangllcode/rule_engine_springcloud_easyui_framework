package com.petecat.ruleengine.basicrule.fmdlinfo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.petecat.ruleengine.basicrule.fildinfo.service.IFildInfoService;
import com.petecat.ruleengine.basicrule.fmdlinfo.entity.RlefmdlH;
import com.petecat.ruleengine.basicrule.fmdlinfo.entity.RlefmdlM;
import com.petecat.ruleengine.basicrule.fmdlinfo.mapper.IFmdlInfoMapper;
import com.petecat.ruleengine.basicrule.fmdlinfo.service.IFmdlInfoService;
import com.petecat.ruleengine.core.exception.CommonException;
import com.petecat.ruleengine.core.service.BaseService;
import com.petecat.ruleengine.core.util.DataCopy;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.AddRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.DelRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.QueryRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

@Service
public class FmdlInfoServiceImpl extends BaseService<RlefmdlM, String> implements
		IFmdlInfoService {
	
	@Autowired
	private IFmdlInfoMapper fmdlInfoMapper;
	
	@Autowired
	private IFildInfoService fildInfoService;

	/**
	 * 规则字段模块分页查询
	 */
	public List<QueryRlefmdlVO> listFmdlByPage(QueryRlefmdlDTO queryRlefmdlDTO) {
			List<RlefmdlM> atctList = fmdlInfoMapper.queryFmdlByPage(queryRlefmdlDTO);
			List<QueryRlefmdlVO> list = DataCopy.copyList(atctList, QueryRlefmdlVO.class);
			//通过模块ID去查字段信息 
			if(null != queryRlefmdlDTO.getId()){
				List<QueryRlefmdlVO> queryFildByFmdlUuId = fildInfoService.queryFildByFmdlUuId(queryRlefmdlDTO.getId());
				return queryFildByFmdlUuId;
			}
			return list;
	}

	/**
	 * 添加规则字段模块
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveFmdl(AddRlefmdlDTO addRlefmdlDTO) {
		RlefmdlM rlefmdlM = new RlefmdlM();
		DataCopy.copyData(addRlefmdlDTO, rlefmdlM);
		rlefmdlM.setFmdluuid(getDefaultPrimaryKeyForChar());
		fmdlInfoMapper.insert(rlefmdlM);
		this.saveFildHInfo(rlefmdlM, "ADD");
	}
	
	/**
	 * 刪除规则字段模块
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeFmdlById(DelRlefmdlDTO delRlefmdlDTO) {
		List<RlefmdlM> rleatctMList = fmdlInfoMapper.getFildById(delRlefmdlDTO.getFmdluuid());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(4003, null, null);
		} else if (rleatctMList.size() > 1) {
			throw new CommonException(4001, null, null);
		} else {
			fmdlInfoMapper.removeFmdlById(delRlefmdlDTO);
			this.saveFildHInfo(rleatctMList.get(0), "DEL");
		}
	}
	
	/**
	 * 修改规则字段模块
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateFmdl(AddRlefmdlDTO addRlefmdlDTO) {
		List<RlefmdlM> rleatctMList = fmdlInfoMapper.getFildById(addRlefmdlDTO.getFmdluuid());
		if (null == rleatctMList || rleatctMList.size() == 0) {
			throw new CommonException(4003, null, null);
		} else if (rleatctMList.size() > 1) {
			throw new CommonException(4001, null, null);
		} else {
			fmdlInfoMapper.updateFmdl(addRlefmdlDTO);
			this.saveFildHInfo(rleatctMList.get(0), "MOD");
		}
	}
	
	/**
	 * @Title: saveAtctHInfo
	 * @Description: 设置数据操作时添加历史表
	 * @param @param rlefmdlM
	 * @param @param edtId
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public int saveFildHInfo(RlefmdlM rlefmdlM, String edtId) {
		RlefmdlH rlefmdlH = new RlefmdlH();
		rlefmdlH.setLastmoddate(DatesUtils.getStringDate());
		DataCopy.copyData(rlefmdlM, rlefmdlH);
		rlefmdlH.setSequuid(getDefaultPrimaryKeyForChar());
		// 改变动作【ADD，MOD，DEL】
		rlefmdlH.setEdtid(edtId);
		return fmdlInfoMapper.insertFmdlH(rlefmdlH);
	}

	@Override
	public List<TreeNodeVO> queryFmdlName() {
		return fmdlInfoMapper.queryFmdlName();
	}
}
