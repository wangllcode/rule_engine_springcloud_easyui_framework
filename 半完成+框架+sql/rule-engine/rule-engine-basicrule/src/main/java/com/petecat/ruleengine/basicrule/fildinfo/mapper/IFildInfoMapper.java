package com.petecat.ruleengine.basicrule.fildinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basicrule.fildinfo.entity.RlefildH;
import com.petecat.ruleengine.basicrule.fildinfo.entity.RlefildM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.AddRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.DelRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fildinfo.dto.QueryRlefildDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.vo.QueryRlefmdlVO;

@Mapper
public interface IFildInfoMapper extends BaseMapper<RlefildM, String>{

	List<RlefildM> queryFildByPage(QueryRlefildDTO queryRlefildDTO);

	List<RlefildM> getFildById(@Param("fildmuuid") String fildmuuid);

	int insertFildH(RlefildH rlefildH);

	void removeFildById(DelRlefildDTO delRlefildDTO);

	void updateFild(AddRlefildDTO addRlefildDTO);
	
	List<QueryRlefmdlVO> queryFildByFmdlUuId(@Param("id") String id);
	
}
