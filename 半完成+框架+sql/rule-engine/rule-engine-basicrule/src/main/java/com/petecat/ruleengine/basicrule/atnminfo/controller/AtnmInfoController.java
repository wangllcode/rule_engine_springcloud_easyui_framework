package com.petecat.ruleengine.basicrule.atnminfo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petecat.ruleengine.basicrule.atnminfo.service.IAtnmInfoService;
import com.petecat.ruleengine.core.controller.BaseController;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.AddRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.DelRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.dto.QueryRleatnmDTO;
import com.petecat.ruleengine.protocol.basicrule.atnminfo.vo.QueryRleatnmVO;
import com.petecat.ruleengine.protocol.global.DatesUtils;
import com.petecat.ruleengine.protocol.global.Result;
import com.petecat.ruleengine.protocol.global.vo.PageVO;

@RestController
@RequestMapping("/atnminfo")
@Api(tags="附件命名规则")
public class AtnmInfoController extends BaseController {
	
	@Autowired
	private IAtnmInfoService atnmInfoService;
	
	@GetMapping("/queryAtnmByPage")
	@ApiOperation("附件命名规则分页查询")
	public Result<PageVO<QueryRleatnmVO>> queryAtnmByPage(
			@ModelAttribute QueryRleatnmDTO queryRleatnmDTO) {
		PageVO<QueryRleatnmVO> atnmList = atnmInfoService.listAtnmByPage(queryRleatnmDTO);
		return Result.success(atnmList);
	}
	
	@PostMapping("/saveAtnm")
	@ApiOperation(value = "添加附件命名规则")
	public Result<?> saveAtnm(@RequestBody AddRleatnmDTO addRleatnmDTO) {
		this.setLastUserName(addRleatnmDTO);
		atnmInfoService.saveAtnm(addRleatnmDTO);
		return Result.success();
	}
	
	@DeleteMapping("/removeAtnmById")
	@ApiOperation(value = "刪除附件命名规则")
	public Result<?> removeAtnmById(DelRleatnmDTO delRleatnmDTO) {
		String userId = this.getCurrentUser().getUserId();
		delRleatnmDTO.setLastModUser(userId);
		atnmInfoService.removeAtnmById(delRleatnmDTO);
		return Result.success();
	}
	
	
	@PutMapping("/updateAtnm")
	@ApiOperation(value = "修改附件命名规则")
	public Result<?> updateAtnm(@RequestBody AddRleatnmDTO addRleatnmDTO) {
		this.setLastUserName(addRleatnmDTO);
		atnmInfoService.updateAtnm(addRleatnmDTO);
		return Result.success();
	}
	
	/**
	 * 
	 * @Title: setLastUserName
	 * @Description: 设置最后操作和操作时间
	 * @param @param addRleatnmDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	public void setLastUserName(AddRleatnmDTO addRleatnmDTO) {
		String userId = this.getCurrentUser().getUserId();
		addRleatnmDTO.setLastModUser(userId);
		addRleatnmDTO.setLastModDate(DatesUtils.getStringDate());
	}
	
}
