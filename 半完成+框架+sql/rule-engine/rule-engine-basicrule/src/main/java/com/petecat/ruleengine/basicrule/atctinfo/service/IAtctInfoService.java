package com.petecat.ruleengine.basicrule.atctinfo.service;

import java.util.List;

import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.AddRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.DelRleatctMDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.dto.QueryRleatctDTO;
import com.petecat.ruleengine.protocol.basicrule.atctinfo.vo.QueryRleatctVO;
import com.petecat.ruleengine.protocol.global.vo.PageVO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

public interface IAtctInfoService {

	/**
	 * 
	 * @Title: listAtctByPage
	 * @Description: 附件目录规则分页查询
	 * @param @param queryRleatctDTO
	 * @param @return 参数
	 * @return List<QueryRleatctVO> 返回类型
	 * @throws
	 */
	List<QueryRleatctVO> listAtctByPage(QueryRleatctDTO queryRleatctDTO);

	/**
	 * 
	 * @Title: getAtctById
	 * @Description: 通过系统代号查询附件目录信息
	 * @param @param sysCode
	 * @param @return 参数
	 * @return QueryRleatctVO 返回类型
	 * @throws
	 */
	QueryRleatctVO getAtctById(String sysCode);

	/**
	 * 
	 * @Title: saveAtct
	 * @Description: 添加目录附件信息
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void saveAtct(AddRleatctDTO addRleatctDTO);

	/**
	 * 
	 * @Title: removeAsysById
	 * @Description: 刪除目录附件信息
	 * @param @param delRleatctMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeAtctById(DelRleatctMDTO delRleatctMDTO);

	/**
	 * 
	 * @Title: updateAtct
	 * @Description: 修改目录附件信息
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateAtct(AddRleatctDTO addRleatctDTO);

	/**
	 * @param sysCode 
	 * 
	 * @Title: queryAtctTree
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param id
	 * @param @return 参数
	 * @return List<TreeNodeVO> 返回类型
	 * @throws
	 */
	List<TreeNodeVO> queryAtctTree(String id, String sysCode);

	List<TreeNodeVO> queryAtctName();

}
