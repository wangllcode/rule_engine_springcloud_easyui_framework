package com.petecat.ruleengine.basicrule.epintinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basicrule.epintinfo.entity.RleatfaH;
import com.petecat.ruleengine.basicrule.epintinfo.entity.RleatfaM;
import com.petecat.ruleengine.basicrule.epintinfo.entity.RlepintH;
import com.petecat.ruleengine.basicrule.epintinfo.entity.RlepintM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.epintinfo.dto.AddRleatfaDTO;
import com.petecat.ruleengine.protocol.epintinfo.dto.AddRlepintDTO;
import com.petecat.ruleengine.protocol.epintinfo.dto.QueryRleatfaDTO;
import com.petecat.ruleengine.protocol.epintinfo.dto.QueryRlepintDTO;

@Mapper
public interface EpintinfoMapper extends BaseMapper<RlepintM, String> {

	/**
	 * 
	 * @Title: queryAtctByPage
	 * @Description: 分页查询业务用途
	 * @param @param queryRleatctDTO
	 * @param @return 参数
	 * @return List<RlepintM> 返回类型
	 * @throws
	 */
	List<RlepintM> queryEpintByPage(QueryRlepintDTO queryRlepintDTO);

	/**
	 * 
	 * @Title: getAtctById
	 * @Description: 查询业务规则用途
	 * @param @param sysCode
	 * @param @return 参数
	 * @return List<RleatctM> 返回类型
	 * @throws
	 */
	List<RlepintM> getEpint(@Param("pintUuid") String pintUuid);
	
	/**
	 * 添加业务用途
	 * @param rlepintM
	 */
	void saveEpint(RlepintM rlepintM);
	
	/**
	 * 
	 * @Title: insertAtctH
	 * @Description: 添加业务用途历史
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	void saveEpintH(RlepintH rlepintH);

	/**
	 * 
	 * @Description: 删除规则用途
	 * @param @param delRleatctMDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void deleteEpint(@Param("pintUuid") String pintUuid);

	/**
	 * 
	 * @Title: updateAtct
	 * @Description: 修改目录附件信息
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateEpint(AddRlepintDTO addRlepintDTO);
	/**
	 * 统计数据条数
	 * @param entity
	 * @return
	 */
	<K> Integer count_(K entity);
	
	/**
	 * 
	 * @Title: queryAtctByPage
	 * @Description: 分页查询字段-附件
	 * @param @param queryRleatctDTO
	 * @param @return 参数
	 * @return List<RlepintM> 返回类型
	 * @throws
	 */
	List<RleatfaM> queryEatfaByPage(QueryRleatfaDTO queryRleatfaDTO);
	/**
	 * 
	 * @param fullClassPath
	 * @param tabelName
	 * @param entity
	 * @return
	 */
	<K> List<K> queryK(@Param("tabelName")String tabelName,@Param("pa_va")String...pa_val);
	
	/**
	 * 添加字段-规则
	 * @param rlepintM
	 */
	void saveEatfa(RleatfaM rleatfaM);
	
	/**
	 * 
	 * @Title: insertAtctH
	 * @Description: 添加字段规则历史
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	void saveEatfaH(RleatfaH rleatfaH);
	
	/**
	 * 
	 * @Title: updateAtct
	 * @Description: 修改目录附件信息
	 * @param @param addRleatctDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateEatfa(AddRleatfaDTO AddRleatfaDTO);
	
	void deleteEatfa(@Param("atfaUuid") String atfaUuid );
}