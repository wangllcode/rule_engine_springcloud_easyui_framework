package com.petecat.ruleengine.basicrule.fmdlinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.petecat.ruleengine.basicrule.fmdlinfo.entity.RlefmdlH;
import com.petecat.ruleengine.basicrule.fmdlinfo.entity.RlefmdlM;
import com.petecat.ruleengine.core.mapper.BaseMapper;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.AddRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.DelRlefmdlDTO;
import com.petecat.ruleengine.protocol.basicrule.fmdlinfo.dto.QueryRlefmdlDTO;
import com.petecat.ruleengine.protocol.global.vo.TreeNodeVO;

@Mapper
public interface IFmdlInfoMapper extends BaseMapper<RlefmdlM, String> {

	/**
	 * 
	 * @Title: queryFmdlByPage
	 * @Description: 规则字段模块分页查询
	 * @param @param queryRlefmdlDTO
	 * @param @return 参数
	 * @return List<RlefmdlM> 返回类型
	 * @throws
	 */
	List<RlefmdlM> queryFmdlByPage(QueryRlefmdlDTO queryRlefmdlDTO);

	/**
	 * 
	 * @Title: getFildById
	 * @Description: 通过iD获取信息
	 * @param @param fmdluuid
	 * @param @return 参数
	 * @return List<RlefmdlM> 返回类型
	 * @throws
	 */
	List<RlefmdlM> getFildById(@Param("fmdluuid") String fmdluuid);

	/**
	 * 
	 * @Title: removeFmdlById
	 * @Description: 刪除规则字段模块
	 * @param @param delRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void removeFmdlById(DelRlefmdlDTO delRlefmdlDTO);

	/**
	 * 
	 * @Title: updateFmdl
	 * @Description: 修改规则字段模块
	 * @param @param addRlefmdlDTO 参数
	 * @return void 返回类型
	 * @throws
	 */
	void updateFmdl(AddRlefmdlDTO addRlefmdlDTO);

	/**
	 * 
	 * @Title: insertFmdlH
	 * @Description: 设置数据操作时添加历史表
	 * @param @param rlefmdlH
	 * @param @return 参数
	 * @return int 返回类型
	 * @throws
	 */
	int insertFmdlH(RlefmdlH rlefmdlH);

	List<TreeNodeVO> queryFmdlName();

}
